### 致可爱的程序员们
- 您使用此Java手册，我们倍感荣幸！

- 此`notebook`持续更新中...

- 如果您有想分享的、比较好的学习资料，可以通过[提issue](https://gitee.com/JustryDeng/notebook/issues) 的方式与我取得联系，格式形如：

  > `[资料分享] {你的任意联系方式}`

  我们看到后，会尽快与您取得联系。
---

### notebook打开方式

#### 方式一：直接在开发工具（如idea）中打开

- 第一步：引入notebook依赖

  ```xml
  <dependency>
      <groupId>com.idea-aedi</groupId>
      <artifactId>notebook</artifactId>
      <version>0.5.1</version>
      <!-- buildImages后，建议将scope设置为test，避免打出来的包过大 -->
      <!-- <scope>test</scope> -->
  </dependency>
  ```

- 第二步：任意main方法内调用`Notebook#buildImages()`方法，构建图片

  ```java
  public static void main(String[] args) {
      // 构建图片
      Notebook.buildImages();
  }
  ```

- 第三步：在开发工具内安装markdown预览插件

  ![raw](https://gitee.com/JustryDeng/shared-files/raw/master/other/picture/2.jpg)

- 第四步：在开发工具内直接查看notebook内容即可

  ![raw](https://gitee.com/JustryDeng/shared-files/raw/master/other/picture/3.jpg)



#### 方式二：使用markdown工具（如Typora）打开

- 第一步：使用git pull拉取项目

- 第二步：使用markdown工具打开笔记即可

  ![raw](https://gitee.com/JustryDeng/shared-files/raw/master/other/picture/1.jpg)



---

### 目录- block-chain
  - [[01]区块链基本原理](./src/main/resources/block-chain/[01]区块链基本原理.md)
  - [[02]区块链开发技术选型](./src/main/resources/block-chain/[02]区块链开发技术选型.md)
  - [[03]搭建智能合约开发环境](./src/main/resources/block-chain/[03]搭建智能合约开发环境.md)
  - [[04]初识Solidity、Remix与Geth节点结合部署智能合约](./src/main/resources/block-chain/[04]初识Solidity、Remix与Geth节点结合部署智能合约.md)
  - [[05]Solidity的数据类型](./src/main/resources/block-chain/[05]Solidity的数据类型.md)
  - [[06]区块链系统的内建对象](./src/main/resources/block-chain/[06]区块链系统的内建对象.md)
  - [[07]智能合约的函数与函数修饰符](./src/main/resources/block-chain/[07]智能合约的函数与函数修饰符.md)
- deploy-re
  - jenkins-re
    - [[01]Jenkins简介](./src/main/resources/deploy-re/jenkins-re/[01]Jenkins简介.md)
    - [[02]Linux下安装Jenkins、启动Jenkins等相关指令](./src/main/resources/deploy-re/jenkins-re/[02]Linux下安装Jenkins、启动Jenkins等相关指令.md)
    - [[03]Jenkins初始化、Jenkins基础配置](./src/main/resources/deploy-re/jenkins-re/[03]Jenkins初始化、Jenkins基础配置.md)
    - [[04]配置Jenkins凭据](./src/main/resources/deploy-re/jenkins-re/[04]配置Jenkins凭据.md)
    - [[05]新建Jenkins任务之基础知识（入门级示例）](./src/main/resources/deploy-re/jenkins-re/[05]新建Jenkins任务之基础知识（入门级示例）.md)
    - [[06]新建Jenkins任务（单个项目、简单使用）](./src/main/resources/deploy-re/jenkins-re/[06]新建Jenkins任务（单个项目、简单使用）.md)
    - [[07]Jenkins（结合Docker）任务（Jenkins与Docker位于同一台机器上）](./src/main/resources/deploy-re/jenkins-re/[07]Jenkins（结合Docker）任务（Jenkins与Docker位于同一台机器上）.md)
    - [[08]Jenkins（结合Docker）任务（Jenkins与Docker位于不同机器上）](./src/main/resources/deploy-re/jenkins-re/[08]Jenkins（结合Docker）任务（Jenkins与Docker位于不同机器上）.md)
- docker
  - [[01]docker-compose相关指令](./src/main/resources/docker/[01]docker-compose相关指令.md)
  - [[02]docker常用指令](./src/main/resources/docker/[02]docker常用指令.md)
- HarmonyOS
  - [[01]搭建HarmonyOS开发环境](./src/main/resources/HarmonyOS/[01]搭建HarmonyOS开发环境.md)
  - [[02]鸿蒙应用基础结构及相关概念介绍](./src/main/resources/HarmonyOS/[02]鸿蒙应用基础结构及相关概念介绍.md)
  - [视频_徐礼文]HarmonyOS操作系统分布式应用开发实战
    - [01]HarmonyOS操作系统介绍
      - [[01]HarmonyOS操作系统简介](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[01]HarmonyOS操作系统介绍/[01]HarmonyOS操作系统简介.md)
    - [02]HarmonyOS应用开发环境搭建与IDE使用
      - [[01]HarmonyOS应用开发流程](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[02]HarmonyOS应用开发环境搭建与IDE使用/[01]HarmonyOS应用开发流程.md)
      - [[02]自动签名HarmonyOS App](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[02]HarmonyOS应用开发环境搭建与IDE使用/[02]自动签名HarmonyOS%20App.md)
    - [03]HarmonyOS应用开发基础入门
      - [[01]HarmonyOS应用开发类型介绍](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[03]HarmonyOS应用开发基础入门/[01]HarmonyOS应用开发类型介绍.md)
      - [[02]Counter 计数器项目入门](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[03]HarmonyOS应用开发基础入门/[02]Counter%20计数器项目入门.md)
      - [[03]HarmonyOS项目结构 & 资源目录详解](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[03]HarmonyOS应用开发基础入门/[03]HarmonyOS项目结构%20&%20资源目录详解.md)
      - [[04]config.json配置文件详解](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[03]HarmonyOS应用开发基础入门/[04]config.json配置文件详解.md)
      - [[05]资源文件的使用](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[03]HarmonyOS应用开发基础入门/[05]资源文件的使用.md)
      - [[06]HiLog日志的使用](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[03]HarmonyOS应用开发基础入门/[06]HiLog日志的使用.md)
    - [04]Page Ability基础
      - [[01]ACE运行时简介 & 鸿蒙应用开发](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[04]Page%20Ability基础/[01]ACE运行时简介%20&%20鸿蒙应用开发.md)
      - [[02]Page Ability和Page AbilitySlice](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[04]Page%20Ability基础/[02]Page%20Ability和Page%20AbilitySlice.md)
      - [[03]演示创建PageAbilty](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[04]Page%20Ability基础/[03]演示创建PageAbilty.md)
      - [[04]页面跳转](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[04]Page%20Ability基础/[04]页面跳转.md)
      - [[05]Page Ability与Page Ability Slice的生命周期方法](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[04]Page%20Ability基础/[05]Page%20Ability与Page%20Ability%20Slice的生命周期方法.md)
    - [05]Java UI 布局
      - [[01]HarmonyOS布局基础知识](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[05]Java%20UI%20布局/[01]HarmonyOS布局基础知识.md)
      - [[02]DirectionalLayout方向布局](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[05]Java%20UI%20布局/[02]DirectionalLayout方向布局.md)
      - [[03]DirectionalLayout-weight权重设置](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[05]Java%20UI%20布局/[03]DirectionalLayout-weight权重设置.md)
      - [[04]DependentLayout相对布局](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[05]Java%20UI%20布局/[04]DependentLayout相对布局.md)
      - [[05]TableLayout表格布局](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[05]Java%20UI%20布局/[05]TableLayout表格布局.md)
      - [[06]StackLayout堆叠布局](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[05]Java%20UI%20布局/[06]StackLayout堆叠布局.md)
      - [[07]PositionLayout位置布局](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[05]Java%20UI%20布局/[07]PositionLayout位置布局.md)
      - [[08]AdaptiveBoxLayout自适应盒子布局](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[05]Java%20UI%20布局/[08]AdaptiveBoxLayout自适应盒子布局.md)
    - [06]Java基础组件
      - [[01]Java基础组件介绍](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[01]Java基础组件介绍.md)
      - [[02]Text组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[02]Text组件.md)
      - [[03]Button组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[03]Button组件.md)
      - [[04]Image组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[04]Image组件.md)
      - [[05]TextField组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[05]TextField组件.md)
      - [[06]Switch组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[06]Switch组件.md)
      - [[07]RadioButton组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[07]RadioButton组件.md)
      - [[08]RadioContainer组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[08]RadioContainer组件.md)
      - [[09]CheckBox组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[09]CheckBox组件.md)
      - [[10]ProgressBar组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[10]ProgressBar组件.md)
      - [[11]RoundProgressBar组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[11]RoundProgressBar组件.md)
      - [[12]ToastDialog组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[12]ToastDialog组件.md)
      - [[13]CommonDialog组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[13]CommonDialog组件.md)
      - [[14]Piker组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[14]Piker组件.md)
      - [[15]ScrollView组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[15]ScrollView组件.md)
      - [[16]TabList组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[16]TabList组件.md)
      - [[17]PageSlider组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[17]PageSlider组件.md)
      - [[18]ListContainer组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[18]ListContainer组件.md)
      - [[19]WebView组件](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[06]Java基础组件/[19]WebView组件.md)
    - [07]HarmonyOS线程管理
      - [[01]HarmonyOS线程管理](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[07]HarmonyOS线程管理/[01]HarmonyOS线程管理.md)
      - [[02]线程间通信](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[07]HarmonyOS线程管理/[02]线程间通信.md)
    - [08]公共事件和通知
      - [[01]公共事件和通知](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[08]公共事件和通知/[01]公共事件和通知.md)
    - [09]Service Ability 详解
      - [[01]Service Ability简介](./src/main/resources/HarmonyOS/[视频_徐礼文]HarmonyOS操作系统分布式应用开发实战/[09]Service%20Ability%20详解/[01]Service%20Ability简介.md)
    - [10]分布式任务调度
    - [11]分布式数据库
    - [12]分布式文件系统
    - [13]服务卡片详解(Java)
    - [14]服务卡片详解(Javascript)
    - [15]媒体与设备
    - [16]网络与连接
    - [17]HarmonyOS分布式点餐系统
- k8s-re
  - [[01]基本概念和术语](./src/main/resources/k8s-re/[01]基本概念和术语.md)
- linux-re
  - [[01]测试地址端口连通性](./src/main/resources/linux-re/[01]测试地址端口连通性.md)
  - [[02]Ubuntu常用指令](./src/main/resources/linux-re/[02]Ubuntu常用指令.md)
  - [[03]grep抓取关键信息](./src/main/resources/linux-re/[03]grep抓取关键信息.md)
  - [[04] CentOS7 firewall防火墙&端口相关指令](./src/main/resources/linux-re/[04]%20CentOS7%20firewall防火墙&端口相关指令.md)
  - [[05]CentOS7安装go-fastdfs](./src/main/resources/linux-re/[05]CentOS7安装go-fastdfs.md)
- maven-re
  - [[01]pom中maven profile的激活方式](./src/main/resources/maven-re/[01]pom中maven%20profile的激活方式.md)
- mysql-re
  - [[01]常用SQL](./src/main/resources/mysql-re/[01]常用SQL.md)
  - [[02]表&列常用SQL](./src/main/resources/mysql-re/[02]表&列常用SQL.md)
  - [[03]查看最新一次的死锁日志](./src/main/resources/mysql-re/[03]查看最新一次的死锁日志.md)
  - [[04]增删改查与数据库行锁表锁](./src/main/resources/mysql-re/[04]增删改查与数据库行锁表锁.md)
  - [[05]MySQL 加锁机制 & index merge导致的死锁](./src/main/resources/mysql-re/[05]MySQL%20加锁机制%20&%20index%20merge导致的死锁.md)
  - [[06]CentOS7安装MySQL8.0.19](./src/main/resources/mysql-re/[06]CentOS7安装MySQL8.0.19.md)
  - [[07]设置允许其它设备跨机连接MYSQL数据库](./src/main/resources/mysql-re/[07]设置允许其它设备跨机连接MYSQL数据库.md)
  - [[08]创建用户并授权](./src/main/resources/mysql-re/[08]创建用户并授权.md)
  - [[09]切换用户密码的加密方式](./src/main/resources/mysql-re/[09]切换用户密码的加密方式.md)
  - [[10]mysqldump语法](./src/main/resources/mysql-re/[10]mysqldump语法.md)
- nginx-re
  - [[01]CentOS7安装Nginx及相关指令](./src/main/resources/nginx-re/[01]CentOS7安装Nginx及相关指令.md)
  - [[02]nginx定位静态资源](./src/main/resources/nginx-re/[02]nginx定位静态资源.md)
  - [[03]nginx配置请求转发](./src/main/resources/nginx-re/[03]nginx配置请求转发.md)
  - [[04]nginx实现用户名密码验证](./src/main/resources/nginx-re/[04]nginx实现用户名密码验证.md)
  - [[05]nginx配置示例](./src/main/resources/nginx-re/[05]nginx配置示例.md)
- oracle-re
  - [[01]Oracle基本知识](./src/main/resources/oracle-re/[01]Oracle基本知识.md)
  - [[02]Oracle工具的常见问题](./src/main/resources/oracle-re/[02]Oracle工具的常见问题.md)
  - [[03]连接他人Oracle数据库的方法、SYS与SYSTEM用户](./src/main/resources/oracle-re/[03]连接他人Oracle数据库的方法、SYS与SYSTEM用户.md)
  - [[04]表空间](./src/main/resources/oracle-re/[04]表空间.md)
  - [[05]用户的创建](./src/main/resources/oracle-re/[05]用户的创建.md)
  - [[06]Oracle的数据类型](./src/main/resources/oracle-re/[06]Oracle的数据类型.md)
  - [[07]伪列](./src/main/resources/oracle-re/[07]伪列.md)
  - [[08]检查约束](./src/main/resources/oracle-re/[08]检查约束.md)
  - [[09]事务](./src/main/resources/oracle-re/[09]事务.md)
  - [[10]Oracle的SQL操作符](./src/main/resources/oracle-re/[10]Oracle的SQL操作符.md)
  - [[11]序列](./src/main/resources/oracle-re/[11]序列.md)
  - [[12]空值转换函数](./src/main/resources/oracle-re/[12]空值转换函数.md)
  - [[13]分析函数](./src/main/resources/oracle-re/[13]分析函数.md)
  - [[14]Oracle数据库中表的备份导出](./src/main/resources/oracle-re/[14]Oracle数据库中表的备份导出.md)
  - [[15]字符类型的主键](./src/main/resources/oracle-re/[15]字符类型的主键.md)
  - [[16]同义词](./src/main/resources/oracle-re/[16]同义词.md)
  - [[17]Oracle中的索引、数据生成器](./src/main/resources/oracle-re/[17]Oracle中的索引、数据生成器.md)
  - [[18]数据库的视图](./src/main/resources/oracle-re/[18]数据库的视图.md)
  - [[19]PLSQL的基本知识语法](./src/main/resources/oracle-re/[19]PLSQL的基本知识语法.md)
  - [[20]PLSQL的游标](./src/main/resources/oracle-re/[20]PLSQL的游标.md)
  - [[21]PLSQL的子程序](./src/main/resources/oracle-re/[21]PLSQL的子程序.md)
  - [[22]Oracle的一些表修改操作以及查询时的注意事项](./src/main/resources/oracle-re/[22]Oracle的一些表修改操作以及查询时的注意事项.md)
  - [[23]个别查询语句示例](./src/main/resources/oracle-re/[23]个别查询语句示例.md)
- [Go]basic
  - [[01]安装Go开发工具GoLand](./src/main/resources/[Go]basic/[01]安装Go开发工具GoLand.md)
  - [[02]搭建Go环境](./src/main/resources/[Go]basic/[02]搭建Go环境.md)
  - [[03]数据类型](./src/main/resources/[Go]basic/[03]数据类型.md)
  - [[04]定义变量](./src/main/resources/[Go]basic/[04]定义变量.md)
  - [[05]定义常量](./src/main/resources/[Go]basic/[05]定义常量.md)
  - [[06]定义枚举](./src/main/resources/[Go]basic/[06]定义枚举.md)
  - [[07]指针、地址、间接赋值](./src/main/resources/[Go]basic/[07]指针、地址、间接赋值.md)
  - [[08]常用的print打印](./src/main/resources/[Go]basic/[08]常用的print打印.md)
  - [[09]分支与循环](./src/main/resources/[Go]basic/[09]分支与循环.md)
  - [[10]函数](./src/main/resources/[Go]basic/[10]函数.md)
  - [[11]数组、切片、Map](./src/main/resources/[Go]basic/[11]数组、切片、Map.md)
  - [[12]结构体、方法封装、结构体内嵌、接口](./src/main/resources/[Go]basic/[12]结构体、方法封装、结构体内嵌、接口.md)
  - [[13]并发编程](./src/main/resources/[Go]basic/[13]并发编程.md)
  - [[14]文件IO的处理、延迟调用](./src/main/resources/[Go]basic/[14]文件IO的处理、延迟调用.md)
  - [[15]搭建TCP服务端、客户端](./src/main/resources/[Go]basic/[15]搭建TCP服务端、客户端.md)
  - [[16]搭建HTTP服务端](./src/main/resources/[Go]basic/[16]搭建HTTP服务端.md)
- [IDE]idea
  - [[01]idea插件编写](./src/main/resources/[IDE]idea/[01]idea插件编写.md)
  - [[02]idea插件推荐](./src/main/resources/[IDE]idea/[02]idea插件推荐.md)
  - [[03]easy-code插件配置](./src/main/resources/[IDE]idea/[03]easy-code插件配置.md)
- [Java]auth-re
  - [[01]Spring Security总体流程、认证流程、鉴权流程浅析](./src/main/resources/[Java]auth-re/[01]Spring%20Security总体流程、认证流程、鉴权流程浅析.md)
  - [[02]Spring Security入门级使用（示例）](./src/main/resources/[Java]auth-re/[02]Spring%20Security入门级使用（示例）.md)
  - [[03]Spring Security账号密码认证 + 自定义鉴权(示例)](./src/main/resources/[Java]auth-re/[03]Spring%20Security账号密码认证%20+%20自定义鉴权(示例).md)
- [Java]class-re
  - [[01]代码混淆之class-winter](./src/main/resources/[Java]class-re/[01]代码混淆之class-winter.md)
  - [[02]【JSR269实战】之编译时操作AST，修改字节码文件，以实现和lombok类似的功能](./src/main/resources/[Java]class-re/[02]【JSR269实战】之编译时操作AST，修改字节码文件，以实现和lombok类似的功能.md)
  - [[03]class热更新](./src/main/resources/[Java]class-re/[03]class热更新.md)
- [Java]component
  - [[01]Java超实用小组件](./src/main/resources/[Java]component/[01]Java超实用小组件.md)
- [Java]design-pattern-re
  - [[01]代理模式](./src/main/resources/[Java]design-pattern-re/[01]代理模式.md)
  - [[02]状态模式](./src/main/resources/[Java]design-pattern-re/[02]状态模式.md)
  - [[03]迭代器与组合模式](./src/main/resources/[Java]design-pattern-re/[03]迭代器与组合模式.md)
  - [[04]模板方法模式](./src/main/resources/[Java]design-pattern-re/[04]模板方法模式.md)
  - [[05]适配器模式](./src/main/resources/[Java]design-pattern-re/[05]适配器模式.md)
  - [[06]命令模式](./src/main/resources/[Java]design-pattern-re/[06]命令模式.md)
  - [[07]工厂模式](./src/main/resources/[Java]design-pattern-re/[07]工厂模式.md)
  - [[08]装饰者模式](./src/main/resources/[Java]design-pattern-re/[08]装饰者模式.md)
  - [[09]观察者模式](./src/main/resources/[Java]design-pattern-re/[09]观察者模式.md)
  - [[10]策略模式](./src/main/resources/[Java]design-pattern-re/[10]策略模式.md)
- [Java]doc-re
  - [[01]smart-doc工具类](./src/main/resources/[Java]doc-re/[01]smart-doc工具类.md)
- [Java]fragments-re
  - [[01]【规则引擎】之easy-rules入门](./src/main/resources/[Java]fragments-re/[01]【规则引擎】之easy-rules入门.md)
  - [[02]thumbnailator图片处理](./src/main/resources/[Java]fragments-re/[02]thumbnailator图片处理.md)
  - [[03]Spring Retry重试组件、Guava Retry重试组件](./src/main/resources/[Java]fragments-re/[03]Spring%20Retry重试组件、Guava%20Retry重试组件.md)
  - [[04]ThreadLocal数据观察及原理验证](./src/main/resources/[Java]fragments-re/[04]ThreadLocal数据观察及原理验证.md)
  - [[05]args4j简单使用示例](./src/main/resources/[Java]fragments-re/[05]args4j简单使用示例.md)
  - [[06]QDox代码解析](./src/main/resources/[Java]fragments-re/[06]QDox代码解析.md)
  - [[07]SPI](./src/main/resources/[Java]fragments-re/[07]SPI.md)
  - [[08]idea GUI Form](./src/main/resources/[Java]fragments-re/[08]idea%20GUI%20Form.md)
  - [[09]URL匹配之AntPathMatcher](./src/main/resources/[Java]fragments-re/[09]URL匹配之AntPathMatcher.md)
  - [[10]mapstruct对象复制&转换](./src/main/resources/[Java]fragments-re/[10]mapstruct对象复制&转换.md)
  - [[11]Beetl模板引擎](./src/main/resources/[Java]fragments-re/[11]Beetl模板引擎.md)
  - [[12]markdown公式大全](./src/main/resources/[Java]fragments-re/[12]markdown公式大全.md)
  - [[13]swagger对特定请求统一追加指定请求头](./src/main/resources/[Java]fragments-re/[13]swagger对特定请求统一追加指定请求头.md)
- [Java]http-re
  - [[01]使用openssl生成CA证书](./src/main/resources/[Java]http-re/[01]使用openssl生成CA证书.md)
- [Java]jdk8-re
  - [[01]Stream学习（上）](./src/main/resources/[Java]jdk8-re/[01]Stream学习（上）.md)
  - [[02]Stream学习（下）](./src/main/resources/[Java]jdk8-re/[02]Stream学习（下）.md)
  - [[03]Function](./src/main/resources/[Java]jdk8-re/[03]Function.md)
  - [[04]Predicate](./src/main/resources/[Java]jdk8-re/[04]Predicate.md)
  - [[05]利用FunctionalInterface获取类、字段、方法](./src/main/resources/[Java]jdk8-re/[05]利用FunctionalInterface获取类、字段、方法.md)
  - [[06]CentOS7上安装jdk11](./src/main/resources/[Java]jdk8-re/[06]CentOS7上安装jdk11.md)
- [Java]job-re
  - xxl-job
    - [[01]xxl-job概览](./src/main/resources/[Java]job-re/xxl-job/[01]xxl-job概览.md)
    - [[02]部署调度中心](./src/main/resources/[Java]job-re/xxl-job/[02]部署调度中心.md)
    - [[03]集成执行器](./src/main/resources/[Java]job-re/xxl-job/[03]集成执行器.md)
    - [[04]编写xxl-job定时任务](./src/main/resources/[Java]job-re/xxl-job/[04]编写xxl-job定时任务.md)
- [Java]log-re
  - [[01]logback-defender实现日志脱敏](./src/main/resources/[Java]log-re/[01]logback-defender实现日志脱敏.md)
  - [[02]log4j2-defender实现日志脱敏](./src/main/resources/[Java]log-re/[02]log4j2-defender实现日志脱敏.md)
  - [[03]log4j2使用filter过滤日志](./src/main/resources/[Java]log-re/[03]log4j2使用filter过滤日志.md)
  - [[04]logback使用filter过滤日志](./src/main/resources/[Java]log-re/[04]logback使用filter过滤日志.md)
- [Java]mybatis-re
  - [[01]mybatis-data-security实现数据库数据加解密](./src/main/resources/[Java]mybatis-re/[01]mybatis-data-security实现数据库数据加解密.md)
  - [[02]Mybatis之一个SQL的运行过程](./src/main/resources/[Java]mybatis-re/[02]Mybatis之一个SQL的运行过程.md)
  - [[03]mybatis自定义类型处理器](./src/main/resources/[Java]mybatis-re/[03]mybatis自定义类型处理器.md)
  - [[04]@MapperScan与@Mapper](./src/main/resources/[Java]mybatis-re/[04]@MapperScan与@Mapper.md)
  - [[05]mybatis-plus自动生成代码](./src/main/resources/[Java]mybatis-re/[05]mybatis-plus自动生成代码.md)
  - [[06]mybatis-plus多租户](./src/main/resources/[Java]mybatis-re/[06]mybatis-plus多租户.md)
- [Java]redis-re
  - [[01]Redis(单机&集群)Pipeline工具类](./src/main/resources/[Java]redis-re/[01]Redis(单机&集群)Pipeline工具类.md)
  - [[02]一个走心的RedisUtil工具类](./src/main/resources/[Java]redis-re/[02]一个走心的RedisUtil工具类.md)
  - [[03]一个简单的（基于redisson的）分布式同步工具类封装](./src/main/resources/[Java]redis-re/[03]一个简单的（基于redisson的）分布式同步工具类封装.md)
  - [[04]CentOS7上搭建Redis-5.0.4（单机版）](./src/main/resources/[Java]redis-re/[04]CentOS7上搭建Redis-5.0.4（单机版）.md)
  - [[05]Redis（服务端客户端）的启动、关闭、查看状态](./src/main/resources/[Java]redis-re/[05]Redis（服务端客户端）的启动、关闭、查看状态.md)
  - [[06]配置Redis需要密码验证](./src/main/resources/[Java]redis-re/[06]配置Redis需要密码验证.md)
  - [[07]scan与批量删除redis key](./src/main/resources/[Java]redis-re/[07]scan与批量删除redis%20key.md)
- [Java]spring-boot-re
  - [[01]spring-boot发送mail邮件](./src/main/resources/[Java]spring-boot-re/[01]spring-boot发送mail邮件.md)
  - [[02]spring-boot-starter的关键、编写示例](./src/main/resources/[Java]spring-boot-re/[02]spring-boot-starter的关键、编写示例.md)
  - [[03]事件监听（基于SpringBoot示例）](./src/main/resources/[Java]spring-boot-re/[03]事件监听（基于SpringBoot示例）.md)
  - [[04]SpringBoot之Actuator入门](./src/main/resources/[Java]spring-boot-re/[04]SpringBoot之Actuator入门.md)
  - [[05]SpringBoot profile整合Maven profile实现多环境下配置、依赖自动切换](./src/main/resources/[Java]spring-boot-re/[05]SpringBoot%20profile整合Maven%20profile实现多环境下配置、依赖自动切换.md)
  - [[06]SpringBoot自定义参数解析器，使被@RequestBody标注的参数能额外接收Content-Type为applicationx-www-form-urlencoded的请求](./src/main/resources/[Java]spring-boot-re/[06]SpringBoot自定义参数解析器，使被@RequestBody标注的参数能额外接收Content-Type为applicationx-www-form-urlencoded的请求.md)
  - [[07]利用EnvironmentPostProcessor动态修改配置](./src/main/resources/[Java]spring-boot-re/[07]利用EnvironmentPostProcessor动态修改配置.md)
- [Java]spring-re
  - [[01]注册spring-bean的常用方式](./src/main/resources/[Java]spring-re/[01]注册spring-bean的常用方式.md)
  - [[02]ext-spring-cache扩展@Cacheable缓存注解](./src/main/resources/[Java]spring-re/[02]ext-spring-cache扩展@Cacheable缓存注解.md)
  - [[03]对普通bean进行Autowired字段注入](./src/main/resources/[Java]spring-re/[03]对普通bean进行Autowired字段注入.md)
  - [[04]spring-mvc配置访问根路径时重定向至指定路径](./src/main/resources/[Java]spring-re/[04]spring-mvc配置访问根路径时重定向至指定路径.md)
  - [[05]动态注册controller](./src/main/resources/[Java]spring-re/[05]动态注册controller.md)
- [Java]test-re
  - [[01]基于docker的test-containers环境百宝箱](./src/main/resources/[Java]test-re/[01]基于docker的test-containers环境百宝箱.md)
- [Java]thread-concurrency-re
  - [[01]ForkJoin浅探与使用](./src/main/resources/[Java]thread-concurrency-re/[01]ForkJoin浅探与使用.md)
  - [[02]CompletableFuture](./src/main/resources/[Java]thread-concurrency-re/[02]CompletableFuture.md)
  - [[03]捕获异步线程异常的常用方式](./src/main/resources/[Java]thread-concurrency-re/[03]捕获异步线程异常的常用方式.md)
- [Java]util-re
  - [[01]加载外部jar(普通jar和springboot jar)、class](./src/main/resources/[Java]util-re/[01]加载外部jar(普通jar和springboot%20jar)、class.md)
  - [[02]生成二维码、识别二维码的工具类](./src/main/resources/[Java]util-re/[02]生成二维码、识别二维码的工具类.md)
- 笔者相关
  - [[01]author注解](./src/main/resources/笔者相关/[01]author注解.md)
- 编程思想与理念
  - [[01]开发设计推论](./src/main/resources/编程思想与理念/[01]开发设计推论.md)