# spring-boot发送mail邮件

- [spring-boot发送mail邮件](#spring-boot发送mail邮件)
	- [<font  face="幼圆" color = "#3399EA" >简介</font>](#font--face幼圆-color--3399ea-简介font)
	- [<font  face="幼圆" color = "#3399EA" >准备工作</font>](#font--face幼圆-color--3399ea-准备工作font)
	- [<font  face="幼圆" color = "#3399EA" >使用示例</font>](#font--face幼圆-color--3399ea-使用示例font)
	- [相关资料](#相关资料)

---

## <font  face="幼圆" color = "#3399EA" >简介</font>

&emsp;&emsp;SpringBoot在Spring提供的抽象可定制的JavaMailSender邮件机制的基础上，作了一些封装和实现，以实现开箱即用、配置即用快速开发。下面就简单示例一下如何使用spring boot mail，更多可详见官网 `https://docs.spring.io/spring-framework/docs/5.3.4/reference/html/integration.html#mail` 。

## <font  face="幼圆" color = "#3399EA" >准备工作</font>

- <font face="幼圆"  color = "#86CA5E">**引入相关依赖**</font>
	```xml
	<dependency>
	    <groupId>org.springframework.boot</groupId>
	    <artifactId>spring-boot-starter-mail</artifactId>
	</dependency>
	```
- <font face="幼圆"  color = "#86CA5E">**配置邮件信息**</font>
	```bash
	# 邮箱服务器地址
	spring.mail.host=smtp.163.com
	# 邮箱服务器(发送邮件的)端口
	spring.mail.port=465
	# 用户名
	spring.mail.username=填写你自己的邮箱
	# 密码(或授权码)
	spring.mail.password=填写你自己邮箱的授权码(或密码)
	# 会话JNDI名称
	#spring.mail.jndi-name=
	# 是否在项目启动时，就对邮箱可用性进行测试
	#spring.mail.test-connection=false
	# 通过spring.mail.properties指定更多配置
	spring.mail.properties.mail.smtp.auth=true
	spring.mail.properties.mail.smtp.ssl.enable=true
	spring.mail.properties.mail.imap.ssl.socketFactory.fallback=false
	spring.mail.properties.mail.smtp.ssl.socketFactory.class=javax.net.ssl.SSLSocketFactory
	spring.mail.properties.mail.smtp.starttls.enable=true
	spring.mail.properties.mail.smtp.starttls.required=true
	spring.mail.properties.mail.smtp.timeout=3000
	spring.mail.properties.[mail.smtp.connectiontimeout]=5000
	spring.mail.properties.mail.smtp.writetimeout.=5000
	```

## <font  face="幼圆" color = "#3399EA" >使用示例</font>

- <font face="幼圆"  color = "#86CA5E">**发送【普通邮件】**</font>
  - 代码
	```java
	@Autowired
	JavaMailSender javaMailSender;
	
	/**
	 * 普通邮件
	 */
	@Test
	void test1() {
	    SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
	    // 邮件发送人（设置昵称）
	    simpleMailMessage.setFrom("邓沙利文<13548417409@163.com>");
	    // 邮件发送人
	    // simpleMailMessage.setFrom("13548417409@163.com");
	    // 邮件接收人
	    simpleMailMessage.setTo("1612513157@qq.com");
	    // 抄送人
	    // simpleMailMessage.setCc();
	    // 密送人
	    // simpleMailMessage.setBcc();
	    // 邮件主题
	    simpleMailMessage.setSubject("我是主题 - 普通邮件");
	    // 邮件内容
	    simpleMailMessage.setText("我是邮件内容");
	    javaMailSender.send(simpleMailMessage);
	}
	```
  - 效果

    ![在这里插入图片描述](../repository/2021031000241314.png)


- <font face="幼圆"  color = "#86CA5E">**发送【带附件的邮件】**</font>

  - 代码

    ```java
    @Autowired
    JavaMailSender javaMailSender;
    
    /**
     * 带附件的邮件
     */
    @Test
    void test2() throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        // 创建multipart message
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        // 邮件发送人（设置昵称）
        helper.setFrom("邓沙利文<13548417409@163.com>");
        // 邮件发送人
        // helper.setFrom("13548417409@163.com");
        // 邮件接收人
        helper.addTo("1612513157@qq.com");
        // 抄送人
        // helper.addCc();
        // 密送人
        // helper.addBcc();
        // 邮件主题
        helper.setSubject("我是主题 - 带附件的邮件");
        // 邮件内容
        helper.setText("我是邮件内容");
        // 附件
        helper.addAttachment("file-1", new File("src/main/resources/pdf/spring-framework-reference.pdf"));
        javaMailSender.send(message);
    }
    ```

  - 效果

    ![在这里插入图片描述](../repository/20210310002437290.png)


- <font face="幼圆"  color = "#86CA5E">**发送【HTML格式的邮件】**</font>
  
	- 代码
	
	    ```java
	    @Autowired
	    JavaMailSender javaMailSender;
	    
	    /**
	     * HTML格式的邮件
	     */
	    @Test
	    void test3() throws MessagingException {
	        MimeMessage message = javaMailSender.createMimeMessage();
	        // 创建multipart message
	        MimeMessageHelper helper = new MimeMessageHelper(message, true);
	        // 邮件发送人（设置昵称）
	        helper.setFrom("邓沙利文<13548417409@163.com>");
	        // 邮件发送人
	        // helper.setFrom("13548417409@163.com");
	        // 邮件接收人
	        helper.addTo("1612513157@qq.com");
	        // 抄送人
	        // helper.addCc();
	        // 密送人
	        // helper.addBcc();
	        // 邮件主题
	        helper.setSubject("我是主题 - html格式的邮件");
	        // 邮件内容(true - 表示是html格式的内容)
	        helper.setText("<p style=\"font-family:arial;color:red;font-size:20px;\">我是邮件内容</p>", true);
	        // 附件
	        // helper.addAttachment("file-1", new File("src/main/resources/pdf/spring-framework-reference.pdf"));
	        javaMailSender.send(message);
      }
      ```
  
  - 效果
  
    ​      ![在这里插入图片描述](../repository/2021031000250173.png)


- <font face="幼圆"  color = "#86CA5E">**发送【内联(图片的)邮件】**</font>

  - 代码

    ```java
    @Autowired
    JavaMailSender javaMailSender;
    
    /**
         * 内联(图片的)邮件
         */
    @Test
    void test4() throws MessagingException {
        MimeMessage message = javaMailSender.createMimeMessage();
        // 创建multipart message
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        // 邮件发送人（设置昵称）
        helper.setFrom("邓沙利文<13548417409@163.com>");
        // 邮件发送人
        // helper.setFrom("13548417409@163.com");
        // 邮件接收人
        helper.addTo("1612513157@qq.com");
        // 抄送人
        // helper.addCc();
        // 密送人
        // helper.addBcc();
        // 邮件主题
        helper.setSubject("我是主题 - 内联(图片的)邮件");
        // 邮件内容(true - 表示是html格式的内容)
        helper.setText("看我后面有图片 <img src='cid:contentId123'/>", true);
        // 设置内联的图片，通过cid指定对应关系(，如这里的contentId123)
        helper.addInline("contentId123", new File("src/main/resources/jpg/panda.jpg"));
        // 附件
        // helper.addAttachment("file-1", new File("src/main/resources/pdf/spring-framework-reference.pdf"));
        javaMailSender.send(message);
    }
    ```

  - 效果

    ![在这里插入图片描述](../repository/20210310002529873.png)

---
## 相关资料

- [官网](https://docs.spring.io/spring-framework/docs/5.3.4/reference/html/integration.html#mail)