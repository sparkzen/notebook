# SpringBoot profile整合Maven profile实现多环境下配置、依赖自动切换

- [SpringBoot profile整合Maven profile实现多环境下配置、依赖自动切换](#springboot-profile整合maven-profile实现多环境下配置依赖自动切换)
  - [相关知识（简述）](#相关知识简述)
  - [SpringBoot profile整合Maven profile实现多环境配置、依赖自动切换](#springboot-profile整合maven-profile实现多环境配置依赖自动切换)
    - [第一步：准备SpringBoot的profile（即：准备相关配置文件）](#第一步准备springboot的profile即准备相关配置文件)
    - [第二步：根据自己的实际需求，在pom.xml中，准备Maven 的profile](#第二步根据自己的实际需求在pomxml中准备maven-的profile)
    - [第三步：将Maven profile与SpringBoot profile关联起来](#第三步将maven-profile与springboot-profile关联起来)
    - [第四步：在系统配置文件中application.properties中，动态获取pom.xml里的激活环境标识](#第四步在系统配置文件中applicationproperties中动态获取pomxml里的激活环境标识)
    - [第五步：测试一下](#第五步测试一下)
  - [简单拓展](#简单拓展)
    - [第五步：在资源目录下创建一个环境切换触发文件](#第五步在资源目录下创建一个环境切换触发文件)
    - [第六步：使用maven profile的activation标签的file功能，设置环境触发条件](#第六步使用maven-profile的activation标签的file功能设置环境触发条件)
    - [第七步：测试一下](#第七步测试一下)
  - [相关资料](#相关资料)

---

## 相关知识（简述）

**SpringBoot profile：** SpringBoot允许我们在配置文件(以application.properties示例)中通过配置*spring.profiles.active=xxx*，来对应激活*application-xxx.properties*配置，这就是SpringBoot profile。

注：spring.profiles.include也有类似的功能。

注：这里不是重点，所以一句带过。

**Maven profile：** Maven允许我们在配置文件(项目pom.xml文件或maven的settings.xml文件)中配置多个profile，并通过mvn指令或触发器指定激活那个(或哪些)profile。

注：关于maven profile的相关知识可详见https://blog.csdn.net/justry_deng/article/details/100537571。

## SpringBoot profile整合Maven profile实现多环境配置、依赖自动切换

**声明：** 本文以test环境与prod环境的切换进行示例。

### 第一步：准备SpringBoot的profile（即：准备相关配置文件）

![1](../repository/1-162965456978524.png)

**说明一：** 本人在src/main/resources下创建了两个文件夹，分别对应两个环境；然后在prod文件夹下放置了生产环境需要的配置，在test文件夹下放置了测试环境需要的配置。

**说明二：** 以上图中的ftp.properties文件所示，**因为已经用文件夹来区分环境了，所以无论是生产环境还是测试环境，关于ftp配置的配置文件名可以相同。但是为了语意更清晰，所以哪怕已经按照文件夹区分环境了，还是建议按照不同环境对应命名**。如,prod文件夹下的应取名ftp-prod.properties;test文件夹下的应取名ftp-test.properties。

 上图中prod下的ftp.properties文件，内容是：

![2](../repository/2.png)

上图中test下的ftp.properties文件，内容是：

![3](../repository/3.png)

### 第二步：根据自己的实际需求，在pom.xml中，准备Maven 的profile

![4](../repository/4.png)

**提示**：本人只是为了示例，所以这里只是简单的一个环境引入fastjson;一个环境引入lombok。

### 第三步：将Maven profile与SpringBoot profile关联起来

![5](../repository/5.png)

**说明：**

- **上图中的1：** 一个自定义的环境标识参数，该参数的值应该与第一步创建的用户区分环境的文件夹名称一致。这个参数两个地方用到：一是pom.xml中的resourses下被用到；而是在系统的配置文件application.properties中被@key@动态取值（具体怎么取的可见下一步）。
- **上图中的2：** 将SpringBoot profile与Maven profile整合到一块儿的关键部分。
- **上图中的2.1：** 指定要打包加载的资源文件所在的根目录。默认该目录下的所有资源都会被打包。
- **上图中的2.2：** filter过滤器，只有被过滤的资源文件里面的占位符，会被对应替换。如果没有这个，那么配置文件通过@key@的方式将不能获取到pom.xml里key对应的值。
- **上图中的2.3、2.4：** 因为要动态打包对应环境的资源，即:从众多资源目录中只加载我们需要的资源目录。方式有两个：一是保留需要的环境目录，然后排除不需要的环境目录；二是先排除所有目录(不管是不是我们需要的)，然后再把我们需要的环境目录加进来。这里采用方式二。

### 第四步：在系统配置文件中application.properties中，动态获取pom.xml里的激活环境标识

![7](../repository/7.png)

**说明：**在spring-boot-starter-parent的pom.xml中，springboot将**配置文件里动态获取pom.xml文件参数值的占位符，由${key}替换成了@key@**。目的是：避免和SpringBoot的占位符${key}语意冲突。

### 第五步：测试一下

**prod测试：**

使用maven -clean -install -P prod-id指令，激活对应的maven profile并打包：

![8](../repository/8.png)

使用gd-gui打开生成的jar包，观察：

![img](../repository/9.png)

![10](../repository/10.png)

**test测试：**

使用maven -clean -install -P test-id指令，激活对应的maven profile并打包：

![11](../repository/11.png)

使用gd-gui打开生成的jar包，观察：

![img](../repository/12.png)

![13](../repository/13.png)

**由此可见，SpringBoot profile 整合 Maven profile成功！**

## 简单拓展

上面的过程依赖于从Maven profile方面主动切换环境；能否从SpringBoot profile主动切换环境呢？能的！笔者之前写过一个Maven plugin来实现这个功能，不过有点瑕疵。下面介绍一种从“SpringBoot profile”方面实现切换环境切换(maven配置环境 + SpringBoot环境)的方法。

**声明：** 下面的内容是基于上面的内容的进一步改造，所以这里步骤接上一步的走。

### 第五步：在资源目录下创建一个环境切换触发文件

![14](../repository/14.png)

注：位置、文件名均可随意，只要与自己的配置(见下面的步骤)对应得上就行。如：本人以env.prod对应生产环境。以env.test对应测试换。

### 第六步：使用maven profile的activation标签的file功能，设置环境触发条件

**提示：** pom.xml同上，不过需要在maven profle的对应位置添加如下信息：

![15](../repository/15.png)

### 第七步：测试一下

**prod测试：**

将环境标识文件重命名为对应生产环境为env.prod：

![16](../repository/16.png)

注：对于某些IDE，对文件关联处理得比较好，在修改文件名时，会对应修改其引用。但是我们这里只需要修改文件名，不需要将我们在pom.xml里的对应引用也改了。所以**建议：先删除该标识文件，然后再按照要激活的环境名，新建标识文件即可**(我们只需要用到该文件的文件名，该文件有没有数据无所谓)。

注：程序运行的环境只可能是一个，所以环境标识文件也只能有一个，如果多了，那么可能出现问题。

直接使用maven -clean -install打包即可：

![17](../repository/17.png)

使用gd-gui打开生成的jar包，观察：

![18](../repository/18.png)

![19](../repository/19.png)

**test测试：**

将环境标识文件重命名为对应测试环境为env.test：

![20](../repository/20.png)

注：对于某些IDE，对文件关联处理得比较好，在修改文件名时，会对应修改其引用。但是我们这里只需要修改文件名，不需要将我们在pom.xml里的对应引用也改了。所以**建议：先删除该标识文件，然后再按照要激活的环境名，新建标识文件即可**(我们只需要用到该文件的文件名，该文件有没有数据无所谓)。

注：程序运行的环境只可能是一个，所以环境标识文件也只能有一个，如果多了，那么可能出现问题。

直接使用maven -clean -install打包即可：

![21](../repository/21.png)

使用gd-gui打开生成的jar包，观察：

![22](../repository/22.png)

![23](../repository/23.png)

**拓展成功！拓展完毕！**

---
## 相关资料

- [<font face="幼圆"  color = "#86CA5E">**demo代码下载**</font>](https://gitee.com/JustryDeng/shared-files/raw/master/springboot-profile-and-maven-profile.rar)
- [`https://maven.apache.org/pom.html#Version_Order_Specification`](https://maven.apache.org/pom.html#Version_Order_Specification)
- [`https://blog.csdn.net/java_collect/article/details/83870215#1_maven_126`](https://blog.csdn.net/java_collect/article/details/83870215#1_maven_126)

