# SpringBoot之Actuator入门

- [SpringBoot之Actuator入门](#springboot之actuator入门)
	- [<font  face="幼圆" color = "#3399EA">Spring Boot Actuator的使用</font>](#font--face幼圆-color--3399easpring-boot-actuator的使用font)
		- [<font  face="幼圆" color = "#86CA5E">第一步：在SpringBoot项目的pom.xml中引入actuator依赖</font>](#font--face幼圆-color--86ca5e第一步在springboot项目的pomxml中引入actuator依赖font)
		- [<font  face="幼圆"  color = "#86CA5E">第二步：配置application.yml或application.properties，使actuator端点可访问</font>](#font--face幼圆--color--86ca5e第二步配置applicationyml或applicationproperties使actuator端点可访问font)
		- [<font  face="幼圆" color = "#86CA5E">第三步：访问Web端点({ip}:{端口}/{management.endpoints.web.base-path}/{端点-id})，观察监控信息</font>](#font--face幼圆-color--86ca5e第三步访问web端点ip端口managementendpointswebbase-path端点-id观察监控信息font)
	- [<font  face="幼圆" color = "#3399EA">Actuator端点ID及说明</font>](#font--face幼圆-color--3399eaactuator端点id及说明font)
	- [<font  face="幼圆" color = "#3399EA">Actuator常用端点使用示例</font>](#font--face幼圆-color--3399eaactuator常用端点使用示例font)
	- [<font  face="幼圆" color = "#3399EA">相关资料</font>](#font--face幼圆-color--3399ea相关资料font)
---

>Spring Boot Actuator主要用于监控和度量SpringBoot应用程序。主要有三种观察Actuator信息的方式：`Web端点`、`远程shell`、`JMX`。本文主要介绍相对常用的方式：通过Web端点(即：通过浏览器访问URL)观察Actuator的统计信息。

**<font face="幼圆"  color = "#F53B45">声明</font>：** 以下内容基于SpringBoot2.x。

## <font  face="幼圆" color = "#3399EA">Spring Boot Actuator的使用</font>

### <font  face="幼圆" color = "#86CA5E">第一步：在SpringBoot项目的pom.xml中引入actuator依赖</font>

```html
<!-- actuator监控 -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>
```
### <font  face="幼圆"  color = "#86CA5E">第二步：配置application.yml或application.properties，使actuator端点可访问</font>

actuator`端点可访问的前提是：目标端点已启用`且`已暴露`。但由于默认情况下，Actuator已启用了除shutdown之外的所有端点，所以如果想访问的端点不是shutdown的话，我们只需要保证该端点已暴露即可。

- **启用/禁用端点：** 通过配置`management.endpoint.<端点id>.enabled`为`true/false`来`启用/禁用`端点，如：

  ```bash
  # 启用shutdown对应的端点
  management.endpoint.shutdown.enabled=true
  # 禁用端点id为<health>的端点
  management.endpoint.health.enabled=false
  ```
  提示一：在下文中，会罗列出Actuator所有的端点id及相应的说明。

  提示二：Actuator会默认启用除shutdown之外的所有端点,如果嫌默认启用的端点过多，只想启用其中一两个端点的话，我们可以先设置默认关闭所有端点(management.endpoints.enabled-by-default=false)，然后再指定启用我们需要的端点(management.endpoint.<端点id>.enabled=true)，如：

  ![在这里插入图片描述](../repository/20191129131927494.png)

- **暴露/隐藏端点：** 通过配置`management.endpoints.web.exposure.include=xxx`来`暴露Web端点`；通过配置`management.endpoints.web.exposure.exclude=xxx`来`隐藏(不暴露)Web端点`。其中`xxx`可以是`一个端点id`，也可以是`通配符*(表示所有端点id)`，还可以是`多个端点id(多个端点id之间使用逗号分割)`，如：

	```bash
	# 暴露所有端点
	management.endpoints.web.exposure.include=*
	# 隐藏(不暴露)端点
	# management.endpoints.web.exposure.exclude=info
	# 隐藏(不暴露)端点
	management.endpoints.web.exposure.exclude=env,beans
	```
	注：这里暴露/隐藏端点的方式是针对Web端点的，如果是jmx的话，key是management.endpoints.jmx.exposure.include或management.endpoints.jmx.exposure.exclude。

	注：Web端点默认暴露的有info、health。

### <font  face="幼圆" color = "#86CA5E">第三步：访问Web端点({ip}:{端口}/{management.endpoints.web.base-path}/{端点-id})，观察监控信息</font>

示例访问(端点id为)env的端点信息：

![在这里插入图片描述](../repository/20191129132810773.png)

注：低版本SpringBoot的management.endpoints.web.base-path值默认为/，`高版本SpringBoot`的`management.endpoints.web.base-path`值`默认为actuator`。可`通过主动设置其值来改变uri`，如`设置management.endpoints.web.base-path=/abc后`，观察env信息，`就需要访问ip:端口/abc/env`了。

注：如果访问Web端点时，需要跨域的话，那么可参考这样配置：
```bash
# 以下配置允许来自 example.com 域的 GET 和 POST 调用
management.endpoints.web.cors.allowed-origins=http://example.com
management.endpoints.web.cors.allowed-methods=GET,POST
```



## <font  face="幼圆" color = "#3399EA">Actuator端点ID及说明</font>

| 端点ID           | 说明                                                         | 已默认开启 | Web端点已默认暴露 |
| ---------------- | ------------------------------------------------------------ | ---------- | ----------------- |
| auditevents      | 暴露当前应用程序的审计事件信息。                             | 是         | 否                |
| beans            | 显示应用程序中所有 Spring bean 的完整列表。                  | 是         | 否                |
| conditions       | 显示在配置和自动配置类上评估的条件以及它们匹配或不匹配的原因。 | 是         | 否                |
| configprops      | 显示所有 @ConfigurationProperties 的校对清单。               | 是         | 否                |
| env              | 暴露全部环境属性。                                           | 是         | 否                |
| flyway           | 显示已应用的 Flyway 数据库迁移。                             | 是         | 否                |
| health           | 报告应用程序的健康指标，这些值由HealthIndicator的实现类提供。<br>注：health有三个标准：<br>never(默认)：默认不显示细节信息。<br>when-authorized：当鉴权通过时，才显示细节信息。<br>always：总是显示细节信息。<br>注：可在配置文件中形如management.endpoint.health.show-details=always来配置使用哪一个标准。<br>注：health的细节信息可包含的东西较全，如：磁盘、数据库、RabbitMQ、Solr、MongoDB、Redis、jms、mail等等。 | 是         | 是                |
| httptrace        | 显示 HTTP 追踪信息(默认情况下，最后 100 个 HTTP 请求/响应交换)。 | 是         | 否                |
| info             | 获取应用程序的定制信息，这些信息由info打头的属性提供。<br>注：可自定义，如:info.key=value。 | 是         | 是                |
| integrationgraph | 显示 Spring Integration 图。                                 | 是         | 否                |
| loggers          | 显示和修改应用程序中日志记录器的配置。                       | 是         | 否                |
| liquibase        | 显示已应用的 Liquibase 数据库迁移。                          | 是         | 否                |
| metrics          | 报告各种应用程序度量信息，比如内存用量和HTTP请求计数。       | 是         | 否                |
| mappings         | 描述全部的URI路径，以及它们和控制器（包含Actuator端点）的映射关系。 | 是         | 否                |
| scheduledtasks   | 显示应用程序中的调度任务。                                   | 是         | 否                |
| sessions         | 允许从 Spring Session 支持的会话存储中检索和删除用户会话。当使用 Spring Session 的响应式 Web 应用程序支持时不可用。 | 是         | 否                |
| shutdown         | 正常关闭应用程序。                                           | 否         | 否                |
| threaddump       | 执行线程 dump。                                              | 是         | 否                |

如果使用web应用(Spring MVC, Spring WebFlux, 或者 Jersey)，还有附加端点：
| 端点ID     | 说明                                                         | 已默认开启 | Web端点已默认暴露 |
| ---------- | ------------------------------------------------------------ | ---------- | ----------------- |
| heapdump   | 返回一个堆dump 文件。                                        | 是         | 否                |
| jolokia    | 通过 HTTP 暴露 JMX bean（当 Jolokia 在 classpath 上时，不适用于 WebFlux）。 | 是         | 否                |
| logfile    | 返回日志文件的内容（如果已设置logging.file 或 logging.path 属性）。支持使用 HTTP Range 头来检索部分日志文件的内容。<br>注：在SpringBoot高版本里，使用logging.file.path代替logging.path；使用logging.file.name代替logging.file。<br>注：logging.file.name可具体指定到哪一个日志文件名；logging.file.path只能指定到日志文件所在的目录，会默认指向该目录下名为Spring.log的日志文件。 | 是         | 否                |
| prometheus | 以可以由 Prometheus 服务器抓取的格式暴露指标。               | 是         | 否                |



## <font  face="幼圆" color = "#3399EA">Actuator常用端点使用示例</font>

- **<font face="幼圆"  color = "#FFBB66">env端点(访问ip:端口/actuator/env)</font>：** 查看所有配置属性(系统属性、环境变量、应用配置等等)。

  ![在这里插入图片描述](../repository/2019112913511862.png)

  注：如果想要单独查看env下某一项的配置，那么可访问/actuator/env/xxx即可，如：

  ![在这里插入图片描述](../repository/20191129135133155.png)

-  **<font face="幼圆"  color = "#FFBB66">beans端点(访问ip:端口/actuator/beans)</font>：** 查看Spring中Ioc的beans。

  ![在这里插入图片描述](../repository/20191129135349565.png)

-  **<font face="幼圆"  color = "#FFBB66">conditions端点(访问ip:端口/actuator/conditions)</font>：** SpringBoot中，autoconfig的一些类及其条件信息(含:哪些自动配置成功，哪些自动配置失败，哪些无条件自动配置)。

  ![在这里插入图片描述](../repository/2019112913544335.png)

  展开一个，观察一下：

  ![在这里插入图片描述](../repository/20191129135454653.png)

  为了方便理解，可看一下MybatisPlusAutoConfiguration类的源码：

  ![在这里插入图片描述](../repository/20191129135503733.png)

- **<font face="幼圆"  color = "#FFBB66">mappings端点(访问ip:端口/actuator/mappings)</font>：** 描述全部的URI路径，以及它们和控制器（包含Actuator端点）的映射关系。

  ![在这里插入图片描述](../repository/20191129135542996.png)

- **<font face="幼圆"  color = "#FFBB66">metrics端点(访问ip:端口/actuator/metrics)</font>：** 报告各种应用程序度量信息，比如内存、系统、jdbc、http、tomcat等等。

  ![在这里插入图片描述](../repository/2019112913563374.png)

  示例查看cpu数量：

  ![在这里插入图片描述](../repository/20191129135649161.png)

- **<font face="幼圆"  color = "#FFBB66">httptrace端点(访问ip:端口/actuator/httptrace)</font>：** 查看(最近100条)HTTP请求信息。

  ![在这里插入图片描述](../repository/20191129135726615.png)

- **<font face="幼圆"  color = "#FFBB66">threaddump端点(访问ip:端口/actuator/threaddump)</font>：** 查看线程dump信息。

  ![在这里插入图片描述](../repository/20191129135757850.png)

- **<font face="幼圆"  color = "#FFBB66">health端点(访问ip:端口/actuator/health)</font>：** 查看应用的健康信息。

  ![在这里插入图片描述](../repository/20191129135832688.png)

- **<font face="幼圆"  color = "#FFBB66">info端点(访问ip:端口/actuator/info)</font>：** 定制info信息。
  比如在application.properties文件中配置有信息：

  ![在这里插入图片描述](../repository/20191129135940667.png)
  
  那么访问/actuator/info就可看到：

  ![在这里插入图片描述](../repository/20191129140022941.png)
  
- **<font face="幼圆"  color = "#FFBB66">logfile端点(访问ip:端口/actuator/logfile)</font>：** 用浏览器直接查看日志文件的内容。
  在系统配置文件中，使用logging.file(或logging.file.name)指定日志文件：

  ![在这里插入图片描述](../repository/20191129140142181.png)

  然后，访问/actuator/logfile：

  ![在这里插入图片描述](../repository/20191129140149939.png)

- **<font face="幼圆"  color = "#FFBB66">configprops端点(访问ip:端口/actuator/configprops)</font>：** 显示所有 @ConfigurationProperties 的校对清单。

  ![在这里插入图片描述](../repository/20191129140247380.png)

  为了方便理解，可看一下HttpTraceProperties类的源码：

  ![在这里插入图片描述](../repository/20191129140302708.png)
---
## <font  face="幼圆" color = "#3399EA">相关资料</font>

- **《Spring Boot实战》** Craig Walls 著，丁雪丰 译
- **《深入浅出Spring Boot》** 杨开振 著
- [`https://docs.spring.io/spring-boot/docs/2.0.5.RELEASE/reference/htmlsingle/#production-ready`](https://docs.spring.io/spring-boot/docs/2.0.5.RELEASE/reference/htmlsingle/#production-ready)
- [`https://docshome.gitbooks.io/springboot/content/pages/production-ready.html#production-ready`](https://docshome.gitbooks.io/springboot/content/pages/production-ready.html#production-ready)