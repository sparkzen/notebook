# Oracle工具的常见问题

- [Oracle工具的常见问题](#oracle工具的常见问题)
  - [核心Lisener的问题](#核心lisener的问题)
  - [数据库服务的问题](#数据库服务的问题)

## 核心Lisener的问题

重新配置：`程序` => `Oracle`  => `配置和移植工具` => `Net Configuration Assistant` => `监听程序配置`，然后就可以对监听器作出一些操作：

![1648534912189](../repository/1648534912189.png)

## 数据库服务的问题

删除该数据库，然后重新创建即可：`程序` => `Oracle` => `配置和移植工具` => `Database Configuration Assistant`，在这里面不仅可以创建数据库，还可以进行删除等操作：

![1648534969998](../repository/1648534969998.png)

注：如果安装Oracle时没有创建数据库，那么也可以通过此方式来创建数据库。