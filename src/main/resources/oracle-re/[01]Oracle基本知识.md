# Oracle基本知识


- [Oracle基本知识](#oracle基本知识)
  - [简介](#简介)
  - [几种数据库的比较](#几种数据库的比较)
  - [wins上oracle的服务](#wins上oracle的服务)
  - [Oracle数据库文件](#oracle数据库文件)


## 简介

Oracle（甲骨文）公司

1977年，三人合伙创办（Software Development Laboratories，SDL）

1979年，更名为Relational Software Inc.，RSI

1983年，为了突出核心产品 ，RSI更名为Oracle

2002年04月26日，启用“甲骨文”作为中文注册商标

Oracle数据库是Oracle（甲骨文）公司的核心产品。

## 几种数据库的比较

- MySQL：免费，功能弱（不支持检查约束）

- DB2（IBM）：易用性好，常用于金融项目的数据库，收费，速度较快

- SQLServer(微软)：易用性好，必须在微软平台服务器下运行，收费

- Oracle：全球最强大的单体数据库（支持检查约束），数据库结构复杂，收费()

- ……

目前（2018年）Oracle数据库的几个主要版本

- oracle8i：基本上没有了

- oracle9i：市场上少量

- oracle10g：市场上大量

- oracle11g：较新产品，少量项目使用

- oracle12c：最新产品

## wins上oracle的服务

安装Oracle10g后，服务中会有

![1648534168602](../repository/1648534168602.png)

其中

![1648534174390](../repository/1648534174390.png)

是Oracle的核心核心服务，剩余的三个是对应一个Oracle数据库的（这三个服务的名字以自己取的数据库名字(大写或小写)结尾，图片中的其它三个服务对应的Oracl数据库库名为ORAC），每创建一个数据库，服务中就会多出三个对应的服务。

 注：oracle的数据库和其他一般数据库不同的最明显的一点就是：oracle的一个数据库会关联三个服务。

 注：启服务时先启动核心监听器服务![1648534363558](../repository/1648534363558.png)，再启动对应数据库名字的![1648534382318](../repository/1648534382318.png)即可（一般情况下，启动该数据库对应的三个服务中这一个服务就够了，如果不够，那么:除了启动核心监听服务外，还需要启动三个对应的服务）。

 注：其服务的方式：除了在服务窗口启动关闭服务外，我们还可以创建一个.bat文件（Windows系统下,新建.txt文件，将内容设置好了之后，再讲后缀名改为.bat即可），右击该文件并以管理员身份运行该文件可以快速启动关闭方服务。这里给出启动关闭数据库名为ORAC的文件示例：

 启动：

```bash
@echo off
echo 正在开启Oracle服务
net start OracleDBConsoleorcl
net start OracleOraDb10g_home1iSQL*Plus
net start OracleOraDb10g_home1TNSListener
net start OracleServiceORCL
echo 完成
```

关闭：

```bash
@echo off
echo 正在关闭Oracle服务
net stop OracleDBConsoleorcl
net stop OracleOraDb10g_home1iSQL*Plus
net stop OracleOraDb10g_home1TNSListener
net stop OracleServiceORCL
echo 完成
```

注：一般的，我们启动`net start OracleOraDb10g_home1TNSListener`和`net start OracleServiceORCL`就够用了。所以一般情况下我们可以不启动其它两个服务。

## Oracle数据库文件

Oracle一个数据库中包含三种文件：

1. 控制文件：后缀名为.CTL

2. 日志文件：后缀名为.LOG

3. 数据文件：后缀名为.DBF

注：Oracle的一个数据库中，可以创建多个表空间；一般地，一个java项目对应一个表空间。

注：MySQL可以用SQLyog_Enterprise(俗称:海豚)等工具，Oracle可以用PLSQLDeveloper(俗称:紫油桶)等工具。

注：在Oracle中，代码不区分大小写，但是数据内容区分大小写。

注：Oracle与MySQL有很多地方不同，但是也有很多地方相同；本章以下内容仅列出不同的地方，没列出的则与MySQL一样。

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 

 