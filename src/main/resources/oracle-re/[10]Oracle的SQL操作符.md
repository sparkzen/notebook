# Oracle的SQL操作符

- [Oracle的SQL操作符](#oracle的sql操作符)
  - [Oracle的SQL操作符](#oracle的sql操作符-1)
  - [集合操作符详细说明](#集合操作符详细说明)

## Oracle的SQL操作符

| SQL操作符 | 算术操作符 |        +       -       *      /         |
| :-------: | :--------: | :-------------------------------------: |
|           | 比较操作符 |   <     >      <=     >=    <>      =   |
|           | 逻辑操作符 |        and        or        not         |
|           | 连接操作符 |                  \|\|                   |
|           | 集合操作符 | union   union all    minus    intersect |

注：虽然`<>`为标准的 SQL不等于，但是各个数据库也都已经支持了  `!=` 

注：Oracle中的赋值运算符是`:=`

## 集合操作符详细说明

> 注：使用集合操作符的前提是：**两次查出来的列的数量必须一样，且对应列的数据类型要一样**
>
> 注：联合后，查询结果的列名为集合操作符前面那个表的定义的列名

- union:  根据不同的查询条件，联合两张表(可以是同一张表，也可以是不同的表)的两个查询结果，若其中有所有对应列的值都相同(如果查询结果中有rowid，那么rowid也要考虑在内)的行，则该行只出现一个(重复的不再出现)
  示例：`select * from result where 条件一  union select * from result where 条件二`

- union all: 根据不同的查询条件，联合两张表(可以是同一张表，也可以是不同的表)的两个查询结果，若其中有所有对应列的值都相同(如果查询结果中有rowid，那么rowid也要考虑在内)的行，则该行有几个就要出现几个(重复的也要出现) 
  示例：`select * from result where 条件一  union all select * from result where 条件二`

- intersect:  根据不同的查询条件，联合两张表(可以是同一张表，也可以是不同的表)的两个查询结果，若某行不止一个，那么出现该行(若某行只有一个，那么不出现该行)
  示例：`select * from result where 条件一  intersect select * from result where 条件二`

- minus:  根据不同的查询条件，联合两张表(可以是同一张表，也可以是不同的表)的两个查询结果，若某行只有一个，那么出现该行(若某行不止一个，那么不出现该行）
  示例：`select * from result where 条件一  minus select * from result where 条件二`