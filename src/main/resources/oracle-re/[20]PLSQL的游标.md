# PL/SQL的游标


- [PL/SQL的游标](#plsql的游标)
  - [示例](#示例)
  - [游标的属性](#游标的属性)

## 示例

游标用法较固定，示例说明：

```sql
declare 
  v_xing student.studentname%type:='王';
  cursor stus is
         select * from student where studentname like v_xing||'%';
  v_stuobj student%rowtype;   
begin
  open stus;
  loop
   fetch stus into v_stuobj;
   exit when stus%notfound;
   dbms_output.put_line('学号：'||v_stuobj.studentno);
   dbms_output.put_line('性别：'||v_stuobj.sex);
   dbms_output.put_line('地址：'||v_stuobj.address);
   dbms_output.put_line('电话：'||v_stuobj.phone);
   dbms_output.put_line('电邮：'||v_stuobj.email); 
   dbms_output.put_line('--------------------------'); 
  end loop;
  close stus;
end;
```

## 游标的属性

![1648606926149](../repository/1648606926149.png)