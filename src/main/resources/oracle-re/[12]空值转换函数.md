# 空值转换函数

- [空值转换函数](#空值转换函数)
  - [简述](#简述)
  - [转换空值的函数（示例说明）](#转换空值的函数示例说明)

## 简述

Oracle的逻辑函数有很多，这里就不一一列举了，仅给出常用的转换空值的函数

## 转换空值的函数（示例说明）

- nvl：`select nvl(address,’xxx’) from student`：如果查询出来的address列有位空值的，那么显示xxx
- nvl2：`select nvl2(address,’xxx1’,’xxx2’) from student`：如果查询出来的address列不为空值，那么显示xxx1；如果为空值，那么显示xxx2
- decode：`select decode(score,60,’xxx1’,70,’xxx2’,’xxx3’) from result`：如果查询出来的score值为60，那么显示xxx1；如果值为70，那么显示xxx2,；否则显示xxx3
  注：例子中只列举了60分，70分的情况；实际上可以列举更多
  注：最后的xxx3是默认值，即:当前面的所有情况都不满足时，那么显示xxx3；当然，可以不写默认值(如果不写默认值的话，当前面都不满足时，原来的数据该显示多少就显示多少)

