# 连接他人Oracle数据库的方法、SYS与SYSTEM用户

- [连接他人Oracle数据库的方法、SYS与SYSTEM用户](#连接他人oracle数据库的方法sys与system用户)
  - [连接本机Oracle](#连接本机oracle)
  - [紫油桶 - 连接他人Oracle数据库的方法](#紫油桶---连接他人oracle数据库的方法)
  - [xml中链接Oracle数据库](#xml中链接oracle数据库)
  - [SYS与SYSTEM用户](#sys与system用户)

## 连接本机Oracle

使用oracle客户端紫油桶连接即可。紫油桶的登录界面是这样的：

![1648535063932](../repository/1648535063932.png)

注：如果我们在安装时,这一步是这么处理的：

![1648535082231](../repository/1648535082231.png)

那么我们初次登录自己Oracle时，可以用账号/密码：`sys/sys` 或 `system/system` 或 `scott/scott` 登录；然后创建用户后，再使用用户身份登录。

## 紫油桶 - 连接他人Oracle数据库的方法

> 提示：这里是紫油桶的方式，如果是Navicat的话，是可以直接连过去的。

1. 第一步：找到Oracle的安装目录下的`de_1` => `network` => `ADMIN`文件夹。
   注：有可能没有db_1文件夹

2. 修改tnsnames.ora文件。
   先复制一份原有的数据库信息，如：
   ![1648535380361](../repository/1648535380361.png)
   黏贴，然后修改信息，如：

   ```
   {自取连接名} =
     (DESCRIPTION =
       (ADDRESS = (PROTOCOL = TCP)(HOST = {要连接的数据库的ip地址})(PORT = {端口，一般为1521}))
       (CONNECT_DATA =
         (SERVER = DEDICATED)
         (SERVICE_NAME = {服务名，一般为orcl})
       )
     )
   ```

   注：tnsnames.ora文件是受保护的文件，修改后直接保存是不行的；时可以另存到其它地方，然后在复制回来覆盖掉旧的tnsnames.ora文件。

   注：如果修改了计算机名，那么要修改（同目录下）listener.ora和tnsnames.ora文件中对应的参数。

## xml中链接Oracle数据库

连接Oracle数据库的xml配置示例（以在Spring核心xml中配置连接Oracle为例）：

![1648535622069](../repository/1648535622069.png)

 ## SYS与SYSTEM用户

![1648535659921](../repository/1648535659921.png)

注：以SYSDBA连接之后，可以通过`select INSTANCE_NAME from v$instance`；查询已创建了的ORACLE数据库的实例名，如下：

![1648540688555](../repository/1648540688555.png)

![1648540692839](../repository/1648540692839.png)