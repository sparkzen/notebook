# scan与批量删除redis key

[toc]

## 代码类

> 提示：都是些危险操作，注意对权限的控制

> 下述代码用到的spring-data-redis依赖版本：
>
> ```java
> <dependency>
>     <groupId>org.springframework.data</groupId>
>     <artifactId>spring-data-redis</artifactId>
>     <version>2.6.2</version>
> </dependency>
> ```

```java
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.nio.charset.StandardCharsets;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 扫描redis中的key
 *
 * @author JustryDeng
 * @since 2022/6/26 13:56:18
 */
@RestController
@RequestMapping("/redisKey")
public class ScanRedisKeyController {
    
    @Resource
    StringRedisTemplate stringRedisTemplate;
    
    /**
     * 根据通配符扫描redis中的key
     *
     * @param pattern
     *         通配， 如: projectName:userId:*
     *         注：安全考虑，前置要求pattern不能为*
     * @param scanCount
     *         每次扫描的key个数
     *
     * @return redis中的key
     */
    @RequestMapping(value = "/scan", method = {RequestMethod.GET, RequestMethod.POST})
    public LinkedHashSet<String> scan(@RequestParam("pattern") String pattern,
                                      @RequestParam(value = "scanCount", defaultValue = "1000") long scanCount) {
        
        if (StringUtils.isBlank(pattern)) {
            throw new IllegalArgumentException("pattern cannot be blank.");
        }
        if ("*".equals(pattern)) {
            throw new IllegalArgumentException("pattern cannot be '*'.");
        }
        if (scanCount <= 0) {
            throw new IllegalArgumentException("scanCount cannot le 0.");
        }
        return stringRedisTemplate.execute((RedisCallback<LinkedHashSet<String>>) connection -> {
            LinkedHashSet<String> keys = new LinkedHashSet<>();
            Cursor<byte[]> cursor = connection.scan(
                    ScanOptions.scanOptions().match(pattern).count(scanCount).build()
            );
            while (cursor.hasNext()) {
                keys.add(new String(cursor.next(), StandardCharsets.UTF_8));
            }
            return keys;
        });
    }
    
    /**
     * 批量删除keys
     *
     * @param keys
     *         批量删除指定key
     *
     * @return 删除结果
     */
    @PostMapping(value = "/delete")
    public String delete(@RequestBody Set<String> keys) {
        if (CollectionUtils.isEmpty(keys)) {
            throw new IllegalArgumentException("keys cannot be empty.");
        }
        keys = keys.stream().filter(StringUtils::isNotBlank).collect(Collectors.toSet());
        if (CollectionUtils.isEmpty(keys)) {
            throw new IllegalArgumentException("valid keys cannot be empty.");
        }
        Long total = stringRedisTemplate.countExistingKeys(keys);
        Long deletedTotal = stringRedisTemplate.delete(keys);
        return String.format("总共%s条，成功删除%s条.", total, deletedTotal);
    }
}
```

