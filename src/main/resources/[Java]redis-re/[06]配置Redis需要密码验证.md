# 配置Redis需要密码验证

- [配置Redis需要密码验证](#配置redis需要密码验证)
  - [配置Redis需要密码验证](#配置redis需要密码验证-1)
    - [第一步：注释掉bind，允许所有机器连接Redis](#第一步注释掉bind允许所有机器连接redis)
    - [第二步：通过requirepass设置密码](#第二步通过requirepass设置密码)
    - [第三步：后台启动Redis](#第三步后台启动redis)
    - [第四步：启动Redis客户端，测试一下](#第四步启动redis客户端测试一下)

## 配置Redis需要密码验证

> 编辑redis.conf文件，配置Redis需要密码验证

### 第一步：注释掉bind，允许所有机器连接Redis

![img](../repository/wps2557.tmp.jpg)

注：如果不注释掉也可以，就需要在bind后面指明允许连接Redis的机器的IP，多个ip空格分隔，如:bind 10.8.109.36 10.8.109.24。

### 第二步：通过requirepass设置密码

![img](../repository/wps2558.tmp.jpg)

### 第三步：后台启动Redis

![img](../repository/wps2559.tmp.jpg)

注：在redis.conf配置文件中，设置了允许后台启动。

### 第四步：启动Redis客户端，测试一下

- 方式一：连接进去后，输入 auth 密码进行验证。

  ![img](../repository/wps255A.tmp.jpg) 

- 方式二：在连接时，就通过参数-a指明密码

  ![img](../repository/wps256A.tmp.jpg) 

由此可见，Redis配置需要密码认证，成功！