# CentOS7上搭建Redis-5.0.4（单机版）

- [CentOS7上搭建Redis-5.0.4（单机版）](#centos7上搭建redis-504单机版)
  - [相关说明](#相关说明)
  - [准备工作 - gcc-c++的安装](#准备工作---gcc-c的安装)
  - [Redis单机版的安装](#redis单机版的安装)
    - [第一步：去官网下载redis稳定版最新的安装包，并将包复制到Linux里](#第一步去官网下载redis稳定版最新的安装包并将包复制到linux里)
    - [第二步：解压这个文件](#第二步解压这个文件)
    - [第三步：进入解压后的目录，对里面的文件进行编译和安装](#第三步进入解压后的目录对里面的文件进行编译和安装)
    - [第四步：进入redis的安装路径，`src/redis-server`启动redis](#第四步进入redis的安装路径srcredis-server启动redis)

## 相关说明

Redis是运行在Linux上的软件，是没有基于Windows的

 Redis的版本分为：

- 单机版 

- 集群版

## 准备工作 - gcc-c++的安装

**第一步：**确保以root身份进行操作

**第二步：**安装gcc-c++

在线安装，电脑连上网，然后执行以下指令即可：

```shell
yum install -y gcc-c++
```

![1](../repository/1-16558328762811.png)

## Redis单机版的安装

### 第一步：去[官网](https://redis.io/download)下载redis稳定版最新的安装包，并将包复制到Linux里

注：本人将redis安装包放置在/opt下

 ![2](../repository/2-16558329962412.png)

![3](../repository/3-16558330019533.png)

![4](../repository/4-16558330132394.png)

注：也可以通过wget指令直接在线下载

```shell
wget http://download.redis.io/releases/redis-5.0.4.tar.gz
```

![5](../repository/5-16558330192395.png)

### 第二步：解压这个文件

```shell
tar xzf redis-5.0.4.tar.gz
```

![6](../repository/6.png)

### 第三步：进入解压后的目录，对里面的文件进行编译和安装

```shell
cd redis-5.0.4
make
```

![7](../repository/7-16558331539376.png)

### 第四步：启动redis

```shell
src/redis-server
```

![8](../repository/8-16558332119767.png)

注：从图中可见，Redis安装并启动成功，其监听端口为6379

注：此方式为前台启动的方式















