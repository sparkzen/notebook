Redis（服务端/客户端）的启动、关闭、查看状态

- [服务端](#服务端)
  - [查看Redis是否启动](#查看redis是否启动)
  - [前台启动](#前台启动)
  - [后台启动](#后台启动)
  - [启动Redis集群](#启动redis集群)
  - [关闭（停止）](#关闭停止)
- [客户端](#客户端)
  - [启动（单机模式）](#启动单机模式)
  - [启动（集群模式）](#启动集群模式)
  - [退出](#退出)
  - [客户端使用示例](#客户端使用示例)

## 服务端

### 查看Redis是否启动

`ps -ef|grep redis`

![img](../repository/wpsEB23.tmp.jpg) 

 注：也可以使用Redis自带的客户端进行测试:

![img](../repository/wpsEB24.tmp.jpg) 

进入redis自带的客户端工具，然后再输入ping，如果返回一个PONG则表示Redis后置启动成功

### 前台启动

切换至Redis安装目录下，执行`src/redis-server`指令

![img](../repository/wpsEB25.tmp.jpg) 

注：也可直接切换至Redis安装目录下的src目录下，运行./redis-server指令启动Redis

### 后台启动

**第一步：**修改Redis安装目录下的配置文件redis.conf。将其中的daemonize由no改为yes。

![img](../repository/wpsEB26.tmp.jpg) 

改为：

![img](../repository/wpsEB36.tmp.jpg) 

 注：不同版本的Redis里，redis.conf文件所在位置可能不一样；在redis-5.0.4中，redis.conf文件直接在Redis解压后的目录下

注：daemonize为是否以守护线程的方式启动Redis（即：是否后台启动Redis）

**第二步：**切换至Redis安装目录下，执行`src/redis-server redis.conf`指令启动redis

![img](../repository/wpsEB37.tmp.jpg) 

 注：也可直接切换至Redis安装目录下的src目录下，运行`./redis-server ../redis.conf`指令后台启动Redis

### 启动Redis集群

若之前已经搭建好了Redis集群，那么只需要启动各个Redis节点即可，集群自己就起来了。

### 关闭（停止）

`kill -9 {pid}`

![img](../repository/wpsEB38.tmp.jpg) 

## 客户端

> 提示：要想启动当前Redis的客户端，必须先启动当前Redis的服务端

### 启动（单机模式）

切换至Redis安装目录下，执行`src/redis-cli`指令

![img](../repository/wpsEB39.tmp.jpg) 

注：不论是启动redis-server还是启动redis-cli，实际上就是执行redis-server（或redis-cli）文件，不论在哪里启动，只要能定位到该文件即可

### 启动（集群模式）

切换至Redis安装目录下，执行`src/redis-cli -c`指令

![img](../repository/wpsEB3A.tmp.jpg) 

注：不论是启动redis-server还是启动redis-cli，实际上就是执行redis-server（或redis-cli）文件，不论在哪里启动，只要能定位到该文件即可

注：在集群中，Redis允许我们在任何一个节点下通过指定ip与端口连接到集群中的任何一个节点下，如：![img](../repository/wpsEB4B.tmp.jpg) 

### 退出

- 方式一：ctrl+c`

- 方式二：quit`

- 方式三：exit`

示例：

![img](../repository/wpsEB4C.tmp.jpg) 

### 客户端使用示例

![img](../repository/wpsEB4D.tmp.jpg) 

 

 