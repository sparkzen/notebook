# spring-mvc配置访问根路径时重定向至指定路径

- [spring-mvc配置访问根路径时重定向至指定路径](#spring-mvc配置访问根路径时重定向至指定路径)
  - [配置](#配置)

## 配置

> 低版本继承`org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter`，高版本实现`org.springframework.web.servlet.config.annotation.WebMvcConfigurer`

```java
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * (non-javadoc)
 *
 * @author JustryDeng
 * @since 2022/6/24 15:28
 */
@Configuration
public class CustomWebMvcConfigurer implements WebMvcConfigurer {
    
    /**
     * 将请求"/"重定向至 指定路径
     */
    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        // 示例1
        // registry.addViewController("/").setViewName("redirect:/user/list");
        // 示例2
        registry.addViewController("/").setViewName("redirect:/index.html");
    }
}
```

