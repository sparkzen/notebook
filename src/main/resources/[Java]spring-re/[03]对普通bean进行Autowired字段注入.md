# 对普通bean进行Autowired字段注入

- [对普通bean进行Autowired字段注入](#对普通bean进行autowired字段注入)
  - [工具类](#工具类)
  - [使用测试](#使用测试)

## 工具类

```java
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.SmartInitializingSingleton;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * 对普通bean(即:非spring-bean)的autowired注入支持
 *
 * @author JustryDeng
 * @since 2020/7/5 13:20:19
 */
@Slf4j
public abstract class ExtAutowiredInjector {
    
    public ExtAutowiredInjector() {
        Object obj = applyAutowired();
        ExtAutowiredInjector.inject(obj);
    }
    
    /**
     * 对返回的object进行autowired注入处理
     *
     * @return 要进行注入处理的Object
     */
    protected Object applyAutowired() {
        return this;
    }
    
    /**
     * 对目标对象中的被@Auwired标注了的字段进行注入
     *
     * @param obj
     *            目标obj
     */
    public static void inject(Object obj) {
        if (obj == null) {
            log.warn("obj is null, skip inject.");
            return;
        }
        if (ExtAutowiredInjectorProcessor.applicationContext != null) {
            ExtAutowiredInjectorProcessor.applicationContext.publishEvent(new AutowiredInjectEvent(obj));
        } else {
            ExtAutowiredInjectorProcessor.queue.add(obj);
        }
    }
    
    /**
     * Autowired注入支持事件
     *
     * @author JustryDeng
     * @since 2020/7/5 15:02:19
     */
    public static class AutowiredInjectEvent extends ApplicationEvent {
        
        @Getter
        private final Object bean;
        
        /**
         * Create a new ApplicationEvent.
         *
         * @param source
         *         the object on which the event initially occurred (never {@code null})
         */
        public AutowiredInjectEvent(Object source) {
            super(source);
            Objects.requireNonNull(source, "source cannot be null.");
            this.bean = source;
        }
    }
    
    /**
     * {@link ExtAutowiredInjector}处理器
     *
     * @author JustryDeng
     * @since 2020/7/5 13:46:25
     */
    @Slf4j
    @Component
    public static class ExtAutowiredInjectorProcessor implements SmartInitializingSingleton, ApplicationListener<AutowiredInjectEvent> {
        
        static ApplicationContext applicationContext;
        
        static Queue<Object> queue = new ConcurrentLinkedQueue<>();
        
        private final AutowireCapableBeanFactory autowireCapableBeanFactory;
        
        public ExtAutowiredInjectorProcessor(AutowireCapableBeanFactory autowireCapableBeanFactory,
                                             ApplicationContext applicationContext) {
            this.autowireCapableBeanFactory = autowireCapableBeanFactory;
            ExtAutowiredInjectorProcessor.applicationContext = applicationContext;
        }
        
        @Override
        public void onApplicationEvent(@NonNull AutowiredInjectEvent event) {
            Object bean = event.getBean();
            autowireCapableBeanFactory.autowireBean(bean);
            afterSingletonsInstantiated();
        }
        
        @Override
        public void afterSingletonsInstantiated() {
            Object obj = queue.poll();
            while (obj != null) {
                autowireCapableBeanFactory.autowireBean(obj);
                obj = queue.poll();
            }
        }
    }
}
```

## 使用测试

```java
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

/**
 * start class
 *
 * @author JustryDeng
 * @since 2022/4/21 11:41
 */
@SpringBootApplication
public class DemoApplication implements ApplicationRunner {
    
    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }
    
    
    @Override
    public void run(ApplicationArguments args) throws Exception {
        // 启动springboot项目， 观察输入
        
        // 测试一，输出：hello 邓二洋~
        new TestClassOne().who();
        
        
        // 测试二，输出：hello 邓帅~
        TestClassTwo testClassTwo = new TestClassTwo();
        ExtAutowiredInjector.inject(testClassTwo);
        testClassTwo.who();
        // 重复注入也没关系
        //ExtAutowiredInjector.inject(testClassTwo);
        //ExtAutowiredInjector.inject(testClassTwo);
        //ExtAutowiredInjector.inject(testClassTwo);
        //testClassTwo.who();
    }
    
    /**
     * 测试用法一：继承使用，自动完成注入
     */
    public static class TestClassOne extends ExtAutowiredInjector {
        
        @Autowired
        @Qualifier("demoApplication.DengErYang")
        private Mine mine;
        
        public void who() {
            System.err.println(mine.hello());
        }
        
        @Override
        protected Object applyAutowired() {
            return this;
        }
    }
    
    /**
     * 测试用法一：不继承， 直接用{@link ExtAutowiredInjector#inject(Object)}注入
     */
    public static class TestClassTwo {
        
        @Autowired
        @Qualifier("demoApplication.DengShuai")
        private Mine mine;
        
        public void who() {
            System.err.println(mine.hello());
        }
    }
    
    public interface Mine {
        String hello();
    }
    
    @Component(value = "demoApplication.DengErYang")
    public static class DengErYang implements Mine {
        @Override
        public String hello() {
            return "hello 邓二洋~";
        }
    }
    
    @Component(value = "demoApplication.DengShuai")
    public static class DengShuai implements Mine {
        @Override
        public String hello() {
            return "hello 邓帅~";
        }
    }
    
}
```

启动程序，控制台输出：

```bash
  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::                (v2.6.6)

2022-04-21 13:47:39.176  INFO 15784 --- [           main] com.example.demo.DemoApplication         : Starting DemoApplication using Java 1.8.0_291 on DESKTOP-K08MNSB with PID 15784 (E:\working\demo\demo\target\classes started by 邓沙利文 in E:\java\flex-server)
2022-04-21 13:47:39.179  INFO 15784 --- [           main] com.example.demo.DemoApplication         : No active profile set, falling back to 1 default profile: "default"
2022-04-21 13:47:39.480  INFO 15784 --- [           main] com.example.demo.DemoApplication         : Started DemoApplication in 0.509 seconds (JVM running for 0.9)
hello 邓二洋~
hello 邓帅~

Process finished with exit code 0
```

