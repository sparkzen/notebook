# ext-spring-cache扩展@Cacheable缓存注解

- [ext-spring-cache扩展@Cacheable缓存注解](#ext-spring-cache扩展cacheable缓存注解)
  - [介绍](#介绍)
  - [前置条件](#前置条件)
  - [功能特性](#功能特性)
  - [相关配置项](#相关配置项)
  - [使用说明](#使用说明)

## 介绍

ext-spring-cache的 **`@ExtCacheable`** 是对spring的 **`@Cacheable`** 的扩展， **`@ExtCacheable`** 功能更丰富、用起来更舒心

## 前置条件

- spring-boot
- redis
- jdk8+

## 功能特性

1. @ExtCacheable支持指定RedisTemplate操作缓存（对比：@Cacheable不支持指定RedisTemplate）
2. @ExtCacheable支持对具体的key设置过期时间（对比：@Cacheable只支持对命名空间级别设置过期时间）
3. @ExtCacheable支持redis缓存、支持caffeine缓存、支持多级缓存（对比：@Cacheable不全支持）
4. @ExtCacheable对@Cacheable保持兼容
5. ...

## 可用版本

其余版本存在已知bug，请使用下述版本

- 1.5.12.RELEASE.DELTA
- 2.0.0.RELEASE.GAMMA
- 2.1.0.RELEASE.DELTA
- 2.2.0.RELEASE.GAMMA
- 2.3.4.RELEASE.DELTA

## 相关配置项

提示：可详见`com.ideaaedi.extspringcache.constant.ExtCacheConfigPlaceholder`

|                         配置项                          | 默认值 |                             说明                             |
| :-----------------------------------------------------: | :----: | :----------------------------------------------------------: |
|              ext.spring.cache.print-banner              |  true  |             控制是否打印ext-spring-cache banner              |
| ext.spring.cache.redis-caffeine.caffeine-as-first-cache |        | 是否以caffeine作为一级缓存<br />**`true`：** caffeine作为一级缓存,redis作为二级缓存<br />**`false`：** redis作为一级缓存,caffeine作为二级缓存 |
|     ext.spring.cache.redis-caffeine.value-back-fill     |  true  | (若一级缓存没数据，二级缓存有数据), 是否回填二级缓存的数据至一级缓存 |
|   ext.spring.cache.use-default-cache-manager-if-miss    |  true  | 当没有ext管理器可以被总管理器ExtCacheManager管理时，管理器ExtCacheManager是否管理默认的CacheManager |
|     ext.spring.cache.redis.response-spring-context      |  true  | **`true`：** 默认对spring-context中的配置作出响应。  即：在spring-context中若存在相应配置或相关bean，那么会影响所有的Redis。此时，可通过在使用@Redis注解时，显示的指定相关配置来覆盖 spring-context中的配置<br />**`false`：** 默认不对spring-context中的配置作出响应。即：不管spring-context是否存在相应配置或相关bean，都不会影响Redis。此时，在使用@Redis注解时，显示的指定相关配置依然有效 |
|    ext.spring.cache.caffeine.response-spring-context    |  true  | **`true`：** 默认对spring-context中的配置作出响应。  即：在spring-context中若存在相应配置或相关bean，那么会影响所有的Caffeine。此时，可通过在使用@Caffeine注解时，显示的指定相关配置来覆盖 spring-context中的配置<br />**`false`：** 默认不对spring-context中的配置作出响应。即：不管spring-context是否存在相应配置或相关bean，都不会影响Caffeine。此时，在使用@Caffeine注解时，显示的指定相关配置依然有效 |
| ext.spring.cache.redis-caffeine.response-spring-context |  true  | **`true`：** 默认对spring-context中的配置作出响应。  即：在spring-context中若存在相应配置或相关bean，那么会影响所有的Redis、Caffeine。此时，可通过在使用@Redis、@Caffeine注解时，显示的指定相关配置来覆盖 spring-context中的配置<br />**`false`：** 默认不对spring-context中的配置作出响应。即：不管spring-context是否存在相应配置或相关bean，都不会影响Redis、Caffeine。此时，在使用@Redis、@Caffeine注解时，显示的指定相关配置依然有效 |
|                           ……                            |   ……   |                              ……                              |



## 使用说明

- 第一步：引入依赖

  ```xml
  <dependency>
      <groupId>com.idea-aedi</groupId>
      <artifactId>ext-spring-cache</artifactId>
      <version>{选择与spring-boot版本号相近的版本}</version>
  </dependency>
  ```

- 第二步：使用`@EnableExtCache`注解启用ext-spring-cache功能

  ```java
  import com.ideaaedi.extspringcache.EnableExtCache;
  import org.springframework.boot.SpringApplication;
  import org.springframework.boot.autoconfigure.SpringBootApplication;
  
  @EnableExtCache
  @SpringBootApplication
  public class AppApplication {
      
      public static void main(String[] args) {
          SpringApplication.run(AppApplication.class, args);
      }
  }
  ```

- 第三步：使用`@ExtCacheable`注解即可

  - 示例一

    ```java
    @GetMapping("/demo")
    @ExtCacheable(
            cacheNames = "cache-demo", key = "'jd' + #param",
            caffeine = @Caffeine(expireTime = 300, maximumSize = 3, expireStrategy = CaffeineExpireStrategyEnum.EXPIRE_AFTER_ACCESS)
    )
    public Object two(String param) {
        System.err.println("进two了\t" + param);
        return param + ThreadLocalRandom.current().nextInt(100);
    }
    ```

  - 示例二

    ```java
    @GetMapping("/demo")
    @ExtCacheable(cacheNames = "cache-demo", key = "'jd' + #param",
                  redis = @Redis(useRedisTemplate = "redisTemplate",
                                 expireTime = 100,
                                 timeUnit = ChronoUnit.MINUTES,
                                 expireStrategy = RedisExpireStrategyEnum.AUTO
                  )
    )
    public Object two(String param) {
        System.err.println("进two了\t" + param);
        return param + ThreadLocalRandom.current().nextInt(100);
    }
    ```

  - 示例三

    ```java
    @GetMapping("/demo")
    @ExtCacheable(
            cacheNames = "cache-demo", key = "'jd' + #param",
            redis = @Redis(useRedisTemplate = "redisTemplate", expireTime = 600),
            caffeine = @Caffeine(expireTime = 300, maximumSize = 3)
    )
    public Object two(String param) {
        System.err.println("进two了\t" + param);
        return param + ThreadLocalRandom.current().nextInt(100);
    }
    ```