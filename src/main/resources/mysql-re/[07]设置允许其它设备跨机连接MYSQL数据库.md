# 设置允许其它设备跨机连接MYSQL数据库

- [设置允许其它设备跨机连接MYSQL数据库](#设置允许其它设备跨机连接mysql数据库)
  - [Windows下的MySQL](#windows下的mysql)
  - [Linux下的MySQL](#linux下的mysql)
    - [第一步：确保端口开放](#第一步确保端口开放)
  - [第二步：添加权限](#第二步添加权限)
    - [MySQL5.7版本](#mysql57版本)
    - [MySQL8版本](#mysql8版本)

## Windows下的MySQL

**第一步：**在mysql中的mysql库中的tables中的user表中，将Host列改为%

**第二步：**在DriverManager.getConnection中，将localhost改为ip地址

**第三步：**重启服务器

注：如果别人连接时，报2003（10060）错误，那么请关闭windows的防火墙

注：如果是数据连接池，那么第二步应为：将context.xml中配置的资源的路径里的localhsot改为ip地址



## Linux下的MySQL

### 第一步：确保端口开放

> 确保数据库的端口（MySQL默认端口是3306）是对外开放的

```shell
# CentOS7查看所有开放的端口
firewall-cmd --zone=public --list-ports	

# CentOS7开放端口
firewall-cmd --zone=public --add-port=3306/tcp --permanent
# CentOS7重新加载防火墙后，开放/关闭的端口才会生效
firewall-cmd --reload	

# CentOS7关闭端口
firewall-cmd --zone=public --remove-port=3306/tcp --permanent
# CentOS7重新加载防火墙后，开放/关闭的端口才会生效
firewall-cmd --reload
```

![image-20220621234704094](../repository/image-20220621234704094.png)

## 第二步：添加权限

### MySQL5.7版本

```shell
# 在MySQL服务器端登录进去
mysql -hlocalhost -uroot -pdengshuai
# 进入名为mysql的数据库
use mysql;
# 简单查看user表权限信息
select user,host from user;
# 添加权限信息，%代表允许所有ip
grant all privileges on *.* to root@'%' identified by '密码';
# 刷新权限信息，使刚才的权限配置生效
flush privileges;
# 再次简单查看user表权限信息，可知刚才的权限添加进去了
select user,host from user;
```

![image-20220621234834966](../repository/image-20220621234834966.png)

### MySQL8版本

```shell
# 在MySQL服务器端登录进去
mysql -hlocalhost -u'root' -p'deng_shuai!Ge1994';
# 进入名为mysql的数据库
use mysql;
# 查看访问权限信息
select user,host from user;
# 修改访问权限信息
update user set host = '%' where user = 'root' and host='localhost';
# 再次简单查看user表权限信息，可知刚才的权限添加进去了
select user,host from user;
# 授权，%代表允许所有ip。 注意:需要执行两次，第一次会报错，第二次不会报错。
GRANT ALL ON *.* TO 'root'@'%';
GRANT ALL ON *.* TO 'root'@'%';
# 刷新权限信息，使刚才的权限配置生效
flush privileges;
```

![image-20220621234905253](../repository/image-20220621234905253.png)





