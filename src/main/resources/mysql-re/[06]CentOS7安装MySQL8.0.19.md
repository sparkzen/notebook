# CentOS7安装MySQL8.0.19

- [CentOS7安装MySQL8.0.19](#centos7安装mysql8019)
  - [准备工作](#准备工作)
    - [第一步：查看是否有安装过mysql](#第一步查看是否有安装过mysql)
    - [第二步：删除mysql](#第二步删除mysql)
    - [第三步：把所有与mysql相关的目录统统删除](#第三步把所有与mysql相关的目录统统删除)
    - [第四步：删除配置文件](#第四步删除配置文件)
    - [第五步：删除mysql的默认密码](#第五步删除mysql的默认密码)
  - [CentOS7安装MySQL8.0.19](#centos7安装mysql8019-1)
    - [第一步：配置Mysql 8.0安装源](#第一步配置mysql-80安装源)
    - [第二步：安装Mysql8](#第二步安装mysql8)
    - [第三步：启动mysql服务（，并查看一下mysql状态，确保启动成功了）](#第三步启动mysql服务并查看一下mysql状态确保启动成功了)
    - [第四步：查看root临时密码](#第四步查看root临时密码)
    - [第五步：更改密码](#第五步更改密码)
      - [第一小步：登录进MySQL](#第一小步登录进mysql)
      - [第二小步：修改密码](#第二小步修改密码)
      - [第三小步（可选）：查看一下当前的密码验证策略](#第三小步可选查看一下当前的密码验证策略)
      - [第四小步（可选）：降低密码验证强度](#第四小步可选降低密码验证强度)
      - [第五小步（可选）：再次更改密码(，此时可以设置简单密码了)](#第五小步可选再次更改密码此时可以设置简单密码了)
    - [第六步：配置允许远程访问](#第六步配置允许远程访问)
  - [常见问题](#常见问题)
    - [解决mysql客户端连接时报错`authentication plugin 'caching_sha2_password’ cnnot bt loaded...`](#解决mysql客户端连接时报错authentication-plugin-caching_sha2_password-cnnot-bt-loaded)

## 准备工作

> 保证之前的MySQL（如果有的话）已删除干净

### 第一步：查看是否有安装过mysql

```shell
rpm -qa | grep -i mysql
```

![image-20220621232730998](../repository/image-20220621232730998.png)

### 第二步：删除mysql

```shell
yum -y remove MySQL-*
# 视第一步的结果而定，可能执行：yum -y remove mysql*
```

![image-20220621232809543](../repository/image-20220621232809543.png)

注：一般用rpm -e 的命令删除mysql，这样表面上删除了mysql，可是mysql的一些残余程序仍然存在，并且通过第一步的方式也查找不到残余，而yum命令比较强大，可以完全删除mysql（P.S.用rpm删除后再次安装的时候会提示已经安装了，这就是rpm没删除干净的原因）

### 第三步：把所有与mysql相关的目录统统删除

```shell
find / -name mysql
```

![image-20220621232941777](../repository/image-20220621232941777.png)

注：因为本人早就清理干净了MySQL,所以我这里是干净的，如果查出来了文件，那么需要使用rm -rf xxx进行删除

### 第四步：删除配置文件

```shell
rm -rf /etc/my.cnf
```

![image-20220621233031420](../repository/image-20220621233031420.png)

### 第五步：删除mysql的默认密码

```shell
rm -rf /root/.mysql_sercret
```

![image-20220621233100408](../repository/image-20220621233100408.png)

## CentOS7安装MySQL8.0.19

### 第一步：配置Mysql 8.0安装源

```shell
sudo rpm -Uvh https://dev.mysql.com/get/mysql80-community-release-el7-3.noarch.rpm
```

![image-20220621233225666](../repository/image-20220621233225666.png)

### 第二步：安装Mysql8

```shell
sudo yum --enablerepo=mysql80-community install mysql-community-server
```

![1657070255237](../repository/1657070255237.png)

注：文件较大，需要一段时间

注：若这一步失败提示`Public key for mysql-community-icu-data-files-8.0.29-1.el7.x86_64.rpm is not installed`

![image-20220622000349843](../repository/image-20220622000349843.png)

则需要重新获取Mysql的GPG，需执行

```shell
rpm --import https://repo.mysql.com/RPM-GPG-KEY-mysql-2022
# 然后再重新执行此步骤
```

### 第三步：启动mysql服务（，并查看一下mysql状态，确保启动成功了）

```shell
#启动mysql服务
sudo service mysqld start

#查看mysql服务状态
service mysqld status
```

![image-20220621233344154](../repository/image-20220621233344154.png)

### 第四步：查看root临时密码

>  提示：安装完mysql之后，会生成一个临时的密码让root用户登录

```shell
grep "A temporary password" /var/log/mysqld.log
```

![image-20220621233431279](../repository/image-20220621233431279.png)

注：之所以有多个，是因为多出来的那个，是卸载之前版本的MySQL时，没有删除/var/log/mysqld.log文件的缘故

### 第五步：更改密码

#### 第一小步：登录进MySQL

```shell
mysql -uroot -p
```

![image-20220621233547372](../repository/image-20220621233547372.png)

#### 第二小步：修改密码

```shell
ALTER USER 'root'@'localhost' IDENTIFIED BY 'deng_shuai!Ge1994';
```

![image-20220621233622138](../repository/image-20220621233622138.png)

注：密码不能太简单，否者会报：

![image-20220621233640091](../repository/image-20220621233640091.png)

#### 第三小步（可选）：查看一下当前的密码验证策略

```shell
SHOW VARIABLES LIKE 'validate_password.%';
```

![image-20220621233723795](../repository/image-20220621233723795.png)

> 策略说明
>
> - validate_password.length：密码的最小长度，默认是8
>
> - validate_password.policy：验证密码的复杂程度，我们把它改成0
>
>   注：0表示复杂度最低
>
>   注：更多关于policy的介绍可见https://blog.csdn.net/wltsysterm/article/details/79649484
>
> - validate_password.check_user_name：用户名检查，用户名和密码不能相同
>
>   注：ON表示开，OFF表示关

#### 第四小步（可选）：降低密码验证强度

```shell
set global validate_password.length=6;
set global validate_password.policy=0;
set global validate_password.check_user_name=off;
```

![image-20220621233932220](../repository/image-20220621233932220.png)



#### 第五小步（可选）：再次更改密码(，此时可以设置简单密码了)

```shell
SHOW VARIABLES LIKE 'validate_password.%';
```

![image-20220621234015184](../repository/image-20220621234015184.png)

### 第六步：配置允许远程访问

> 见 [设置允许其它设备跨机连接MYSQL数据库](./[07]设置允许其它设备跨机连接MYSQL数据库.md)

## 常见问题

### 解决mysql客户端连接时报错`authentication plugin 'caching_sha2_password’ cnnot bt loaded...`

```shell
# 登录进mysql。建议:不要接着上面的第六步直接操作，建议先退出MySQL，然后再重新登录进去。
mysql -hlocalhost -u'root' -p'deng_shuai!Ge1994';
# 切换数据库
use mysql;
# 修改密码加密方式
ALTER USER root@'%' IDENTIFIED WITH mysql_native_password BY 'deng_shuai!Ge1994';
```

![image-20220621234219023](../repository/image-20220621234219023.png)

