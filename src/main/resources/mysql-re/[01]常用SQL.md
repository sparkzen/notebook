# 常用SQL

- [常用SQL](#常用sql)
  - [时间相关](#时间相关)
  	- [返回date的月份中日期，在1到31范围内](#返回date的月份中日期在1到31范围内)
  	- [返回date在一年中的日数, 在1到366范围内。](#返回date在一年中的日数-在1到366范围内)
  	- [返回date的月份，范围1到12](#返回date的月份范围1到12)
  	- [返回date的星期名字](#返回date的星期名字)
  	- [返回date的月份名字](#返回date的月份名字)
  	- [返回date一年中的季度，范围1到4](#返回date一年中的季度范围1到4)
  	- [返回date的年份，范围在1000到9999](#返回date的年份范围在1000到9999)
  	- [返回time的小时，范围是0到23](#返回time的小时范围是0到23)
  	- [返回time的分钟，范围是0到59](#返回time的分钟范围是0到59)
  	- [返回time的秒数，范围是0到59](#返回time的秒数范围是0到59)
  	- [日期时间增、减](#日期时间增减)
  	- [根据format格式化date值](#根据format格式化date值)
  	- [获取取系统时间](#获取取系统时间)
  	- [计算两个日期的时间差](#计算两个日期的时间差)
  	- [时间戳(秒级) 与 日期格式的字符串 互转](#时间戳秒级-与-日期格式的字符串-互转)
  - [流程控制相关](#流程控制相关)
  	- [IFNULL(expr1, expr2)](#ifnullexpr1-expr2)
  	- [NULLIF(expr1, expr2)](#nullifexpr1-expr2)
  	- [IF(expr1, expr2, expr3)](#ifexpr1-expr2-expr3)
  	- [CASE-WHEN-THEN，CASE后有相关列](#case-when-thencase后有相关列)
  	- [CASE-WHEN-THEN，CASE后没有相关列，直接在WHERE后体现](#case-when-thencase后没有相关列直接在where后体现)
  - [字符串相关](#字符串相关)
  	- [拼接字符串](#拼接字符串)
  	- [根据指定的符号，拼接字符串](#根据指定的符号拼接字符串)
  	- [对GROUP BY分组内容进行拼接](#对group-by分组内容进行拼接)
  	- [字符长度、字节长度](#字符长度字节长度)
  	- [查找子串](#查找子串)
  	- [从指定位置开始，查找子串](#从指定位置开始查找子串)
  	- [填充字符串至指定长度](#填充字符串至指定长度)
  	- [返回指定长度的字符串](#返回指定长度的字符串)
  	- [删除字符串两侧的空格](#删除字符串两侧的空格)
  	- [返回由N个空格字符组成的一个字符串](#返回由n个空格字符组成的一个字符串)
  	- [字符串替换](#字符串替换)
  	- [重复拼接字符串](#重复拼接字符串)
  	- [颠倒字符串](#颠倒字符串)
  	- [返回众多字符串中指定位置的字符串](#返回众多字符串中指定位置的字符串)
  	- [返回众多字符串中指定字符串（第一次出现）的位置](#返回众多字符串中指定字符串第一次出现的位置)
  	- [大小写转换](#大小写转换)
  	- [字符串转换为数字](#字符串转换为数字)
  	- [截取字符串](#截取字符串)
  	- [查询某字符串中，指定字符串的个数](#查询某字符串中指定字符串的个数)
  	- [FIND_IN_SET](#find_in_set)
  - [数字](#数字)
  	- [格式化](#格式化)
  	- [数字转字符串](#数字转字符串)

---

## 时间相关

### 返回date的月份中日期，在1到31范围内

```sql
SELECT
	DAYOFMONTH( '1998-02-03' );
```

> 3

### 返回date在一年中的日数, 在1到366范围内。 

```sql
SELECT
	DAYOFYEAR( '1998-02-03' );
```

> 34

### 返回date的月份，范围1到12

```sql
SELECT MONTH('1998-12-03');
```

> 12

### 返回date的星期名字

```sql
SELECT DAYNAME( "1998-02-05" );
```

> Thursday

### 返回date的月份名字

```sql
SELECT MONTHNAME( "1998-02-05" );
```

> February

### 返回date一年中的季度，范围1到4

```sql
SELECT QUARTER( "1998-04-05" );
```

> 2

### 返回date的年份，范围在1000到9999

```sql
SELECT YEAR( "1998-04-05" );
```

> 1998

### 返回time的小时，范围是0到23

```sql
SELECT HOUR('10:05:03');
```

> 10

### 返回time的分钟，范围是0到59

```sql
SELECT MINUTE('10:05:03');
```

> 5

### 返回time的秒数，范围是0到59

```sql
SELECT SECOND('10:05:03');
```

> 3

### 日期时间增、减

> 格式：
>
> - `DATE_ADD(date,INTERVAL expr type)`
> - `ADDDATE(date,INTERVAL expr type)`
> - `DATE_SUB(date,INTERVAL expr type)`
> - `SUBDATE(date,INTERVAL expr type)`
>
> 其中type取值：
>
> - SECOND 秒
> - MINUTE 分
> - HOUR 时
> - DAY 天
> - MONTH 月
> - YEAR 年
> - ......
>
> 注：若增时，interval是负数，则为减；若减时，interval是负数，则为增

```sql
SELECT DATE_ADD('2021-08-01',INTERVAL 1 MONTH);
-- 或者
SELECT adddate('2021-08-01',INTERVAL 1 MONTH);
```

> 2021-09-01

```sql
SELECT DATE_SUB('2021-08-01',INTERVAL 1 MONTH);
-- 或者
SELECT SUBDATE('2021-08-01',INTERVAL 1 MONTH);
```

> 2021-07-01

### 根据format格式化date值

> 格式：
>
> `DATE_FORMAT(date, format)`
>
> 常用format：
>
> - `%Y-%m-%d %H:%i:%s`
>
>   > 形如：2021-08-01 17:37:25
>
> - `%Y-%m-%d`
>
>   > 形如：2021-08-01
>
> format中的占位符：
>
> - %W 星期名字(Sunday……Saturday)
> - %D 有英语前缀的月份的日期(1st, 2nd, 3rd, 等等。） 
> - %Y 年, 数字, 4 位
> - %y 年, 数字, 2 位
> - %a 缩写的星期名字(Sun……Sat) 
> - %d 月份中的天数, 数字(00……31)
> - %e 月份中的天数, 数字(0……31)
> - %m 月, 数字(01……12)
> - %c 月, 数字(1……12)
> - %b 缩写的月份名字(Jan……Dec)
> - %j 一年中的天数(001……366)
> - %H 小时(00……23)
> - %k 小时(0……23)
> - %h 小时(01……12)
> - %I 小时(01……12) 注：
> - %l 小时(1……12)
> - %i 分钟, 数字(00……59)
> - %r 时间,12 小时(hh:mm:ss [AP]M)
> - %T 时间,24 小时(hh:mm:ss)
> - %S 秒(00……59)
> - %s 秒(00……59)
> - %p AM或PM
> - %w 一个星期中的天数(0=Sunday ……6=Saturday）
> - %U 星期(0……52), 这里星期天是星期的第一天
> - %u 星期(0……52), 这里星期一是星期的第一天
> - %% 一个文字"%"

```sql
SELECT DATE_FORMAT('2021-08-01 17:37:25.857', '%Y-%m-%d %H:%i:%s');
```

> 2021-08-01 17:37:25

### 获取取系统时间

```sql
SELECT now();
```

> 2021-08-01 17:47:12

```sql
SELECT sysdate();
```

> 2021-08-01 17:47:17

```sql
SELECT curdate();
```

> 2021-08-01

```sql
SELECT curtime();
```

> 17:49:01

### 计算两个日期的时间差

> 格式：
>
> `TIMESTAMPDIFF(interval, datetime_expr1, datetime_expr2)`
>
> > `datetime_expr2` - `datetime_expr1` =  时间差值
>
> `interval`可选值：
>
> - SECOND 秒
> - MINUTE 分
> - HOUR 时
> - DAY 天
> - MONTH 月
> - YEAR 年

```sql
SELECT TIMESTAMPDIFF(MONTH,'2009-10-01','2009-12-01');
```

> 2

### 时间戳(秒级) 与 日期格式的字符串 互转

> 获取当前时间戳：
>
> - SELECT UNIX_TIMESTAMP()
> - SELECT UNIX_TIMESTAMP(NOW())
>
> 日期转换为时间戳：
>
> - SELECT UNIX_TIMESTAMP('2018-05-08 08:26:30')
>
> 时间戳转换为日期：
>
> - SELECT FROM_UNIXTIME('1525740032')
> - SELECT FROM_UNIXTIME(1525740032)
> - SELECT FROM_UNIXTIME('1525740032', '%Y-%m-%d %H:%i:%s')
> - SELECT FROM_UNIXTIME(1525740032, '%Y-%m-%d %H:%i:%s')
>
> > 注：FROM_UNIXTIME函数，转换后得到的时间，是当前MYSQ设置的时区的时间。若MYSQ设置的时区不对，那么FROM_UNIXTIME得到的结果可能不对。这时，我们有两个办法：
> >
> > 1. 修改MySQL的时区， 保证`select now()`结果为我们的当前时间即可
> > 2. 使用`CONVERT_TZ`函数作时区转换，如：`CONVERT_TZ(FROM_UNIXTIME(1566200972, '%Y-%m-%d %H:%i:%s'),'UTC','+8:00')`或者`CONVERT_TZ('2018-05-08 08:26:30','UTC','+8:00')`

## 流程控制相关

### IFNULL(expr1, expr2) 

> 如果 expr1 不为 NULL ，则返回 expr1，否则返回 expr2

### NULLIF(expr1, expr2)

>  如果 expr1 = expr2 为真，返回 NULL，否则返回 expr1

### IF(expr1, expr2, expr3)

> 如果 expr1 为真(expr1 <> 0 以及 expr1 <> NULL)，那么返回 expr2，否则返回 expr3

> 注：如果查询不出该行的话，那么IF语句失效，这时可以考略使用`case-when-then`，如：
>
> ```sql
> SELECT
> CASE WHEN
> 		( SELECT count( 1 ) FROM children WHERE id = 1 ) = 0 THEN
> 			"" ELSE IFNULL( motto, "" ) 
> 			END AS '座右铭' 
> 	FROM
> 		children 
> WHERE
> 	id = 1;
> ```

### CASE-WHEN-THEN，CASE后有相关列

```sql
SELECT
CASE id 
		WHEN 1 THEN '第一行' 
		WHEN 2 THEN '第二行' 
		ELSE  motto 
	END AS '座右铭' 
FROM
	`children`
```

### CASE-WHEN-THEN，CASE后没有相关列，直接在WHERE后体现

```sql
SELECT
CASE  WHEN id = 1 THEN '第一行' 
		  WHEN id = 2 THEN '第二行' 
			ELSE motto 
		   END AS '座右铭' 
FROM
	`children`
```

## 字符串相关

### 拼接字符串

> 格式：
>
> `CONCAT(str1, str2, ...) `
>
> 返回来自于参数连结的字符串。如果任何参数是NULL，返回NULL。
>
> 注：当有数字参数时，该数字参数会被变换为等价的字符串形式

```sql
SELECT CONCAT('My','S','QL');
```

> MySQL

```sql
SELECT CONCAT('My',NULL,'QL');
```

> （Null）

### 根据指定的符号，拼接字符串

> 格式：
>
> `concat_ws(SEPARATOR, str1, str2, ...)`
>
> 注：第一个参数指定分隔符，其余参数同`CONCAT(str1, str2, ...) `

```sql
SELECT concat_ws('-', 'My','S','QL');
```

> My-S-QL

### 对GROUP BY分组内容进行拼接

> 格式
>
> `group_concat(DISTINCT {要连接的字段} [ORDER BY {排序字段} ASC/DESC] [SEPARATOR '{分隔符}'])`
>
> 注：不指定分隔符的话，默认以逗号分割
>
> >  注：MySQL对GROUP_CONCAT连接字符串的长度有限制，如果超过了该限制，那么会自动截断丢弃超出的那一部分，此时我们可以：
> >
> > - 通过`SET [SESSION | GLOBAL] group_concat_max_len = xxx`来设置长度上限，-1表示无限制
> > - 通过在MySQL的配置文件中设置`group_concat_max_len = -1`设置长度上限，-1表示无限制

```sql
SELECT GROUP_CONCAT(DISTINCT em.name ORDER BY em.id DESC SEPARATOR '+') FROM `employee` em GROUP BY em.age
```

> 张三+王五+李四

### 字符长度、字节长度

> - CHAR_LENGTH(str)：返回字符长度
> - LENGTH(str)：返回字节长度

```SQL
SELECT CHAR_LENGTH('邓沙利文')
```

> 4

```sql
-- 一个汉字占三个字节
SELECT LENGTH('邓沙利文')
```

> 12

### 查找子串

> 返回子串substr在字符串str第一个出现的位置，如果str中不存在substr，则返回0
>
> 格式：
>
> - `LOCATE(substr,str)`
> - `POSITION(substr IN str)`
> - `INSTR(str,substr)`

```sql
SELECT LOCATE('bar','foobarbar')
```

> 4

```sql
SELECT POSITION('bar' IN 'foobar')
```

> 4

```sql
SELECT INSTR('foobarbar','bar')
```

> 4

### 从指定位置开始，查找子串

> 返回子串substr在字符串str第一个出现的位置，从位置pos开始。如果substr不是在str里面，返回0
>
> 格式：
>
> `LOCATE(substr, str, pos) `

```sql
SELECT LOCATE('bar','foobarbar',5)
```

> 7

```sql
SELECT LOCATE('bar','foobarbar',4)
```

> 4

### 填充字符串至指定长度

> - `LPAD(str,len,padstr)`：用字符串padstr填补到str左侧，直到str是len个字符长
> - `RPAD(str,len,padstr)`：用字符串padstr填补到str右侧，直到str是len个字符长

```sql
SELECT LPAD('hi',4,'*')
```

> **hi

```sql
SELECT RPAD('hi',4,'*')
```

> hi**

### 返回指定长度的字符串

> - `LEFT(str,len)`：返回字符串str的最左面len个字符
> - `RIGHT(str,len)`：返回字符串str的最右面len个字符
> - `SUBSTRING(str,pos,len) `：从字符串str返回一个len个字符的子串，从位置pos开始

```sql
SELECT LEFT ('foobarbar',5)
```

> fooba

```sql
SELECT RIGHT ('foobarbar',4)
```

> rbar

```sql
SELECT SUBSTRING('foobarbar',5,3)
```

> arb

### 删除字符串两侧的空格

> - `LTRIM(str)`：去除str左侧空格
> - `RTRIM(str)`：去除str右侧空格
> - `TRIM(str)`：去除str两侧空格

```sql
SELECT LTRIM('  JustryDeng   ')
```

> JustryDeng   
>
> 注：上一行JustryDeng右侧还有空格的

```sql
SELECT RTRIM('  JustryDeng   ')
```

>   JustryDeng
>
> 注：上一行JustryDeng左侧还有空格的

```SQL
SELECT TRIM('  JustryDeng   ')
```

> JustryDeng

### 返回由N个空格字符组成的一个字符串

```SQL
SELECT SPACE(6)
```

> ​      
>
> 注：上一行是6个空格

### 字符串替换

> - `REPLACE(str,from_str,to_str)`：替换字符串str中的from_str为to_str
> - `INSERT(str,pos,len,newstr)`：替换字符串str中，从位置pos开始的len个长度的字符串为newstr

```sql
SELECT REPLACE ('www.mysql.com','www','哈哈')
```

> 哈哈.mysql.com

```sql
SELECT INSERT ('abcdefg',3,2,'咿呀咔咔')
```

> ab咿呀咔咔efg

### 重复拼接字符串

> `REPEAT(str,count)`：返回由字符串str重复count次组成的一个字符串
>
> 注：如果count <= 0，返回一个空字符串。如果str或count是NULL，返回NULL。

```sql
SELECT REPEAT ('MySQL',3)
```

> MySQLMySQLMySQL

### 颠倒字符串

```sql
SELECT REVERSE('abc')
```

> cba

### 返回众多字符串中指定位置的字符串

> `ELT(N,str1,str2,str3,...) `：如果N= 1，返回str1，如果N= 2，返回str2，等等
>
> 注：如果N小于1或大于参数个数，返回NULL

```sql
SELECT ELT(1,'ej','Heja','hej','foo')
```

> ej

### 返回众多字符串中指定字符串（第一次出现）的位置

> `FIELD(str,str1,str2,str3,...)`：返回str在str1, str2, str3, ...清单的索引
>
> 注：返回str1……等字符串中，与str字符串一样的字符串的位置索引。如果str没找到，返回0

```sql
SELECT FIELD('ej','Hej','ej','Heja','ej','hej','foo')
```

> 2

### 大小写转换

> - `LOWER(str)`：将str转换为小写
> - `UPPER(str)`：将str转换为大写

### 字符串转换为数字

> - SELECT CAST('123' AS SIGNED)
> - SELECT CONVERT('123',SIGNED)
> - SELECT '123'+0

### 截取字符串

> - `left(str,len)`：从左开始截取指定长度的字符串
> - `right(str,len)`：从右开始截取指定长度的字符串
> - `substring(str,index)`：当index>0从左边第index个开始往右截取直到结束；当index<0从右边第index个开始往右截取直到结束；当index=0返回空字符串
> - `substring(str,index,len)`：在`substring(str,index)`的基础上，指定了截取的长度
> - `substring_index(str,delim,index)`：
>   - str是要截取的字符串
>   - delim是分隔符
>   - index是从哪里开始截取，怎么截取
>     - 为0则返回空字符串
>     - 为正数，则是截取(从左往右数)第index个delim左边的字符串
>     - 为负数，则是截取(从右往左数)第index个delim右边的字符串
>     - 当index个数超出时，返回全部字符串
>     - 当str中不存在delim，index为0时，返回空字符串；index不为0时，返回全量字符串

```sql
SELECT substring_index("我是ABC一只小ABC小小小鸟！ABC嗷~嗷！！",'ABC',3)
```

> 我是ABC一只小ABC小小小鸟！

```sql
select substring_index("我是ABC一只小ABC小小小鸟！ABC嗷~嗷！！", 'ABC', -3)
```

> 一只小ABC小小小鸟！ABC嗷~嗷！！

### 查询某字符串中，指定字符串的个数

```sql
SELECT(
	CHAR_LENGTH( '哈哈,B,C' )- CHAR_LENGTH(REPLACE ( '哈哈,B,C', ',', '' ))
)
```

> 2

### FIND_IN_SET

>  `FIND_IN_SET(str,strlist)`：返回strlist中(从左到右)第一个str元素的位置
>
> 注：其中str形如'xxx'， strlist形如'xxx,xxx,xxx'
>
> 注：当str或strlist为null时，直接返回null
>
> 注：当strlist中找不到str时，返回0

```sql
SELECT FIND_IN_SET('a','a,b,c,a')
```

> 1

## 数字

### 格式化

> 格式：
>
> `FOMRAT(要格式化的数字, 四舍五入后保留的小数位数, 可选的local参数)`

```sql
 SELECT FORMAT(1234567, 2)
```

> 1,234,567.00

```sql
SELECT FORMAT(1234.5, 2)
```

> 1,234.50

```sql
SELECT FORMAT(1234.555, 2)
```

> 1,234.56

```sql
SELECT FORMAT(1234.554, 2)
```

> 1,234.55

```sql
SELECT FORMAT(-1234.554, 2)
```

> -1,234.55

```sql
SELECT FORMAT(-1234.555, 2)
```

> -1,234.56

```sql
-- 以,分割千位，以.分割小数和整数
SELECT FORMAT(1234567,2, 'en_US')
```

> 1,234,567.00

```sql
-- 以.分割千位，以,分割小数和整数
SELECT FORMAT(1234567,2, 'de_DE')
```

> 1.234.567,00

### 数字转字符串

```sql
SELECT CONCAT("",123)
```

> 123
>
> 注：这是字符串123