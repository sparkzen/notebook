# 表&列常用SQL

- [表&列常用SQL](#表列常用sql)
	- [查询表结构](#查询表结构)
	- [查询所有表的表名](#查询所有表的表名)
	- [查询表的列名](#查询表的列名)
	- [查询表DDL](#查询表ddl)
	- [修改表名](#修改表名)
	- [删除表](#删除表)
	- [新增列](#新增列)
	- [删除列](#删除列)
	- [修改列](#修改列)

---

## 查询表结构

```sql
SELECT
	COLUMN_NAME AS '列名',
	COLUMN_TYPE AS '类型',
	COLUMN_DEFAULT AS '默认值',
	IS_NULLABLE AS '是否可空',
	COLUMN_COMMENT AS '字段注释',
	COLUMN_KEY AS '索引类型',
	EXTRA AS '附加信息' 
FROM
	INFORMATION_SCHEMA.COLUMNS 
WHERE
	table_schema = '{库名}' 
	AND table_name = '{表名}'
```

> ![image-20210731014312392](../repository/image-20210731014312392.png)

## 查询所有表的表名

```sql
SELECT
	table_name 
FROM
	information_schema.TABLES 
WHERE
	table_schema = '{库名}';
```

## 查询表的列名

- 一列展示

```sql
SELECT
	column_name 
FROM
	information_schema.COLUMNS 
WHERE
	table_schema = '{库名}' 
	AND table_name = '{表名}';
```

- 一行展示

```sql
SELECT
	GROUP_CONCAT( column_name ) 
FROM
	information_schema.COLUMNS 
WHERE
	table_schema = '{库名}' 
	AND table_name = '{表名}'
GROUP BY
	1 = 1;
```

## 查询表DDL

```sql
SHOW CREATE TABLE {表名};
```

## 修改表名

```sql
ALTER TABLE {旧表名} RENAME {新表名};
```

## 删除表

```sql
DROP TABLE {表名};
```

## 新增列

> `ALTER TABLE {表名} ADD {列名} {数据类型} {相关约束} COMMENT '{注释}' {位置};`

示例

```sql
ALTER TABLE employee_t1 ADD abc1 VARCHAR (100);
ALTER TABLE employee_t1 ADD abc2 VARCHAR (100) DEFAULT NULL;
ALTER TABLE employee_t1 ADD abc3 VARCHAR (100) DEFAULT 'abc' NOT NULL;
ALTER TABLE employee_t1 ADD abc4 VARCHAR (100) DEFAULT 'abc' NOT NULL COMMENT '我是注释' FIRST;
ALTER TABLE employee_t1 ADD abc5 VARCHAR (100) DEFAULT 'abc' NOT NULL COMMENT '我是注释' AFTER age;

ALTER TABLE employee_t1 ADD id INT UNSIGNED NOT NULL auto_increment PRIMARY KEY COMMENT '我是注释'  FIRST;
```

## 删除列

```sql
ALTER TABLE {表名} DROP {列名};
```

## 修改列

- 修改列属性

```sql
ALTER TABLE employee_t1 MODIFY abc11 VARCHAR (100);
ALTER TABLE employee_t1 MODIFY abc22 VARCHAR (100) DEFAULT NULL;
ALTER TABLE employee_t1 MODIFY abc33 VARCHAR (100) DEFAULT 'abc' NOT NULL;
ALTER TABLE employee_t1 MODIFY abc44 VARCHAR (100) DEFAULT 'abc' NOT NULL COMMENT '我是注释' FIRST;
ALTER TABLE employee_t1 MODIFY abc55 VARCHAR (100) DEFAULT 'abc' NOT NULL COMMENT '我是注释' AFTER age;
```

- 修改列名和属性

提示：修改列名时，最好将列行相关属性也指定上，若不指定，那么原列对应的属性是会被重置的。

```sql
ALTER TABLE employee_t1 CHANGE abc1 abc11 VARCHAR (100);
ALTER TABLE employee_t1 CHANGE abc2 abc22 VARCHAR (100) DEFAULT NULL;
ALTER TABLE employee_t1 CHANGE abc3 abc33 VARCHAR (100) DEFAULT 'abc' NOT NULL;
ALTER TABLE employee_t1 CHANGE abc4 abc44 VARCHAR (100) DEFAULT 'abc' NOT NULL COMMENT '我是注释' FIRST;
ALTER TABLE employee_t1 CHANGE abc5 abc55 VARCHAR (100) DEFAULT 'abc' NOT NULL COMMENT '我是注释' AFTER age; 
```

