# mysqldump语法

- [mysqldump语法](#mysqldump语法)
  - [占位符统一说明](#占位符统一说明)
  - [导出（备份）库](#导出备份库)
    - [普通导出](#普通导出)
    - [多数据库普通导出](#多数据库普通导出)
    - [gzip导出](#gzip导出)
    - [多数据库gzip导出](#多数据库gzip导出)
  - [导出（备份）表](#导出备份表)
    - [普通导出](#普通导出-1)
    - [gzip导出](#gzip导出-1)
  - [导入库](#导入库)
    - [注意事项](#注意事项)
    - [普通导入](#普通导入)
    - [gzip导入](#gzip导入)

## 占位符统一说明

> 占位符为{xxx}，其中xxx即未对应的key，下面对本文中涉及到的占位符key进行统一说明

- hostname：mysql主机地址

  注：当需要备份远程主机上的mysql数据到本地机器上时，可以使用这个参数

- username：用户名

- password：密码

- dbname：数据库名

- tablename：数据表名

- backupFilepath：备份出来的文件

  注：可以是绝对路径，也可以是相对路径

## 导出（备份）库

### 普通导出

- 语法

  ```shell
  mysqldump [--set-gtid-purged=off] [--skip-add-drop-table] [--no-data] [--all-databases] [-h{hostname}] -u{username} -p{password} {dbname} > {backupFilepath}
  ```

  `--set-gtid-purged=off`的作用：不将全局事务信息导出。若将其导出，那么在导入的时候，就需要处理一下才行，否则会导入报错

  `--skip-add-drop-table`的作用：mysqldump在导出时，会默认携带drop表的语句，如果不需要drop表，那就可以加上此参数

  `--no-data`的作用：仅包含结构，不包含数据

  `--all-databases`的作用：备份所有数据库；当指定此参数后，后面就不需要指定数据库或者表了，直接形如`mysqldump [--skip-add-drop-table] [--no-data] [--all-databases] [-h{hostname}] -u{username} -p{password}  [| gzip] > {backupFilepath}`

- 示例

  ```todo
  mysqldump --set-gtid-purged=off -uroot -ppwd123 xxl_job > xxl_job.bk
  
  mysqldump --set-gtid-purged=off -h10.8.0.101 -uroot -ppwd123 xxl_job > xxl_job.bk
  
  mysqldump --set-gtid-purged=off --skip-add-drop-table -uroot -ppwd123 xxl_job > xxl_job.bk
  
  mysqldump --set-gtid-purged=off --no-data -uroot -ppwd123 xxl_job > xxl_job.bk
  
  mysqldump --set-gtid-purged=off --all-databases -uroot -ppwd123 > db-all.bk
  ```

### 多数据库普通导出

- 语法

  ```shell
  mysqldump [--set-gtid-purged=off] [--skip-add-drop-table] [--no-data] [--all-databases] [-h{hostname}] -u{username} -p{password} --databases {dbname1} [{dbname2} {dbname3}...] > {backupFilepath}
  ```

  `--set-gtid-purged=off`的作用：不将全局事务信息导出。若将其导出，那么在导入的时候，就需要处理一下才行，否则会导入报错

  `--skip-add-drop-table`的作用：mysqldump在导出时，会默认携带drop表的语句，如果不需要drop表，那就可以加上此参数

  `--no-data`的作用：仅包含结构，不包含数据

  `--all-databases`的作用：备份所有数据库；当指定此参数后，后面就不需要指定数据库或者表了，直接形如`mysqldump [--skip-add-drop-table] [--no-data] [--all-databases] [-h{hostname}] -u{username} -p{password}  [| gzip] > {backupFilepath}`

- 示例

  ```todo
  mysqldump --set-gtid-purged=off -uroot -ppwd123 --databases xxl_job test > multi_db.bk
  
  mysqldump --set-gtid-purged=off -h10.8.0.101 -uroot -ppwd123 --databases xxl_job test > multi_db.bk
  
  mysqldump --set-gtid-purged=off --skip-add-drop-table -uroot -ppwd123 --databases xxl_job test > multi_db.bk
  
  mysqldump --set-gtid-purged=off --no-data -uroot -ppwd123 --databases xxl_job test > multi_db.bk
  
  mysqldump --set-gtid-purged=off --all-databases -uroot -ppwd123 > db-all.bk
  ```

### gzip导出

- 语法

  ```shell
  mysqldump [--set-gtid-purged=off] [--skip-add-drop-table] [--no-data] [--all-databases] [-h{hostname}] -u{username} -p{password} {dbname} | gzip > {backupFilepath}
  ```

  `--set-gtid-purged=off`的作用：不将全局事务信息导出。若将其导出，那么在导入的时候，就需要处理一下才行，否则会导入报错

  `--skip-add-drop-table`的作用：mysqldump在导出时，会默认携带drop表的语句，如果不需要drop表，那就可以加上此参数

  `--no-data`的作用：仅包含结构，不包含数据

  `--all-databases`的作用：备份所有数据库；当指定此参数后，后面就不需要指定数据库或者表了，直接形如`mysqldump [--skip-add-drop-table] [--no-data] [--all-databases] [-h{hostname}] -u{username} -p{password}  [| gzip] > {backupFilepath}`

- 示例

  ```todo
  mysqldump --set-gtid-purged=off -uroot -ppwd123 xxl_job | gzip > db.bk.gz
  
  mysqldump --set-gtid-purged=off -h10.8.0.101 -uroot -ppwd123 xxl_job | gzip > db.bk.gz
  
  mysqldump --set-gtid-purged=off --skip-add-drop-table -uroot -ppwd123 xxl_job | gzip > db.bk.gz
  
  mysqldump --set-gtid-purged=off --no-data -uroot -ppwd123 xxl_job | gzip > db.bk.gz
  
  mysqldump --set-gtid-purged=off --all-databases -uroot -ppwd123 | gzip > db-all.bk.gz
  ```

### 多数据库gzip导出

- 语法

  ```shell
  mysqldump [--set-gtid-purged=off] [--skip-add-drop-table] [--no-data] [--all-databases] [-h{hostname}] -u{username} -p{password} --databases {dbname1} [{dbname2} {dbname3}...] | gzip > {backupFilepath}
  ```

  `--set-gtid-purged=off`的作用：不将全局事务信息导出。若将其导出，那么在导入的时候，就需要处理一下才行，否则会导入报错

  `--skip-add-drop-table`的作用：mysqldump在导出时，会默认携带drop表的语句，如果不需要drop表，那就可以加上此参数

  `--no-data`的作用：仅包含结构，不包含数据

  `--all-databases`的作用：备份所有数据库；当指定此参数后，后面就不需要指定数据库或者表了，直接形如`mysqldump [--skip-add-drop-table] [--no-data] [--all-databases] [-h{hostname}] -u{username} -p{password}  [| gzip] > {backupFilepath}`

- 示例

  ```todo
  mysqldump --set-gtid-purged=off -uroot -ppwd123 --databases xxl_job test | gzip > multi_db.bk.gz
  
  mysqldump --set-gtid-purged=off -h10.8.0.101 -uroot -ppwd123 --databases xxl_job test | gzip > multi_db.bk.gz
  
  mysqldump --set-gtid-purged=off --skip-add-drop-table -uroot -ppwd123 --databases xxl_job test | gzip > multi_db.bk.gz
  
  mysqldump --set-gtid-purged=off --no-data -uroot -ppwd123 --databases xxl_job test | gzip > multi_db.bk.gz
  
  mysqldump --set-gtid-purged=off --all-databases -uroot -ppwd123 | gzip > db-all.bk.gz
  ```

## 导出（备份）表

### 普通导出

- 语法

  ```shell
  mysqldump [--set-gtid-purged=off] [--skip-add-drop-table] [--no-data] [-h{hostname}] -u{username} -p{password} {dbname} {tablename1} [{tablename2} {tablename3}...] > {backupFilepath}
  ```

  `--set-gtid-purged=off`的作用：不将全局事务信息导出。若将其导出，那么在导入的时候，就需要处理一下才行，否则会导入报错

  `--skip-add-drop-table`的作用：mysqldump在导出时，会默认携带drop表的语句，如果不需要drop表，那就可以加上此参数

  `--no-data`的作用：仅包含结构，不包含数据

- 示例

  ```todo
  mysqldump --set-gtid-purged=off -uroot -ppwd123 xxl_job xxl_job_user xxl_job_group > xxl_job-tables.bk
  
  mysqldump --set-gtid-purged=off -h10.8.0.101 -uroot -ppwd123 xxl_job xxl_job_user xxl_job_group >  xxl_job-tables.bk
  
  mysqldump --set-gtid-purged=off --skip-add-drop-table -uroot -ppwd123 xxl_job xxl_job_user xxl_job_group >  xxl_job-tables.bk
  
  mysqldump --set-gtid-purged=off --no-data -uroot -ppwd123 xxl_job xxl_job_user xxl_job_group >  xxl_job-tables.bk
  ```

### gzip导出

- 语法

  ```shell
  mysqldump [--set-gtid-purged=off] [--skip-add-drop-table] [--no-data] [-h{hostname}] -u{username} -p{password} {dbname} {tablename1} [{tablename2} {tablename3}...] | gzip > {backupFilepath}
  ```

  `--set-gtid-purged=off`的作用：不将全局事务信息导出。若将其导出，那么在导入的时候，就需要处理一下才行，否则会导入报错

  `--skip-add-drop-table`的作用：mysqldump在导出时，会默认携带drop表的语句，如果不需要drop表，那就可以加上此参数

  `--no-data`的作用：仅包含结构，不包含数据

- 示例

  ```todo
  mysqldump --set-gtid-purged=off -uroot -ppwd123 xxl_job xxl_job_user xxl_job_group | gzip > xxl_job-tables.bk.gz
  
  mysqldump --set-gtid-purged=off -h10.8.0.101 -uroot -ppwd123 xxl_job xxl_job_user xxl_job_group | gzip >  xxl_job-tables.bk.gz
  
  mysqldump --set-gtid-purged=off --skip-add-drop-table -uroot -ppwd123 xxl_job xxl_job_user xxl_job_group | gzip >  xxl_job-tables.bk.gz
  
  mysqldump --set-gtid-purged=off --no-data -uroot -ppwd123 xxl_job xxl_job_user xxl_job_group | gzip >  xxl_job-tables.bk.gz
  ```

## 导入库

### 注意事项

> 1. 导入前，需要先对原数据库进行备份
> 2. 导入前，需要先检查一下要导入的文件里面的sql

### 普通导入

- 语法

  ```shell
  mysql -u{username} -p{password} {dbname} < {backupFilepath}
  ```

- 示例

  ```shell
  mysql -uroot -ppwd123 testDb < testDb.bk
  ```

### gzip导入

- 语法

  ```shell
  gunzip < {backupFilepath} | mysql -u{username} -p{password} {dbname}
  ```

- 示例

  ```shell
  gunzip <  testDb.bk.gz | mysql -uroot -ppwd123 testDb
  ```

  