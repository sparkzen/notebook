# @MapperScan与@Mapper


- [@MapperScan与@Mapper](#mapperscan与mapper)
  - [背景说明](#背景说明)
  - [@MapperScan与@Mapper的作用](#mapperscan与mapper的作用)
  - [通过@Mapper让Mybatis对接口提供代理实现](#通过mapper让mybatis对接口提供代理实现)
  - [通过@MapperScan让Mybatis对接口提供代理实现](#通过mapperscan让mybatis对接口提供代理实现)


## 背景说明

我们在编写mapper时，只需要编写接口而不需要对其实现，由Mybatis框架对接口提供对应的代理实现类（，并将代理实现类注册进容器中）。但是Mybatis是怎么知道需要对哪些接口进行代理实现呢，就是通过@MapperScan与@Mapper注解来知道的。



## @MapperScan与@Mapper的作用

使Mybatis知道需要对哪些接口提供代理实现。



## 通过@Mapper让Mybatis对接口提供代理实现

> 声明：不同版本mybatis代理类生成机制可能存在差异，以下结论`@MapperScan`基于`org.mybatis:mybatis-spring:2.0.6`，`@Mapper`基于`org.mybatis:mybatis:3.5.9`

1. 在目标接口上使用@Mapper注解进行标识

2. 需保证该接口源码必须在当前项目下

   即：该接口必须是当前项目的源码接口，不能是其它依赖类库中的，否者@Mapper不会生效。

   注：有人可能会以为使用@ComponentScan将其它依赖类库中的mapper导入就可以了。其实是不可以的，@ComponentScan扫描的是被@Component等注解标注了的类并注册进Spring容器，与Mybatis扫描生成代理类的识别机制是两个概念。

   注：如果需要让Mybatis对其它依赖类库中的mapper接口也生成代理类，那么请使用@MapperScan

## 通过@MapperScan让Mybatis对接口提供代理实现

> 声明：不同版本mybatis代理类生成机制可能存在差异，以下结论`@MapperScan`基于`org.mybatis:mybatis-spring:2.0.6`，@Mapper基于`org.mybatis:mybatis:3.5.9`

1. 只需要指定要范围即可。Mybatis会对该范围下的所有接口进行对应的代理实现（，并将代理实现类注册进容器中）。

   注：范围既可以包含当前项目内的范围，也可以包含当前项目所依赖的类库的范围。

   注：@MapperScan指定范围下的所有接口，是所有接口，不论是mapper接口、还是service接口、或者是其它什么接口，只要接口是在@MapperScan指定的范围内，Mybatis都会对该接口进行对应的代理实现（，并将代理实现类注册进容器中）。所以**在使用@MapperScan时，一定要注意指定的范围不能过大**。避免以下情况的发生：

   ![image-20220417115335630](../repository/image-20220417115335630.png)