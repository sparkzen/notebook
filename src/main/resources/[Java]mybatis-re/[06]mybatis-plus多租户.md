# mybatis-plus多租

- [mybatis-plus多租](#mybatis-plus多租)
  - [简述](#简述)
  - [TenantLineInnerInterceptor配置示例](#tenantlineinnerinterceptor配置示例)
  - [TenantLineHandler说明](#tenantlinehandler说明)
    - [接口说明](#接口说明)
    - [实现示例](#实现示例)
  - [使用多租户插件后的CURD说明](#使用多租户插件后的curd说明)
  - [相关资料](#相关资料)

## 简述

使用非常简单，只需要在注册`MybatisPlusInterceptor`的时候，设置添加mybatis-plus的内部多租户插件`TenantLineInnerInterceptor`即可

## TenantLineInnerInterceptor配置示例

```java
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.baomidou.mybatisplus.extension.plugins.inner.TenantLineInnerInterceptor;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author miemie
 * @since 2018-08-10
 */
@Configuration
public class MybatisPlusConfig {

    /**
     * 新多租户插件配置,一缓和二缓遵循mybatis的规则,需要设置 MybatisConfiguration#useDeprecatedExecutor = false 避免缓存万一出现问题
     * 
     * 注：高版本mybatis-plus移除了useDeprecatedExecutor字段
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new TenantLineInnerInterceptor(new TenantLineHandler() {
            @Override
            public Expression getTenantId() {
                // 返回租户id
                return new LongValue(1);
            }
    
            @Override
            public String getTenantIdColumn() {
                return "tenant_id";
            }
    
            // 这是 default 方法,默认返回 false 表示所有表都需要拼多租户条件
            @Override
            public boolean ignoreTable(String tableName) {
                return !"user".equalsIgnoreCase(tableName);
            }
        }));
        // 如果用了分页插件注意先 add TenantLineInnerInterceptor 再 add PaginationInnerInterceptor
        // 用了分页插件必须设置 MybatisConfiguration#useDeprecatedExecutor = false
//        interceptor.addInnerInterceptor(new PaginationInnerInterceptor());
        return interceptor;
    }

//    @Bean
//    public ConfigurationCustomizer configurationCustomizer() {
//        return configuration -> configuration.setUseDeprecatedExecutor(false);
//        注：高版本mybatis-plus移除了useDeprecatedExecutor字段
//    }
}
```

## TenantLineHandler说明

> 我们在配置TenantLineInnerInterceptor时，需要构造TenantLineHandler对象，这里对TenantLineHandler进行方法说明

### 接口说明

```java
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.schema.Column;

import java.util.List;

/**
 * 租户处理器（ TenantId 行级 ）
 *
 * @author hubin
 * @since 3.4.0
 */
public interface TenantLineHandler {

    /**
     * 获取租户 ID 值表达式，只支持单个 ID 值
     * <p>
     *
     * @return 租户 ID 值表达式
     */
    Expression getTenantId();

    /**
     * 获取租户字段名
     * <p>
     * 默认字段名叫: tenant_id
     *
     * @return 租户字段名
     */
    default String getTenantIdColumn() {
        return "tenant_id";
    }

    /**
     * 根据表名判断是否忽略拼接多租户条件
     * <p>
     * 默认都要进行解析并拼接多租户条件
     *
     * @param tableName 表名
     * @return 是否忽略, true:表示忽略，false:需要解析并拼接多租户条件
     */
    default boolean ignoreTable(String tableName) {
        return false;
    }

    /**
     * 忽略插入租户字段逻辑
     *
     * @param columns        插入字段
     * @param tenantIdColumn 租户 ID 字段
     * @return
     */
    default boolean ignoreInsert(List<Column> columns, String tenantIdColumn) {
        return columns.stream().map(Column::getColumnName).anyMatch(i -> i.equalsIgnoreCase(tenantIdColumn));
    }
}
```

### 实现示例

```java
@Override
public Expression getTenantId() {
    // 租户id应该根据当前用户，动态获取； 这里简单演示，写死为1
    return new LongValue(1);
}

@Override
public String getTenantIdColumn() {
    // 数据库表中，代表租户id的列名
    return "jd_tenant_id";
}

@Override
public boolean ignoreTable(String tableName) {
    // 只要表名不是user的表，都不做多租户区分， 都忽略
    return !"user".equalsIgnoreCase(tableName);
}
```

## 使用多租户插件后的CURD说明

> 这里通过代码上的注释，进行说明

```java
import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.baomidou.mybatisplus.samples.tenant.entity.User;
import com.baomidou.mybatisplus.samples.tenant.mapper.UserMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 多租户 Tenant 演示
 * </p>
 *
 * @author hubin
 * @since 2018-08-11
 */
@SpringBootTest
public class TenantTest {
    @Resource
    private UserMapper mapper;
    
    /**
     * 新增的时候，如果该表 没有被
     * {@link TenantLineHandler#ignoreTable(java.lang.String)}
     * 忽略，那么会自动插入租户id
     */
    @Test
    public void aInsert() {
        User user = new User();
        user.setName("一一");
        Assertions.assertTrue(mapper.insert(user) > 0);
        user = mapper.selectById(user.getId());
        Assertions.assertTrue(1 == user.getTenantId());
    }
    
    
    /**
     * 删除的时候，如果该表 没有被
     * {@link TenantLineHandler#ignoreTable(java.lang.String)}
     * 忽略，那么会在where后自动追加租户id判等条件
     */
    @Test
    public void bDelete() {
        Assertions.assertTrue(mapper.deleteById(3L) > 0);
    }
    
    /**
     * 更新的时候，如果该表 没有被
     * {@link TenantLineHandler#ignoreTable(java.lang.String)}
     * 忽略，那么会在where后自动追加租户id判等条件
     */
    @Test
    public void cUpdate() {
        Assertions.assertTrue(mapper.updateById(new User().setId(1L).setName("mp")) > 0);
    }
    
    /**
     * 查询的时候，如果该表 没有被
     * {@link TenantLineHandler#ignoreTable(java.lang.String)}
     * 忽略，那么会在where后自动追加租户id判等条件
     */
    @Test
    public void dSelect() {
        List<User> userList = mapper.selectList(null);
        userList.forEach(u -> Assertions.assertTrue(1 == u.getTenantId()));
    }

    /**
     * 自定义SQL：默认也会增加多租户条件
     * 参考打印的SQL
     */
    @Test
    public void manualSqlTenantFilterTest() {
        System.out.println(mapper.myCount());
    }

    /**
     *
     * 1. 多张表联表查询的时候，如果from表没有被
     * {@link TenantLineHandler#ignoreTable(java.lang.String)}
     *  略，那么会在where后面 and加 租户过滤 条件
     *  如（addr_name表被忽略， user表没有被忽略）: SELECT u.id, u.name, a.name AS addr_name FROM user u LEFT JOIN user_addr a ON a.user_id = u.id WHERE u.name LIKE concat(concat('%', ?), '%') AND u.tenant_id = 1
     *
     * 2.多张表联表查询的时候，如果被join表没有被
     * {@link TenantLineHandler#ignoreTable(java.lang.String)}
     *  略，那么会在在 joins...on..后面加on后面 and加 租户过滤 条件
     * 如（addr_name表被忽略， user表没有被忽略）: SELECT a.name AS addr_name, u.id, u.name FROM user_addr a LEFT JOIN user u ON u.id = a.user_id AND u.tenant_id = 1
     */
    @Test
    public void testTenantFilter(){
        mapper.getAddrAndUser(null).forEach(System.out::println);
        mapper.getAddrAndUser("add").forEach(System.out::println);
        mapper.getUserAndAddr(null).forEach(System.out::println);
        mapper.getUserAndAddr("J").forEach(System.out::println);
    }
}
```

## 相关资料

- [示例代码](https://gitee.com/JustryDeng/shared-files/raw/master/MybatisPlus多租户学习示例代码.rar)

- [官网](https://baomidou.com/pages/aef2f2/#tenantlineinnerinterceptor)
- [更多mybatis-plus使用示例](https://gitee.com/baomidou/mybatis-plus-samples)