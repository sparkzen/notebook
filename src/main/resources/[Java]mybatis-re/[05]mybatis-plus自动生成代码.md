# mybatis-plus自动生成代码

- [mybatis-plus自动生成代码](#mybatis-plus自动生成代码)
  - [示例说明](#示例说明)
    - [相关依赖](#相关依赖)
    - [生成器代码](#生成器代码)

## 示例说明

### 相关依赖

```xml
<!-- mybatis-plus -->
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-boot-starter</artifactId>
    <version>3.5.2</version>
</dependency>

<!-- 代码生成器相关 -->
<dependency>
    <groupId>com.baomidou</groupId>
    <artifactId>mybatis-plus-generator</artifactId>
    <version>3.5.2</version>
    <scope>test</scope>
</dependency>

<dependency>
    <groupId>org.freemarker</groupId>
    <artifactId>freemarker</artifactId>
    <version>2.3.30</version>
    <scope>test</scope>
</dependency>
```

### 生成器代码

```java
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.ideaaedi.commonds.io.IOUtil;
import com.ideaaedi.commonds.path.PathUtil;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;

/**
 * mybatis-plus代码生成 &emsp;
 * <a href="https://baomidou.com/pages/779a6e/#%E4%BD%BF%E7%94%A8">代码生成官网</a>
 * &emsp;
 * <a href="https://baomidou.com/pages/981406/#%E5%9F%BA%E7%A1%80%E9%85%8D%E7%BD%AE">配置说明官网</a>
 *
 * @author JustryDeng
 * @since 2022/4/14 11:10
 */
@SpringBootTest
class MybatisPlusGeneratorHelper {
    
    @Value("${spring.datasource.url}")
    private String url;
    
    @Value("${spring.datasource.username}")
    private String username;
    
    @Value("${spring.datasource.password}")
    private String password;
    
    @Test
    void generate() {
        String outputRootDir = PathUtil.getProjectRootDir(MybatisPlusGeneratorHelper.class)
                .replace("/target/test-classes/", "/src/test/java/");
        String packagePath = MybatisPlusGeneratorHelper.class.getPackage().getName() + ".generator";
        // 先清空./generator/目录
        IOUtil.delete(new File(outputRootDir + packagePath.replace(".", "/")));
        FastAutoGenerator.create(url, username, password)
                .globalConfig(builder -> builder
                        // 设置作者
                        .author("mybatis-plus generator")
                        .commentDate("yyyy-MM-dd HH:mm:ss")
                        // 指定输出目录
                        .outputDir(outputRootDir)
                        // 开启swagger模式
                        .enableSwagger()
                )
                // 包配置
                .packageConfig(builder -> builder
                        // 指定生成的类的所属包
                        .parent(packagePath)
                        .entity("entity.po")
                        .service("service")
                        .serviceImpl("service.impl")
                        .mapper("mapper")
                        .xml("mapper.xml")
                        .controller("controller")
                        .other("other"))
                // 策略配置
                .strategyConfig(builder -> builder
                        // => 表配置
                        // 设置需要生成的表名
                        .addInclude(
                                "sys_dict",
                                "sys_dict_type"
                        )
                        // 设置过滤表前缀
                        // .addTablePrefix("tmp_")
                        // 设置过滤表后缀
                        // .addTableSuffix("_tmp")
                        
                        // => entity配置
                        .entityBuilder()
                        .convertFileName((entityName) -> entityName + "PO")
                        // 启用TableField等相关注解
                        .enableTableFieldAnnotation()
                        .enableLombok()
                        
                        // => controller配置
                        .controllerBuilder()
                        .enableRestStyle()
                        .convertFileName((entityName) -> entityName + "Controller")
                        
                        // => service配置
                        .serviceBuilder()
                        .convertServiceFileName((entityName) -> entityName + "Service")
                        .convertServiceImplFileName((entityName) -> entityName + "ServiceImpl")
                        
                        // => mapper配置
                        .mapperBuilder()
                        .enableMapperAnnotation()
                        .convertMapperFileName((entityName) -> entityName + "Mapper")
                        .convertXmlFileName((entityName) -> entityName + "Mapper"))
                // 使用模板生成文件
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();
    }
}
```

