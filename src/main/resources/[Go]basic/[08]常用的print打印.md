# 常用的print打印

- [常用的print打印](#常用的print打印)
	- [定义枚举](#定义枚举)
	- [相关资料](#相关资料)

---

## 定义枚举

> - Printf需要自己指定格式化
> -  Println会按照默认的值表示方法打印并且输出一个换行

- 示例一：

```go
import "fmt"

func main() {
	var a, b, c = 100, 3.14, "Hello World"

	// ******** Printf需要自己指定格式化
	/*
	 * Printf不会自动换行， 所以需要使用\n主动换行，如果需要换行的话
	 * %d 整型占位符
	 */
	fmt.Printf("%d\n", a) // 输出：100
	/*
	 * %f 浮点型占位符
	 * .2表示小数点后两位
	 */
	fmt.Printf("%f\n", b) // 输出：3.140000
	fmt.Printf("%.2f\n", b) // 输出：3.14
	// %s 字符串占位符
	fmt.Printf("%s\n", c) // 输出：Hello World

	// ******** Println会按照默认的值表示方法打印并且输出一个换行
	fmt.Println(a, b, c) // 输出：100 3.14 Hello World
}
```

---
## 相关资料

- **《Go语言区块链应用开发从入门到精通》** **高野** 编著