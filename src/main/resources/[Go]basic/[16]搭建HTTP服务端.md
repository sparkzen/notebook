# 搭建HTTP服务端

- [搭建HTTP服务端](#搭建http服务端)
	- [ListenAndServe函数](#listenandserve函数)
	- [HandleFunc函数](#handlefunc函数)
	- [搭建HTTP服务端（示例）](#搭建http服务端示例)
	- [相关资料](#相关资料)

---

## ListenAndServe函数

`ListenAndServe`函数是用来侦听并启动服务的，它同时完成了绑定`ip`和端口、启动侦听、提供`HTTP`服务的作用。

> 格式：
>
> `func ListenAndServe(addr string, handler Handler) error`
>
> - `addr`：服务器地址
> - `handler`：服务器提供服务的函数指针，一般填`nil`

## HandleFunc函数

如果说`ListenAndServe`函数是用来提供HTTP服务的，那么`HandleFunc`函数就是用来处理`HTTP`请求的了。

> 格式：
>
> `func HandleFunc(pattern string, handler func(ResponseWriter, *Request))`
>
> - `pattern`：路由规则
> - `handler func(ResponseWriter, *Request)`：路由处理函数

## 搭建HTTP服务端（示例）

```go
import (
	"io"
	"log"
	"net/http"
)

func main() {
	// 添加路由处理器
	http.HandleFunc("/hello", HelloServer)
	http.HandleFunc("/bye", ByeServer)
	// 创建http服务端
	err := http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}

func HelloServer(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "hello world~\n")
}

func ByeServer(w http.ResponseWriter, req *http.Request) {
	io.WriteString(w, "Bye, Bye!\n")
}
```

启动，访问：

![1](../repository/1-16281832568841.jpg)

![2](../repository/2-16281832610822.jpg)

---
## 相关资料

- **《Go语言区块链应用开发从入门到精通》** **高野** 编著

