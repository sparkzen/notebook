# 数组、切片、Map

- [数组、切片、Map](#数组切片map)
	- [数组array](#数组array)
	- [切片slice](#切片slice)
	- [键值对map](#键值对map)
	- [相关资料](#相关资料)

---

## 数组array

> 格式形如：
>
> `var variable_name [SIZE]variable_type`
>
> 注：数组的长度是固定的

- 示例一：基操

```go
import "fmt"

func main() {
	var arr [3]string = [3]string{"张三", "李四", "王五"}
	fmt.Println(arr) // 输出：[张三 李四 王五]
}
```

- 示例二：如果不赋值，那么值默认为"零"值

```go
import "fmt"

func main() {
	var arr1 [3]int
	fmt.Println(arr1) // 输出：[0 0 0]
	// 字符串的"零"值为""
	var arr2 [3]string
	fmt.Println(arr2) // 输出：[  ]
	var arr3 [3]float64
	fmt.Println(arr3) // 输出：[0 0 0]
	var arr4 [3]bool
	fmt.Println(arr4) // 输出：[false false false]
}
```

- 示例三：`:=`也可以用

```go
import "fmt"

func main() {
	v1 := [2]string{"邓沙利文", "JustryDeng"}
	fmt.Println(v1) // 输出：[邓沙利文 JustryDeng]
}
```

- 示例四：访问与赋值

```go
import "fmt"

func main() {
	v1 := [2]string{"邓沙利文", "JustryDeng"}
	// 访问
	fmt.Println(v1[0]) // 输出：邓沙利文
	// 赋值
	v1[0] = "邓帅"
	fmt.Println(v1[0]) // 输出：邓帅
	fmt.Println(v1) // 输出：[邓帅 JustryDeng]
}
```

- 示例五：多维数组

```go
import "fmt"

func main() {
	// 3行2列
	arr := [3][2]string{
		{"邓沙利文", "JustryDeng"},
		{"邓帅", "亨得帅"},
		{"美男子", "帅哥"},
	}
	// 全输出
	fmt.Println(arr) // 输出：[[邓沙利文 JustryDeng] [邓帅 亨得帅] [美男子 帅哥]]
	// 第一行
	fmt.Println(arr[0]) // 输出：[邓沙利文 JustryDeng]
	// 第二行
	fmt.Println(arr[1]) // 输出：[邓帅 亨得帅]
	// 第三行
	fmt.Println(arr[2]) // 输出：[美男子 帅哥]
	// 第三行第二列
	fmt.Println(arr[2][1]) // 输出：帅哥
}
```

- 示例六：循环遍历

```go
import "fmt"

func main() {

	arr0 := [3]int{9527, 1888, 666}
	for i := 0; i < len(arr0); i++ {
		fmt.Println(arr0[i])
	}

	fmt.Println()

	arr1 := [3][2]string{
		{"邓沙利文", "JustryDeng"},
		{"邓帅", "亨得帅"},
		{"美男子", "帅哥"},
	}
	for i := 0; i < len(arr1); i++ {
		for j := 0; j < len(arr1[i]); j++ {
			fmt.Println(arr1[i][j])
		}
	}
}
```

> 输出
>
> ```ba
> 9527
> 1888
> 666
> 
> 邓沙利文
> JustryDeng
> 邓帅
> 亨得帅
> 美男子
> 帅哥
> ```
>
> 其它说明
>
> - 遍历数组的方式还有很多，这里就不一一示例了

## 切片slice

> 简单的，可以将切片理解为动态数组（即：长度可变的数组）
>
> - 获得方式
>
>   1. 截取已有的数组或切片（里面的元素的内存地址共享）
>
>      > 格式形如
>      >
>      > `array|slice[startIndx:endIndex]`
>      >
>      > - `array`代表数组
>      > - `|`代表或者
>      > - `slice`代表切片
>      > - `startIndex`代表截取开始位置（含`startIndex`）
>      >   注：`startIndex`可省略，若`startIndex`省略，则代表从头开始截取
>      > - `:`此处代表至
>      > - `endIndex`代表截取结束位置（不含`endIndex`）
>      >   注：`endIndex`可省略，若`endIndex`省略，则代表截取后面的全部
>
>   2. 试用make函数构造切片
>
>      >  格式形如
>      >
>      > `make ([]T, length, capacity)`
>      >
>      > - `make` 函数名
>      > - `T`切片内的元素类型
>      > - `length`长度
>      > - `capacity`容量（可省略，省略即表示capacity取值与length保持一致）
>      >
>      > 提示：make 函数可构造的数据有很多，切片只是其中之一
>
> - 和数组一样，切片内的元素初始值默认为”零“值
> - 切片的常用方法
>   1. `len(slice)`：计算切片的长度
>   
>   2. `cap(slice)`：计算切片的容量
>   
>   3. `append(s1, T...)`：往切片`s1`中追加元素，并返回新切片
>      注：新切片与旧切片是独立的，修改互不干预
>   
>   4. `copy(s2, s1)`：将`s1`的内容拷贝到`s2`
>   
>      注：`s1`与`s2`是独立的，修改互不干预
>   
>   5. ......

- 示例一：通过已有的数组获得切片

```go
import "fmt"

func main() {
	arr := [5]int{1, 2, 3, 4, 5}
    // 截取数组arr以获得切片
	slice := arr[1:4]
	fmt.Println(slice) // 输出：[2 3 4]
}
```

- 示例二：只取数组（或切片）得到的切片，里面的元素地址与原数组（或切片）是共享的

```go
import "fmt"

func main() {
	arr := [5]int{1, 2, 3, 4, 5}
	slice := arr[1:4]

	fmt.Println(slice) // 输出：[2 3 4]
	fmt.Println(arr) // 输出：[1 2 3 4 5]

	slice[0] = 100
	arr[3] = 888

	fmt.Println(slice) // 输出：[100 3 888]
	fmt.Println(arr) // 输出：[1 100 3 888 5]
}
```

- 示例三：通过make构造切片

```go
import (
	"fmt"
)

func main() {
	slice1 := make([]int, 1, 1)
	slice1[0] = 100
	fmt.Println(slice1) // 输出：[100]
}
```

- 示例四：append获得新切片

```go
import (
	"fmt"
)

func main() {
	slice1 := make([]int, 1, 1)
	slice1[0] = 100
	fmt.Println(slice1) // 输出：[100]

	// append
	var slice2 []int = append(slice1, 123, 888)
	fmt.Println(slice2) // 输出：[100 123 888]

	// 他们的值互不影响
	slice1[0] = -1
	slice2[0] = -2
	fmt.Println(slice1[0]) // 输出：-1
	fmt.Println(slice2[0]) // 输出：-2
}
```

- 示例五：copy切片

```go
import (
	"fmt"
)

func main() {
	slice1 := make([]int, 1, 1)
	slice1[0] = 100
	fmt.Println(slice1) // 输出：[100]
	slice2 := make([]int, 1, 1)
	fmt.Println(slice2) // 输出：[0]

	// copy
	copy(slice2, slice1)
	fmt.Println(slice2) // 输出：[100]

	// 他们的值互不影响
	slice1[0] = -1
	slice2[0] = -2
	fmt.Println(slice1[0]) // 输出：-1
	fmt.Println(slice2[0]) // 输出：-2
}
```

## 键值对map

> 格式形如
>
> ````go
> var map_variable map[key_data_type]value_data_type
> map_variable = make(map[key_data_type]value_data_type)
> 
> // 或者
> map_variable := make(map[key_data_type]value_data_type)
> ````
>
> - 从map中获取值时，支持以下方式接收返回值：
>
>   - 一个返回值：`值`
>
>     `value = map[key]`
>
>   - 两个返回值：`值, 键值对对是否存在`
>
>     value, existKeyValue = map[key]

示例一：map的存值、取值

```go
import (
	"fmt"
)

func main() {
	// 声明并构造map
	myMap := make(map[string]string)
	// 设置值
	myMap["k1"] = "v1"
	myMap["k2"] = "v2"
	myMap["k3"] = "v3"
	fmt.Println(myMap)

	fmt.Println(myMap["k2"])

	// 如果map中不存在对应的键值对，那么获取出来的为对应value_data_type类型的"零"值; 如下面这行代码获取到的就是string的零值，即空字符串""
	fmt.Println(myMap["non-exist-key"])

	myValue, hit := myMap["non-exist-key"]
	if hit {
		fmt.Println("myMap中存在key为non-exist-key的键值对, 对应的值为" + myValue)
	} else {
		fmt.Println("myMap中不存在key为non-exist-key的键值对")
	}
}
```

> 输出
>
> ```go
> map[k1:v1 k2:v2 k3:v3]
> v2
> 
> myMap中不存在key为non-exist-key的键值对
> ```

- 示例二：遍历map，同时输出k-v

```go
import (
	"fmt"
)

func main() {
	// 声明并构造map
	myMap := make(map[string]string)
	// 设置值
	myMap["k1"] = "v1"
	myMap["k2"] = "v2"
	myMap["k3"] = "v3"

	// 遍历map
	for k, v := range myMap {
		fmt.Println(k, v)
	}
}
```

> 输出
>
> ```go
> k2 v2
> k3 v3
> k1 v1
> ```
>
> 注：range除了遍历map外，还能遍历其它数据结构，如：数组、切片等等

- 示例三：使用占位符`_`，不获取不需要的数据

```go
import (
	"fmt"
)

func main() {
	// 声明并构造map
	myMap := make(map[string]string)
	// 设置值
	myMap["k1"] = "v1"
	myMap["k2"] = "v2"
	myMap["k3"] = "v3"

	// 遍历map, key的位置使用占位符_替代，表示不获取key
	for _, v := range myMap {
		fmt.Println(v)
	}

	fmt.Println()

	// 遍历map, value的位置使用占位符_替代，表示不获取value
	for k, _ := range myMap {
		fmt.Println(k)
	}
}
```

> 输出
>
> ```
> v1
> v2
> v3
> 
> k1
> k2
> k3
> ```

---

## 相关资料

- **《Go语言区块链应用开发从入门到精通》** **高野** 编著