# CentOS7上安装jdk11

- [CentOS7上安装jdk11](#centos7上安装jdk11)
  - [CentOS7上安装jdk11](#centos7上安装jdk11-1)
    - [第一步：去官网下载jdk11](#第一步去官网下载jdk11)
    - [第二步：将jdk上传至服务器，并解压](#第二步将jdk上传至服务器并解压)
    - [第三步：设置环境变量](#第三步设置环境变量)
    - [第四步：刷新环境变量](#第四步刷新环境变量)
    - [测试验证](#测试验证)
  - [相关资料](#相关资料)

## CentOS7上安装jdk11

### 第一步：去[官网](http://jdk.java.net/11/)下载jdk11

![image-20220621225916110](../repository/image-20220621225916110.png)

![image-20220621230308322](../repository/image-20220621230308322.png)

### 第二步：将jdk上传至服务器，并解压

**将jdk上传至服务器：**

```shell
[root@iZ2ze3a5ws0mjyt8jyqbpiZ opt]# # 本人习惯将软件安装放置ll
[root@iZ2ze3a5ws0mjyt8jyqbpiZ opt]# cd /opt/
[root@iZ2ze3a5ws0mjyt8jyqbpiZ opt]#
[root@iZ2ze3a5ws0mjyt8jyqbpiZ opt]# ll
total 183216
-rw-r--r-- 1 root root 187611826 Jun 21 23:05 openjdk-11+28_linux-x64_bin.tar.gz
[root@iZ2ze3a5ws0mjyt8jyqbpiZ opt]#
```



![image-20220621230632853](../repository/image-20220621230632853.png)

**使用指令`tar zxvf xxx.tar.gz`进行解压：**

```sheel
[root@iZ2ze3a5ws0mjyt8jyqbpiZ opt]# ls
openjdk-11+28_linux-x64_bin.tar.gz
[root@iZ2ze3a5ws0mjyt8jyqbpiZ opt]#
[root@iZ2ze3a5ws0mjyt8jyqbpiZ opt]# tar zxvf openjdk-11+28_linux-x64_bin.tar.gz
jdk-11/bin/jaotc
jdk-11/bin/jar
jdk-11/bin/jarsigner
jdk-11/bin/java
......（省略）
[root@iZ2ze3a5ws0mjyt8jyqbpiZ opt]# ls
jdk-11  openjdk-11+28_linux-x64_bin.tar.gz
```

### 第三步：设置环境变量

**编辑环境变量文件：**

```shell
vim /etc/profile
```

**在文末追加：**

```shell
export JAVA_HOME=/opt/jdk-11
export JRE_HOME=$JAVA_HOME/jre
export CLASSPATH=.:$JAVA_HOME/lib:$JRE_HOME/lib
export PATH=$JAVA_HOME/bin:$PATH
```

![image-20220621231339870](../repository/image-20220621231339870.png)

保存并退出

### 第四步：刷新环境变量

```shell
source /etc/profile
```

![image-20220621231615714](../repository/image-20220621231615714.png)



### 测试验证

输入`java -version`进行验证

![image-20220621231733202](../repository/image-20220621231733202.png)

## 相关资料

- [51CTO博客](https://blog.51cto.com/u_15009374/3144990)