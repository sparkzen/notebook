# Predicate

- [Predicate](#predicate)
  - [Predicate简介](#predicate简介)
  - [Predicate学习](#predicate学习)
    - [test(T t)：判断t，是否满足Predicate实例所代表的表达式](#testt-t判断t是否满足predicate实例所代表的表达式)
    - [isEqual(Object targetRef)：判断targetRef，是否与Predicate实例所代表的对象相等](#isequalobject-targetref判断targetref是否与predicate实例所代表的对象相等)
    - [and(Predicate<? super T> other)：对两个Predicate实例取`&&`，得到新的Predicate实例](#andpredicate-super-t-other对两个predicate实例取得到新的predicate实例)
    - [or(Predicate<? super T> other)：对两个Predicate实例取`||`，得到新的Predicate实例](#orpredicate-super-t-other对两个predicate实例取得到新的predicate实例)
    - [negate()：对当前Predicate实例取`!`，得到新的Predicate实例](#negate对当前predicate实例取得到新的predicate实例)
  - [相关资料](#相关资料)

---

## Predicate简介

**`Predicate`是一个功能性的接口，其功能是判断某个参数是否满足表达式** 。相似的还有`BiPredicate<T, U>`(使用 `test(T t, U u)`方法，判断参数t、u是否满足`BiPredicate`实例所代表的表达式)；`DoublePredicate`(对于基本数据类型`double`的`Predicate`)；`LongPredicate`(对于基本数据类型`int`的`Predicate`)；`IntPredicate`(对于基本数据类型`long`的`Predicate`)。

注：本文主要学习`Predicate`，学会了`Predicate`，那么自然就学会了`BiPredicate<T, U>`、`DoublePredicate`、`IntPredicate`、`LongPredicate`。

## Predicate学习

### test(T t)：判断t，是否满足Predicate实例所代表的表达式

```java
/**
 * test(T t): 判断t，是否满足Predicate实例所代表的表达式
 */
@Test
public void test1() {

    // 形参x的数据类型，由Predicate<T>的泛型T指定
    // 定义一个 用于判断的表达式(这里为 x >= 1)
    Predicate<Integer> predicate = x -> x >= 1;

    // 判断参数是否满足 predicate代表的表达式
    boolean resultOne = predicate.test(0);
    boolean resultTwo = predicate.test(1);

    // 输出结果为： false
    System.out.println(resultOne);
    // 输出结果为： true
    System.out.println(resultTwo);
}
```

运行测试类，控制台输出：

![img](../repository/20190819152353207.png)

### isEqual(Object targetRef)：判断targetRef，是否与Predicate实例所代表的对象相等

```java
/**
 * isEqual(Object targetRef): 判断targetRef，是否与Predicate实例所代表的对象相等
 */
@Test
public void test2() {

    /*
     * 等价于:
     *  Predicate<Object> predicate = x -> {
     *      if (x == objA) {
     *         return true;
     *      }
     *      return x.equals(objA);
     *  };
     */
    Object objA = new Object();
    Predicate<Object> predicate = Predicate.isEqual(objA);


    // 判断参数是否满足 predicate代表的表达式
    boolean resultOne = predicate.test(objA);
    boolean resultTwo = predicate.test(null);
    boolean resultThree = predicate.test(new Object());

    // 输出结果为： true
    System.out.println(resultOne);
    // 输出结果为： false
    System.out.println(resultTwo);
    // 输出结果为： false
    System.out.println(resultThree);
}
```

运行测试类，控制台输出：

![img](../repository/20190819152806864.png)

### and(Predicate<? super T> other)：对两个Predicate实例取`&&`，得到新的Predicate实例

```java
/**
 * and(Predicate<? super T> other): 对两个Predicate实例取&&，得到新的Predicate实例
 */
@Test
public void test3() {

    // 定义两个 用于判断的表达式
    Predicate<Integer> predicateOne = x -> x >= 1;
    Predicate<Integer> predicateTwo = x -> x <= 2;

    /*
     * 对两个表达式取 &&
     *
     * 等价于：Predicate<Integer> predicateThree = x -> x >= 1 && x <= 2;
     */
    Predicate<Integer> predicateThree = predicateOne.and(predicateTwo);

    // 判断参数是否满足 predicateThree代表的表达式
    boolean resultOne = predicateThree.test(0);
    boolean resultTwo = predicateThree.test(1);
    boolean resultThree = predicateThree.test(2);
    boolean resultFour = predicateThree.test(3);

    // 输出结果为： false
    System.out.println(resultOne);
    // 输出结果为： true
    System.out.println(resultTwo);
    // 输出结果为： true
    System.out.println(resultThree);
    // 输出结果为： false
    System.out.println(resultFour);
}
```

运行测试类，控制台输出：

![img](../repository/20190819152850278.png)

### or(Predicate<? super T> other)：对两个Predicate实例取`||`，得到新的Predicate实例

```java
/**
 * or(Predicate<? super T> other): 对两个Predicate实例取||，得到新的Predicate实例
 */
@Test
public void test4() {

    // 定义两个 用于判断的表达式
    Predicate<Integer> predicateOne = x -> x >= 1;
    Predicate<Integer> predicateTwo = x -> x <= -1;

    /*
     * 对两个表达式取 ||
     *
     * 等价于：Predicate<Integer> predicateThree = x -> x >= 1 || x <= -1;
     */
    Predicate<Integer> predicateThree = predicateOne.or(predicateTwo);

    // 判断参数是否满足 predicateThree代表的表达式
    boolean resultOne = predicateThree.test(-2);
    boolean resultTwo = predicateThree.test(-1);
    boolean resultThree = predicateThree.test(0);
    boolean resultFour = predicateThree.test(1);
    boolean resultFive = predicateThree.test(2);

    // 输出结果为： true
    System.out.println(resultOne);
    // 输出结果为： true
    System.out.println(resultTwo);
    // 输出结果为： false
    System.out.println(resultThree);
    // 输出结果为： true
    System.out.println(resultFour);
    // 输出结果为： true
    System.out.println(resultFive);
}
```

运行测试类，控制台输出：

![img](../repository/20190819152929800.png)

### negate()：对当前Predicate实例取`!`，得到新的Predicate实例

```java
/**
 * negate(): 对当前Predicate实例取!，得到新的Predicate实例
 */
@Test
public void test5() {

    // 定义两个 用于判断的表达式
    Predicate<Integer> predicateOne = x -> x >= 1;

    /*
     * 等价于：Predicate<Integer> predicate = x -> !(x >= 1);
     * 等价于：Predicate<Integer> predicate = x -> x < 1;
     */
    Predicate<Integer> predicate = predicateOne.negate();

    // 判断参数是否满足 predicateThree代表的表达式
    boolean resultOne = predicate.test(0);
    boolean resultTwo = predicate.test(1);
    boolean resultThree = predicate.test(2);

    // 输出结果为： true
    System.out.println(resultOne);
    // 输出结果为： false
    System.out.println(resultTwo);
    // 输出结果为： false
    System.out.println(resultThree);
}
```

运行测试类，控制台输出：

![img](../repository/20190819153015594.png)



## 相关资料

- [<font face="幼圆"  color = "#86CA5E">**demo代码下载**</font>](https://gitee.com/JustryDeng/shared-files/raw/master/jdk8-feature.rar)
- **《jdk api 1.8_google.CHM》**
- [`https://www.jianshu.com/p/b38ff80e3039`](https://www.jianshu.com/p/b38ff80e3039)

