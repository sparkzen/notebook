# 基本概念和术语

- [基本概念和术语](#基本概念和术语)
  - [1. 资源对象概述](#1-资源对象概述)
  - [2. 集群类资源对象](#2-集群类资源对象)
    - [2.1 Master](#21-master)
    - [2.2 Node](#22-node)
    - [2.3 命名空间](#23-命名空间)
  - [3. 应用类资源对象](#3-应用类资源对象)
    - [3.1 Service](#31-service)
    - [3.2 Pod](#32-pod)
    - [3.3 Label与标签选择器](#33-label与标签选择器)
    - [3.4 Pod与Deployment](#34-pod与deployment)
    - [3.5 Service的ClusterlP地址](#35-service的clusterlp地址)
    - [3.6 Service的外网访问问题](#36-service的外网访问问题)
    - [3.7 有状态的应用集程](#37-有状态的应用集程)
    - [3.8 批处理应用](#38-批处理应用)
    - [3.9 应用的配置问题](#39-应用的配置问题)
      - [3.9.1 ConfigMap](#391-configmap)
      - [3.9.2 Secret](#392-secret)
    - [3.10 应用的运维问题](#310-应用的运维问题)
      - [3.10.1 HPA(Horizontal Pod Autoscaler)](#3101-hpahorizontal-pod-autoscaler)
      - [3.10.2 YPA(Vertical Pod Autoscaler)](#3102-ypavertical-pod-autoscaler)
  - [4. 存储类资源对象](#4-存储类资源对象)
    - [4.1 Volume（存储卷）](#41-volume存储卷)
    - [4.2 动态存储管理](#42-动态存储管理)
      - [4.2.1 Persistent Volume（PV）](#421-persistent-volumepv)
      - [4.2.2 StorageClass](#422-storageclass)
      - [4.2.3 PVC（PV Claim）](#423-pvcpv-claim)
  - [5. 安全类资源对象](#5-安全类资源对象)
    - [5.1 用户认证](#51-用户认证)
    - [5.2 角色权限](#52-角色权限)
    - [5.3 授权](#53-授权)
    - [5.4 用户应用之间的网络隔离和授权](#54-用户应用之间的网络隔离和授权)
  - [6. 相关资料](#6-相关资料)

## 1. 资源对象概述

Kubernetes中的基本概念和术语大多是围绕资源对象(ResourceObject)来说的，而资源对象在总体上可分为以下两类：

- 某种资源的对象
  - 节点（Node）
  - Pod
  - 服务（Service）
  - 存储卷（Volume）
- 与资源对象相关的事物与动作
  - 标签（Label）
  - 注解（Annotation）
  - 命名空间（Namespace）
  - 部署（Deployment）
  - HPA
  - PVC

资源对象一般包括几个通用属性：

- 版本：在版本信息里包括了此对象所属的资源组， 一 些资源对象的属性会随着版本的升级而变化，在定义资源对象时要特别注意这一点 
- 类别（Kind）：用于定义资源对象所属类型
- 名称（属于资源对象的元数据）：资源对象的名称要唯一
- 标签（属于资源对象的元数据）：标签是很重要的数据，也是Kubernetes 的一大设计特性，比如通过标
  签来表明资源对象的特征 、类别，以及通 过标签筛选不同的资源对象并实现对象之间的关联 、 控制或协作功能
- 注解（属于资源对象的元数据）：可理解为一种特殊的标签，不过更多地是与程序挂钩，通常用于实现资源对象属性的自定义扩展

我们可以采用YAML或JSON格式声明（定义或创建）一个Kubernetes资源对象，每个资源对象都有自己的特定结构定义（可以理解为数据库中一个特定的表），并且统一保存在etcd这种非关系型数据库中，以实现最快的读写速度。此外，所有资源对象都可以通过Kubernetes提供的kubectl工具（或者API编程调用）执行增、删、改、查等操作

一些资源对象有自己的生命周期及相应的状态，比如Pod,我们通过kubectl客户端工具创建一个Pod并将其提交到系统中后，它就处千等待调度的状态，调度成功后为Pending状态，等待容器镜像下载和启动、启动成功后为Running状态，正常停止后为Succeeded状态，非正常停止后为Failed状态

**为了更好地理解和学习Kubernetes的基本概念和术语，特别是数呈众多的资源对象，这里按照功能或用途对其进行分类，将其分为集群类、应用类、存储类及安全类这四大类**

## 2. 集群类资源对象

集群（Cluster）表示一个由Master和Node组成的Kubernetes集群

### 2.1 Master

Master指的是集群的控制节点。在每个Kubemetes集群中都需要有一个或一组被称为Master的节点，来负责整个集群的管理和控制

注：Master通常占据一个独立的服务器（在高可用部署中建议至少使用3台服务器），是整个集群的"大脑"，如果它发生宒机或者不可用，那么对集群内容器应用的管理都将无法实施

在Master上运行着以下关键进程：

- Kubemetes API Server(kube-apiserver)：提供HTTP RESTful API接口的主要服务，是Kubernetes里对所有资源进行增、删、改、查等操作的唯一入口，也是集群控制的入口进程
- Kubernetes Controller Manager (kube-controller-manager)：Kubernetes里所有资源对象的自动化控制中心，可以将其理解为资源对象的"大总管"
- Kubemetes Scheduler ( kube-scheduler)：负责资源调度(Pod调度）的进程，相当千公交公司的调度室

另外，在 Master 上通常还需要部署etcd服务（etcd是用于共享配置和服务发现的分布式，一致性的KV存储系统）

### 2.2 Node

Kubemetes集群中除Mater外的其他服务器被称为Node,Node在较早的版本中也被称为Minion

与Master一样，Node可以是一台物理主机，也可以是一台虚拟机。Node是Kubemetes集群中的工作负载节点，每个Node都会被Master分配一些工作负载(Docker容器），当某个Node右机时，其上的工作负载会被Master自动转移到其他Node上。

在每个Node上都运行着以下关键进程：

- kubelet：负责Pod对应容器的创建、启停等任务，同时与Master密切协作，实现集群管理的基本功能
- kube-proxy：实现Kubernets Service的通信与负载均衡机制的服务
- 容器运行时（如Docker）：负责本机的容器创建和管理

Node可以再运行期间动态增加到Kubernetes集群中，前提是在这个Node上已正确安装、配置和启动了上述关键进程

在默认情况下，kubelet会向Master注册自己，这也是Kubernetes推荐的Node管理方式。一旦Node被纳入集群管理范畴，kubelet进程就会定时向Master汇报自身的情报，例如操作系统、主机CPU和内存使用情况，以及当前有哪些Pod在运行等，这样Master就可以获知每个Node的资源使用情况，并实现高效负载均衡的资源调度策略。而某个Node在超过指定时间不上报消息时，会被Master判定为"失联"，该Node的状态就被标记为不可用（Not Ready），Master随后会触发"工作负载大转移"的自动流程

如果一本Node存在问题，比如存在安全隐患、硬件资源不足要升级或者计划淘汰，我们就可以给这个Node打一种特殊的标签——污点（Taint），避免新的容器被调度到该Node上。而如果某些Pod可以（短期）容忍（Toleration）某种污点的存在，则可以继续将其调度到该Node上。

Node相关指令：

```shell
# 查看集群中有多少个Node
kubectl get nodes

# 查看指定node的详细信息
kubectl describe node {node_name}

# 查看集群中的pod
kubectl get pods

# 查看集群中指定命名空间下的pod（如果不指定namespace，那么默认查询的是namespace=default下的pod）
kubectl get pods --namespace={xxx}
```

### 2.3 命名空间

命名空间在很多情况下用于实现多租户的资源隔离，典型的一种思路就是给每个租户都分配一个命名空间。

命名空间属于Kubernetes集群范畴的资源对象，在一个集群里可以创建多个命名空间，每个命名空间都是相互独立的存在，属于不同命名空间的资源对象从逻辑上相互隔离

在每个Kubernetes集群安装完成且正常运行之后，Master会自动创建两个命名空间，一个是默认的(default)、一个是系统级的(kube-systern)。用户创建的资源对象如果没有指定命名空间，则被默认存放在default命名空间中；而系统相关的资源对象如网络组件、DNS组件、监控类组件等，都被安装在kube-systern命名空间中

我们可以通过命名空间将集群内部的资源对象“分配”到不同的命名空间中，形成逻辑上分组的不同项目、小组或用户组，便千不同的分组在共享使用整个集群的资源的同时能被分别管理。当给每个租户都创建一个命名空间来实现多租户的资源隔离时，还能结合Kubernetes的资源配额管理，限定不同租户能占用的资源，例如CPU使用蜇、内存使用证等

命名空间的定义很简单，如下所示的YAML文件定义了名为development的命名空间：

![1657518977963](../repository/1657518977963.png)

一旦创建了命名空间，我们在创建资源对象时就可以指定这个资源对象属于哪个命名空间。比如在下面的例子中定义了一个名为busybox的Pod,并将其放入development这个命名空间中：

![1657519012195](../repository/1657519012195.png)

![1657519062272](../repository/1657519062272.png)

## 3. 应用类资源对象

应用类相关的资源对象主要是围绕Service（服务）和Pod这两个核心对象展开的

### 3.1 Service

一般说来，Service指的是无状态服务，通常由多个程序副本提供服务，在特殊情况下也可以是有状态的单实例服务，比如MySQL这种数据存储类的服务。与我们常规理解的服务不同，Kubernetes里的Service具有一个全局唯一的虚拟ClusterIP地址，Service一旦被创建，Kubernetes就会自动为它分配一个可用的ClusterIP地址，而且在Service的整个生命周期中，它的ClusterIP地址都不会改变，客户端可以通过这个虚拟IP地址＋服务的端口直接访问该服务，再通过部署Kubernetes集群的DNS服务，就可以实现ServiceName（域名）到ClusterIP地址的DNS映射功能，我们只要使用服务的名称(DNS名称）即可完成到目标服务的访问请求。“服务发现“这个传统架构中的棘手问题在这里首次得以完美解决，同时，凭借ClusterIP地址的独特设计，Kubernetes进一步实现了Service的透明负载均衡和故障自动恢复的高级特性

通过分析、识别并建模系统中的所有服务为微服务一一KubernetesService,我们的系统最终由多个提供不同业务能力而又彼此独立的微服务单元组成，服务之间通过TCP/IP进行通信，从而形成强大又灵活的弹性网格，拥有强大的分布式能力、弹性扩展能力、容错能力，程序架构也变得简单和直观许多，如图所示：

![1657519490603](../repository/1657519490603.png)

### 3.2 Pod

Pod是Kubernetes中最重要的基本概念之一，如图1.5所示是Pod的组成示意图，我们看到每个Pod都有一个特殊的被称为“根容祥＂的Pause容器。Pause容器对应的镜像属千Kubernetes平台的一部分，除了Pause容器，每个Pod都还包含一个或多个紧密相关的用户业务容器

![1657519605194](../repository/1657519605194.png)

Kubernetes设计出一个全新的Pod概念并且Pod有这样特殊的组成结构，主要原因是：

- 为多进程之间的协作提供一个抽象模型，使用Pod作为基本的调度、复制等管理工作的最小单位，让多个应用进程能一起有效地调度和伸缩
- Pod里的多个业务容器共享Pause容器的IP,共享Pause容器挂接的Volume,这样既简化了密切关联的业务容器之间的通信问题，也很好地解决了它们之间的文件共享问题

Kubernetes为每个Pod都分配了唯一的IP地址，称之为PodIP,一个Pod里的多个容器共享PodIP地址。Kubernetes要求底层网络支待集群内任意两个Pod之间的TCP/IP直接通信，这通常采用虚拟二层网络技术实现，例如Flannel、OpenvSwitch等，因此我们需要牢记一点·在Kubernetes里，一个Pod里的容器与另外主机上的Pod容器能够直接通信

Pod其实有两种类型：普通的Pod及静态Pod(StaticPod)。后者比较特殊，它并没被存放在Kubernetes的etcd中，而是被存放在某个具体的Node上的一个具体文件中，并且只能在此Node上启动、运行。而普通的Pod一旦被创建，就会被放入etcd中存储，随后被KubernetesMaster调度到某个具体的Node上并绑定(Binding)，该Pod技对应的Node上的kubelet进程实例化成一组相关的Docker容器并启动。在默认情况下，当Pod里的某个容器停止时，Kubernetes会自动检测到这个问题并且重新启动这个Pod（重启Pod里的所有容器），如果Pod所在的Node宥机，就会将这个Node上的所有Pod都重新调度到其他节点上。Pod、容器与Node的关系如图1.6所示：

![1657521058817](../repository/1657521058817.png)

Pod的资源定义文件形如：

![1657521139553](../repository/1657521139553.png)

在以上定义中，kind属性的值为Pod,表明这是一个Pod类型的资源对象；metadata里的name属性为Pod的名称，在metadata里还能定义资源对象的标签，这里声明myweb拥有一个name=myweb标签。在Pod里所包含的容器组的定义则在spec部分中声明，这里定义了一个名为myweb且对应的镜像为kubeguide/tomcat-app:vl的容器，并在8080端口(containerPort)启动容器进程。Pod的IP加上这里的容器端口(containerPort)组成了一个新的概念一Endpoint,代表此Pod里的一个服务进程的对外通信地址。一个Pod也存在具有多个Endpoint的情况，比如当我们把Tomcat定义为一个Pod时，可以对外暴露管理端口与服务端口这两个Endpoint

我们所熟悉的Docker Volume在Kubernetes里也有对应的概念—-Pod Volume，Pod Volume是被定义在Pod上，然后被各个容器挂载到自己的文件系统中的。Volume简单来说就是（被挂载到Pod里的）文件目录

这里顺便提一下Kubernetes的Event概念。Event是一个事件的记录，记录了事件的最早产生时间、最后重现时间、重复次数、发起者、类型，以及导致此事件的原因等众多信息。Event通常会被关联到某个具体的资源对象上，是排查故障的重要参考信息。之前我们看到在Node的描述信息中包括Event,而Pod同样有Event记录，当我们发现某个Pod迟迟无法创建时，可以用kubectl describe pod xxxx来查看它的描述信息，以定位问题的成因

如图1.7所示给出了Pod及Pod周边对象的示意图，后面的部分还会涉及这张图里的对象和概念：

![1657521467263](../repository/1657521467263.png)

### 3.3 Label与标签选择器

Label（标签）是Kubernetes系统中的另一个核心概念，相当于我们熟悉的”标签"。一个Label是一个key=value的键值对，其中的key与value由用户自己指定。Label可以被附加到各种资源对象上，例如Node、Pod、Service、Deployment等，一个资源对象可以定义任意数蜇的Label,同一个Label也可以被添加到任意数侃的资源对象上。Label通常在资源对象定义时确定，也可以在对象创建后动态添加或者删除。我们可以通过给指定的资湃对象捆绑一个或多个不同的Labe]来实现多维度的资源分组管理功能，以便灵活、方便地进行资源分配、调度、配置、部署等管理工作，例如，部署不同版本的应用到不同的环境中，以及监控、分析应用（日志记录、监控、告警）等

一些常用的Label示例如下：

![1657523790316](../repository/1657523790316.png)

给某个资源对象定义一个Label,就相当千给它打了一个标签，随后可以通过Label Selector（标签选择器）查询和筛选拥有某些Label的资源对象，Kubernetes通过这种方式实现了类似SQL的简单又通用的对象查询机制。Label Selector可以被类比为SQL语句中的where查询条件，例如，"name=redis-slave"这个LabelSelector作用千Pod时，可以被类比为`select * from pod where pod's name='redis-slave'`这样的语句。当前有两种Label Selector表达式：基于等式的(Equality-based)Selector表达式和基于集合的(Set-based)Selector表达式：

- 基于等式的Selector表达式采用等式类表达式匹配标签，下面是一些具体的例子
  - name = redis-slave：匹配所有具有name=redis-slave标签的资源对象
  - env != production：匹配所有不具有env=production标签的资源对象，比如"env=test"
    就是满足此条件的标签之一
- 基于集合的Selector表达式则使用集合操作类表达式匹配标签，下面是一些具体的例子
  - name in (redis-master, redis-slave)：匹配所有具有name=redis-master标签或者name=redis-slave标签的资源对象
  - name not in (php-fronlend)：匹配所有不具有name=php-frontend标签的资源对象

可以通过多个 Label Selector 表达式的组合来实现复杂的条件选择 ，多个表达式之间用","进行分隔即可，几个条件之间是"AND"的关系，即同时满足多个条件，比如下 面的例子：

- name=redis-slave, env != preduction
- name not in (php-frontend), env != production

![1657524117907](../repository/1657524117907.png)

![1657524131142](../repository/1657524131142.png)

Service就是通过标签来对Pod进行关联的，如：

Pod设置的标签，app=myweb

![1657524205237](../repository/1657524205237.png)

Service设置关联的Pod时，就在Label Selector指定匹配标签app=myweb的Pod，进而关联上该Pod

![1657524220516](../repository/1657524220516.png)

### 3.4 Pod与Deployment

Deployment负责根据我们指定的模板自动创建指定（副本）数量的Pod实例

Deployment案例（省略部分内容）：

![1657588369541](../repository/1657588369541.png)

- replicas：Pod的副本数量
- selector：目标Pod的标签选择器
- template：用于自动创建新Pod副本的模板

注：在大多数情况下，即便只创建一个Pod副本实例，也需要使用Deployment来自动创建；这是因为：Deployment除了自动创建Pod副本外，还有自动控制的特性，举个例子：如果Pod所在的节点发生宕机事件，Kubernetes就会第一时间观察到这个故障，并自动创建一个新的Pod对象，将其调度到其它合适的节点上，Kubernetes会实时监控集群中目标Pod的副本数量，并且尽量与Deployment中声明的replicas数量保持一致

再给一个Deployment描述文件tomcat-deployment.yaml的例子：

![1657588829277](../repository/1657588829277.png)

查看Deployment的信息：

```shell
kubectl get deployments
```

![1657589015120](../repository/1657589015120.png)

上述输出各字段的含义解释如下：

- DESIRED：Pod副本数谥的期望值，即在Deployment里定义的replicas
- CURRENT：当前replicas的值，实际上是Deployment创建的ReplicaSet对象里的replicas值，这个值不断增加，直到达到DESIRED为止，表明整个部署过程完成
- UP-TO-DATE：最新版本的Pod的副本数址，用于指示在滚动升级的过程中，有多少个Pod副本已经成功升级
- AVAILABLE：当前集群中可用的Pod副本数证，即集群中当前存活的Pod数量

Deployment资源对象其实还与ReplicaSet资源对象密切相关，Kubernetes内部会根据Deployment对象自动创建相关联的ReplicaSet对象，通过以下命令，我们可以看到它的命名与Deployment的名称有对应关系：

![1657589334009](../repository/1657589334009.png)

不仅如此，我们发现Pod的命名也是以Deployment对应的ReplicaSet对象的名称为前缀的，这种命名很清晰地表明了一个ReplicaSet对象创建了哪些Pod,对于Pod滚动升级(Pod Rolling update)这种复杂的操作过程来说，很容易排查错误：

![1657589369984](../repository/1657589369984.png)



Deployme

nt典型的使用场景：

- 创建一个Deployment对象来完成相应Pod副本数量的创建
- 检查Deployment的状态来看部署动作是否完成（Pod副本数昼是否达到预期的值）
- 更新Deployment以创建新的Pod(比如镜像升级），如果当前Deployment不稳定，则回滚到一个早先的Deployment版本
- 扩展Deployment以应对高负载



Pod 、 Deplo y ment 与 Service 的 逻辑关系：

![1657589517942](../repository/1657589517942.png)



### 3.5 Service的ClusterlP地址

既然每个Pod都会被分配一个单独的IP地址，而且每个Pod都提供了一个独立的Endpoint(Pod IP-container Port)以被客户端访问，那么现在多个Pod副本组成了一个集群来提供服务，客户端如何访问它们呢？传统的做法是部署一个负载均衡器（软件或硬件），为这组Pod开启一个对外的服务端口如8000端口，并且将这些Pod的Endpoint列表加入8000端口的转发列表中，客户端就可以通过负载均衡器的对外IP地址＋8000端口来访问此服务了。Kubernetes也是类似的做法，Kubernetes内部在每个Node上都运行了一套全局的虚拟负载均衡器，自动注入并自动实时更新集群中所有Service的路由表，通过iptables或者IPVS机制，把对Service的请求转发到其后端对应的某个Pod实例上，并在内部实现服务的负载均衡与会话保待机制。不仅如此，Kubernetes还采用了一种很巧妙又影响深远的设计一ClusterlP地址。我们知道，Pod的Endpoint地址会随着Pod的销毁和重新创建而发生改变，因为新Pod的IP地址与之前旧Pod的不同。Service一旦被创建，Kubernetes就会自动为它分配一个全局唯一的虚拟IP地址——ClusterIP地址，而且在Service的整个生命周期内．其ClusterIP地址不会发生改变，这样一来，每个服务就变成了具备唯一IP地址的通信节点，远程服务之间的通信间题就变成了基础的TCP网络通信问题

任何分布式系统都会涉及“服务发现“这个基础问题，大部分分布式系统都通过提供特定的API来实现服务发现功能，但这样做会导致平台的侵入性较强，也增加了开发、测试的难度。Kubernetes则采用了直观朴素的思路轻松解决了这个棘手的问题：只要用Service的Name与ClusterIP地址做一个DNS域名映射即可。比如我们定义一个MySQLService,Service的名称是mydbserver,Service的端口是3306,则在代码中直接通过mydbserver:3306即可访问此服务，不再需要任何API来获取服务的IP地址和端口信息

之所以说ClusterIP地址是一种虚拟IP地址，原因有以下几点：

- ClusterIP地址仅仅作用于Kubemetes Service这个对象，并由Kubemetes管理和分配IP地址（来源于ClusterIP地址池），与Node和Master所在的物理网络完全无关
- 因为没有一个"实体网络对象"来响应，所以ClusterIP地址无法被Ping通。ClusterIP地址只能与Service Port组成一个具体的服务访问端点，单独的ClusterIP不具备TCP/IP通信的基础
- Cluste丑P属于Kubernetes集群这个封闭的空间，如果集群外的节点要访问这个通信端口，则需要做一些额外的工作



Service定义文件示例：

![1657603879283](../repository/1657603879283.png)

以上代码定义了一个名为tomcat-service的Service,它的服务端口为8080,拥有tier=frontend标签的所有Pod实例都属于它

我们可以通过以下指令查看指定名称的Service的信息：

```shell
kubectl get svc {service_name} -o yaml
```

![1657610320250](../repository/1657610320250.png)

在spec.ports的定义中，targetPort属性用来确定提供该服务的容器所悬露(Expose)的端口号，即具体的业务进程在容器内的targetPort上提供TCP/IP接入；port属性则定义了Service的端口。定义服务时如果没有指定targetPort,那么默认targetPort与port相同。除了正常的Service，还有一种特殊的Service——HeadlessService，只要在Service的定义中设置了clusterIP:None,就定义了一个Headless Service,它与普通Service的关键区别在于它没有ClusterIP地址，如果解析Headless Service的DNS域名，则返回的是该Service对应的全部Pod的Endpoint列表，这意味着客户端是直接与后端的P式建立TCP/IP连接进行通信的，没有通过虚拟ClusterIP地址进行转发，因此通信性能最高，等同于"原生网络通信"

很多服务都存在多个端口，通常一个端口提供业务服务，另一个端口提供管理服务，比如Mycat、Codis等常见中间件。Kubernetes Service支待多个Endpoint,在存在多个Endpoint的情况下，要求每个Endpoint都定义一个名称进行区分。下面是Tomcat多端口的Service定义样例：

```yaml
apiVersion: v1
kind: Service
metadata:
  name: tomcat-service
spec:
  ports:
  - port: 8080
    name: service-port
  - port: 8005
    name: shutdown-port
  selector:
    tier: frontend
```

### 3.6 Service的外网访问问题

服务的ClusterIP地址在Kubernetes集群内才能被访问，那么如何让集群外的应用访问我们的服务呢？这也是一个相对复杂的问题。要弄明白这个问题的解决思路和解决方法，我们需要先弄明白Kubernetes的三种IP,这三种IP分别如下：

- Node IP：Node的IP地址

  NodeIP是Kubemetes集群中每个节点的物理网卡的IP地址，是一个真实存在的物理网络，所有属于这个网络的服务器都能通过这个网络直接通信，不管其中是否有部分节点不属于这个Kubernetes集群。这也表明Kubernetes集群之外的节点访问Kubernetes集群内的某个节点或者TCP/IP服务时，都必须通过NodeIP通信

- Pod IP：Pod的IP地址

  Pod IP是每个Pod的IP地址，在使用Docker作为容器支持引擎的情况下，它是Docker Engine艰据docker0网桥的IP地址段进行分配的，通常是一个虚拟二层网络。前面说过，Kubemetes要求位于不同Node上的Pod都能够彼此直接通信，所以Kubernetes中一个Pod里的容器访问另外一个Pod里的容器时，就是通过PodIP所在的虚拟二层网络进行通信的，而真实的TCP/IP流量是通过NodeIP所在的物理网卡流出的

- Service IP：Service的IP地址

  在Kubernetes集群内，Service的ClusterIP地址属于集群内的地址，无法在集群外直接使用这个地址。为了解决这个问题，Kubernetes首先引入了NodePort这个概念，NodePort也是解决集群外的应用访问集群内服务的直接、有效的常见做法：

  ```yaml
  apiVersion: v1
  kind: Service
  metadata:
    name: tomcat-service
  spec:
    type: NodePort
    ports:
      - port: 8080
        nodePort: 31002
    selector:
      tier: fronteend
  ```

  其中，nodePort:31002这个属性表明手动指定tomcat-service的NodePort为31002,否则Kubernetes会自动为其分配一个可用的端口。接下来在浏览器里访问http://{nodeIP}:31002/

  NodePort的实现方式是，在Kubernetes集群的每个Kode上都为需要外部访问的Service开启一个对应的TCP监听端口，外部系统只要用任意一个Node的IP地址＋NodePort端口号即可访间此服务，在任意Node上运行netstat命令，就可以看到有NodePort端口被监听：

  ![1657612386179](../repository/1657612386179.png)

  但NodePort还没有完全解决外部访问Service的所有问题，比如负载均衡问题。假如在我们的集群中有10个Node,则此时最好有一个负载均衡器，外部的请求只帣访问此负载均衡器的IP地址，由负载均衡器负责转发流蜇到后面某个Node的NodePort上，如图1.12所示：

  ![1657612451211](../repository/1657612451211.png)

  图1.12中的负载均衡器组件独立千Kubernetes集群之外，通常是一个硬件的负载均衡器，也有以软仁方式实现的，例如HAProxy或者Nginx。对于每个Service,我们通常需要配置一个对应的负载均衡器实例来转发流盘到后端的Node上，这的确增加了工作址及出错的概率。于是Kubernetes提供了自动化的解决方案，如果我们的集群运行在谷歌的公有云GCE上，那么只要把Service的"type=NodePort"改为"type=LoadBalancer",Kubernetes就会自动创建一个对应的负载均衡器实例并返回它的IP地址供外部客户端使用。其他公有云提供商只要实现了支持此特性的驱动，则也可以达到以上目的。

NodePort的确功能强大且通用性强，但也存在一个问题，即每个Service都需要在Node上独占一个端口，而端口又是有限的物理资源，那能不能让多个Service共用一个对外端口呢？这就是后来增加的Ingress资源对象所要解决的问题。在一定程度上，我们可以把Ingress的实现机制理解为基于Nginx的支持虚拟主机的HTTP代理。下面是一个Ingress的实例：

```yaml
kind: Ingress
metadata:
  name: name-virtual-host-ingress
spec:
  rules:
  - host: first.bar.com
    http:
      paths:
      - backend:
          serviceName: servicel
          servicePort: 80
  -host: second.foo.com
    http:
      paths:
      - backend:
          serviceName: service2
          servicePort: 80
```

在以上Ingress的定义中，到虚拟域名first.bar.com请求的流量会被路由到serviceI,到second.foo.com请求的流最会被路由到service2。通过上面的例子，我们也可以看出，Ingress其实只能将多个HTTP(HTTPS)的Service"聚合”，通过虚拟域名或者URLPath的特征进行路由转发功能C考虑到常见的微服务都采用了HTTPREST协议，所以Ingress这种聚合多个Service并将其暴露到外网的做法还是很有效的

### 3.7 有状态的应用集程

> 什么是有状态？什么是无状态？
>
> 有状态的应用被类比为宠物(Pet)，无状态的应用则被类比为牛羊，每个宠物在主人那里都是“唯一的存在＂，宠物生病了，我们是要花很多钱去治疗的，需要我们用心照料，而无差别的牛羊则没有这个待遇，牛生病死了，大不了再重新养一头就行了，新牛和老牛对主人来说都一样

有状态集群的自动部署和管理，一开始是依赖StatefulSet（曾用名PetSet）解决的，但后来发现对于一些复杂的有状态的集群应用来说，StatefulSet还是不够通用和强大，所以后面又出现了Kubernetes Operator

有状态集群中一般有如下特殊共性：

- 每个节点都有固定的身份ID，通过这个ID，集群中的成员可以相互发现并通信
- 集群的规模是比较固定的，集群规模不能随意变动
- 集群中的每个节点都是有状态的，通常会待久化数据到永久存储中，每个节点在重启后都需要使用原有的持久化数据
- 集群中成员节点的启动顺序（以及关闭顺序）通常也是确定的
- 如果磁盘损坏，则集群里的某个节点无法正常运行，集群功能受损

如果通过Deployment控制Pod副本数量来实现以上有状态的集群，我们就会发现上述很多特性大部分难以满足，比如Deployment创建的Pod因为Pod的名称是随机产生的，我们事先无法为每个Pod都确定唯一不变的ID,不同Pod的启动顺序也无法保证，所以在集群中的某个成员节点岩机后，不能在其他节点上随意启动一个新的Pod实例。另外，为了能够在其他节点上恢复某个失败的节点，这种集群中的Pod需要挂接某种共享存储，为了解决有状态集群这种复杂的特殊应用的建模，Kubernetes引入了专门的资源对象一Statefu!Set。StatefulSet从本质上来说，可被看作Deployment/RC的一个特殊变种，它有如下特性：

- Statefu]Set里的每个Pod都有稳定、唯一的网络标识，可以用来发现集群内的其他成员。假设Statefu!Set的名称为kafka，那么第1个Pod叫kafka-0，第2个叫kafka-1，以此类推
- StatefulSet控制的Pod副本的启停顺序是受控的，操作第n个Pod时，前n-1个Pod已经是运行且准备好的状态
- StatefulSet里的Pod采用稳定的持久化存储卷，通过PV或PVC来实现，删除Pod时默认不会删除与StatefulSet相关的存储卷（为了保证数据安全）

StatefulSet除了要与PV卷捆绑使用，以存储Pod的状态数据，还要与HeadlessService配合使用，即在每个StatefulSet定义中都要声明它属千哪个HeadlessService。StatefulSet在HeadlessService的基础上又为StatefulSet控制的每个Pod实例都创建了一个DNS域名，这个域名的格式如下：

![1657694422006](../repository/1657694422006.png)

比如一个3节点的Kaflca的StatefulSet集群对应的Headless Service的名称为kafka,StatefulSet的名称为kaflca,则StatefulSet里3个Pod的DNS名称分别为kafka-0.kaflca、kaflca-1.kafka、kafka-2.kafka,这些DNS名称可以直接在集群的配翌文件中固定下来

### 3.8 批处理应用

除了无状态服务、有状态集群、常见的第三种应用，还有批处理应用。批处理应用的特点是一个或多个进程处理一组数据（图像、文件、视频等），在这组数据都处理完成后，批处理任务自动结束。为了支持这类应用，Kubemetes引入了新的资源对象一一Job,下面是一个计算圆周率的经典例子：

![1657694557623](../repository/1657694557623.png)

Jobs控制器提供了两个控制并发数的参数：completions和parallelism,completions表示需要运行任务数的总数，parallelism表示并发运行的个数，例如设置parallelism为1'则会依次运行任务，在前面的任务运行后再运行后面的任务。Job所控制的Pod副本是短暂运行的，可以将其视为一组容器，其中的每个容器都仅运行一次。当Job控制的所有Pod副本都运行结束时，对应的Job也就结束了。Job在实现方式上与Deployment等副本控制器不同，Job生成的Pod副本是不能自动重启的，对应Pod副本的restartFolicy都被设置为Never，因此，当对应的Pod副本都执行完成时，相应的Job也就完成了控制使命。后来，Kubernetes增加了CronJob,可以周期性地执行某个任务

### 3.9 应用的配置问题

目前，我们了解了三种应用建模的资源对象：

- 无状态服务的建模：Deployment
- 有状态集群的建模：StatefulSet
- 批处理引用的建模：Job

在进行应月建模时，应该如何解决应用需耍在不同的环境中修改配置的问题呢？这就涉及ConfigMap和Secret两个对象了

#### 3.9.1 ConfigMap

ConfigMap是保存配置项(key=value)的一个Map，ConfigMap是分布式系统中"配置中心"的独特实现之一

我们知道，几乎所有应用都需要一个静态的配置文件来提供启动参数，当这个应用是一个分布式应用，有多个副本部署在不同的机器上时，配置文件的分发就成为一个让人头疼的问题，所以很多分布式系统都有一个配置中心组件，来解决这个问题。但配置中心通常会引入新的API，从而导致应用的耦合和侵入。Kubemetes则采用了一种简单的方案来规避这个问题，如图1.13所示，具体做法如下：

![1657694942575](../repository/1657694942575.png)

- 用户将配置文件的内容保存到ConfigMap中，文件名可作为key,value就是整个文件的内容，多个配笠文件都可被放入同一个ConfigMap
- 在建模用户应用时，在Pod里将ConfigMap定义为特殊的Volume进行挂载。在Pod被调度到某个具体Node上时，ConfigMap里的配置文件会被自动还原到本地目录下，然后映射到Pod里指定的配笠目录下，这样用户的程序就可以无感知地读取配置了
- 在ConfigMap的内容发生修改后，Kubernetes会自动重新获取ConfigMap的内容，并在目标节点上更新对应的文件

#### 3.9.2 Secret

Secret也用于解决应用配置的问题，不过它解决的是对敏感信息的配置问题，比如数据库的用户名和密码、应用的数字证书、Token、SSH密钥及其他需要保密的敏感配置。对千这类敏感信息，我们可以创建一个Secret对象，然后被Pod引用

注：Secret中的数据要求以BASE64编码格式存放。注意，BASE64编码并不是加密的，在Kubernetes1.7版本以后，Secret中的数据才可以以加密的形式进行保存，更加安全

### 3.10 应用的运维问题

应用的自动运维相关的几个重要对象

#### 3.10.1 HPA(Horizontal Pod Autoscaler)

HPA(Horizontal Pod Autoscaler)，即（水平）横向Pod自动扩缩容，即自动控制Pod数量的增加或减少；通过追踪分析指定Deployment控制的所有目标Pod的负载变化情况，来确定是否需要有针对性地调整目标Pod的副本数量，这是HPA的实现原理
注：Kubernetes内置了基于Pod的CPU利用率进行自动扩缩容的机制，应用开发者也可以自定义度量指标如每秒请求数，来实现自定义的HPA功能

下面是一个HPA定义的例子：

![1657695331217](../repository/1657695331217.png)

根据上面的定义，我们可以知道这个HPA控制的目标对象是一个名为php-apache的Deployment里的Pod副本，当这些Pod副本的CPU利用率的值超过90％时，会触发自动动态扩容，限定Pod的副本数量为1~10

#### 3.10.2 YPA(Vertical Pod Autoscaler)

YPA(Vertical Pod Autoscaler)，即垂直Pod自动扩缩容，它根据容器资源使用率自动推测并设置Pod合理的CPU和内存的需求指标，从而更加精确地调度Pod,实现整体上节省集群资源的目标，因为无须人为操作，因此也进一步提升了运维自动化的水平

## 4. 存储类资源对象

### 4.1 Volume（存储卷）

Volume是Pod中能够被多个容器访问的共享目录。Kubernetes中的Volume概念、用途和目的与Docker中的Volume比较类似，但二者不能等价。首先，Kubernetes中的Volume被定义在Pod上，被一个Pod里的多个容器挂载到具体的文件目录下；其次，Kubernetes中的Volume与Pod的生命周期相同，但与容器的生命周期不相关，当容器终止或者重启时心lume中的数据也不会丢失；最后，Kubemetes支持多种类型的Volume，例如GlusterFS、Ceph等分布式文件系统

Volume的使用也比较简单，在大多数情况下，我们先在Pod上声明一个Volume,然后在容器里引用该Volume并将其挂载(Mount)到容器里的某个目录下。举例来说，若我们要给之前举例的Tomcat Pod增加一个名为datavol的Volume,并将其挂载到容器的/mydata-data目录下，则只对Pod的定义文件做如下修正即可（代码中的粗体部分）：

![1657695784020](../repository/1657695784020.png)

Kubernetes提供了非常丰富的Volume类型供容器使用，例如临时目录、宿主机目录、共享存储等，下面对其中一些常见的类型进行说明：

- emptyDir

  一个emptyDir是在Pod分配到Node时创建的。从它的名称就可以看出，它的初始内容为空，并且无须指定宿王机上对应的目录文件，因为这是Kubernetes自就分配的一个目录，当Pod从Node上移除时，emptyDir中的数据也被永久移除。emptyDir的一些用途如下：

  - 临时空间，例如用于某些应用程序运行时所需的临时目录，且无须永久保留
  - 长时间任务执行过程中使用的临时目录
  - 一个容器需要从另一个容器中获取数据的目录（多容器共享目录）

  注：在默认情况下，emptyDir使用的是节点的存储介质，例如磁盘或者网络存储。还可以使用emptyDir.medium属性，把这个属性设笠为"Memory",就可以使用更快的基于内存的后端存储了。需要注意的是，这种情况下的emptyDir使用的内存会被计入容器的内存消耗，将受到资源限制和配额机制的管理

- hostPath

  hostPath为在Pod上挂载宿主机上的文件或目录，通常可以用于以下几方面：

  - 在容器应用程序生成的日志文件需要永久保存时，可以使用宿主机的高速文件系统对其进行存储
  - 需要访问宿主机上Docker引擎内部数据结构的容器应用时，可以通过定义hostPath为宿主机／var/lib/docker目录，使容器内部的应用可以直接访问Docker的文件系统

  注：在使用这种类型的Volume时，需要注意以下几点：

  - 在不同的Node上具有相同配置的Pod,可能会因为宿主机上的目录和文件不同，而导致欢Volume上目录和文件的访问结果不一致
  - 如果使用了资源配额管理，则Kubernetes无法将hostPath在宿主机上使用的资源纳入管理

  用宿主机的/data目录定义一个hostPath类型的Volume（示例）:

  ![1657696312329](../repository/1657696312329.png)

- 公有云Volume

  公有云提供的Volume类型包括谷歌公有云提供的GCEPersistentDisk、亚马逊公有云提供的AWS Elastic Block  Store(EBS Volume)等。当我们的Kubernetes集群运行在公有云上或者使用公有云厂家提供的Kubernetes集群时，就可以使用这类Volume

- 其他类型的Volume

  - iscsi：将iSCSI存储设备上的目录挂载到Pod中
  - nfs：将NFS Server上的目录挂载到Pod中
  - glusterfs：将开源GlusterFS网络文件系统的目录挂载到Pod中
  - rbd：将Ceph块设备共享存储(Rados Block Device)挂载到Pod中
  - gitRepo：通过挂载一个空目录，并从Git库克隆(clone)一个git repository以供Pod使用
  - configmap：将配置数据挂载为容器内的文件
  - secret：将Secret数据挂载为容器内的文件

### 4.2 动态存储管理

Volume属于静态管理的存储，即我们需要事先定义每个Volume，然后将其挂载到Pod中去用，这种方式存在很多弊端，典型的弊端如下：

- 配置参数烦琐，存在大量手工操作，违背了Kubernetes自动化的追求目标
- 预定义的静态Volume可能不符合目标应用的需求，比如容最问题、性能问题

所以Kubernetes后面就发展了存储动态化的新机制，来实现存储的自动化管理。相关的核心对象（概念）有三个：Persistent Volume(简称PV)、StorageClass、PVC

#### 4.2.1 Persistent Volume（PV）

PV表示由系统动态创建(dynamically provisioned)的一个存储卷，可以被理解成Kubernetes集群中某个网络存储对应的一块存储，它与Volume类似，但PV并不是被定义在Pod上的，而是独立千Pod之外定义的

#### 4.2.2 StorageClass

我们知道，Kubernetes支持的存储系统有多种，那么系统怎么知道从哪个存储系统中创建什么规格的PV存储卷呢？这就涉及StorageC!ass与PVC。StorageClass用来描述和定义某种存储系统的特征，下面给出一个具体的例子：

![1657696877704](../repository/1657696877704.png)

provisioner代表了创建PV的第三方存储插件，parameters是创建PV时的必要参数，reclaimPolicy则表明了PV回收策略，回收策略包括删除或则保留

#### 4.2.3 PVC（PV Claim）

PVC正如其名，表示应用希望申请的PV规格，其中重要的属性包括accessModes（存储访问模式）、storageClassName(用哪种StorageClass来实现动态创建）及resources（存储的具体规格）

StorageClass的名称会在PVC中出现，下面就是一个典型的PVC定义：

![1657697073565](../repository/1657697073565.png)

有了以StorageC!ass与PVC为基础的动态PV管理机制，我们就很容易管理和使用Volume了，只要在Pod里引用PVC即可达到目的，如下面的例子所示：

![1657697160756](../repository/1657697160756.png)

## 5. 安全类资源对象

从本质上来说，Kubernetes可被看作一个多用户共享资源的资源管理系统，这里的资源主要是各种Kubernetes里的各类资源对象，比如Pod、Service、Deployment等。只有通过认证的用户才能通过Kubernetes的APIServer查询、创建及维护相应的资源对象，理解这一点很关键。

Kubernetes里的用户有两类：

- 运行在Pod里的应用
- 普通用户（如典型的kubectl命令行工具，基本上由指定的运维人员（集群管理员）使用）

在更多的情况下，我们开发的Pod应用需要通过APIServer查询、创建及管理其他相关资源对象，所以这类用户才是Kubernetes的关键用户。为此，Kubernetes设计了ServiceAccount这个特殊的资源对象，代表Pod应用的账号，为Pod提供必要的身份认证。在此基础上，Kubernetes进一步实现和完善了基于角色的访问控制权限系统一—RBAC(Role-Based Access Control)

### 5.1 用户认证

在默认情况下，Kubernetes在每个命名空间中都会创建一个默认的名称为default的ServiceAccount,因此ServiceAccount是不能全局使用的，只能被它所在命名空间中的Pod使用

通过以下命令可以查看集群中的所有Service Account：

```shell
kubectl get sa --all-namespaces
```

![1657697455108](../repository/1657697455108.png)

Service Account是通过Secret来保存对应的用户（应用）身份凭证的，这些凭证信息有CA根证书数据(ca.crt)和签名后的Token信息(Token)。在Token信息中就包括了对应的Service Account的名称，因此APIServer通过接收到的Token信息就能确定Service Account的身份

在默认悄况下，用户创建一个Pod时，Pod会绑定对应命名空间中的default这个Service Account作为其"公民身份证"。当Pod里的容器被创建时，Kubernetes会把对应的Secret对象中的身份信息(ca.crt、Token等）待久化保存到容器里固定位置的本地文件中，因此当容器里的用户进程通过Kubernetes提供的客户端API去访问API Server时，这些API会自动读取这些身份信息文件，并将其附加到HTTPS请求中传递给APIServer以完成身份认证逻辑。在身份认证通过以后，就涉及"访问授权"的问题，这就是RBAC要解决的问题了

### 5.2 角色权限

Role这个资源对象，包括Role与ClusterRole两种类型的角色；局限于某个命名空间的角色由Role对象定义，作用千整个Kubernetes集群范围内的角色则通过ClusterRole对象定义

角色定义了一组特定权限的规则，比如可以操作某类资源对象

下面是Role的一个例子，表示在命名空间default中定义一个Role对象，用于授予对Pod资源的读访问权限，绑定到该Role的用户则具有对Pod资源的get、watch和list权限：

![1657697685487](../repository/1657697685487.png)

### 5.3 授权

可以通过RoleBinding（或ClusterRoleBinding）将Role（或ClusterRole）与具体用户绑定，即完成用户授权

下面是一个具体的例子，在命名空间default中将"pod-reader"角色授予用户"Caden"，结合对应的Role的定义，表明这一授权将允许用户"Caden"从命名空间default中读取pod：

![1657697869677](../repository/1657697869677.png)

在RoleBinding中使用subjects(目标主体）来表示要授权的对象，这是因为我们可以授权三类目标账号：Group（用户组）、User（某个具体用户）和ServiceAccount(Pod应用所使用的账号）

### 5.4 用户应用之间的网络隔离和授权

在安全领域，除了以上针对APIServer访问安全相关的资源对象，还有一种特殊的资源对象——NetworkPolicy（网络策略），它是网络安全相关的资源对象，用于解决用户应用之间的网络隔离和授权问题。NetworkPolicy是一种关于Pod间相互通信，以及Pod与其他网络端点间相互通信的安全规则设定

NetworkPolicy资源使用标签选择Pod,并定义选定Pod所允许的通信规则。在默认悄况下，Pod间及Pod与其他网络端点间的访问是没有限制的，这假设了Kubernetes集群被一个厂商（公司／租户）独占，其中部署的应用都是相互可信的，无须相互防范。但是，如果存在多个厂商共同使用一个Kubernetes集群的情况，则特别是在公有云环境中，不同厂商的应用要相互隔离以增加安全性，这就可以通过NetworkPolicy来实现了

## 6. 相关资料

- 《Kubernetes权威指南（第五版）》

