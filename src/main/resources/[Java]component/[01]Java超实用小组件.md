# Java超实用小组件

- [Java超实用小组件](#java超实用小组件)
  - [component-compile：编译](#component-compile编译)
    - [maven坐标](#maven坐标)
    - [使用示例](#使用示例)
  - [component-decompile：反编译](#component-decompile反编译)
    - [maven坐标](#maven坐标-1)
    - [使用示例](#使用示例-1)
  - [component-decompile：dump出class](#component-decompiledump出class)
    - [maven坐标](#maven坐标-2)
    - [使用示例](#使用示例-2)

---

## component-compile：编译

### maven坐标

```xml
<dependency>
    <groupId>com.idea-aedi</groupId>
    <artifactId>component-compile</artifactId>
    <version>${最新版本}</version>
</dependency>
```

### 使用示例

```java
// 全类名-class字节数组
Map<String, byte[]> byteCodes = Compiler.create().addSource(
        CompilerTest.class.getClassLoader().getResourceAsStream("TestLogger1.java")
        ).buildByteCodes();
```

更多用法，详见[其它示例](https://gitee.com/JustryDeng/components/tree/master/component-compile/src/test/java/com/ideaaedi/component/compile)，或者直接看源码。

## component-decompile：反编译

### maven坐标

```xml
<dependency>
    <groupId>com.idea-aedi</groupId>
    <artifactId>component-decompile</artifactId>
    <version>${最新版本}</version>
</dependency>
```

### 使用示例

```java
List<byte[]> list = new ArrayList<>(8);
list.add(IOUtil.toBytes(new File(decompileDir, "klass/AbstractClassGenerator$ClassLoaderData$1.class")));
list.add(IOUtil.toBytes(new File(decompileDir, "klass/AbstractClassGenerator$ClassLoaderData.class")));
list.add(IOUtil.toBytes(new File(decompileDir, "klass/AbstractClassGenerator.class")));
list.add(IOUtil.toBytes(new File(decompileDir, "klass/AbstractClassGenerator$ClassLoaderData$3.class")));
list.add(IOUtil.toBytes(new File(decompileDir, "klass/AbstractClassGenerator$Source.class")));
list.add(IOUtil.toBytes(new File(decompileDir, "klass/AbstractClassGenerator$ClassLoaderData$2.class")));
/*
 * 反编译获取源码并解析全类名<br>
 * @see Decompiler#decompile(List, String)
 *
 * @param classByteList
 *            待反编译的clas
 * @return <ul>
 *     <li>左 - 全类名<br>形如：com.szlzcl.jdk8feature.OptionalTests.MyConsumer</li>
 *     <li>中 - 源码的classFilePath<br>形如：com/szlzcl/jdk8feature/OptionalTests$MyConsumer.class</li>
 *     <li>右 - 源码<br>注意：反编译失败或找不存要反编译的目标方法时，值为空白字符串</li>
 * </ul>
 */
List<Triple<String, String, String>> triples = Decompiler.defaultDecompiler().decompileAsTriple(list);
```

更多用法，详见[其它示例](https://gitee.com/JustryDeng/components/tree/master/component-decompile/src/test/java/com/ideaaedi/component/decompile)，或者直接看源码。

## component-decompile：dump出class

### maven坐标

```xml
<dependency>
    <groupId>com.idea-aedi</groupId>
    <artifactId>component-dump-class</artifactId>
    <version>${最新版本}</version>
</dependency>
```

### 使用示例

```java
// key - 类名,   value - (k - 加载该类的类加载器信息, v - class字节码)
Map<String, Map<String, byte[]>> dumpedClasses = NonExitClassFileTransformerExecutor.create("com.ideaaedi.component.dump", null, false).exec();
dumpedClasses.forEach((k, map) -> {
    System.out.println(k + "\t" + map.keySet() + "\t" + map.values());
});
```

更多用法，详见[其它示例](https://gitee.com/JustryDeng/components/tree/master/component-dump-class/src/test/java/com/ideaaedi/component/dump)，或者直接看源码。