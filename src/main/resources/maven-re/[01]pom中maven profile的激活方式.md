# pom中maven profile的激活方式

- [pom中maven profile的激活方式](#pom中maven-profile的激活方式)
  - [maven profiles简单介绍](#maven-profiles简单介绍)
  - [maven profile的激活方式](#maven-profile的激活方式)
    - [方式一：使用mvn ... -P xxx指令，指定激活id为xxx的profile](#方式一使用mvn---p-xxx指令指定激活id为xxx的profile)
    - [方式二：在profile里面使用activation标签，当满足条件时激活该profile](#方式二在profile里面使用activation标签当满足条件时激活该profile)
  - [相关资料](#相关资料)

---

## maven profiles简单介绍

`maven profiles`是`maven`从`POM4.0`开始给我们提供的一种新的特性。它允许`maven`根据不同的环境采用不同的`maven`配置。一个`profiles`标签中可以有很多个`profile`，只需要根据不同的项目环境，激活不同的profile即可。

注：也可以同时激活多个profile。

**profiles的组成(示例)：**

![img](../repository/20190904135412638.png)

注：使用maven的profile功能，我们可以实现多环境配置文件的动态切换。

## maven profile的激活方式

### 方式一：使用`mvn ... -P xxx`指令，指定激活`id`为`xxx`的`profile`

**第一步：** 在pom.xml中编写profiles。

```html
<!-- 使用mvn ... -P xxx指令，指定激活id为xxx的profile -->
<profiles>
    <!-- 测试环境 -->
    <profile>
        <!-- profile唯一id -->
        <id>test-id</id>
        <!-- 引入fastjson依赖 -->
        <dependencies>
            <!-- fastjson -->
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>fastjson</artifactId>
                <version>1.2.58</version>
            </dependency>
        </dependencies>
    </profile>

    <!-- 生产环境 -->
    <profile>
        <!-- profile唯一id -->
        <id>prod-id</id>
        <!-- 引入lombok依赖 -->
        <dependencies>
            <!-- lombok -->
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <optional>true</optional>
            </dependency>
        </dependencies>
    </profile>
</profiles>
```

**第二步：** 打包时通过maven指令，指定激活哪一个profile。

![img](../repository/xyasdf.png)

如果需要激活多个profile的话，使用逗号隔开多个profile的id即可：

![img](../repository/20190904135800752.png)

### 方式二：在`profile`里面使用`activation`标签，当满足条件时激活该`profile`

**提示：** activation标签，允许我们定义默认激活配置、定义条件激活(满足什么条件就进行激活)配置，下面一一介绍。

- **`activeByDefault`默认激活**

```html
<!-- ************** 设置默认激活 ************** -->
<profiles>
    <!-- 测试环境 -->
    <profile>
        <id>test-id</id>
        <activation>
            <!-- 设置默认激活 -->
            <activeByDefault>true</activeByDefault>
        </activation>
        <dependencies>
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>fastjson</artifactId>
                <version>1.2.58</version>
            </dependency>
        </dependencies>
    </profile>

    <!-- 生产环境 -->
    <profile>
        <id>prod-id</id>
        <dependencies>
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <optional>true</optional>
            </dependency>
        </dependencies>
    </profile>
</profiles>
```

注：默认激活级别最低！一旦其它某个激活指令(mvn指令激活或activation的其他条件激活)生效时，那么默认激活指令
     就不会生效。

- **根据`jdk`激活**

```html
<!-- ************** 根据jdk激活 ************** -->
<profiles>
    <!-- 测试环境 -->
    <profile>
        <id>test-id</id>
        <dependencies>
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>fastjson</artifactId>
                <version>1.2.58</version>
            </dependency>
        </dependencies>
    </profile>

    <!-- 生产环境 -->
    <profile>
        <id>prod-id</id>
        <activation>
            <!--
               当jdk的版本号是以下面的值开头时， 激活此profile
               即:当jdk的版本号是startWith下面的值时，激活此profile
             -->
            <jdk>1.8.0_</jdk>
            <!--
                还可以取反，当jdk的版本号 不是 startWith下面的值时，激活此配置
             -->
            <!-- <jdk>!1.8</jdk> -->
        </activation>
        <dependencies>
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <optional>true</optional>
            </dependency>
        </dependencies>
    </profile>
</profiles>
```

- **根据`os`激活**

```html
<!-- ************** 根据os激活 ************** -->
<profiles>
    <!-- 测试环境 -->
    <profile>
        <id>test-id</id>
        <dependencies>
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>fastjson</artifactId>
                <version>1.2.58</version>
            </dependency>
        </dependencies>
    </profile>

    <!-- 生产环境 -->
    <profile>
        <id>prod-id</id>
        <activation>
            <!--
                当操作系统满足下述所有条件时，激活
                注: 如果有多个条件，那么必须所有条件都满足时，才会激活对应的profile
             -->
            <os>
                <!-- 操作系统名，如【Windows 10】 -->
                <name>Windows 10</name>
                <!-- 操作系统隶属， 如【windows】、【unix】 -->
                <family>windows</family>
                <!-- 操作系统的体系结构，如【amd64】 -->
                <arch>amd64</arch>
                <!-- 操作系统版本号，如【10.0】 -->
                <version>10.0</version>
            </os>
        </activation>
        <dependencies>
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <optional>true</optional>
            </dependency>
        </dependencies>
    </profile>
</profiles>
```

注：对于os的基本信息，我们可以这样获取：

```java
/**
 * 获取操作系统信息
 *
 * @date 2019/9/4 11:21
 */
private static void osInfo() {
    Properties osProperties = System.getProperties();
    // name-操作系统名，如【Windows 10】
    System.out.println(osProperties.get("os.name"));

    // family-操作系统隶属， 如【windows】、【unix】, 可根据name来获取
    System.out.println(
            osProperties.get("os.name").toString().startsWith("Window") ? "windows" : "unix"
    );

    // arch-操作系统的体系结构，如【amd64】
    System.out.println(osProperties.get("os.arch"));

    // version-操作系统版本号，如【10.0】
    System.out.println(osProperties.get("os.version"));
}
```

- **根据`file存在与否`激活**

```html
<!-- ************** 根据file存在与否激活 ************** -->
<profiles>
    <!-- 测试环境 -->
    <profile>
        <id>test-id</id>
        <dependencies>
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>fastjson</artifactId>
                <version>1.2.58</version>
            </dependency>
        </dependencies>
    </profile>

    <!-- 生产环境 -->
    <profile>
        <id>prod-id</id>
        <!--
            根据文件存在与否， 来激活profile

            注:文件路径可以是绝对路径，也可以是相对路径(相对pom.xml的路径)。

            注:在exists标签里，如果写绝对路径，不要使用${project.basedir}或
               ${pom.basedir};经本人测试，在exists标签里使用${}取不到值。

            注:missing与exists最好不要同时使用。 如果同时使用的话，missing就
               会形同虚设，是否激活此profile完全由exists决定,。
        -->
        <activation>
            <file>
                <!-- 相对路径示例 -->
                <exists>src/main/resources/xyza.yml</exists>
                <!-- <missing>src/main/resources/abcd.yml</missing> -->

                <!-- 绝对路径示例 -->
                <!-- <exists>C:/Users/JustryDeng/Desktop/zx-lw.log</exists> -->
                <!-- <missing>/usr/apps/file/info.properties</missing> -->
            </file>
        </activation>
        <dependencies>
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <optional>true</optional>
            </dependency>
        </dependencies>
    </profile>
</profiles>
```

- **根据`maven指令-D参数`激活**

```html
<!-- ************** 根据 maven指令参数-D 激活 ************** -->
<profiles>
    <!-- 测试环境 -->
    <profile>
        <id>test-id</id>
        <dependencies>
            <dependency>
                <groupId>com.alibaba</groupId>
                <artifactId>fastjson</artifactId>
                <version>1.2.58</version>
            </dependency>
        </dependencies>
    </profile>

    <!-- 生产环境 -->
    <profile>
        <id>prod-id</id>
        <activation>
            <!--
                maven指令参数-D激活
                注:与根据maven指令参数-P 指定profile的id进行激活类似
            -->
            <property>
                <name>pk</name>
                <value>pv</value>
            </property>
        </activation>
        <dependencies>
            <dependency>
                <groupId>org.projectlombok</groupId>
                <artifactId>lombok</artifactId>
                <optional>true</optional>
            </dependency>
        </dependencies>
    </profile>
</profiles>
```

打包时通过maven指令-D，指定激活哪一个profile：

![img](../repository/20190904140126401.png)

如果想激活多个profile，可以这样：

![img](../repository/20190904140136142.png)

> 提示：除了在项目的pom.xml中可以对profile进行配置激活外，还可以在当前用户下的.m2/settings.xml文件里，对profile进行配置激活;还可以在maven安装目录下的conf/settings.xml文件里，对profile进行配置激活。上述三者的区别是:在项目的pom.xml中配置profile,那么配置的profile只对该项目有效；在用户下的.m2/settings.xml文件里配置profile,那么配置的profile只对该用户下的(所有)项目有效；在maven安装目录下的conf/settings.xml文件里配置profile,那么配置的profile对使用该maven程序的(所有)项目有效。
>
> 感兴趣的话，可以自行去尝试。

------

## 相关资料

- [<font face="幼圆"  color = "#86CA5E">**demo代码下载**</font>](https://gitee.com/JustryDeng/shared-files/raw/master/maven-profile.rar)
- [`https://maven.apache.org/pom.html#Version_Order_Specification`](https://maven.apache.org/pom.html#Version_Order_Specification)
- [`http://c.biancheng.net/view/5286.html`](http://c.biancheng.net/view/5286.html)
