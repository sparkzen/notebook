# logback使用filter过滤日志

- [logback使用filter过滤日志](#logback使用filter过滤日志)
  - [<font  face="幼圆" color = "#3399EA">过滤器简介</font>](#font--face幼圆-color--3399ea过滤器简介font)
  - [<font  face="幼圆" color = "#3399EA">过滤器的FilterReply状态枚举</font>](#font--face幼圆-color--3399ea过滤器的filterreply状态枚举font)
  - [<font  face="幼圆" color = "#3399EA">过滤器的使用(示例)</font>](#font--face幼圆-color--3399ea过滤器的使用示例font)
    - [<font  face="幼圆" color = "#86CA5E">使用LevelFilter的logback.xml(示例)</font>](#font--face幼圆-color--86ca5e使用levelfilter的logbackxml示例font)
    - [<font  face="幼圆" color = "#86CA5E">使用ThresholdFilter的logback.xml(示例)</font>](#font--face幼圆-color--86ca5e使用thresholdfilter的logbackxml示例font)
    - [<font  face="幼圆" color = "#86CA5E">使用EvaluatorFilter的logback.xml(示例)</font>](#font--face幼圆-color--86ca5e使用evaluatorfilter的logbackxml示例font)
    - [<font  face="幼圆" color = "#86CA5E">使用MDCFilter的logback.xml(示例)</font>](#font--face幼圆-color--86ca5e使用mdcfilter的logbackxml示例font)
    - [<font  face="幼圆" color = "#86CA5E">使用DynamicThresholdFilter的logback.xml(示例)</font>](#font--face幼圆-color--86ca5e使用dynamicthresholdfilter的logbackxml示例font)
  - [<font  face="幼圆" color = "#3399EA">自定义过滤器</font>](#font--face幼圆-color--3399ea自定义过滤器font)
    - [<font  face="幼圆" color = "#86CA5E">自定义Filter</font>](#font--face幼圆-color--86ca5e自定义filterfont)
    - [<font  face="幼圆" color = "#86CA5E">自定义TurboFilter</font>](#font--face幼圆-color--86ca5e自定义turbofilterfont)
  - [<font  face="幼圆" color = "#3399EA">相关资料</font>](#font--face幼圆-color--3399ea相关资料font)

---
## <font  face="幼圆" color = "#3399EA">过滤器简介</font>
- 简介

  &emsp;&emsp;`logback`具有过滤器支持。**`logbcak`允许给日志记录器appender配置一个或多个Filter(或者给整体配置一个或多个TurboFilter)，来控制：当满足过滤器指定的条件时，才记录日志(或不满足条件时，拒绝记录日志)**。`logback`支持自定义过滤器，当然`logback`也自带了一些常用的过滤器，在绝大多数时候，自带的过滤器其实就够用了，一般是不需要自定义过滤器的。

- `logback`提供的过滤器支持主要分两大类
  
  - `ch.qos.logback.core.filter.Filter`

    ![在这里插入图片描述](../repository/20200818123002240.png)

  - `ch.qos.logback.classic.turbo.TurboFilter`
  
    ![在这里插入图片描述](../repository/20200818123019106.png)
  
- `Filter`与`TurboFilter`自带的几种常用过滤器

|          过滤器          |     来源      |                             说明                             | 相对常用 |
| :----------------------: | :-----------: | :----------------------------------------------------------: | :------: |
|      `LevelFilter`       |    Filter     | 对指定level的日志进行记录(或不记录)，对不等于指定level的日志不记录(或进行记录) |    是    |
|    `ThresholdFilter`     |    Filter     | 对大于或等于指定level的日志进行记录(或不记录)，对小于指定level的日志不记录(或进行记录)<br/>提示：info级别是大于debug的 |    是    |
|    `EvaluatorFilter`     |    Filter     | 对满足指定表达式的日志进行记录(或不记录)，对不满足指定表达式的日志不作记录(或进行记录) |    是    |
|       `MDCFilter`        | `TurboFilter` |   若`MDC`域中存在指定的key-value，则进行记录，否者不作记录   |    是    |
| `DuplicateMessageFilter` | `TurboFilter` |                根据配置不记录多余的重复的日志                |    是    |
| `DynamicThresholdFilter` | `TurboFilter` | 动态版的`ThresholdFilter`，根据`MDC`域中是否存在某个键，该键对应的值是否相等，可实现日志级别动态切换<br/>示例：`<turboFilter class="ch.qos.logback.classic.turbo.DynamicThresholdFilter"><DefaultThreshold>ERROR</DefaultThreshold><OnHigherOrEqual>ACCEPT</OnHigherOrEqual><OnLower>NEUTRAL</OnLower><Key>userRole</Key><MDCValueLevelPair><value>admin</value><level>DEBUG</level></MDCValueLevelPair><MDCValueLevelPair><value>dba</value><level>INFO</level></MDCValueLevelPair><MDCValueLevelPair><value>user</value><level>WARN</level></MDCValueLevelPair></turboFilter>`此时，作用有以下几种情况：<br/>情况一：`MDC`域中不存在对应的键`userRole`，或者存在对应的 键`userRole`，但是其值不等于任何一个`MDCValueLevelPair`中的`value`值。那么此时会以`${DefaultThreshold}`为界线，大于等于该级别的日志将会被`${OnHigherOrEqual}`，小于该级别的日志将会被`${OnLower}`<br/>情况二：`MDC`域中存在对应的键`userRole`，且其对应的值等于其中一个`MDCValueLevelPair`中的`value`值。那么此时会以该`MDCValueLevelPair`中的`${level}`为界线，大于等于该级别的日志将会被`${OnHigherOrEqual}`，小于该级别的日志将会被`${OnLower}` |    是    |
|      `MarkerFilter`      | `TurboFilter` |        针对带有指定标记的日志，进行记录（或不作记录）        |    否    |
|           ...            |      ...      |                             ...                              |   ...    |

- `若过滤器已经返回了需要记录，那么就一定会对该日志进行记录(不论当前日志的level是否大于等于系统设置的最低日志级别)`。
- `TurboFilter的性能是优于Filter的，这是因为TurboFilter的作用时机是在创建日志事件ILoggingEvent对象之前，而Filter的作用时机是在创建之后`。若一个日志注定是会被过滤掉不记录的，那么创建`ILoggingEvent`对象(包括后续的参数组装方法调用等)这个步骤无疑是非常消耗性能的。

---

## <font  face="幼圆" color = "#3399EA">过滤器的FilterReply状态枚举</font>
对于`Filter`而言，需不需要记录日志，取决于`ch.qos.logback.core.filter.Filter#decide`方法的返回：
![在这里插入图片描述](../repository/20200818124044229.png)
对于`TurboFilter`而言，需不需要记录日志，取决于`ch.qos.logback.classic.turbo.TurboFilter#decide`方法的返回：
![在这里插入图片描述](../repository/20200818124055858.png)
可以看到，返回的都是`FilterReply`这个枚举：

![在这里插入图片描述](../repository/202008181241076.png)

- `FilterReply`有三种枚举值：
  - `DENY`：表示不用看后面的过滤器了，这里就给拒绝了，不作记录。
  - `NEUTRAL`：表示需不需要记录，还需要看后面的过滤器。若所有过滤器返回的全部都是NEUTRAL，那么需要记录日志。
  - `ACCEPT`：表示不用看后面的过滤器了，这里就给直接同意了，需要记录。

---

## <font  face="幼圆" color = "#3399EA">过滤器的使用(示例)</font>

### <font  face="幼圆" color = "#86CA5E">使用LevelFilter的logback.xml(示例)</font>

![在这里插入图片描述](../repository/20200818124401309.png)

**测试一下：**

- 编写测试方法：

  ![在这里插入图片描述](../repository/20200818124521617.png)

- 运行方法，产出日志：

  ![在这里插入图片描述](../repository/20200818124530705.png)

### <font  face="幼圆" color = "#86CA5E">使用ThresholdFilter的logback.xml(示例)</font>

![在这里插入图片描述](../repository/20200818124606763.png)

**测试一下：**
- 编写测试方法：

  ![在这里插入图片描述](../repository/20200818124616271.png)

- 运行方法，产出日志：

  ![在这里插入图片描述](../repository/20200818124626693.png)

### <font  face="幼圆" color = "#86CA5E">使用EvaluatorFilter的logback.xml(示例)</font>

- 需要引入额外的解析库依赖`janino`：
	```xml
	<dependency>
	    <groupId>org.codehaus.janino</groupId>
	    <artifactId>janino</artifactId>
	    <version>3.1.2</version>
	</dependency>
	```
	
- logbaxk.xml

  ![在这里插入图片描述](../repository/20200818124751385.png)



**测试一下：**

- 编写测试方法：

  ![在这里插入图片描述](../repository/20200818124802822.png)

- 运行方法，产出日志：

  ![在这里插入图片描述](../repository/20200818124811907.png)

### <font  face="幼圆" color = "#86CA5E">使用MDCFilter的logback.xml(示例)</font>

![在这里插入图片描述](../repository/20200818124912524.png)

**测试一下：**

- 编写测试方法：

  ![在这里插入图片描述](../repository/20200818124922806.png)

- 运行方法，产出日志：

  ![在这里插入图片描述](../repository/20200818124931322.png)

### <font  face="幼圆" color = "#86CA5E">使用DynamicThresholdFilter的logback.xml(示例)</font>

![在这里插入图片描述](../repository/2020110114111844.png)

**测试一下：**
- 编写测试方法：

  ![在这里插入图片描述](../repository/20201101141154192.png)

- 运行方法，产出日志：

  ![在这里插入图片描述](../repository/20201101141205473.png)



---

## <font  face="幼圆" color = "#3399EA">自定义过滤器</font>
### <font  face="幼圆" color = "#86CA5E">自定义Filter</font>

- 编写自定义Filter

  ![在这里插入图片描述](../repository/20200818125256160.png)

- 在logback.xml中配置使用此过滤器

  ![在这里插入图片描述](../repository/20200818125305792.png)

  

**测试一下：**

- 编写测试方法：

  ![在这里插入图片描述](../repository/20200818125313339.png)

- 运行方法，产出日志：

  ![在这里插入图片描述](../repository/20200818125321765.png)

### <font  face="幼圆" color = "#86CA5E">自定义TurboFilter</font>

- 编写自定义`TurboFilter`

  ![在这里插入图片描述](../repository/2020081812540176.png)

- 在logback.xml中配置使用此过滤器

  ![在这里插入图片描述](../repository/20200818125410525.png)

**测试一下：**

- 编写测试方法：

  ![在这里插入图片描述](../repository/20200818125418896.png)

- 运行方法，产出日志：

  ![在这里插入图片描述](../repository/20200818125432149.png)

---

 <font  face="幼圆" color = "#86CA5E" size = 4 >__logback使用filter过滤日志，初步学习完毕！__</font>

---

## <font  face="幼圆" color = "#3399EA">相关资料</font>

- [<font face="幼圆"  color = "#86CA5E">**demo代码下载**</font>](https://gitee.com/JustryDeng/shared-files/raw/master/logback-filter-demo.rar)
- [`http://logback.qos.ch/manual/filters.html`](http://logback.qos.ch/manual/filters.html)