# log4j2使用filter过滤日志
- [log4j2使用filter过滤日志](#log4j2使用filter过滤日志)
  - [<font  face="幼圆" color = "#3399EA">背景说明</font>](#font--face幼圆-color--3399ea背景说明font)
  - [<font  face="幼圆" color = "#3399EA">Filter.Result的三种过滤结果</font>](#font--face幼圆-color--3399eafilterresult的三种过滤结果font)
  - [<font  face="幼圆" color = "#3399EA">log4j2提供的过滤器(功能简述)</font>](#font--face幼圆-color--3399ealog4j2提供的过滤器功能简述font)
  - [<font  face="幼圆" color = "#3399EA">Filter的作用范围</font>](#font--face幼圆-color--3399eafilter的作用范围font)
  - [<font  face="幼圆" color = "#3399EA">常用过滤器使用示例</font>](#font--face幼圆-color--3399ea常用过滤器使用示例font)
  - [<font  face="幼圆" color = "#3399EA">自定义Filter</font>](#font--face幼圆-color--3399ea自定义filterfont)
  - [<font  face="幼圆" color = "#3399EA">相关资料</font>](#font--face幼圆-color--3399ea相关资料font)

---
## <font  face="幼圆" color = "#3399EA">背景说明</font>
&emsp;&emsp;`log4j2`作为`log4j`的升级版本，其性能自然是大大优于`log4j`的，同时其其性能又是优于`logback`的，甚至在部分领域，`log4j2`的性能远超`logback`几个数量级。`log4j2`的亮点主要在这几个方法进行体现:异步、并发、配置优化、插件机制等。本文初步学习基于`log4j2`插件机制的Filter。

---
## <font  face="幼圆" color = "#3399EA">Filter.Result的三种过滤结果</font>
`log4j2`走过滤器的逻辑后，会返回对应的过滤Result结果，以控制是否记录日志、怎样记录日志。过滤器的结果有：
![在这里插入图片描述](../repository/2020110111515539.png)

- `ACCEPT`：(不需要再走后面的过滤器了，)需要记录当前日志。
- `NEUTRAL`：需不需要记录当前日志，由后续过滤器决定。若所有过滤器返回的结果都是NEUTRAL，那么需要记录日志。
- `DENY`：(不需要再走后面的过滤器了，)不需要记录当前日志。
提示：log4j2的此机制与logback是一样的。

---
## <font  face="幼圆" color = "#3399EA">log4j2提供的过滤器(功能简述)</font>
**提示：** 下图基于log4j2.13.3。
![在这里插入图片描述](../repository/20201101115357603.png)

|          过滤器          |                             说明                             | 是否常用 |
| :----------------------: | :----------------------------------------------------------: | :------: |
|   `StringMatchFilter`    | 如果格式化后(即：最终)的`日志信息中包含${指定的字符串}`，则onMatch，否则onMismatch<br/>即： `msg.contains(this.text) ? onMatch : onMismatch;` |    是    |
|    `LevelRangeFilter`    | 若`${maxLevel} <= 日志级别 <= ${minLevel}`， 则onMatch，否则onMismatch<br/>如： <LevelRangeFilter minLevel="warn" maxLevel="info" onMatch="ACCEPT" onMismatch="DENY"/>即为只记录日志info及warn级别的日志。 |    是    |
|      `RegexFilter`       | 如果`日志信息匹配${指定的正则表达式}`，则onMatch，否则onMismatch<br/>注：可通过useRawMsg属性来控制这个日志信息是格式化处理后(即：最终)的日志信息，还是格式化处理前(即：代码中输入)的日志信息。 |    是    |
|    `ThresholdFilter`     | 若`日志级别 >= ${指定的日志级别}`， 则onMatch，否则onMismatch |    是    |
|    `LevelMatchFilter`    | 如果`日志级别等于${指定的日志级别}`，则onMatch，否则onMismatch |    是    |
| `ThreadContextMapFilter` | 通过context(可以理解为一个Map)中对应的key-value值进行过滤<br/>注：上下文默认是`ThreadContext`，也可以自定义使用`ContextDataInjectorFactory`配置`ContextDataInjector`来指定。 |    是    |
| `DynamicThresholdFilter` | 若上下文中包含指定的key，则触发`DynamicThresholdFilter`生效；若该key对应的value值等于任意一个我们指定的值，那么针对本条日志，可记录日志级别的约束下限调整为指定的级别<br/>注：上下文默认是`ThreadContext`，也可以自定义使用`ContextDataInjectorFactory`配置`ContextDataInjector`来指定。<br/>示例说明：`<DynamicThresholdFilter key="loginRole" defaultThreshold="ERROR" onMatch="ACCEPT" onMismatch="NEUTRAL"><KeyValuePair key="admin" value="DEBUG"/><KeyValuePair key="user"  value="warn"/></DynamicThresholdFilter>`配置，有以下情况：<br/>情况一：存在键`loginRole`，假设从上下文(可以理解为一个Map)中取出来的对应的值为user,那么此时,对于日志级别大于等于warn的日志，会走`onMatch`;其它的日志级别走`onMismatch`。<br/>情况二：存在键`loginRole`，假设从context(可以理解为一个Map)中取出来的对应的值为`admin`,那么此时,对于日志级别大于等于debug的日志，会走`onMatch`;其它的日志级别走`onMismatch`。<br/>情况三：【上下文(可以理解为一个Map)中，不存在键`loginRole`】或【存在键`loginRole`，但从日志上下文中取出来的值(假设)为`abc`, 没有对应的`KeyValuePair`配置】，那么此时`<DynamicThresholdFilter key="userRole" defaultThreshold="AAA" onMatch="BBB" onMismatch="CCC">`等价于`<LevelMatchFilter level="AAA" onMatch="BBB" onMismatch="CCC">`。 |    是    |
|    `CompositeFilter`     | 组合过滤器,即：按照xml配置中的配置，一个过滤器一个过滤器的走，如果在这过程中，任意一个过滤器ACCEPT或DENY了，那么就不会往后走了，直接返回对应的结果。 |    是    |
|       `TimeFilter`       | 如果记录日志时的当前`时间落在每天指定的时间范围[start, end]内`，则onMatch，否则onMismatch<br/>如：`<TimeFilter start="05:00:00" end="05:30:00" onMatch="ACCEPT" onMismatch="DENY"/>`。 |    否    |
|      `ScriptFilter`      |           是否匹配取决于指定的脚本返回值是否为true           |    否    |
|     `DenyAllFilter`      |     This filter causes all logging events to be dropped      |    否    |
|      `BurstFilter`       |      对`低于或等于${指定日志级别}的日志`，进行限流控制       |    否    |
|     `NoMarkerFilter`     | 如果从对应事件对象获取(`LogEvent#getMarker`)到的marker为null， 则`onMatch`，否则`onMismatch` |    否    |
|      `MarkerFilter`      | 如果从对应事件对象获取(`LogEvent#getMarker`)到的`marker的name值为等于${指定的值}`， 则`onMatch`，否则`onMismatch` |    否    |
|       `MapFilter`        | `The MapFilter allows filtering against data elements that are in a MapMessage.`<br/>注：需要使用`org.apache.logging.log4j.Logger`进行记录，且记录`org.apache.logging.log4j.message.MapMessage`日志，才会生效。<br/>注：因为暂时不兼容`Slf4j`这里不多作说明 |    否    |
|  `StructuredDataFilter`  | `The StructuredDataFilter is a MapFilter that also allows filtering on the event id, type and message.`<br/>注：需要使用`org.apache.logging.log4j.Logger`进行记录，且记录`org.apache.logging.log4j.core.filter.StructuredDataFilter`日志，才会生效。<br/>注：因为暂时不兼容`Slf4j`这里不多作说明 |    否    |
|           ...            |                             ...                              |   ...    |

## <font  face="幼圆" color = "#3399EA">Filter的作用范围</font>
&emsp;&emsp;`log4j2`在处理日志时，各个Filter会组成过滤链，越靠前的`Filter`越先过滤，自然影响范围就越大。在`log4j2`的`xml`配置文件中，Filter可以配置在四个位置，由全局到局部依次是 `Context-wide`、 `Logger`和`Appender`、`AppenderReference`， 图示说明：

![在这里插入图片描述](../repository/20201101121255610.png)

---

## <font  face="幼圆" color = "#3399EA">常用过滤器使用示例</font>
- **<font  face="幼圆" color = "#86CA5E">StringMatchFilter</font>**
  
  - log4j2.xml配置
  
    ![在这里插入图片描述](../repository/2020110112184681.png)

  - 测试代码与输出日志
  
    ![在这里插入图片描述](../repository/20201101121859601.png)
  
- **<font  face="幼圆" color = "#86CA5E">LevelRangeFilter</font>**
  
  - log4j2.xml配置
  
    ![在这里插入图片描述](../repository/20201101121911638.png)

  - 测试代码与输出日志
  
    ![在这里插入图片描述](../repository/20201101121922243.png)
  
- **<font  face="幼圆" color = "#86CA5E">RegexFilter</font>**
  - log4j2.xml配置

    ![在这里插入图片描述](../repository/20201101121932360.png)

  - 测试代码与输出日志
  
    ![在这里插入图片描述](../repository/20201101121942606.png)
  
- **<font  face="幼圆" color = "#86CA5E">ThresholdFilter</font>**
  
  - log4j2.xml配置
  
    ![在这里插入图片描述](../repository/20201101121951682.png)

  - 测试代码与输出日志
  
    ![在这里插入图片描述](../repository/20201101122001966.png)
  
- **<font  face="幼圆" color = "#86CA5E">LevelMatchFilter</font>**
  - log4j2.xml配置

    ![在这里插入图片描述](../repository/20201101122017921.png)

  - 测试代码与输出日志
  
    ![在这里插入图片描述](../repository/20201101122029127.png)
  
- **<font  face="幼圆" color = "#86CA5E">ThreadContextMapFilter</font>**
  - **示例一：**
    - log4j2.xml配置

      ![在这里插入图片描述](../repository/20201101122039718.png)

    - 测试代码与输出日志
    
      ![在这里插入图片描述](../repository/2020110112210754.png)
  - **示例二：**
    - log4j2.xml配置

      ![在这里插入图片描述](../repository/20201101122221314.png)
  
    - 测试代码与输出日志
    
      ![在这里插入图片描述](../repository/2020110112223115.png)
  
- **<font  face="幼圆" color = "#86CA5E">DynamicThresholdFilter</font>**
  - log4j2.xml配置

    ![在这里插入图片描述](../repository/20201101122241959.png)

  - 测试代码与输出日志
  
    ![在这里插入图片描述](../repository/20201101142738262.png)
  
- **<font  face="幼圆" color = "#86CA5E">CompositeFilter</font>**
  - log4j2.xml配置

    ![在这里插入图片描述](../repository/20201101122717798.png)

  - 测试代码与输出日志
  
    ![在这里插入图片描述](../repository/20201101122726606.png)


---
## <font  face="幼圆" color = "#3399EA">自定义Filter</font>
一般来说，`log4j2`官方提供的过滤器就足够我们使用了，如果非要自定义过滤器，可以`继承AbstractFilter`，或者直接`实现Filter`，这里不再演示自定义过滤器。

---

## <font  face="幼圆" color = "#3399EA">相关资料</font>

- [<font face="幼圆"  color = "#86CA5E">**demo代码下载**</font>](https://gitee.com/JustryDeng/shared-files/raw/master/log4j2-filter-demo.rar)
- [`https://logging.apache.org/log4j/2.x/manual/filters.html`](https://logging.apache.org/log4j/2.x/manual/filters.html)
- [`http://logging.apache.org/log4j/2.x/log4j-core/apidocs/org/apache/logging/log4j/core/filter/`](http://logging.apache.org/log4j/2.x/log4j-core/apidocs/org/apache/logging/log4j/core/filter/)
