# log4j2-defender实现日志脱敏

- [log4j2-defender实现日志脱敏](#log4j2-defender实现日志脱敏)
  - [介绍](#介绍)
  - [功能特性](#功能特性)
  - [前置依赖](#前置依赖)
  - [使用步骤](#使用步骤)
  - [脱敏配置说明](#脱敏配置说明)
    - [json脱敏器](#json脱敏器)
    - [string脱敏器](#string脱敏器)
    - [正则脱敏器](#正则脱敏器)
    - [自定义脱敏器](#自定义脱敏器)
  - [相关资料](#相关资料)

---

## 介绍

&emsp;&emsp;log4j2-defender是一款基于log4j2实现的无侵入的日志脱敏工具框架，使用此框架，只需要简单的三步。

## 功能特性

1. (默认提供)支持json脱敏器
2. (默认提供)支持string脱敏器
3. (默认提供)支持正则脱敏器
4. 支持自定义脱敏器
5. 支持通过插件实现局部自定义脱敏器（即：轻量的自定义脱敏器）

## 前置依赖

- jdk8+
- log4j2
- spring-boot

## 使用步骤

1. 引入依赖。
    ```xml
    <dependency>
        <groupId>com.idea-aedi</groupId>
        <artifactId>log4j2-defender</artifactId>
        <version>${version}</version>
    </dependency>
    ```
2. 配置application.yml。
   提示：这里只给出了最简单的配置示例，更灵活的用法、更多配置示例，详见下方脱敏配置说明。
    ```yaml
    log:
      defender:
        enable: true # 启用log4j2-defender
        include-logger-prefix: com.ideaaedi.log4j2 # 通过logger所属类的全类名前缀，指定脱敏范围（,多个实用逗号分割）
        opt: default_json # 指定脱敏器
        config-json:
          strategies: # 指定脱敏策略
            com.ideaaedi.log4j2.defender.strategy.SimpleDefenderStrategy__ACCOUNT_NO: account,accountNo
            com.ideaaedi.log4j2.defender.strategy.SimpleDefenderStrategy__EMAIL: email,mail,emailList
            com.ideaaedi.log4j2.defender.strategy.SimpleDefenderStrategy__PHONE_NUMBER: phone,mobile,telphone
    ```
3. 在log4j2配置文件(如：log4j2.xml)中，使用log4j2-defender转换器的占位符`def_msg`或`defender_message`，替代原来的`m`或`msg`或`message`。示例如下：
    - 替换前
    ```xml
    <Property name="pattern">%d{yyyy-MM-dd HH:mm:ss,SSS} %5p %t %c{1}:%L - %msg%n</Property>
    ```
    - 替换后
    ```xml
    <Property name="pattern">%d{yyyy-MM-dd HH:mm:ss,SSS} %5p %t %c{1}:%L - %def_msg%n</Property>
    ```

## 脱敏配置说明

### json脱敏器

- 全量配置及说明
   ```yaml
   ################################# 说明 #################################
   # 1.下述为全量配置示例说明,使用时按需配置即可                              #
   #######################################################################
   
   log:
     defender:
       enable: true # 启用脱敏器
       debug: true # 开启调试模式，(当脱敏器本身出现异常时，)以便打印错误日志
       include-logger-prefix: com.ideaaedi.log4j2.defender,aaa,bbb,ccc # 通过(logger所属类的全类名)前缀，定位要脱敏的日志
       exclude-logger-prefix: com.ideaaedi.log4j2.defender.TmpApplication,xxx,yyy,zzz # 通过(logger所属类的全类名)前缀，指定不需要脱敏的日志
       opt: default_json # 脱敏模型，支持 default_json-基于json脱敏、default_string-基于字符串脱敏、default_regex-基于正则脱敏、custom-自定义脱敏器脱敏
       config-json: # 当基于default_json脱敏时，需配置strategies
         hit-string-value-is-json: true # 当k-v中v本身是字符串，但是长json样时，是否继续对长json样的字符串进行脱敏
         strategies: # 指定脱敏类别及该类别下的keys
           # 指定脱敏类别的方式，方式一[对枚举值]：  {实现了DefenderStrategy接口的枚举类全类名}__{枚举item}
           com.ideaaedi.log4j2.defender.strategy.SimpleDefenderStrategy__ACCOUNT_NO: account,accountNo
           com.ideaaedi.log4j2.defender.strategy.SimpleDefenderStrategy__EMAIL: email,mail,emailList
           com.ideaaedi.log4j2.defender.strategy.SimpleDefenderStrategy__PHONE_NUMBER: phone,mobile,telphone
           # 指定脱敏类别的方式，方式二[对注册到spring中的bean]：  {实现了DefenderStrategy并且注册到spring容器中的bean的name}
           demoDefenderStrategy: name,chineseName,englishName
         # 短路插件机制：支持对部分日志定制化脱敏
         # 注: 比起自定义脱敏器，插件更轻量化
         # 注：若日志脱敏走了插件，那么是不会再走脱敏器的(详见AbstractLog4j2MessageDefender#desensitize)
         short-circuit-plugins: pluginOne,pluginTwo #值为 {实现了Log4j2MessageDefender并且注册到spring容器中的bean的name}
   ```

- 其中，枚举策略实现示例：
   ```java
   public enum SimpleDefenderStrategy implements DefenderStrategy {
   
       /** 姓名类脱敏策略 */
       NAME("name", 1, 1, '*'),
   
       /** 账号类脱敏策略 */
       ACCOUNT_NO("accountNo", 2, 2, '*'),
   
       /** 邮箱类脱敏策略 */
       EMAIL("email", 2, 7, '*'),
   
       /** 身份证号类脱敏策略 */
       ID_CARD("idCard", 6, 4, '*'),
   
       /** 手机号码类脱敏策略 */
       PHONE_NUMBER("phoneNumber", 3, 4, '*'),
   
       /** 住址类脱敏策略 */
       ADDRESS("address", 3, 4, '*');
   
       // 省略...
   }
   ```

- 其中，自定义脱敏策略demoDefenderStrategy的实现示例：
   ```java
   @Component("demoDefenderStrategy")
   public class DemoDefenderStrategy implements DefenderStrategy{
       @Override
       public String category() {
           return "name";
       }
       
       @Override
       public int retainPrefixCount() {
           return 1;
       }
       
       @Override
       public int retainSuffixCount() {
           return 1;
       }
       
       @Override
       public char replaceChar() {
           return '$';
       }
   }
   ```

- 其中，插件pluginOne实现示例：
   ```java
   @Component
   public class PluginOne implements Log4j2MessageDefender {
       
       public static final String HANDLE_BY_PLUGIN = "PluginOneAbc";
       
       @Override
       public boolean support(LogEvent event) {
           return event.getContextData().containsKey(HANDLE_BY_PLUGIN);
       }
       
       @Override
       public void desensitize(LogEvent event, Message message, StringBuilder buffer) {
           buffer.append("[O_O] ").append(message.getFormattedMessage()).append(" [O_O]");
       }
   }
   ```

### string脱敏器

- 全量配置及说明
   ```yaml
   ################################# 说明 #################################
   # 1.下述为全量配置示例说明,使用时按需配置即可                             #
   #######################################################################
   
   log:
     defender:
       enable: true # 启用脱敏器
       debug: true # 开启调试模式，(当脱敏器本身出现异常时，)以便打印错误日志
       include-logger-prefix: com.ideaaedi.log4j2.defender,aaa,bbb,ccc # 通过(logger所属类的全类名)前缀，定位要脱敏的日志
       exclude-logger-prefix: com.ideaaedi.log4j2.defender.TmpApplication,xxx,yyy,zzz # 通过(logger所属类的全类名)前缀，指定不需要脱敏的日志
       opt: default_string # 脱敏模型，支持 default_json-基于json脱敏、default_string-基于字符串脱敏、default_regex-基于正则脱敏、custom-自定义脱敏器脱敏
       config-string: # 当基于default_STRING脱敏时，需配置strategies
         compat-backslash: true # 是否兼容反斜杠 \
         strategies: # 指定脱敏类别及该类别下的keys
           # 指定脱敏类别的方式，方式一[对枚举值]：  {实现了DefenderStrategy接口的枚举类全类名}__{枚举item}
           com.ideaaedi.log4j2.defender.strategy.SimpleDefenderStrategy__ACCOUNT_NO: account,accountNo
           com.ideaaedi.log4j2.defender.strategy.SimpleDefenderStrategy__EMAIL: email,mail,emailList
           com.ideaaedi.log4j2.defender.strategy.SimpleDefenderStrategy__PHONE_NUMBER: phone,mobile,telphone
           # 指定脱敏类别的方式，方式二[对注册到spring中的bean]：  {实现了DefenderStrategy并且注册到spring容器中的bean的name}
           demoDefenderStrategy: name,chineseName,englishName
         # 短路插件机制：支持对部分日志定制化脱敏
         # 注: 比起自定义脱敏器，插件更轻量化
         # 注：若日志脱敏走了插件，那么是不会再走脱敏器的(详见AbstractLog4j2MessageDefender#desensitize)
         short-circuit-plugins: pluginOne,pluginTwo #值为 {实现了Log4j2MessageDefender并且注册到spring容器中的bean的name}
         key-value-delimiter: COLON,EQUAL,ARROW # 指定key-value之间的连接符 可以从(:、=、->)这三个中选
   ```
- 其中，枚举策略实现示例：同json脱敏器中的示例。
- 其中，自定义脱敏策略demoDefenderStrategy的实现示例：同json脱敏器中的示例。
- 其中，插件pluginOne实现示例：同json脱敏器中的示例。

### 正则脱敏器

- 全量配置及说明
   ```yaml
   ################################# 说明 #################################
   # 1.下述为全量配置示例说明,使用时按需配置即可                             #
   #######################################################################
   
   log:
     defender:
       enable: true # 启用脱敏器
       debug: true # 开启调试模式，(当脱敏器本身出现异常时，)以便打印错误日志
       include-logger-prefix: com.ideaaedi.log4j2.defender,aaa,bbb,ccc # 通过(logger所属类的全类名)前缀，定位要脱敏的日志
       exclude-logger-prefix: xxx,yyy,zzz # 通过(logger所属类的全类名)前缀，指定不需要脱敏的日志
       opt: default_regex # 脱敏模型，支持 default_json-基于json脱敏、default_string-基于字符串脱敏、default_regex-基于正则脱敏、custom-自定义脱敏器脱敏
       config-regex:
         strategyProvider: demoPatternStrategyProvider #值为 {实现了RegexDefenderStrategyProvider并且注册到spring容器中的bean的name}
         short-circuit-plugins: pluginOne,pluginTwo #值为 {实现了Log4j2MessageDefender并且注册到spring容器中的bean的name}
   ```

- 其中，strategyProvider的实现示例：
   ```java
   @Component
   public class DemoPatternStrategyProvider implements RegexDefenderStrategyProvider {
           
       private static final Pattern ACCOUNT_PATTERN = Pattern.compile("account\\s*=\\s*[a-zA-Z0-9\u4e00-\u9fa5!@#$+%^&*(){}|:\"?.<>/'\\\\\\-_`。；！]*");
           
       private static final Pattern EMAIL_PATTERN = Pattern.compile("mail\\s*=\\s*[a-zA-Z0-9\u4e00-\u9fa5!@#$+%^&*(){}|:\"?.<>/'\\\\\\-_`。；！]*");
     
       private static final Pattern NAME_PATTERN = Pattern.compile("name\\s*=\\s*[a-zA-Z0-9\u4e00-\u9fa5!@#$+%^&*()|:\"?.<>/'\\\\\\-_`。；！]*");
   
       @Override
       public Map<Pattern, DefenderStrategy> provideRegexStrategyMap() {
           Map<Pattern, DefenderStrategy> map = new HashMap<>(8);
           map.put(ACCOUNT_PATTERN, SimpleDefenderStrategy.ACCOUNT_NO);
           map.put(EMAIL_PATTERN, SimpleDefenderStrategy.EMAIL);
           map.put(NAME_PATTERN, SimpleDefenderStrategy.NAME);
           return map;
       }
   }
   ```

### 自定义脱敏器

- 全量配置及说明
   ```yaml
   ################################# 说明 #################################
   # 1.下述为全量配置示例说明,使用时按需配置即可                             #
   #######################################################################
   
   log:
     defender:
       enable: true # 启用脱敏器
       debug: true # 开启调试模式，(当脱敏器本身出现异常时，)以便打印错误日志
       include-logger-prefix: com.ideaaedi.log4j2.defender,aaa,bbb,ccc # 通过(logger所属类的全类名)前缀，定位要脱敏的日志
       exclude-logger-prefix: xxx,yyy,zzz # 通过(logger所属类的全类名)前缀，指定不需要脱敏的日志
       opt: custom # 脱敏模型，支持 default_json-基于json脱敏、default_string-基于字符串脱敏、default_regex-基于正则脱敏、custom-自定义脱敏器脱敏
       config-custom:
         bean: myLog4j2MessageDefender #值为 {实现了Log4j2MessageDefender并且注册到spring容器中的bean的name}
   ```

- 其中，bean的实现示例：
   ```java
   @Component
   public class MyLog4j2MessageDefender implements Log4j2MessageDefender{
       
       @Override
       public boolean support(LogEvent event) {
           return true;
       }
       
       @Override
       public void desensitize(LogEvent event, Message message, StringBuilder buffer) {
           String formattedMessage = message.getFormattedMessage();
           // some logic
           buffer.append(formattedMessage);
       }
   }
   ```

---

## 相关资料

- [官网详见Gitee](https://gitee.com/JustryDeng/log4j2-defender)