# CentOS7安装Nginx及相关指令

- [CentOS7安装Nginx及相关指令](#centos7安装nginx及相关指令)
  - [CentOS7安装Nginx](#centos7安装nginx)
    - [第一步：下载软件包](#第一步下载软件包)
    - [第二步：安装依赖](#第二步安装依赖)
    - [第三步：安装Nginx](#第三步安装nginx)
    - [第四步（可选）：修改nginx配置文件](#第四步可选修改nginx配置文件)
    - [第五步：启动&停止](#第五步启动停止)
  - [更多命令](#更多命令)
  - [以systemd的方式启动停止NG](#以systemd的方式启动停止ng)
    - [配置systemd方式管理NG](#配置systemd方式管理ng)
    - [启动停止NG](#启动停止ng)
  - [相关资料](#相关资料)

## CentOS7安装Nginx

### 第一步：下载软件包

```shell
cd /opt/
wget http://nginx.org/download/nginx-1.22.0.tar.gz
```

### 第二步：安装依赖

```shell
yum -y install gcc pcre-devel zlib-devel openssl openssl-devel
```

### 第三步：安装Nginx

```shell
#解压
tar -zxvf nginx-1.22.0.tar.gz

#进入NG目录
cd ./nginx-1.22.0

#配置
./configure --prefix=/usr/local/nginx

#编译
make
make install
```

### 第四步（可选）：修改nginx配置文件

> 注：`/usr/local/nginx/`是nginx的默认安装位置，我们修改配置文件都是去安装位置修改，而不是去下载位置修改

```shell
cd /usr/local/nginx/conf
vim nginx.conf
```

### 第五步：启动&停止

```shell
#启动
/usr/local/nginx/sbin/nginx

#查看nginx状态
ps aux | grep nginx

#重新加载配置
/usr/local/nginx/sbin/nginx -s reload

#停止
/usr/local/nginx/sbin/nginx -s stop
```

## 更多命令

- 以特定目录下的配置文件启动：`/usr/local/nginx/sbin/nginx -c /特定目录/nginx.conf`
- 重新加载配置：`/usr/local/nginx/sbin/nginx -s reload`，执行这个命令后，master进程会等待worker进程处理完当前请求，然后根据最新配置重新创建新的worker进程，完成Nginx配置的热更新
- 立即停止服务：`/usr/local/nginx/sbin/nginx -s stop`
- 从容停止服务：`/usr/local/nginx/sbin/nginx -s quit`，执行该命令后，Nginx在完成当前工作任务后再停止。
- 检查配置文件是否正确：`/usr/local/nginx/sbin/nginx -t`
- 检查特定目录的配置文件是否正确：`/usr/local/nginx/sbin/nginx -t -c /特定目录/nginx.conf`
- 查看版本信息：`/usr/local/nginx/sbin/nginx -v`

注：`/usr/local/nginx/`即为nginx的安装目录

## 以systemd的方式启动停止NG

### 配置systemd方式管理NG

```shell
vim /etc/systemd/system/nginx.service
```

```shell
[Unit]
Description=The Nginx HTTP Server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/usr/local/nginx/logs/nginx.pid
ExecStart=/usr/local/nginx/sbin/nginx
ExecReload=/usr/local/nginx/sbin/nginx -s reload
ExecStop=/usr/local/nginx/sbin/nginx -s stop
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

### 启动停止NG

```shell
#（启动nginx服务）
systemctl start nginx.service
#（停止nginx服务）
systemctl stop nginx.service
#（查看服务当前状态）
systemctl status nginx.service
#（设置开机自启动）
systemctl enable nginx.service
#（停止开机自启动）
systemctl disable nginx.service
#（重新启动服务）
systemctl restart nginx.service
#（查看所有已启动的服务）
systemctl list-units --type=service
```

## 相关资料

- [CSDN](https://blog.csdn.net/wdy0078/article/details/121902965)