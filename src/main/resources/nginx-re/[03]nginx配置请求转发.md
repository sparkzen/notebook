# nginx配置请求转发

- [nginx配置请求转发](#nginx配置请求转发)
  - [简述](#简述)
    - [方式一（最常用）：proxy_pass](#方式一最常用proxy_pass)
      - [核心](#核心)
      - [使用示例](#使用示例)
    - [方式二：return](#方式二return)
      - [核心](#核心-1)
      - [示例](#示例)
    - [方式三：return](#方式三return)
      - [核心](#核心-2)
      - [示例](#示例-1)
  - [相关资料](#相关资料)

## 简述

> 提示：配置完成后，需要重新加载nginx配置（或重启nginx）才能生效

### 方式一（最常用）：proxy_pass

#### 核心

![图片1](../repository/图片1-1657684110551.png)

#### 使用示例

- 示例一：基础用法

  ```shell
  server {
      listen      80;
      server_name chat.paas.scorpio.uat.newtank.cn;
      
       # 转发请求到 http://www.example.com
      location / {
          proxy_pass http://www.example.com;
      }
  }
  ```

- 示例二：（除地址端口外转发的目标地址无url的）绝对转发

  ```shell
  server {
      listen      80;
      server_name www.test.com;
      
      # 当访问 http://test.yeguxin.top/proxy/aaa/bbb.text时,nginx匹配到 /proxy/路径,把请求转发给127.0.0.1:8080服务.
      # 实际请求代理服务器路径为 " 127.0.0.1:8080/proxy/aaa/bbb.text "
      location /proxy/ {
           # 不以/结尾
           proxy_pass http://127.0.0.1:8080;
      }
  }
  ```

- 示例三：（除地址端口外转发的目标地址无url的）相对转发

  ```shell
  server {
      listen      80;
      server_name www.test.com;
      
      # 当访问 http://test.yeguxin.top/proxy/aaa/bbb.text时,nginx匹配到 /proxy/路径,把请求转发给127.0.0.1:8080服务.
      # 实际请求代理服务器路径为 " 127.0.0.1:8080/aaa/bbb.text "
      location /proxy/ {
           # 以/结尾
           proxy_pass http://127.0.0.1:8080/;
      }
  }
  ```

- 示例四：（除地址端口外转发的目标地址有url的）转发

  ```shell
  server {
      listen      80;
      server_name www.test.com;
      
      # 当访问 http://test.yeguxin.top/proxy/aaa/bbb.text时,nginx匹配到 /proxy/路径,把请求转发给127.0.0.1:8080服务.
      # 实际请求代理服务器路径为 " 127.0.0.1:8080/static/aaa/bbb.text "
      location /proxy/ {
           proxy_pass http://127.0.0.1:8080/static/;
      }
  }
  ```

  ```shell
  server {
      listen      80;
      server_name www.test.com;
      
      # 当访问 http://test.yeguxin.top/proxy/aaa/bbb.text时,nginx匹配到 /proxy/路径,把请求转发给127.0.0.1:8080服务.
      # 实际请求代理服务器路径为 " 127.0.0.1:8080/staticaaa/bbb.text "
      location /proxy/ {
           proxy_pass http://127.0.0.1:8080/static;
      }
  }
  ```

### 方式二：return

#### 核心

![图片2](../repository/图片2.png)

#### 示例

```shell
# 下面代码中，listen 指令表明 server 块同时用于 HTTP流量。
# server_name 指令匹配包含域名 ‘www.old-name.com’ 的请求。return 指令告诉 Nginx 停止处理请求，直接返回 301 (Moved Permanently) 代码和指定的重写过的 URL 到客户端。
# $scheme 是协议（HTTP 或 HTTPS），$request_uri 是包含参数的完整的 URI。 
server{
    listen         80;
    server_name www.old-name.com;
    
    # return 指令的第一个参数是响应码。第二个参数可选，可以是重定向的 URL
    # location 和 server 上下文中都可以使用 return 指令。
    return 301 $scheme://www.new-name.com$request_uri;
}
```

### 方式三：return

#### 核心

![图片3](../repository/图片3.png)

#### 示例

```shell
server {
    listen      80;
    server_name chat.paas.scorpio.uat.newtank.cn;
    
    # 第一个参数 regex 是正则表达式。
    rewrite ^(.*)$  https://$host$1 permanent;
}
```

## 相关资料

- [CSDN](https://blog.csdn.net/yeguxin/article/details/94020476)