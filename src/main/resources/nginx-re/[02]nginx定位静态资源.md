# nginx定位静态资源

- [nginx定位静态资源](#nginx定位静态资源)
  - [核心](#核心)
  - [示例](#示例)

## 核心

![图片1](../repository/图片1.png)

## 示例

```shell
worker_processes  1;


events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    sendfile        on;

    keepalive_timeout  65;

    server {
        listen       80;
        server_name cps.45wan.com;

        # 设置当访问 ip:端口/static-resources/xxx时， 映射到  /apps/files/xxx
        location ^~ /static-resources/ {
            alias  /apps/files/;
            # 如果希望访问静态资源时，静态直接作为一个附件被浏览器下载(而不是作为一个文件被浏览器打开)，那么把此功能启用
            # 注: 当访问一些浏览器支持打开的文件时，浏览器优先选择打开而不是下载。只有当url指向浏览器不支持打开的文件时，浏览器才是下载。
            #if ($request_filename ~* ^.*?\.(txt|doc|pdf|rar|gz|zip|docx|exe|xlsx|ppt|pptx|jpg|png)$){
            #     add_header Content-Disposition "attachment";
            #}
            expires 30d;
        }

        location / {
            root   html;
            index  index.html index.htm;
        }

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

    }

}
```

