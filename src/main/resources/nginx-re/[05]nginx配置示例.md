# nginx配置示例

- [nginx配置示例](#nginx配置示例)
  - [示例](#示例)

## 示例

```shell
#user  nobody;
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;
    server {
        listen       80;
        server_name  localhost;
        # HTTP重定向至HTTPS（301-代表永久性转移； 302-代表暂时性转移）
        return 301 https://47.93.61.185;
    }
    
    server {
        # 因为端口配置一样，所以可以直接在一个server中写多个listen；如果端口配置不一样，那么需要拆开写多个server
        listen       8182 ssl;
        listen       443 ssl;
        server_name  localhost;

        # ssl证书配置（相对当前nginx.cong文件的路径）
        ssl_certificate     ssl/pension.crt;
        ssl_certificate_key ssl/pension.key;
        ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
        ssl_ciphers         HIGH:!aNULL:!MD5; 
        
        # 转发真实ip地址
        proxy_set_header HTTP_X_FORWARDED_FOR $remote_addr;

        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }
        
        location / {
            root   /usr/local/nginx/pension-web;
            index  index.html index.htm;
        }

        # 设置当访问 ip:端口/static-resources/xxx时， 映射到  /apps/files/xxx
        location ^~ /static-resources/ {
            alias  /apps/files/;
            # 如果希望访问静态资源时，静态直接作为一个附件被浏览器下载(而不是作为一个文件被浏览器打开)，那么把此功能启用
            # 注: 当访问一些浏览器支持打开的文件时，浏览器优先选择打开而不是下载。只有当url指向浏览器不支持打开的文件时，浏览器才是下载。
            #if ($request_filename ~* ^.*?\.(txt|doc|pdf|rar|gz|zip|docx|exe|xlsx|ppt|pptx|jpg|png)$){
            #     add_header Content-Disposition "attachment";
            #}
            # 设置页面资源在浏览器缓存的时间（在指定时间内再次访问该静态资源，将不再像nginx发送请求，而是直接从浏览器缓存中获取）
            expires 6h;
        }
        
        # 当访问 http://test.yeguxin.top/api/aaa/bbb.text时,nginx匹配到 /api/路径,把请求转发给127.0.0.1:10002服务.
        # 实际请求代理服务器路径为 "127.0.0.1:10002/aaa/bbb.text"
        location /api/ {
            # 以/结尾
            proxy_pass   http://127.0.0.1:10002/;
        }
    
        # 当访问 http://test.yeguxin.top/dash/aaa/bbb.text时,nginx匹配到 /dash/路径,把请求转发给127.0.0.1:10003服务.
        # 实际请求代理服务器路径为 " 127.0.0.1:10003/dash/aaa/bbb.text "
        location /dash/ {
            # 不以/结尾
            proxy_pass   http://127.0.0.1:10003;
        } 
    }

}
```

