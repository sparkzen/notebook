# nginx实现用户名密码验证

- [nginx实现用户名密码验证](#nginx实现用户名密码验证)
  - [nginx实现用户名密码验证](#nginx实现用户名密码验证-1)
    - [第一步：生成账号密码文件](#第一步生成账号密码文件)
    - [第二步：配置nginx.conf](#第二步配置nginxconf)
  - [相关资料](#相关资料)

## nginx实现用户名密码验证

### 第一步：生成账号密码文件

```shell
# 安装工具
yum -y install httpd

# 创建账号并指定密码
htpasswd -c /usr/local/nginx/passwd admin

# 然后根据提示输入两次密码
New password:
Re-type new password:
# 结束后就会在指定的位置生成一个密钥文件(这里体现即为/usr/local/nginx/passwd文件)
```

### 第二步：配置nginx.conf

> 提示：配置完成后，需要重新加载nginx配置（或重启nginx）才能生效

```shell
auth_basic "提示信息";
# 指定生成的账号密钥文件的位置
auth_basic_user_file /usr/local/nginx/passwd;
```

![图片4](../repository/图片4.png)

## 相关资料

- [简书](https://www.jianshu.com/p/76869538772a)