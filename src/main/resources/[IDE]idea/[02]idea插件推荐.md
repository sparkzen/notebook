# idea插件推荐

- [idea插件推荐](#idea插件推荐)
  - [实用插件推荐](#实用插件推荐)

## 实用插件推荐

- [Rainbow Brackets](https://plugins.jetbrains.com/plugin/index?xmlId=izhangzhihao.rainbow.brackets)：彩色括号（花括号）
- [Archive Browser](https://plugins.jetbrains.com/plugin/index?xmlId=com.github.b3er.idea.plugins.arc.browser)：压缩包浏览支持
- [CamelCase](https://plugins.jetbrains.com/plugin/index?xmlId=de.netnexus.camelcaseplugin)：字体命名格式转换
- [Convert YAML and Properties File](https://plugins.jetbrains.com/plugin/index?xmlId=com.github.chencn.yamlandprops)：yaml与properties文件互相转换
- [Fast Coding](https://plugins.jetbrains.com/plugin/index?xmlId=com.idea-aedi.plugin.fast-coding)：提供识别泛型的bean转json、根据端口杀进程等高效开发功能
- [Grep Console](https://plugins.jetbrains.com/plugin/index?xmlId=GrepConsole)：控制台日志高亮显示
- [GsonFormatPlus](https://plugins.jetbrains.com/plugin/index?xmlId=GsonFormatPlus)：根据json生成bean
- [Json Parser](https://plugins.jetbrains.com/plugin/index?xmlId=com.godwin.json.parser)：格式化json
- [Maven Helper](https://plugins.jetbrains.com/plugin/index?xmlId=MavenRunHelper)：maven依赖分析
- [Restful Fast Request](https://plugins.jetbrains.com/plugin/index?xmlId=io.github.kings1990.FastRequest)：快速发送http请求（类似于postman，做自测时用）
- [Translate](https://plugins.jetbrains.com/plugin/index?xmlId=cn.yiiguxing.plugin.translate)：翻译
- [MybatisX](https://plugins.jetbrains.com/plugin/10119-mybatisx)：mapper接口与xml文件双向跳转