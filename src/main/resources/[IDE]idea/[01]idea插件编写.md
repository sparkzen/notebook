# idea插件编写

- [idea插件编写](#idea插件编写)
  - [准备工作：添加对应版本的JDK](#准备工作添加对应版本的jdk)
  - [编写插件](#编写插件)
    - [第一步：创建IntelliJ Platform Plugin项目](#第一步创建intellij-platform-plugin项目)
    - [第二步：配置plugin.xml，填写基本信息](#第二步配置pluginxml填写基本信息)
    - [第三步：编写插件、配置actions](#第三步编写插件配置actions)
      - [方式一：完全手动](#方式一完全手动)
      - [方式二：借助IDEA插件开发助手IDEA Plugin DevKit Helper，半自动](#方式二借助idea插件开发助手idea-plugin-devkit-helper半自动)
      - [常用group-id说明](#常用group-id说明)
    - [第四步：运行、调试插件](#第四步运行调试插件)
    - [第五步（可选）：引入依赖](#第五步可选引入依赖)
    - [第六步：打包插件](#第六步打包插件)
    - [第七步：安装插件](#第七步安装插件)
  - [相关补充](#相关补充)
    - [idea如何打开一个现有的插件项目](#idea如何打开一个现有的插件项目)
    - [发布插件到插件市场（手动发布）](#发布插件到插件市场手动发布)
  - [相关资料](#相关资料)


---

## 准备工作：添加对应版本的JDK

编写插件时，不同版本的`IntelliJ IDEA`，对`JDK`的版本要求不同。本人使用的是2021版本的`IntelliJ IDEA`进行的插件编写，需要设置`JDK11`。

![image-20211211110854508](../repository/image-20211211110854508.png)

![image-20211211110959736](../repository/image-20211211110959736.png)

## 编写插件

### 第一步：创建IntelliJ Platform Plugin项目

![image-20211211111152171](../repository/image-20211211111152171.png)

![image-20211211111305460](../repository/image-20211211111305460.png)

![image-20211211111501671](../repository/image-20211211111501671.png)

![image-20211211111641631](../repository/image-20211211111641631.png)

![image-20211211111703782](../repository/image-20211211111703782.png)

![image-20211211111802746](../repository/image-20211211111802746.png)

![image-20211211112153964](../repository/image-20211211112153964.png)



### 第二步：配置plugin.xml，填写基本信息

```xml
<idea-plugin>
    <!-- 插件唯一id，不能和其他插件id重复 -->
    <id>com.idea-aedi.plugin</id>

    <!-- 插件名称（在插件市场搜索插件时，搜索的就是这个名称） -->
    <name>JustryDeng</name>

    <!-- 版本号 -->
    <version>1.0</version>

    <!-- 供应商主页和email -->
    <vendor email="13548417409@163.com" url="https://gitee.com/JustryDeng/projects"/>

    <!-- 插件描述，支持HTML标签 -->
    <description><![CDATA[
        JustryDeng common plugin, Welcome to use.
        ]]></description>

    <!-- 插件版本变更信息，支持HTML标签； 将展示在 settings | Plugins 对话框和插件仓库的Web页面 -->
    <change-notes><![CDATA[
        <ul>
            <li>since 2021-12-11</li>
        </ul>
        ]]></change-notes>

    <!--
        插件兼容IDEA的最大和最小 build 号，两个属性可以任选一个或者同时使用,
        please see https://plugins.jetbrains.com/docs/intellij/build-number-ranges.html for description
    -->
    <idea-version since-build="173.0"/>

    <!-- 插件所依赖的其他插件的id -->
    <depends>com.intellij.modules.platform</depends>
    <depends>com.intellij.modules.lang</depends>
    <depends>com.intellij.modules.java</depends>

    <extensions defaultExtensionNs="com.intellij">
        <!-- Add your extensions here -->
    </extensions>

    <actions>
        <!--
            !!!!!! 这部分内容需要在下面的步骤中配置（手动配置、或借助插件开发助手自动配置） !!!!!!
        -->
    </actions>

</idea-plugin>
```

### 第三步：编写插件、配置actions

#### 方式一：完全手动

- 第一步：编写想要实现的功能类，需要继承`com.intellij.openapi.actionSystem.AnAction`

  ```java
  import com.intellij.openapi.actionSystem.AnAction;
  import com.intellij.openapi.actionSystem.AnActionEvent;
  import com.intellij.openapi.actionSystem.PlatformDataKeys;
  import com.intellij.openapi.project.Project;
  import com.intellij.openapi.ui.Messages;
  
  public class HelloAction extends AnAction {
      
      @Override
      public void actionPerformed(AnActionEvent event) {
          // your logic
          Project project = event.getData(PlatformDataKeys.PROJECT);
          String txt= Messages.showInputDialog(project, "What is your name?", "Question", Messages.getQuestionIcon());
          Messages.showMessageDialog(project, "Hello, " + txt + "!\nI am glad to see you.", "Information", Messages.getInformationIcon());
      }
  }
  ```

- 第二步：配置`plugin.xml`中的`actions`

  > 提示：为了示例的多样性，这里给出两个配置示例

  ```xml
  <!-- 
      提示：action标签和group标签的id字段值自取即可
   -->
  <actions>
      <!--
          示例一：在MainMenu菜单下的HelpMenu菜单里，最前面增加一个叫'Hello'的按钮
      -->
      <action id="com.idea-aedi.plugin.Hello" class="com.example.ideaplugin.HelloAction" text="Hello"
              description="Say hello.">
          <!--  指定父节点 action 或 action group 被添加到的位置 -->
          <add-to-group group-id="HelpMenu" anchor="first"/>
      </action>
  
      <!--
          示例二：在MainMenu菜单的最后面，增加一个 叫'JustryDeng'的菜单,其中字母J带下划线
      -->
      <group id="com.idea-aedi.plugin.JustryDeng" text="_JustryDeng" description="JustryDeng's common util.">
          <!--  指定父节点 action 或 action group 被添加到的位置 -->
          <add-to-group group-id="MainMenu" anchor="last"/>
          <!-- 当前菜单下，再添加一个叫'Say Hey'的按钮,其中字母S带下划线 （注：当然还可以嵌套菜单） -->
          <action id="com.idea-aedi.plugin.JustryDeng.Hello" class="com.example.ideaplugin.HelloAction" text="Say _Hello" description="Say hello."/>
      </group>
  </actions>
  ```

#### 方式二：借助IDEA插件开发助手IDEA Plugin DevKit Helper，半自动

> 插件开发助手，可以在帮助我们快速创建Action并在`plugin.xml`中作对应的配置，而不需要我们手动配置。

- 第一步：安装IDEA插件开发助手`IDEA Plugin DevKit Helper`

  ![image-20211211112816499](../repository/image-20211211112816499.png)

- 第二步：使用插件开发助手创建Action

  ![image-20211211113307613](../repository/image-20211211113307613.png)

![image-20211211133651627](../repository/image-20211211133651627.png)

> 其中设置插件位置处里，Action代表一个"按钮"，Group代表菜单，Group里可能有多个Action；先选择一个Group或者一个Action，然后选择Anchor位置，即可通过相对位置的方式，将插件的位置确定下来了。比如说需要将按钮设置在`Help`菜单的最上面：
>
> ![image-20211211130040366](../repository/image-20211211130040366.png)

点击OK，插件开发助手就会创建对应名字的`AnAction`类，同时会将新按钮的信息（按钮id、`AnAction`类、按钮名称、描述、位置等信息）添加进`plugin.xml`中的`actions`标签里。

- 第三步：在生成的`AnAction`类中编写逻辑

  ```java
  import com.intellij.openapi.actionSystem.AnAction;
  import com.intellij.openapi.actionSystem.AnActionEvent;
  import com.intellij.openapi.actionSystem.PlatformDataKeys;
  import com.intellij.openapi.project.Project;
  import com.intellij.openapi.ui.Messages;
  
  public class HelloAction extends AnAction {
      
      @Override
      public void actionPerformed(AnActionEvent event) {
          // your logic
          Project project = event.getData(PlatformDataKeys.PROJECT);
          String txt= Messages.showInputDialog(project, "What is your name?", "Question", Messages.getQuestionIcon());
          Messages.showMessageDialog(project, "Hello, " + txt + "!\nI am glad to see you.", "Information", Messages.getInformationIcon());
      }
  }
  ```

#### 常用group-id说明

- `MainMenu`：主菜单

  ![image-20211213201555027](../repository/image-20211213201555027.png)

- `CutCopyPasteGroup`：左侧导航栏菜单

  ![image-20211213202149641](../repository/image-20211213202149641.png)

- `EditorPopupMenu`：右侧代码栏菜单

  ![image-20211213202504738](../repository/image-20211213202504738.png)

- `GenerateGroup`：按`alt + insert`时弹出的菜单栏

  ![image-20211213202704938](../repository/image-20211213202704938.png)

### 第四步：运行、调试插件

![image-20211211134604566](../repository/image-20211211134604566.png)

运行插件时，会新打开一个idea：

![image-20211211122042588](../repository/image-20211211122042588.png)

![image-20211211134855577](../repository/image-20211211134855577.png)

![image-20211211134949544](../repository/image-20211211134949544.png)

测试一下功能，也是OK的：

![image-20211211140410996](../repository/image-20211211140410996.png)

![image-20211211140055804](../repository/image-20211211140055804.png)

### 第五步（可选）：引入依赖

在我们实际开发idea插件过程中，可能还会需要其它的依赖，这时可采用官方推荐的`gradle`创建idea插件的方式，但是会略显麻烦；简单的，你可以直接将依赖的lib包as library引入进当前项目即可：

![image-20211211151644142](../repository/image-20211211151644142.png)

![image-20211211152004764](../repository/image-20211211152004764.png)

检查一下：

![image-20211211151858796](../repository/image-20211211151858796.png)

### 第六步：打包插件

![image-20211211135356702](../repository/image-20211211135356702.png)

注：如果项目有依赖其它jar包，那么打出来的就不是jar包，而是zip包了。

### 第七步：安装插件

> 如果插件有发布到插件市场，那么可以直接在线从插件市场安装；如果插件没有发布到插件市，那么可以采用下面这种从磁盘安装的方式进行安装。

打开`File | Settings | Plugins`，选择从磁盘安装：

![image-20211211135543115](../repository/image-20211211135543115.png)

选中插件包后，可看到：

![image-20211211135845833](../repository/image-20211211135845833.png)

点击`OK`或`Apply`，**重启idea完成安装**。

## 相关补充

### idea如何打开一个现有的插件项目

- 问题所在

  如果你使用idea直接打开一个plugin项目，那么可能是这样的（**没有被idea认出来是插件项目**）：

  ![image-20220105232253811](../repository/image-20220105232253811.png)

  

  而实际的插件项目是这样的（**有被idea认出来是插件项目**)：

  ![image-20220105232403343](../repository/image-20220105232403343.png)

- 解决办法

  直接修改项目的.iml文件：

  ![image-20220105233134683](../repository/image-20220105233134683.png)

  将type修改为`PLUGIN_MODULE`，并通过增加`<component name="DevKit.ModuleBuildProperties" url="file://$MODULE_DIR$/resources/META-INF/plugin.xml" />`来指定plugin.xml的位置：

  修改前：

  ![image-20220105232702072](../repository/image-20220105232702072.png)

  修改后：

  ![image-20220105233027578](../repository/image-20220105233027578.png)

  此时，再观察项目，会发现idea已经识别出是插件项目了：

  ![image-20220105233345725](../repository/image-20220105233345725.png)

### 发布插件到插件市场（手动发布）

> 提示：如果使用的gradle编写构建的插件，那么可以使用gradle集成的能力，自动将插件发布到插件市场，而不需要手动发布。

- 第一步：访问`https://plugins.jetbrains.com/`

- 第二步：登录（支持多种方式登录，本人常用Github登录）

  ![image-20220107234134042](../repository/image-20220107234134042.png)

- 第三步：上传插件

  ![image-20220107234209989](../repository/image-20220107234209989.png)

  ![image-20220107234623239](../repository/image-20220107234623239.png)

- 第四步：等审核通过后，就可以在插件市场搜索到你的插件了

---

## 相关资料

- [官网](https://plugins.jetbrains.com/docs/intellij/basics.html)
- [**`demo代码下载`**](https://gitee.com/JustryDeng/shared-files/raw/master/custom-idea-plugin.rar)
