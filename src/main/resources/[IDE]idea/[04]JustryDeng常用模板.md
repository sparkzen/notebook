# JustryDeng常用模板

- [JustryDeng常用模板](#justrydeng常用模板)
  - [简介](#简介)
  - [模板](#模板)
    - [重写toString方法](#重写tostring方法)
    - [自动生成类注释](#自动生成类注释)
    - [自动生成接口注释](#自动生成接口注释)
    - [自动生成方法注释](#自动生成方法注释)
    - [自动生成多行注释](#自动生成多行注释)
    - [自动生成JavaDoc注释（横向）](#自动生成javadoc注释横向)
    - [自动生成JavaDoc注释（纵向）](#自动生成javadoc注释纵向)

## 简介

我们可以在idea中设置一些模板，来快速生成相关代码

> Settings => Editor => Live Templates

## 模板

### 重写toString方法

- 快捷键

  ```bash
  2str + tab
  ```

- 模板

  ```java
  @Override
  public String toString() {
      return GsonUtil.toJson(this);
  }
  ```

### 自动生成类注释

- 快捷键

  ```bash
  c + tab
  ```

- 模板

  ```java
  /**
   * (non-javadoc)
   *
   * @author JustryDeng
   * @since $date$ $time$
   */
  ```

### 自动生成接口注释

- 快捷键

  ```bash
  i + tab
  ```

- 模板

  ```java
  /**
   * (non-javadoc)
   *
   * @author {@link JustryDeng}
   * @since $date$ $time$
   */
  ```

### 自动生成方法注释

- 快捷键

  ```bash
  m + tab
  ```

- 模板

  ```java
  /**
   * (non-javadoc)
   *
   * @param $param$
   *             
   * @return $return$ 
   * @throws $exception$ 
   */
  ```

### 自动生成多行注释

- 快捷键

  ```bash
  ml + tab
  ```

- 模板

  ```java
  /*
   * 
   */
  ```

### 自动生成JavaDoc注释（横向）

- 快捷键

  ```bash
  p + tab
  ```

- 模板

  ```java
  /**  */
  ```

### 自动生成JavaDoc注释（纵向）

- 快捷键

  ```bash
  pv + tab
  ```

- 模板

  ```java
  /**
   * 
   */
  ```