# easy-code插件配置

- [easy-code插件配置](#easy-code插件配置)
  - [字段类型映射配置](#字段类型映射配置)
  - [模板文件配置](#模板文件配置)
    - [常用模板文件](#常用模板文件)
      - [po模板](#po模板)
      - [controller模板](#controller模板)
      - [service模板](#service模板)
      - [service-impl模板](#service-impl模板)
      - [mapper模板](#mapper模板)
      - [mapper-provider模板](#mapper-provider模板)
    - [JPA专用模板文件](#jpa专用模板文件)
      - [po模板](#po模板-1)
      - [controller模板](#controller模板-1)
      - [service模板](#service模板-1)
      - [service-impl模板](#service-impl模板-1)
      - [mapper模板](#mapper模板-1)

## 字段类型映射配置

![image-20220714112224458](../repository/image-20220714112224458.png)



## 模板文件配置

### 常用模板文件

#### po模板

```text
##引入宏定义
$!define

##使用宏定义设置回调（保存位置与文件后缀）
#save("/model/po", "PO.java")

##使用宏定义设置包后缀
#setPackageSuffix("model.po")

##使用全局变量实现默认包导入
$!autoImport
import lombok.*;
import com.alibaba.fastjson.JSON;
import com.niantou.niantou.common.author.JustryDeng;

 /**
  * the po auto-generated for table `$!{tableInfo.obj.name}`#if(${tableInfo.comment})($!{tableInfo.comment})#end 

  *
  * @author {@link JustryDeng}
  * @date $!time.currTime('yyyy/M/d HH:mm:ss')
  */
@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class $!{tableInfo.name}PO {
    
#foreach($column in $tableInfo.fullColumn)
    #if(${column.comment})
   /** ${column.comment} */
       private $!{tool.getClsNameByFullName($column.type)} $!{column.name};
    #else  
   private $!{tool.getClsNameByFullName($column.type)} $!{column.name};
    #end

#end
    @Override
    public String toString() {
        return JSON.toJSONString(this);
    }
}
```

#### controller模板

```text
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "Controller"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/controller"))
#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}controller;

import lombok.RequiredArgsConstructor;
import $!{tableInfo.savePackageName}.service.$!{tableInfo.name}Service;
import com.niantou.niantou.common.author.JustryDeng;
import org.springframework.web.bind.annotation.RestController;

/**
 * controller for `$!{tableInfo.obj.name}`
 * 
 * @author {@link JustryDeng}
 * @date $!time.currTime('yyyy/M/d HH:mm:ss')
 */
@RestController
@RequiredArgsConstructor
public class $!{tableName} {

    private final $tool.append($tableInfo.name, "Service") $tool.firstLowerCase("$!{tableInfo.name}Service");

}
```

#### service模板

```text
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "Service"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/service"))


#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}service;

import com.niantou.niantou.common.author.JustryDeng;

/**
 * service for `$!{tableInfo.obj.name}`
 *
 * @author {@link JustryDeng}
 * @date $!time.currTime('yyyy/M/d HH:mm:ss')
 */
public interface $!{tableName} {
}
```

#### service-impl模板

```text
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "ServiceImpl"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/service/impl"))


#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}service.impl;

import lombok.RequiredArgsConstructor;
import com.niantou.niantou.common.author.JustryDeng;
import org.springframework.stereotype.Service;
import $!{tableInfo.savePackageName}.mapper.$!{tableInfo.name}Mapper;
import $!{tableInfo.savePackageName}.service.$!{tableInfo.name}Service;

/**
 * impl for $!{tableInfo.name}Service
 *
 * @author {@link JustryDeng}
 * @date $!time.currTime('yyyy/M/d HH:mm:ss')
 */
@Service
@RequiredArgsConstructor
public class $!{tableName} implements $!{tableInfo.name}Service {

    private final $tool.append($tableInfo.name, "Mapper") $tool.firstLowerCase("$!{tableInfo.name}Mapper");

}
```

#### mapper模板

```text
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "Mapper"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/mapper"))

#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}mapper;

##使用全局变量实现默认包导入
import org.apache.ibatis.annotations.*;
import com.niantou.niantou.common.author.JustryDeng;
import $!{tableInfo.savePackageName}.model.po.$!{tableInfo.name}PO;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * mapper for `$!{tableInfo.obj.name}`#if(${tableInfo.comment})($!{tableInfo.comment})#end 

 *
 * @author {@link JustryDeng}
 * @date $!time.currTime('yyyy/M/d HH:mm:ss')
 */
@Mapper
public interface $!{tableName} extends BaseMapper<$!{tableInfo.name}PO>{
}
```

#### mapper-provider模板

```text
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "MapperSqlProvider"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/mapper/provider"))

#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}mapper.provider;

##使用全局变量实现默认包导入
import com.niantou.niantou.common.author.JustryDeng;
import $!{tableInfo.savePackageName}.mapper.$!{tableInfo.name}Mapper;

/**
 * provider sql for {@link $!{tableInfo.name}Mapper}
 *
 * @author {@link JustryDeng}
 * @date $!time.currTime('yyyy/M/d HH:mm:ss')
 */
@SuppressWarnings("unused")
public class $!{tableName} {
}
```

### JPA专用模板文件

#### po模板

```text
##引入宏定义
$!define

##使用宏定义设置回调（保存位置与文件后缀）
#save("/entity/po", "PO.java")

##使用宏定义设置包后缀
#setPackageSuffix("entity.po")


import lombok.Data;

##使用全局变量实现默认包导入
$!autoImport
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

import java.io.Serializable;

##使用宏定义实现类注释信息
/**
 * $!{tableInfo.comment}($!{tableInfo.obj.name})实体类  auto-generated at $!time.currTime('yyyy/M/d HH:mm:ss')
 *
 * @author kuoyi
 * @since 1.0.0
 */
@Data
@Entity
@Table(name = "$!{tableInfo.obj.name}")
public class $!{tableInfo.name}PO implements Serializable {

    private static final long serialVersionUID = $!tool.serial();
#foreach($column in $tableInfo.fullColumn)

#if(${column.comment})
    /** ${column.comment} */
    #if($!{column.obj.name} == "id")
    @Id
    #end
    @Column(name = "$!{column.obj.name}")
    private $!{tool.getClsNameByFullName($column.type)} $!{column.name};
#else
    #if($!{column.obj.name} == "id")
    @Id
    #end
    @Column(name = "$!{column.obj.name}")
    private $!{tool.getClsNameByFullName($column.type)} $!{column.name};
#end
#end
}
```

#### controller模板

```text
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "Controller"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/controller"))
##拿到主键
#if(!$tableInfo.pkColumn.isEmpty())
    #set($pk = $tableInfo.pkColumn.get(0))
#end

#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}controller;

import $!{tableInfo.savePackageName}.service.$!{tableInfo.name}Service;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * $!{tableInfo.comment}
 *
 * @author kuoyi
 * @since 1.0.0
 */
@RestController
@RequestMapping("$!tool.firstLowerCase($tableInfo.name)")
public class $!{tableName} {

    @Resource
    private $!{tableInfo.name}Service $!tool.firstLowerCase($tableInfo.name)Service;

}
```

#### service模板

```text
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "Service"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/service"))

##拿到主键
#if(!$tableInfo.pkColumn.isEmpty())
    #set($pk = $tableInfo.pkColumn.get(0))
#end

#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}service;

/**
 * $!{tableInfo.comment}($!{tableInfo.obj.name})服务接口  auto-generated at $!time.currTime('yyyy/M/d HH:mm:ss')
 *
 * @author kuoyi
 * @since 1.0.0
 */
public interface $!{tableName} {

}
```

#### service-impl模板

```text
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "ServiceImpl"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/service/impl"))

##拿到主键
#if(!$tableInfo.pkColumn.isEmpty())
    #set($pk = $tableInfo.pkColumn.get(0))
#end

#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}service.impl;

import $!{tableInfo.savePackageName}.mapper.$!{tableInfo.name}Mapper;
import $!{tableInfo.savePackageName}.service.$!{tableInfo.name}Service;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * $!{tableInfo.comment}($!{tableInfo.obj.name})服务实现类  auto-generated at $!time.currTime('yyyy/M/d HH:mm:ss')
 *
 * @author kuoyi
 * @since 1.0.0
 */
@Service
public class $!{tableName} implements $!{tableInfo.name}Service {

    @Resource
    private $!{tableInfo.name}Mapper $!tool.firstLowerCase($!{tableInfo.name})Mapper;

}
```

#### mapper模板

```text
##定义初始变量
#set($tableName = $tool.append($tableInfo.name, "Mapper"))
##设置回调
$!callback.setFileName($tool.append($tableName, ".java"))
$!callback.setSavePath($tool.append($tableInfo.savePath, "/mapper"))

##拿到主键
#if(!$tableInfo.pkColumn.isEmpty())
    #set($pk = $tableInfo.pkColumn.get(0))
#end

#if($tableInfo.savePackageName)package $!{tableInfo.savePackageName}.#{end}mapper;

import $!{tableInfo.savePackageName}.entity.po.$!{tableInfo.name}PO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
 * $!{tableInfo.comment}($!{tableInfo.obj.name})数据库访问层  auto-generated at $!time.currTime('yyyy/M/d HH:mm:ss')
 *
 * @author kuoyi
 * @since 1.0.0
 */
public interface $!{tableName} extends JpaRepository<$!{tableInfo.name}PO, Long>, JpaSpecificationExecutor<$!{tableInfo.name}PO> {

}
```

