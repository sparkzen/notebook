# class热更新

- [class热更新](#class热更新)
  - [<font  face="幼圆" color = "#3399EA" >功能与特性</font>](#font--face幼圆-color--3399ea-功能与特性font)
  - [<font  face="幼圆" color = "#3399EA" >maven坐标</font>](#font--face幼圆-color--3399ea-maven坐标font)
  - [<font  face="幼圆" color = "#3399EA" >使用说明</font>](#font--face幼圆-color--3399ea-使用说明font)

---

## <font  face="幼圆" color = "#3399EA" >功能与特性</font>

- 支持基于 **java源码** ，走 **文件** 进行class热更新
- 支持基于 **java源码** ，走 **内存** 进行class热更新
- 支持基于 **class字节码** ，走 **文件** 进行class热更新
- 支持基于 **class字节码** ，走 **内存** 进行class热更新

## <font  face="幼圆" color = "#3399EA" >maven坐标</font>

```xml
<dependency>
    <groupId>com.idea-aedi</groupId>
    <artifactId>hot-update-class</artifactId>
    <version>{版本号}</version>
</dependency>
```

## <font  face="幼圆" color = "#3399EA" >使用说明</font>

**记得先引入上述依赖噢，朋友们。**

 - #### <font  face="幼圆" color = "#86CA5E" >Java源码（走文件）热更新</font>
	```java
	/**
	 * 基于java源码文件的class热更新
	 *
	 * @param pid
	 *            要热更新的jvm的进程id
	 * @param agentJarPath
	 *            代理jar包的绝对路径
	 * @param javaPathsList
	 *            要进行更新的java类文件的绝对路径集合
	 */
	ClassHotUpdateExecutor.BASE64_JAVA_PATHS.execute(List<String> javaPathsList);
	///ClassHotUpdateExecutor.BASE64_JAVA_PATHS.execute(String agentJarPath, List<String> javaPathsList);
	///ClassHotUpdateExecutor.BASE64_JAVA_PATHS.execute(String pid, String agentJarPath, List<String> javaPathsList);
	```
	
- #### <font  face="幼圆" color = "#86CA5E" >Java源码（走内存）热更新</font>
  ```java
  /**
   * 基于java源码，走内存进行class热更新
   *
   * @param agentJarPath
   *            代理jar包的绝对路径
   * @param javaBytesList
   *            要进行更新的java源码文件的绝对路径集合
   */
  ClassHotUpdateExecutor.MEMORY_JAVA_BYTES.execute(List<byte[]> javaBytesList);
  ///ClassHotUpdateExecutor.MEMORY_JAVA_BYTES.execute(String agentJarPath, List<byte[]> javaBytesList);
  ```


- <font  face="幼圆" color = "#86CA5E" >Class字节码（走文件）热更新</font>

  ```java
  /**
   * 基于class字节码文件的class热更新
   *
   * @param pid
   *            要热更新的jvm的进程id
   * @param agentJarPath
   *            代理jar包的绝对路径
   * @param classPathsList
   *            要进行更新的class字节码文件的绝对路径集合
   */
  ClassHotUpdateExecutor.BASE64_CLASS_PATHS.execute(List<String> classPathsList);
  ///ClassHotUpdateExecutor.BASE64_CLASS_PATHS.execute(String agentJarPath, List<String> classPathsList);
  ///ClassHotUpdateExecutor.BASE64_CLASS_PATHS.execute(String pid, String agentJarPath, List<String> classPathsList);
  ```

- <font  face="幼圆" color = "#86CA5E" >Class字节码（走内存）热更新</font>

  ```java
  /**
   * 基于java源码文件的class热更新
   *
   * @param pid
   *            要热更新的jvm的进程id
   * @param agentJarPath
   *            代理jar包的绝对路径
   * @param javaPathsList
   *            要进行更新的java类文件的绝对路径集合
   */
  ClassHotUpdateExecutor.MEMORY_CLASS_BYTES.execute(List<String> javaPathsList);
  ///ClassHotUpdateExecutor.MEMORY_CLASS_BYTES.execute(String agentJarPath, List<String> javaPathsList);
  ///ClassHotUpdateExecutor.MEMORY_CLASS_BYTES.execute(String pid, String agentJarPath, List<String> javaPathsList);
  ```

  

