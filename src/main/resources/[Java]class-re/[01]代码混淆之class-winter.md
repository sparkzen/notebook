# 代码混淆之class-winter

- [代码混淆之class-winter](#代码混淆之class-winter)
  - [<font  face="幼圆" color = "#3399EA" >功能与特性</font>](#font--face幼圆-color--3399ea-功能与特性font)
  - [<font  face="幼圆" color = "#3399EA" >加密</font>](#font--face幼圆-color--3399ea-加密font)
  - [<font  face="幼圆" color = "#3399EA" >加密参数</font>](#font--face幼圆-color--3399ea-加密参数font)
  - [<font  face="幼圆" color = "#3399EA" >解密(启动)</font>](#font--face幼圆-color--3399ea-解密启动font)
  - [<font  face="幼圆" color = "#3399EA" >解密参数</font>](#font--face幼圆-color--3399ea-解密参数font)
  - [<font  face="幼圆" color = "#3399EA" >相关资料</font>](#font--face幼圆-color--3399ea-相关资料font)

---

<font face="幼圆"  color = "#F53B45">**郑重声明**</font>

&emsp;&emsp;[**class-winter**](https://gitee.com/JustryDeng/class-winter) 是本人在学习完 [**class-final(v1.1.9)**](https://gitee.com/roseboy/classfinal) 后，仿照class-final进行编写的，部分思路与class-final一致。

## <font  face="幼圆" color = "#3399EA" >功能与特性</font>

- 支持war加密。
- 支持jar（普通jar+可执行jar）加密。
- 支持xml加密（掩耳盗铃版）。

## <font  face="幼圆" color = "#3399EA" >加密</font>

- #### <font face="幼圆" ><font face="幼圆"  color = "#86CA5E">**方式一**</font>：通过maven插件自动加密。</font>
	```xml
	<!--
	    class-winter插件
	    注:自Maven3.0.3起, 绑定到同一phase的Maven插件将按照pom.xml中声明的顺序执行
	    注:此插件最好放置在同一phase的最后执行。
	-->
	<plugin>
	    <groupId>com.idea-aedi</groupId>
	    <artifactId>class-winter-maven-plugin</artifactId>
	    <version>最新版本号</version>
	    <!-- 相关配置 -->
	    <configuration>
	        <!-- <finalName></finalName>-->
	        <includePrefix>加密范围</includePrefix>
	        <!-- <excludePrefix></excludePrefix>-->
	        <!-- <includeXmlPrefix></includeXmlPrefix>-->
	        <!-- <excludeXmlPrefix></excludeXmlPrefix>-->
	        <!-- <toCleanXmlChildElementName></toCleanXmlChildElementName>-->
	        <!-- <password></password>-->
	        <!-- <includeLibs></includeLibs>-->
	        <!-- <alreadyProtectedLibs></alreadyProtectedLibs>-->
	        <!-- <supportFile></supportFile>-->
	        <!-- <tips></tips>-->
	        <!-- <debug></debug>-->
	    </configuration>
	    <executions>
	        <execution>
	            <phase>package</phase>
	            <goals>
	                <goal>class-winter</goal>
	            </goals>
	        </execution>
	    </executions>
	</plugin>
	```
	注：不必担心信息泄漏问题，使用此方式生成混淆的jar包时，会擦除pom.xml中关于class-winter-plugin的信息。
	
- #### <font face="幼圆" ><font face="幼圆"  color = "#86CA5E">**方式二**</font>：通过 [**class-winter-core.jar**](https://gitee.com/JustryDeng/class-winter/raw/master/class-winter-core/src/test/resources/class-winter-core-2.1.0.jar) 主动加密。</font>

	```bash
	java -jar class-winter-core-${version}.jar originJarOrWar=${要加密的项目.jar或.war包} includePrefix=${加密范围} [k3=v3 k4=v4 ...]
	# 对于复杂的参数值，可以使用引号引起来
	# linux
	java -jar class-winter-core-${version}.jar k1='v1' k2='v2'
	# windows
	java -jar class-winter-core-${version}.jar k1="v1" k2="v2"
	```

## <font  face="幼圆" color = "#3399EA" >加密参数</font>

<table>
  <tr align="center">
    <td>参数</td>
    <td>是否必填</td>
    <td>说明</td>
    <td>示例</td>
  </tr>
  <tr align="center">
    <td>originJarOrWar</td>
    <td>是</td>
    <td>指定要加密的jar/war文件<br/>注：当使用maven插件进行自动加密时，无此参数。</td>
    <td>originJarOrWar=/my-project.jar</td>
  </tr>
    <tr align="center">
    <td>includePrefix</td>
    <td>是</td>
    <td>通过前缀匹配的形式定位要加密的class
    <br/>
注：多个通过逗号分割。</td>
    <td>includePrefix=com
    <br/>
includePrefix=com,org</td>
  </tr>
    <tr align="center">
    <td>excludePrefix</td>
    <td>否</td>
    <td>通过前缀匹配的形式排除class，不对其加密
    <br/>
注：多个通过逗号分割。
<br/>
注:excludePrefix优先级高于includePrefix。</td>
    <td>excludePrefix=com.example.service,com.example.util.StrUtil.class</td>
  </tr>
      </tr>
    <tr align="center">
    <td>includeXmlPrefix</td>
    <td>否</td>
    <td>通过打出来的包中条目的entryName前缀匹配的形式定位要加密的xml
    <br/>
注：多个通过逗号分割。    
    <br/>
注：如果您打出来的加密包是准备作为一个lib包提供给第三方使用的，那么请不要使用此参数，因为解密时是不会解密项目所依赖的lib包中的xml的。</td>
    <td>includeXmlPrefix=BOOT-INF/classes/
    <br/>
includeXmlPrefix=BOOT-INF/classes/com/demo/mapper/,BOOT-INF/classes/com/demo/dao/</td>
  </tr>
  </tr>
    <tr align="center">
    <td>excludeXmlPrefix</td>
    <td>否</td>
    <td>通过打出来的包中条目的entryName前缀匹配的形式排除xml，不对其加密
    <br/>
注：多个通过逗号分割。</td>
    <td>excludeXmlPrefix=BOOT-INF/classes/com/demo/mapper/
    <br/>
excludeXmlPrefix=BOOT-INF/classes/com/demo/mapper/,BOOT-INF/classes/com/demo/dao/UserDao.xml</td>
  </tr>
  </tr>
    <tr align="center">
    <td>toCleanXmlChildElementName</td>
    <td>否</td>
    <td>加密xml中的哪些一级元素
    <br/>
注：默认值为resultMap,sql,insert,update,delete,select
    <br/>
注：多个通过逗号分割。</td>
    <td>toCleanXmlChildElementName=select,delete,resultMap</td>
  </tr>
    <tr align="center">
    <td>finalName</td>
    <td>否</td>
    <td>指定加密后生成的jar包名
    <br/>
注：若finalName与加密的包一致，那么生成的加密后的包会覆盖原来的包。</td>
    <td>finalName=mine-project</td>
  </tr>
    <tr align="center">
    <td>password</td>
    <td>否</td>
    <td>主动指定密码
    <br/>
注：密码不能包含空格和逗号。</td>
    <td>password=123456</td>
  </tr>
    <tr align="center">
    <td>includeLibs</td>
    <td>否</td>
    <td>指定将lib包也纳入加密范围内
    <br/>
注:多个通过逗号分割。
<br/>
注：lib中的class是否会被加密，还得由includePrefix和excludePrefix决定。</td>
    <td>includeLibs=a.jar,b.jar</td>
  </tr>
    <tr align="center">
    <td>alreadyProtectedLibs</td>
    <td>否</td>
    <td>指明项目所以来的lib中，哪些lib本身就已经是被class-winter加密了的
    <br/>
注：多个通过逗号分割。
<br/>
注:主要用于处理第三方提供的由class-winter加密了的依赖包的场景。
<br/>
注：若lib需要密码，那么需要在指定lib的同时通过冒号接上密码。
<br/>
注：如果lib有密码，那么密码不能包含逗号。</td>
    <td>alreadyProtectedLibs=a.jar,b.jar
    <br/>
alreadyProtectedLibs=a.jar,b.jar:pwd123
<br/>
alreadyProtectedLibs=a.jar:pwd1,b.jar:pwd2</td>
  </tr>
    <tr align="center">
    <td>supportFile</td>
    <td>否</td>
    <td>指定一个加密辅助jar文件（或jar文件所在的目录）
    <br/>
注：当为目录时，该目录(含子孙目录)下的所有jar都会被作采集作为辅助文件。
<br/>
注:主要用于解决因ClassNotFound导致的加密失败问题。</td>
    <td>supportFile=/abc.jar
    <br/>
supportFile=/libs</td>
  </tr>
    <tr align="center">
    <td>tips</td>
    <td>否</td>
    <td>指定提示语。
    <br/>
注：当直接使用加密后的jar/war时，用到了加密了的类后，会先System.err.println输出此tips,然后System.exit退出程序。</td>
    <td>windows示例：tips="请不要直接使用混淆后的jar/war"
<br/>
linux示例：tips='请不要直接使用混淆后的jar/war'</td>
  </tr>
    <tr align="center">
    <td>debug</td>
    <td>否</td>
    <td>是否开启debug模式</td>
    <td>debug=true</td>
  </tr>
</table>




## <font  face="幼圆" color = "#3399EA" >解密(启动)</font>

通过-javaagent指定代理进行解密启动。
 - #### <font  face="幼圆" color = "#86CA5E" >jar解密(启动)</font>
	```bash
	# 假设your-project-encrypted.jar是由class-winter加密后的包，那么你可以这么启动
	java -javaagent:/your-project-encrypted.jar -jar /your-project-encrypted.jar
	# 也可以用class-winter-core-1.0.0.jar
	# java -javaagent:/class-winter-core-1.0.0.jar -jar /your-project-encrypted.jar
	# 或者指定参数
	# java -javaagent:/your-project-encrypted.jar=debug=true,password=pwd12345 -jar /your-project-encrypted.jar
	# 参数可以引起来(linux)
	# java -javaagent:/your-project-encrypted.jar='debug=true,password=pwd12345' -jar /your-project-encrypted.jar
	# 参数可以引起来(windows)
	# java -javaagent:/your-project-encrypted.jar="debug=true,password=pwd12345" -jar /your-project-encrypted.jar
	```

- #### <font  face="幼圆" color = "#86CA5E" >war解密(启动)</font>
  > **以Tomcat9为例**
  - linux方式一

    编辑tomcat/bin/catalina.sh文件，在最上面加上

	```bash
	# 如果你有参数， 那么 -javaagent:/class-winter-core-1.0.0.jar=k1=v1,k2=v2
	CATALINA_OPTS="$CATALINA_OPTS -javaagent:/class-winter-core-1.0.0.jar=debug=true";
	export CATALINA_OPTS;
	```

  - linux方式二

    在tomcat/bin目录下创建setenv.sh文件，并写上

	```bash
	# 如果你有参数， 那么 -javaagent:/class-winter-core-1.0.0.jar=k1=v1,k2=v2
	JAVA_OPTS="$JAVA_OPTS -javaagent:/class-winter-core-1.0.0.jar=debug=true";
	export JAVA_OPTS;
	```

  - windows方式一

    编辑tomcat/bin/catalina.bat文件，在@echo off后加上catalina参数

	```bash
	rem 如果你有参数， 那么 -javaagent:D:/class-winter-core-1.0.0.jar=k1=v1,k2=v2
	set CATALINA_OPTS="-javaagent:D:/class-winter-core-1.0.0.jar"
	```

  - windows方式二

    在tomcat/bin目录下创建setenv.bat文件，并写上

	```bash
	rem 如果你有参数， 那么 -javaagent:D:/class-winter-core-1.0.0.jar=k1=v1,k2=v2
	set JAVA_OPTS="-javaagent:D:/class-winter-core-1.0.0.jar"
	```

## <font  face="幼圆" color = "#3399EA" >解密参数</font>

<table>
  <tr align="center">
    <td>参数</td>
    <td>是否必填</td>
    <td>说明</td>
    <td>示例</td>
  </tr>
  <tr align="center">
    <td>password</td>
    <td>否</td>
    <td>指定解密密码</td>
    <td>password=pwd123</td>
  </tr>
    <tr align="center">
    <td>debug</td>
    <td>否</td>
    <td>是否开启debug模式</td>
    <td>debug=true</td>
  </tr>
</table>

---

## <font  face="幼圆" color = "#3399EA" >相关资料</font>

- [官网](https://gitee.com/JustryDeng/class-winter)