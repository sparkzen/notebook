# 编写xxl-job定时任务&使用示例

- [编写xxl-job定时任务&使用示例](#编写xxl-job定时任务使用示例)
  - [编写定时任务&使用示例](#编写定时任务使用示例)
    - [Bean模式 - 在执行器中编写代码](#bean模式---在执行器中编写代码)
      - [示例1：BEAN模式（方法形式）](#示例1bean模式方法形式)
        - [第一步：在执行器项目中，编写定时任务代码](#第一步在执行器项目中编写定时任务代码)
        - [第二步：重新部署执行器所在的项目](#第二步重新部署执行器所在的项目)
        - [第三步：新增执行器](#第三步新增执行器)
        - [第四步：新增任务](#第四步新增任务)
        - [第五步：验证执行一下](#第五步验证执行一下)
        - [第六步：启动任务](#第六步启动任务)
      - [示例2：BEAN模式（类形式）](#示例2bean模式类形式)
        - [第一步：在执行器项目中，编写定时任务代码](#第一步在执行器项目中编写定时任务代码-1)
        - [第二步：重新部署执行器所在的项目](#第二步重新部署执行器所在的项目-1)
        - [第三步：新增执行器](#第三步新增执行器-1)
        - [第四步：新增任务](#第四步新增任务-1)
        - [第五步：验证执行一下](#第五步验证执行一下-1)
        - [第六步：启动任务](#第六步启动任务-1)
    - [GLUE模式 - 直接在调度中心编写代码](#glue模式---直接在调度中心编写代码)
      - [示例1：GLUE - Java模式](#示例1glue---java模式)
        - [第一步：新增执行器](#第一步新增执行器)
        - [第二步：新增任务](#第二步新增任务)
        - [第三步：直接GLUE编写代码](#第三步直接glue编写代码)
        - [第四步：执行验证一下](#第四步执行验证一下)
        - [第五步：启动任务](#第五步启动任务)
      - [示例2：GLUE - Shell模式](#示例2glue---shell模式)
        - [第一步：新增执行器](#第一步新增执行器-1)
        - [第二步：新增任务](#第二步新增任务-1)
        - [第三步：直接GLUE编写代码](#第三步直接glue编写代码-1)
        - [第四步：执行验证一下](#第四步执行验证一下-1)
        - [第五步：启动任务](#第五步启动任务-1)
        - [补充 - xxl-job如何判断shell任务执行失败](#补充---xxl-job如何判断shell任务执行失败)

## 编写定时任务&使用示例

### Bean模式 - 在执行器中编写代码

#### 示例1：BEAN模式（方法形式）

##### 第一步：在执行器项目中，编写定时任务代码

> 提示：spring bean + @XxlJob即可完成

![image-20220630234203951](../../repository/image-20220630234203951.png)

```java
package com.szlaozicl.mybatisplusdemo.job;

import com.ideaaedi.commonds.exception.ExceptionUtil;
import com.szlaozicl.mybatisplusdemo.SysController;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.annotation.XxlJob;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class XxlJobs {
    
    /*
     * BEAN模式下，也是可以正常使用@Slf4j之类的注解的
     */
    private Logger log = LoggerFactory.getLogger(XxlJobs.class);
    
    @Autowired
    SysController sysController;

    /**
     * XxlJob(value = "demoJobHandler") 指定jobHandler的名称为demoJobHandler
     */
    @XxlJob(value = "demoJobHandler")
    public void demoJobHandler() {
        try {
           // 如果想在调度中心查看到任务执行日志，需要使用XxlJobHelper打印日志， 和logback类似，支持占位符{}解析
            XxlJobHelper.log("定时任务业务逻辑开始了.");
            // 程序日志还是需要用常规log打的
            log.info("定时任务业务逻辑开始了.");
            
            // 获取到参数
            String jobParamStr = XxlJobHelper.getJobParam();
            XxlJobHelper.log("jobParamStr is -> {}", jobParamStr);
            log.info("jobParamStr is -> {}", jobParamStr);
            int length = Integer.parseInt(jobParamStr);
            
            String randomStr = sysController.randomStr(length);
            XxlJobHelper.log("randomStr is -> {}", randomStr);
            log.info("randomStr is -> {}", randomStr);
    
            XxlJobHelper.log("定时任务业务逻辑结束了.");
            log.info("定时任务业务逻辑结束了.");
            
            // 表示任务执行成功 (不设置，默认成功)
            // XxlJobHelper.handleSuccess();
            XxlJobHelper.handleSuccess("处理成功啦!");
        } catch (Exception e) {
            log.error("demoJobHandler occur exception.", e);
            
            // 表示任务执行失败
            // XxlJobHelper.handleFail();
            XxlJobHelper.handleFail("处理失败啦!\t" + ExceptionUtil.getStackTraceMessage(e));
        }
        
    }
    
}
```

##### 第二步：重新部署执行器所在的项目

##### 第三步：新增执行器

> 提示：该执行器的AppName及相关的ip地址，需要和执行器保持一致；若执行器项目集群部署，那么他们的AppName需要保持一致
>
> 提示：若要使用的执行器已存在，则可跳过此步

![image-20220630222628628](../../repository/image-20220630222628628.png)

![image-20220630234652274](../../repository/image-20220630234652274.png)

注：点击保存后可查看到自动获取的ip（如果没有出来的话，可能还需要手动点一下编辑，然后啥也不做，直接确认，再次触发自动获取ip），如果与实际的不符，则需要手动编辑一下，改成手动录入，手动指定正确的机器地址

##### 第四步：新增任务

![image-20220630222603745](../../repository/image-20220630222603745.png)

![image-20220630235229563](../../repository/image-20220630235229563.png)

![image-20220630235259132](../../repository/image-20220630235259132.png)

##### 第五步：验证执行一下

1. 先执行一下，看看是否达到了预期效果

   ![image-20220630235425555](../../repository/image-20220630235425555.png)

2. 查看执行结果（查看调度日志）

   ![image-20220701000827753](../../repository/image-20220701000827753.png)

3. 查看该任务的执行日志（在`调度日志`界面）

   > 执行日志，即执行器侧打印的日志

   ![image-20220701000915579](../../repository/image-20220701000915579.png)
   
   ![image-20220701000946747](../../repository/image-20220701000946747.png)

##### 第六步：启动任务

确认任务无误后，就可以启动任务了

![image-20220630222215514](../../repository/image-20220630222215514.png)

![image-20220630222240609](../../repository/image-20220630222240609.png)

#### 示例2：BEAN模式（类形式）

> 说明：此示例与`示例1：BEAN模式（方法形式）`只有第一步不同，其余都一样，所以下面只示例第一步，其余步骤不再赘述

##### 第一步：在执行器项目中，编写定时任务代码

- 编写`IJobHandler`子类

  ```java
  package com.szlaozicl.mybatisplusdemo.job;
  
  import com.ideaaedi.commonds.exception.ExceptionUtil;
  import com.szlaozicl.mybatisplusdemo.SysController;
  import com.xxl.job.core.context.XxlJobHelper;
  import com.xxl.job.core.handler.IJobHandler;
  import org.slf4j.Logger;
  import org.slf4j.LoggerFactory;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.stereotype.Component;
  
  @Component
  public class DemoXxlJob extends IJobHandler {
      
      /*
       * BEAN模式下，也是可以正常使用@Slf4j之类的注解的
       */
      private Logger log = LoggerFactory.getLogger(DemoXxlJob.class);
      
      @Autowired
      SysController sysController;
      
      @Override
      public void execute() throws Exception {
          try {
              // 如果想在调度中心查看到任务执行日志，需要使用XxlJobHelper打印日志， 和logback类似，支持占位符{}解析
              XxlJobHelper.log("定时任务业务逻辑开始了.");
              // 程序日志还是需要用常规log打的
              log.info("定时任务业务逻辑开始了.");
              
              // 获取到参数
              String jobParamStr = XxlJobHelper.getJobParam();
              XxlJobHelper.log("jobParamStr is -> {}", jobParamStr);
              log.info("jobParamStr is -> {}", jobParamStr);
              int length = Integer.parseInt(jobParamStr);
              
              String randomStr = sysController.randomStr(length);
              XxlJobHelper.log("randomStr is -> {}", randomStr);
              log.info("randomStr is -> {}", randomStr);
              
              XxlJobHelper.log("定时任务业务逻辑结束了.");
              log.info("定时任务业务逻辑结束了.");
              
              // 表示任务执行成功 (不设置，默认成功)
              // XxlJobHelper.handleSuccess();
              XxlJobHelper.handleSuccess("处理成功啦!");
          } catch (Exception e) {
              log.error("MyGlueJobHandler occur exception.", e);
              
              // 表示任务执行失败
              // XxlJobHelper.handleFail();
              XxlJobHelper.handleFail("处理失败啦!\t" + ExceptionUtil.getStackTraceMessage(e));
          }
          
      }
      
  }
  ```

- 注册任务，并指定JobHandler的名称

  > 在启动项目时（或启动项目后），使用`XxlJobExecutor`进行注册

  ```java
  package com.szlaozicl.mybatisplusdemo;
  
  import com.szlaozicl.mybatisplusdemo.job.DemoXxlJob;
  import com.xxl.job.core.executor.XxlJobExecutor;
  import org.springframework.beans.factory.annotation.Autowired;
  import org.springframework.boot.ApplicationArguments;
  import org.springframework.boot.ApplicationRunner;
  import org.springframework.boot.SpringApplication;
  import org.springframework.boot.autoconfigure.SpringBootApplication;
  
  import java.io.IOException;
  
  @SpringBootApplication
  public class SpringBootDemoApplication implements ApplicationRunner {
      
      public static void main(String[] args) throws IOException {
          SpringApplication.run(SpringBootDemoApplication.class, args);
      }
      
      @Autowired
      DemoXxlJob demoXxlJob;
      
      @Override
      public void run(ApplicationArguments args) throws Exception {
          XxlJobExecutor.registJobHandler("demoJobHandler", demoXxlJob);
      }
  }
  ```

##### 第二步：重新部署执行器所在的项目

同`示例1：BEAN模式（方法形式）`

##### 第三步：新增执行器

同`示例1：BEAN模式（方法形式）`

##### 第四步：新增任务

同`示例1：BEAN模式（方法形式）`

##### 第五步：验证执行一下

同`示例1：BEAN模式（方法形式）`

##### 第六步：启动任务

同`示例1：BEAN模式（方法形式）`

### GLUE模式 - 直接在调度中心编写代码

> GLUE模式又可分为：
>
> - GLUE - Java模式
> - GLUE - Shell模式
> - GLUE - Python模式
> - GLUE - PHP模式
> - GLUE - NodeJS模式
> - GLUE - Powershell模式
>
> 下面以`GLUE - Java模式`、`GLUE - Shell模式`进行示例说明

#### 示例1：GLUE - Java模式

##### 第一步：新增执行器

> 提示：若要使用的执行器已存在，则可跳过此步

![image-20220630222628628](../../repository/image-20220630222628628.png)

![image-20220630223154172](../../repository/image-20220630223154172.png)

注：机器地址不限于ip和端口，（为了便于nginx转发，）也可以将uri前缀，如`http://127.0.0.1:9999/job`



![image-20220630223223033](../../repository/image-20220630223223033.png)

##### 第二步：新增任务

![image-20220630222603745](../../repository/image-20220630222603745.png)

![image-20220630223516902](../../repository/image-20220630223516902.png)

![image-20220630223545568](../../repository/image-20220630223545568.png)

##### 第三步：直接GLUE编写代码

> 提示，可以先去执行器所在的项目里完成代码的编写，然后将代码粘贴到GLUE(Java)面板里（，部分版本里，该面板会自动去掉package包声明；我们不管，我们全部复制进去就行）

![image-20220630223615831](../../repository/image-20220630223615831.png)

![image-20220630232016287](../../repository/image-20220630232016287.png)

给出文字版：

```java
import com.ideaaedi.commonds.exception.ExceptionUtil;
import com.szlaozicl.mybatisplusdemo.SysController;
import com.xxl.job.core.context.XxlJobHelper;
import com.xxl.job.core.handler.IJobHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * 1. 需要保证： 当前代码中所有用到的类，在运行中的执行器里都能找到
 * 2. 在import时，最好把每一个类都导入进来；不要因为是在执行器项目IDE中编写的，同包不用import，就不import； 最好都import
 */
public class MyGlueJobHandler extends IJobHandler {
    
    // Glue Java 模式下， 尽量避免使用一些如lombok之类的编译时生成代码的工具
    private Logger logger = LoggerFactory.getLogger(MyGlueJobHandler.class);
    
    // 可以使用@Resource或@Autowired注入需要的spring bean
    @Autowired
    SysController sysController;
    
    
    @Override
    public void execute() throws Exception {
        try {
           // 如果想在调度中心查看到任务执行日志，需要使用XxlJobHelper打印日志， 和logback类似，支持占位符{}解析
            XxlJobHelper.log("定时任务业务逻辑开始了.");
            // 程序日志还是需要用常规log打的
            logger.info("定时任务业务逻辑开始了.");
            
            // 获取到参数
            String jobParamStr = XxlJobHelper.getJobParam();
            XxlJobHelper.log("jobParamStr is -> {}", jobParamStr);
            logger.info("jobParamStr is -> {}", jobParamStr);
            int length = Integer.parseInt(jobParamStr);
            
            String randomStr = sysController.randomStr(length);
            XxlJobHelper.log("randomStr is -> {}", randomStr);
            logger.info("randomStr is -> {}", randomStr);
    
            XxlJobHelper.log("定时任务业务逻辑结束了.");
            logger.info("定时任务业务逻辑结束了.");
            
            // 表示任务执行成功 (不设置，默认成功)
            // XxlJobHelper.handleSuccess();
            XxlJobHelper.handleSuccess("处理成功啦!");
        } catch (Exception e) {
            logger.error("MyGlueJobHandler occur exception.", e);
            
            // 表示任务执行失败
            // XxlJobHelper.handleFail();
            XxlJobHelper.handleFail("处理失败啦!\t" + ExceptionUtil.getStackTraceMessage(e));
        }
        
    }
    
}
```

##### 第四步：执行验证一下

1. 先执行一下，看看是否达到了预期效果

   ![image-20220630232315803](../../repository/image-20220630232315803.png)

2. 查看执行结果（查看调度日志）

   ![image-20220630232815847](../../repository/image-20220630232815847.png)

3. 查看该任务的执行日志（在`调度日志`界面）

   > 执行日志，即执行器侧打印的日志

   ![image-20220630232905053](../../repository/image-20220630232905053.png)
   
   ![image-20220630232953643](../../repository/image-20220630232953643.png)

##### 第五步：启动任务

确认任务无误后，就可以启动任务了

![image-20220630222215514](../../repository/image-20220630222215514.png)

![image-20220630222240609](../../repository/image-20220630222240609.png)

#### 示例2：GLUE - Shell模式

##### 第一步：新增执行器

> 提示：若要使用的执行器已存在，则可跳过此步

![1656569588102](../../repository/1656569588102.png)

![1656571689818](../../repository/1656571689818.png)

注：机器地址不限于ip和端口，（为了便于nginx转发，）也可以将uri前缀，如`http://127.0.0.1:9999/job`



![1656569818303](../../repository/1656569818303.png)

##### 第二步：新增任务

![1656569862588](../../repository/1656569862588.png)

![1656570281365](../../repository/1656570281365.png)

![1656570651615](../../repository/1656570651615.png)

##### 第三步：直接GLUE编写代码

![1656570678389](../../repository/1656570678389.png)

![1656573737710](../../repository/1656573737710.png)

这是一个定时备份数据库的shell，给出文字版：

```shell
#!/bin/bash
echo "xxl-job: backup database start"

echo "脚本位置：$0"
echo "任务参数：$1"
echo "分片序号：$2"
echo "分片总数：$3"

# 切换至备份文件存放目录
cd /usr/local/tmp/backup/
D=`date  +"%Y%m%d"`
if [ ! -f "DB.$D.tar.gz" ];then
        if [ ! -d $D ] ; then
                mkdir $D
        fi 
        cd $D
        if [ ! -f "DB.$D.tar.gz" ];then
                # 利用mysqldump备份数据库（输入你自己的账号、密码、要备份的数据库名）
                mysqldump -uroot -ppwd123 myDbName1 | gzip > myDbName1.$D.sql.gz
                mysqldump -uroot -ppwd123 myDbName2 | gzip > myDbName2.$D.sql.gz
                # 切换至上一级目录
                cd ../
                # 将$D文件夹 打包成 DB.$D.tar.gz文件
                tar -cvzf DB.$D.tar.gz $D
                # 删除$D文件夹
                rm -rf $D
        fi
fi
echo "backup database end"
exit 0
```

注：上述脚本的输出内容，可以去**执行器运行日志文件**中查看

##### 第四步：执行验证一下

> 先执行一下，看看是否达到了预期效果

1. 执行一次

   ![1656571169575](../../repository/1656571169575.png)

2. 查看执行结果（查看调度日志）

   ![1656572048191](../../repository/1656572048191.png)

3. 查看该任务的执行日志（在`调度日志`界面）

   > 执行日志，即执行器侧打印的日志

   ![image-20220630232635627](../../repository/image-20220630232635627.png)

   ```
   2022-06-30 15:19:56 [com.xxl.job.core.thread.JobThread#run]-[133]-[xxl-job, JobThread-3-1656573596424] <br>----------- xxl-job job execute start -----------<br>----------- Param:我是参数
   2022-06-30 15:19:56 [com.xxl.job.core.handler.impl.ScriptJobHandler#execute]-[80]-[xxl-job, JobThread-3-1656573596424] ----------- script file:/data/applogs/xxl-job/jobhandler/gluesource/3_1656573588000.sh -----------
   xxl-job: backup database start
   脚本位置：/data/applogs/xxl-job/jobhandler/gluesource/3_1656573588000.sh
   任务参数：我是参数
   分片序号：0
   分片总数：1
   mysqldump: [Warning] Using a password on the command line interface can be insecure.
   Warning: A partial dump from a server that has GTIDs will by default include the GTIDs of all transactions, even those that changed suppressed parts of the database. If you don't want to restore GTIDs, pass --set-gtid-purged=OFF. To make a complete dump, pass --all-databases --triggers --routines --events. 
   20220630/
   20220630/xxl_job.20220630.sql.gz
   backup database end
   2022-06-30 15:19:57 [com.xxl.job.core.thread.JobThread#run]-[179]-[xxl-job, JobThread-3-1656573596424] <br>----------- xxl-job job execute end(finish) -----------<br>----------- Result: handleCode=200, handleMsg = null
   2022-06-30 15:19:57 [com.xxl.job.core.thread.TriggerCallbackThread#callbackLog]-[197]-[xxl-job, executor TriggerCallbackThread] <br>----------- xxl-job job callback finish.
   ```

4. 去执行器所在的服务器上，验证一下

   ![1656573854867](../../repository/1656573854867.png)

   注：且`tar -zxvf xxx.tar.gz`解压缩后，进一步验证完里面的内容，也是符合预期的

##### 第五步：启动任务

确认任务无误后，就可以启动任务了

![image-20220630222215514](../../repository/image-20220630222215514.png)

![image-20220630222240609](../../repository/image-20220630222240609.png)

##### 补充 - xxl-job如何判断shell任务执行失败

> 特别注意：只有当收到exit信号且返回不是0时，才认为是失败！！！详见源码`com.xxl.job.core.handler.impl.ScriptJobHandler#execute`
>
> 注：如果程序还没走到exit指令之前就异常，那么xxl-job还是会认为任务成功的，
>
> - exit 0：shell任务执行成功
>
> - exit -1：shell任务执行失败（其实退出值不是0，就会被认为是任务执行是失败的）

**一个根据http响应返回0或-1的shell例子：**

```shell
#!/bin/bash
echo "xxl-job: hello shell"

# 发送请求并接收结果
result=$(curl -X POST -H "'Content-type':'application/json'" -d '{"token":"abc"}' -s http://local.idea-aedi.com/api/token/verfy)
echo $result
# 利用jd指令（如果没有安装，则需要安装），提取json中的status字段值
# 可以使用以下指令进行安装jq （可参考https://blog.csdn.net/weixin_44320761/article/details/122818874）
## 安装EPEL源
#yum install epel-release -y
## 安装完EPEL源后，可以查看下jq包是否存在
#yum list jq
## 安装jq
##yum install jq -y
status=$(echo $result | jq '.status')

echo $status
if [ $status == '1' ]; then
    echo 'success!!'
    exit 0
elif [ $status == 'true' ]; then
    echo 'success!!'
    exit 0
elif [ $status == '0' ]; then
    echo 'false!!'
    exit -1
else
    echo 'false!!'
    exit -1
fi
```







