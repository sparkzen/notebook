# Centos7上安装容器运行时Containerd和命令行工具nerdctl、crictl

- [Centos7上安装容器运行时Containerd和命令行工具nerdctl、crictl](#centos7上安装容器运行时containerd和命令行工具nerdctlcrictl)
  - [安装Container](#安装container)
    - [cgroup driver说明](#cgroup-driver说明)
    - [安装](#安装)
    - [配置](#配置)
  - [安装nerdctl](#安装nerdctl)
    - [说明](#说明)
    - [安装](#安装-1)
  - [安装crictl](#安装crictl)
    - [说明](#说明-1)
    - [安装](#安装-2)
  - [安装buildkit(可选，用于nerdctl build)](#安装buildkit可选用于nerdctl-build)
    - [说明](#说明-2)
    - [安装](#安装-3)
  - [相关资料](#相关资料)

## 安装Container

### cgroup driver说明

Linux使用cgroup进行资源的隔离控制

CentOS启动时使用systemd(即我们使用的systemctl，CentOS7之前是使用/etc/rc.d/init.d进行初始化系统，会生成一个cgroup manager去管理cgroupfs。如果让Containerd直接去管理cgroupfs，又会生成一个cgroup manager。一个系统有两个cgroup manager很不稳定。所以我们需要配置Containerd直接使用systemd去管理cgroupfs

### 安装

下载并解压

```SHELL
# 拿一个目录来放置containerd下载的东西
cd /usr/local/
mkdir containerd
cd containerd/
# 下载
wget https://github.com/containerd/containerd/releases/download/v1.4.12/cri-containerd-cni-1.4.12-linux-amd64.tar.gz
mkdir cri-containerd-cni
tar -zxvf cri-containerd-cni-1.4.12-linux-amd64.tar.gz -C cri-containerd-cni
```

将解压的文件，复制到系统的配置目录和执行目录

```shell
cp -a cri-containerd-cni/etc/systemd/system/containerd.service /etc/systemd/system
cp -a cri-containerd-cni/etc/crictl.yaml /etc
cp -a cri-containerd-cni/etc/cni /etc
cp -a cri-containerd-cni/usr/local/sbin/runc /usr/local/sbin
cp -a cri-containerd-cni/usr/local/bin/* /usr/local/bin
cp -a cri-containerd-cni/opt/* /opt
```

启动containerd

```shell
# systemctl重新加载所有服务
systemctl daemon-reload
# 立即启动containerd，设置开机启动containerd
systemctl enable containerd --now 
```

### 配置

获取默认配置文件（通过tee将其写入到指定文件）

```
mkdir /etc/containerd
containerd config default | tee /etc/containerd/config.toml
```

生成的/etc/containerd/config.toml文件内容形如

```yaml
version = 2
root = "/var/lib/containerd"
state = "/run/containerd"
plugin_dir = ""
disabled_plugins = []
required_plugins = []
oom_score = 0

[grpc]
  address = "/run/containerd/containerd.sock"
  tcp_address = ""
  tcp_tls_cert = ""
  tcp_tls_key = ""
  uid = 0
  gid = 0
  max_recv_message_size = 16777216
  max_send_message_size = 16777216

[ttrpc]
  address = ""
  uid = 0
  gid = 0

[debug]
  address = ""
  uid = 0
  gid = 0
  level = ""

[metrics]
  address = ""
  grpc_histogram = false

[cgroup]
  path = ""

[timeouts]
  "io.containerd.timeout.shim.cleanup" = "5s"
  "io.containerd.timeout.shim.load" = "5s"
  "io.containerd.timeout.shim.shutdown" = "3s"
  "io.containerd.timeout.task.state" = "2s"

[plugins]
  [plugins."io.containerd.gc.v1.scheduler"]
    pause_threshold = 0.02
    deletion_threshold = 0
    mutation_threshold = 100
    schedule_delay = "0s"
    startup_delay = "100ms"
  [plugins."io.containerd.grpc.v1.cri"]
    disable_tcp_service = true
    stream_server_address = "127.0.0.1"
    stream_server_port = "0"
    stream_idle_timeout = "4h0m0s"
    enable_selinux = false
    selinux_category_range = 1024
    sandbox_image = "k8s.gcr.io/pause:3.2"
    stats_collect_period = 10
    systemd_cgroup = false
    enable_tls_streaming = false
    max_container_log_line_size = 16384
    disable_cgroup = false
    disable_apparmor = false
    restrict_oom_score_adj = false
    max_concurrent_downloads = 3
    disable_proc_mount = false
    unset_seccomp_profile = ""
    tolerate_missing_hugetlb_controller = true
    disable_hugetlb_controller = true
    ignore_image_defined_volumes = false
    [plugins."io.containerd.grpc.v1.cri".containerd]
      snapshotter = "overlayfs"
      default_runtime_name = "runc"
      no_pivot = false
      disable_snapshot_annotations = true
      discard_unpacked_layers = false
      [plugins."io.containerd.grpc.v1.cri".containerd.default_runtime]
        runtime_type = ""
        runtime_engine = ""
        runtime_root = ""
        privileged_without_host_devices = false
        base_runtime_spec = ""
      [plugins."io.containerd.grpc.v1.cri".containerd.untrusted_workload_runtime]
        runtime_type = ""
        runtime_engine = ""
        runtime_root = ""
        privileged_without_host_devices = false
        base_runtime_spec = ""
      [plugins."io.containerd.grpc.v1.cri".containerd.runtimes]
        [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc]
          runtime_type = "io.containerd.runc.v2"
          runtime_engine = ""
          runtime_root = ""
          privileged_without_host_devices = false
          base_runtime_spec = ""
          [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
    [plugins."io.containerd.grpc.v1.cri".cni]
      bin_dir = "/opt/cni/bin"
      conf_dir = "/etc/cni/net.d"
      max_conf_num = 1
      conf_template = ""
    [plugins."io.containerd.grpc.v1.cri".registry]
      [plugins."io.containerd.grpc.v1.cri".registry.mirrors]
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.io"]
          endpoint = ["https://registry-1.docker.io"]
    [plugins."io.containerd.grpc.v1.cri".image_decryption]
      key_model = ""
    [plugins."io.containerd.grpc.v1.cri".x509_key_pair_streaming]
      tls_cert_file = ""
      tls_key_file = ""
  [plugins."io.containerd.internal.v1.opt"]
    path = "/opt/containerd"
  [plugins."io.containerd.internal.v1.restart"]
    interval = "10s"
  [plugins."io.containerd.metadata.v1.bolt"]
    content_sharing_policy = "shared"
  [plugins."io.containerd.monitor.v1.cgroups"]
    no_prometheus = false
  [plugins."io.containerd.runtime.v1.linux"]
    shim = "containerd-shim"
    runtime = "runc"
    runtime_root = ""
    no_shim = false
    shim_debug = false
  [plugins."io.containerd.runtime.v2.task"]
    platforms = ["linux/amd64"]
  [plugins."io.containerd.service.v1.diff-service"]
    default = ["walking"]
  [plugins."io.containerd.snapshotter.v1.devmapper"]
    root_path = ""
    pool_name = ""
    base_image_size = ""
    async_remove = false
```

修改/etc/containerd/config.toml配置文件

```txt
[root@k8s-master ~]# cat /etc/containerd/config.toml 
......省略部分......
    enable_selinux = false
    selinux_category_range = 1024
    # 使用国内的阿里云镜像
    #sandbox_image = "k8s.gcr.io/pause:3.2"
    sandbox_image = "registry.cn-hangzhou.aliyuncs.com/google_containers/pause:3.2"
    stats_collect_period = 10
    systemd_cgroup = false
......省略部分......
          privileged_without_host_devices = false
          base_runtime_spec = ""
          [plugins."io.containerd.grpc.v1.cri".containerd.runtimes.runc.options]
            # 配置Containerd直接使用systemd去管理cgroupfs
            SystemdCgroup = true
    [plugins."io.containerd.grpc.v1.cri".cni]
      bin_dir = "/opt/cni/bin"
......省略部分......
      [plugins."io.containerd.grpc.v1.cri".registry.mirrors]
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.io"]
          #endpoint = ["https://registry-1.docker.io"]
          # 注释上面那行，添加下面三行（切换插件镜像下载位置）
          endpoint = ["https://docker.mirrors.ustc.edu.cn"] # line1
        [plugins."io.containerd.grpc.v1.cri".registry.mirrors."k8s.gcr.io"] # line2
          endpoint = ["https://registry.cn-hangzhou.aliyuncs.com/google_containers"] # line3
    [plugins."io.containerd.grpc.v1.cri".image_decryption]
      key_model = ""
......省略部分......
```

重启containerd

```shell
systemctl daemon-reload
systemctl restart containerd
```

## 安装nerdctl

### 说明

nerdctl是containerd原生的命令行管理工具，和Docker的命令行兼容

nerdctl管理的容器、镜像与Kubernetes的容器、镜像是完全隔离的，不能互通

目前只能通过crictl命令行工具来查看、拉取Kubernetes的容器、镜像

nerdctl和containerd的版本对应关系，参考[github](https://github.com/containerd/nerdctl/blob/v0.6.1/go.mod)：

![1658394187789](../repository/1658394187789.png)

### 安装

```shell
# 下载
wget https://github.com/containerd/nerdctl/releases/download/v0.6.1/nerdctl-0.6.1-linux-amd64.tar.gz
# 创建一个目录来放解压后的文件
mkdir nerdctl
tar -zxvf nerdctl-0.6.1-linux-amd64.tar.gz -C nerdctl
## 文件复制至/usr/bin
cp -a nerdctl/nerdctl /usr/bin
# 运行测试一下
nerdctl images
```

注： `nerdctl run -p containerPort:hostPort`的端口映射无效，只能通过`nerdctl run -net host`来实现

## 安装crictl

### 说明

crictl是Kubernetes用于管理Containerd上的镜像和容器的一个命令行工具，主要用于Debug

### 安装

下载解压

```shell
wget https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.24.0/crictl-v1.24.0-linux-amd64.tar.gz
tar -zxvf crictl-v1.24.0-linux-amd64.tar.gz -C /usr/local/bin
```

创建并配置crictl

```shell
# 创建配置文件并配置
cat > /etc/crictl.yaml <<EOF
runtime-endpoint: unix:///var/run/containerd/containerd.sock
image-endpoint: unix:///var/run/containerd/containerd.sock
timeout: 10
debug: false
pull-image-on-create: false
EOF

# 重新加载服务
systemctl daemon-reload
```

使用测试

```shell
crictl images
```

## 安装buildkit(可选，用于nerdctl build)

### 说明

buildkit主要用于nerdctl 使用`nerdctl build`指令进行Dockerfile的镜像构建

buildkit和containerd的版本对应关系，参考[github](https://github.com/moby/buildkit/blob/v0.8.3/go.mod)：

![1658394753693](../repository/1658394753693.png)

### 安装

下载、解压、安装客户端buildkit

```shell
# 创建目录放置相关文件
mkdir buildkit
cd buildkit/
# 下载
wget https://github.com/moby/buildkit/releases/download/v0.8.3/buildkit-v0.8.3.linux-amd64.tar.gz
# 解压
tar -zxvf buildkit-v0.8.3.linux-amd64.tar.gz
# 复制
cp -a bin /usr/local
```

编写buildkitd的启动文件

```shell
vim /etc/systemd/system/buildkit.service
```

/etc/systemd/system/buildkit.service的内容：

```txt
[Unit]
Description=BuildKit
Documentation=https://github.com/moby/buildkit

[Service]
ExecStart=/usr/local/bin/buildkitd --oci-worker=false --containerd-worker=true

[Install]
WantedBy=multi-user.target
```

启动buildkitd服务端程序

```shell
立即启动buildkit，设置开机启动buildkit
systemctl enable buildkit --now
```

## 相关资料

- [CSDN](https://blog.csdn.net/yy8623977/article/details/124707389)