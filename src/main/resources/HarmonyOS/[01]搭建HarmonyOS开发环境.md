# 搭建HarmonyOS开发环境

- [搭建HarmonyOS开发环境](#搭建harmonyos开发环境)
  - [第一步：下载开发工具DevEco Studio](#第一步下载开发工具deveco-studio)
  - [第二步：下载后解压出exe安装包，双击进行安装](#第二步下载后解压出exe安装包双击进行安装)
  - [第三步：打开DevEco Studio](#第三步打开deveco-studio)
  - [第四步：设置SDK、工具集、构建工具](#第四步设置sdk工具集构建工具)
  - [第五步：新建一个项目](#第五步新建一个项目)
  - [第六步：安装模拟器（需要华为账号登录、授权、认证等）](#第六步安装模拟器需要华为账号登录授权认证等)
  - [第七步：运行模拟器](#第七步运行模拟器)
  - [第八步：运行项目代码，预览调试](#第八步运行项目代码预览调试)

---

## 第一步：下载开发工具[DevEco Studio](https://developer.harmonyos.com/cn/develop/deveco-studio)

![image-20220117231904482](../repository/image-20220117231904482.png)

## 第二步：下载后解压出exe安装包，双击进行安装

![image-20220117232408787](../repository/image-20220117232408787.png)

![image-20220117232251546](../repository/image-20220117232251546.png)

## 第三步：打开DevEco Studio

提示：第二步完成后，会提示你进行重启，重启后打开DevEco Studio即可。

![image-20220117233141163](../repository/image-20220117233141163.png)

![image-20220117233615662](../repository/image-20220117233615662.png)

## 第四步：设置SDK、工具集、构建工具

![image-20220117234005761](../repository/image-20220117234005761.png)

![image-20220117234235825](../repository/image-20220117234235825.png)

![image-20220117234321086](../repository/image-20220117234321086.png)

![image-20220117234651956](../repository/image-20220117234651956.png)

## 第五步：新建一个项目

![image-20220117234802037](../repository/image-20220117234802037.png)

![image-20220117234953256](../repository/image-20220117234953256.png)

![image-20220117235801494](../repository/image-20220117235801494.png)

![image-20220118000016488](../repository/image-20220118000016488.png)

注：如果第四步时不指定已安装好了的gradle的话，那么第一次创建项目时，会自动下载gradle。

## 第六步：安装模拟器（需要华为账号登录、授权、认证等）

![image-20220118000445815](../repository/image-20220118000445815.png)

![image-20220118000817689](../repository/image-20220118000817689.png)

![image-20220118000824375](../repository/image-20220118000824375.png)

![image-20220118000829372](../repository/image-20220118000829372.png)

![image-20220118000851527](../repository/image-20220118000851527.png)

再返回开发哦工具，点击Agree

![image-20220118000932621](../repository/image-20220118000932621.png)

然后提示需要进行实名认证，按提示点击进入实名认证

![image-20220118001123621](../repository/image-20220118001123621.png)

![image-20220118001215653](../repository/image-20220118001215653.png)

![image-20220118001444886](../repository/image-20220118001444886.png)

先跳转至华为云进行授权

![image-20220118001434154](../repository/image-20220118001434154.png)

![image-20220118001529996](../repository/image-20220118001529996.png)

![image-20220118001548789](../repository/image-20220118001548789.png)

![image-20220118001620838](../repository/image-20220118001620838.png)

完善个人信息

![image-20220118002016776](../repository/image-20220118002016776.png)

![image-20220118002051398](../repository/image-20220118002051398.png)

此时，需要先登出，然后再登入即可

![image-20220118003038855](../repository/image-20220118003038855.png)

同一位置，点击登入

![image-20220118003144301](../repository/image-20220118003144301.png)

再进入设备管理

![image-20220118003228119](../repository/image-20220118003228119.png)

![image-20220118003332586](../repository/image-20220118003332586.png)

## 第七步：运行模拟器

![image-20220118003501239](../repository/image-20220118003501239-16424373020841.png)

![image-20220118003705224](../repository/image-20220118003705224.png)

##  第八步：运行项目代码，预览调试

提示：第七步只是模拟器运行起来了，和代码运行起来了是两回事。

![image-20220118004508845](../repository/image-20220118004508845.png)

运行起来后可看到

![image-20220118004712313](../repository/image-20220118004712313.png)

注：如果gradle没有安装好，那么代码是run不起来的。
