# HiLog日志的使用

- [HiLog日志的使用](#hilog日志的使用)
  - [第一步：定义日志标签](#第一步定义日志标签)
  - [第二步：使用HiLog打印日志](#第二步使用hilog打印日志)
  - [第三步：查看日志](#第三步查看日志)

## 第一步：定义日志标签

创建日志标签对象`HiLogLabel(int type, int domain, String tag)`，其中：

- type：用于指定输出日志的类型。HiLog中当前只提供了一种日志类型，即应用/服务日志类型`LOG_APP`
- domain：用于指定输出日志所对应的业务领域，取值范围为0x0~0xFFFFF，开发者可以根据需要进行自定义
- tag：用于指定日志标识，可以为任意字符串，建议标识调用所在的类或者业务行为

示例

```java
static final HiLogLabel hiLogLabel = new HiLogLabel(HiLog.LOG_APP, 0x00001, "study-harmony-os");
```

## 第二步：使用HiLog打印日志

相关方法有`debug`/`info`/`warn`/`error`/`fatal`，相关参数（以debug为例）有`debug(HiLogLabel label, String format, Object... args)`，其中：

- label：定义好的HiLogLabel标签

- format：格式字符串，用于日志的格式化输出。格式字符串中可以设置多个参数，例如格式字符串为"Failed to visit %s， %d"，"%s"为参数类型为string的变参标识，"%d"为参数类型为整数的变参标识，具体取值在args中定义。

  注：每个参数需添加隐私标识，分为`{public}`或`{private}`，**默认为{private}**。{public}表示日志打印结果可见；{private}表示日志打印结果不可见，输出结果为\<private\>。

示例

```java
HiLog.info(hiLogLabel, "%s -> %d", "当前值", 1);
```

## 第三步：查看日志

![1648880391915](../../../repository/1648880391915.png)