# HarmonyOS项目结构 & 资源目录详解

- [HarmonyOS项目结构 & 资源目录详解](#harmonyos项目结构--资源目录详解)
  - [项目结构 & 资源目录详解](#项目结构--资源目录详解)

## 项目结构 & 资源目录详解

```
│  build.gradle  // gradle项目全局配置文件
│  gradle.properties // gradle存放复用变量key-value的抽取文件
│  local.properties  // DevEco Studio记录本地hwsdk、nodejs、npm等位置信息的配置文件
│  package.json   // 对包信息的补充文件
│  settings.gradle  // gradle设置文件
├─build   // 项目的编译输出文件夹
├─entry
│  │  build.gradle   // 当前模块的gradle配置文件
│  │  proguard-rules.pro // 代码混淆规则配置文件
│  │  
│  ├─build   // 当前模块的编译输出文件夹
│  ├─libs    // 第三方依赖
│  └─src
│      ├─main
│      │  │  config.json // 模块配置文件
│      │  │  
│      │  ├─java
│      │  │  └─com
│      │  │      └─example
│      │  │          └─countdemo
│      │  │              │  MainAbility.java   // 模块的"主页"
│      │  │              │  MyApplication.java // 相当于模块的启动类
│      │  │              │  
│      │  │              └─slice
│      │  │                      MainAbilitySlice.java   // 一个"页面"可能由多个子页面组成，比如MainAbilitySlice就是MainAbility的一个"子页面"；我们写代码一般都在slice"子页面"里面写
│      │  │                      
│      │  └─resources
│      │      ├─base
│      │      │  ├─element // 存放文本元素资源。按文本元素数据类型不同，存进不同的json文件中（boolean.json、color.jsonfloat.json、intarray.json、integer.json、pattern.json、plural.json、strarray.json、string.json）
│      │      │  ├─graphic // 存放可绘制资源，采用XML文件格式
│      │      │  ├─layout  // 存放（界面）布局资源，采用XML文件格式
│      │      │  ├─media   // 存放媒体资源，包括图片、音频、视频等非文本格式的文件
│      │      │  └─profile // 存放其他类型文件，以原始文件形式保存
│      │      ├─rawfile // 存放原始文件
│      │      ├─en // 对base的国际化支持，其它与base一样
│      │      └─zh // 对base的国际化支持，其它与base一样
│      │                  
│      ├─ohosTest // 鸿蒙测试文件夹
│      └─test     // 基础单元测试文件夹
            
```

注：资源文件夹`resources`下，除了`rawfile`下的文件外，其余文件都会被编译为二进制文件，并被赋予资源文件ID，进而可以通过`ResourceTable`进行引用（引用资源文件或资源文件内的数据内容）。

注：rawfile中的文件可以通过ohos.global.resource.ResourceManager来引用。

注：资源文件的介绍详见[here](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/basic-resource-file-categories-0000001052066099)，资源文件的引用详见[here](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/basic-resource-file-example-0000001051733014)。