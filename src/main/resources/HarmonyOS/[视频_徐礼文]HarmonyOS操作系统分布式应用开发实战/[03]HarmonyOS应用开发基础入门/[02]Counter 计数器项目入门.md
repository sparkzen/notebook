# Counter 计数器项目入门

- [Counter 计数器项目入门](#counter-计数器项目入门)
  - [背景](#背景)
  - [相关代码](#相关代码)
    - [关键类位置](#关键类位置)
    - [界面布局ability_main](#界面布局ability_main)
    - [逻辑联动MainAbilitySlice](#逻辑联动mainabilityslice)
  - [相关资料](#相关资料)

## 背景

做一个简单的累加器，如下图，点击【加一】时，上面的值加1。

![1648782078653](../../../repository/1648782078653.png)

## 相关代码

### 关键类位置

![1648782154940](../../../repository/1648782154940.png)

### 界面布局ability_main

```xml
<?xml version="1.0" encoding="utf-8"?>
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:alignment="center"
    ohos:orientation="vertical">

    <!-- 文本 -->
    <Text
        ohos:id="$+id:text_num"
        ohos:height="match_content"
        ohos:width="match_content"
        ohos:background_element="$graphic:background_ability_main"
        ohos:layout_alignment="horizontal_center"
        ohos:text="0"
        ohos:text_size="40vp"
        />

    <!-- 按钮 -->
    <Button
        ohos:id="$+id:add_one"
        ohos:height="60vp"
        ohos:width="100vp"
        ohos:background_element="#fc0"
        ohos:text="加一"
        ohos:text_size="30vp"
        />

</DirectionalLayout>
```

### 逻辑联动MainAbilitySlice

```java
import com.example.countdemo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

import java.util.concurrent.atomic.AtomicInteger;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        // 指定和哪个页面绑定在一起
        super.setUIContent(ResourceTable.Layout_ability_main);
    
        // 找到id为'text_num'的资源
        Text text = (Text)findComponentById(ResourceTable.Id_text_num);
        // 找到id为'add_one'的资源
        Button button = (Button)findComponentById(ResourceTable.Id_add_one);
    
        AtomicInteger num = new AtomicInteger(0);
        // 给按钮添加 点击事件
        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                // 加一
                int currValue = num.addAndGet(1);
                text.setText("当前值：" + currValue);
            }
        });
    }
    
    @Override
    public void onActive() {
        super.onActive();
    }
    
    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
    
}
```

## 相关资料

- [demo下载](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/CountDemo.rar)

