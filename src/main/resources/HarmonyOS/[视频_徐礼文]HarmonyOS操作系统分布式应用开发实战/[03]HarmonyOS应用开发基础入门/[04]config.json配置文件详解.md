# config.json配置文件详解

- [config.json配置文件详解](#configjson配置文件详解)
  - [配置文件的内部结构](#配置文件的内部结构)
  - [app](#app)
  - [deviceConfig](#deviceconfig)
  - [module](#module)

## 配置文件的内部结构

> 提示：更多详见[官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/basic-config-file-elements-0000000000034463#ZH-CN_TOPIC_0000001064016070__table293119513457)

- **app**：表示应用的全局配置信息。同一个应用的不同HAP包的app配置必须保持一致。
- **deviceConfig**：表示应用在具体设备上的配置信息。
- **module**：表示HAP包的配置信息。该标签下的配置只对当前HAP包生效。

## app

app对象包含应用的全局配置信息。

- app
  - bundleName：应用的包名，用于标识应用的唯一性
  - vendor：对应用开发厂商的描述
  - version
    - name：应用的版本号，用于向应用的终端用户呈现
    - code：应用的版本号，仅用于HarmonyOS管理该应用，不对应用的终端用户呈现
    - minCompatibleVersionCode：应用可兼容的最低版本号，用于在跨设备场景下，判断其他设备上该应用的版本是否兼容（格式与version.code字段的格式要求相同）
  - smartWindowSize：用于在悬浮窗场景下表示应用的模拟窗口的尺寸
  - smartWindowDeviceType：可以在哪些设备上使用模拟窗口打开
  - targetBundleList：允许以免安装方式拉起的其他HarmonyOS应用，列表取值为每个HarmonyOS应用的bundleName，多个bundleName之间用英文“,”区分，最多配置5个bundleName

示例

```json
"app": {
    "bundleName": "com.huawei.hiworld.example", 
    "vendor": "huawei", 
    "version": {
        "code": 2, 
        "name": "2.0.0"
    }
}
```

## deviceConfig

deviceConfig包含在具体设备上的应用配置信息，可以包含default、phone、tablet、tv、car、wearable、liteWearable和smartVision等属性。default标签内的配置是适用于所有设备通用，其他设备类型如果有特殊的需求，则需要在该设备类型的标签下进行配置。

- deviceConfig
  - default：所有设备通用的应用配置信息
    - jointUserId：应用的共享userid
    - process：应用或者Ability的进程名
    - supportBackup：应用是否支持备份和恢复
    - compressNativeLibs：libs库是否以压缩存储的方式打包到HAP包
    - network：网络安全性配置
      - cleartextTraffic：是否允许应用使用明文网络流量
      - securityConfig：应用的网络安全配置
        - domainSettings
          - cleartextPermitted：自定义的网域范围内是否允许明文流量传输
          - domains：域名配置信息
  - phone：手机类设备的应用信息配置（当前对象下的详细配置同deviceConfig.default）
  - tablet：平板的应用配置信息（当前对象下的详细配置同deviceConfig.default）
  - tv：智慧屏特有的应用配置信息（当前对象下的详细配置同deviceConfig.default）
  - car：车机特有的应用配置信息（当前对象下的详细配置同deviceConfig.default）
  - wearable：智能穿戴特有的应用配置信息（当前对象下的详细配置同deviceConfig.default）
  - liteWearable：轻量级智能穿戴特有的应用配置信息（当前对象下的详细配置同deviceConfig.default）
  - smartVision：智能摄像头特有的应用配置信息（当前对象下的详细配置同deviceConfig.default）

示例

```json
 "deviceConfig": {
    "default": {
        "process": "com.huawei.hiworld.example", 
        "supportBackup": false,
        "network": {
            "cleartextTraffic": true, 
            "securityConfig": {
                "domainSettings": {
                    "cleartextPermitted": true, 
                    "domains": [
                        {
                            "subdomains": true, 
                            "name": "example.ohos.com"
                        }
                    ]
                }
            }
        }
    }
}
```

## module

module对象包含HAP包的配置信息。

- module
  - mainAbility：HAP包的入口ability名称
  - package：HAP的包结构名称，在应用内应保证唯一性
  - name：HAP的类名
  - description：HAP的描述信息
  - supportedModes：应用支持的运行模式
  - deviceType：允许Ability运行的设备类型
  - distro：HAP发布的具体描述
    - deliveryWithInstall：当前HAP是否支持随应用安装
    - moduleName：当前HAP的名称
    - moduleType：当前HAP的类型，包括两种类型：entry和feature
    - installationFree：当前HAP是否支持免安装特性
  - metaData：HAP的元信息
    - parameters：调用Ability时所有调用参数的元信息
      - description：对调用参数的描述，可以是表示描述内容的字符串，也可以是对描述内容的资源索引以支持多语言
      - name：调用参数的名称
      - type：调用参数的类型，如Integer
    - results：Ability返回值的元信息
      - description：对返回值的描述
      - name：返回值的名字
      - type：返回值的类型
    - customizeData：父级组件的自定义元信息
      - name：数据项的键名称
      - value：数据项的值
      - extra：用户自定义数据格式
  - abilities：当前模块内的所有Ability
    - name：Ability名称
    - description：对Ability的描述
    - icon：Ability图标资源文件的索引
    - label：Ability对用户显示的名称
    - uri：Ability的统一资源标识符
    - launchType：Ability的启动模式
    - visible：Ability是否可以被其他应用调用
    - permissions：其他应用的Ability调用此Ability时需要申请的权限
    - skills：Ability能够接收的Intent的特征
    - deviceCapability：Ability运行时要求设备具有的能力
    - metaData：Ability的元信息
    - type：Ability的类型
    - orientation：Ability的显示模式
    - backgroundModes：后台服务的类型
    - readPermission：读取Ability的数据所需的权限
    - writePermission：向Ability写数据所需的权限
    - configChanges：Ability关注的系统配置集合
    - mission：Ability指定的任务栈
    - targetAbility：当前Ability重用的目标Ability
    - multiUserShared：Ability是否支持多用户状态进行共享，该标签仅适用于data类型的Ability
    - supportPipMode：Ability是否支持用户进入PIP模式
    - formsEnabled：Ability是否支持卡片（forms）功能
    - forms：服务卡片的属性
    - resizeable：Ability是否支持多窗口特性
  - js：基于ArkUI框架开发的JS模块集合，其中的每个元素代表一个JS模块的信息
    - name：JS Component的名字
    - pages：JS Component的页面用于列举JS Component中每个页面的路由信息[页面路径+页面名称]
    - window：用于定义与显示窗口相关的配置
      - designWidth：页面设计基准宽度
      - autoDesignWidth：页面设计基准宽度是否自动计算
    - type：JS应用的类型
  - shortcuts：应用的快捷方式信息
    - shortcutId：快捷方式的ID
    - label：快捷方式的标签信息，即快捷方式对外显示的文字描述信息
    - intents：快捷方式内定义的目标intent信息集合，每个intent可配置两个子标签，targetClass, targetBundle
      - targetClass：快捷方式目标类名
      - targetBundle：快捷方式目标Ability所在应用的包名
  - defPermissions：应用定义的权限
  - reqPermissions：应用运行时向系统申请的权限
  - colorMode：应用自身的颜色模式
  - resizeable：应用是否支持多窗口特性
  - distroFilter：应用的分发规则
    - apiVersion：支持的apiVersion范围
      - policy：该子属性取值的黑白名单规则
      - value：支持的取值为API Version存在的整数值
    - screenShape：屏幕形状的支持策略
      - policy：该子属性取值的黑白名单规则
      - value：支持的取值为circle（圆形）、rect（矩形）
    - screenWindow：应用运行时窗口的分辨率支持策略
      - policy：该子属性取值的黑白名单规则
      - value：单个字符串的取值格式为："宽\*高"，取值为整数像素值，例如"454\*454"