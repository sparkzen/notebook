# HarmonyOS应用开发类型介绍

- [HarmonyOS应用开发类型介绍](#harmonyos应用开发类型介绍)
  - [鸿蒙应用类型](#鸿蒙应用类型)


## 鸿蒙应用类型

![1648779275614](../../../repository/1648779275614.png)

- 原子服务（Atomic service）：即我们常说的服务卡片
- 应用（Application）：即传统的应用程序

