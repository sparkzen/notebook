# 演示创建Page Ability

- [演示创建Page Ability](#演示创建page-ability)
  - [创建Page Ability](#创建page-ability)

## 创建Page Ability

>  提示：默认的，创建Page Ability时，会默认创建一个属于该Page Ability的主路由Page Ability Slice，同时会创建一个该Page Ability Slice的layout布局文件

示例：

![image-20220403143225795](../../../repository/image-20220403143225795.png)

![image-20220403143357355](../../../repository/image-20220403143357355.png)

创建完成后，可以看见多个三个文件：

![image-20220403143818802](../../../repository/image-20220403143818802.png)

同时，在模块的config.json文件中，module.abilities会自动添加一个刚创建的Page Ability进去：

![image-20220403144050232](../../../repository/image-20220403144050232.png)
