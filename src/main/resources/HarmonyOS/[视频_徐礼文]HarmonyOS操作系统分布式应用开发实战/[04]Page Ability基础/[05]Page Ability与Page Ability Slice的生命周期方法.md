# Page Ability与Page Ability Slice的生命周期方法

- [Page Ability与Page Ability Slice的生命周期方法](#page-ability与page-ability-slice的生命周期方法)
  - [Page Ability的生命周期](#page-ability的生命周期)
  - [Page Ability Slice的生命周期方法](#page-ability-slice的生命周期方法)
  - [相关资料](#相关资料)

## Page Ability的生命周期



![5526730950037](../../../repository/5526730950037.png)



## Page Ability Slice的生命周期方法

当AbilitySlice处于前台且具有焦点时，其生命周期状态随着所属Page的生命周期状态的变化而变化。当一个Page拥有多个AbilitySlice时，例如：MyAbility下有FooAbilitySlice和BarAbilitySlice，当前FooAbilitySlice处于前台并获得焦点，并即将导航到BarAbilitySlice，在此期间的生命周期状态变化顺序为：

1. FooAbilitySlice从ACTIVE状态变为INACTIVE状态。
2. BarAbilitySlice则从INITIAL状态首先变为INACTIVE状态，然后变为ACTIVE状态（假定此前BarAbilitySlice未曾启动）。
3. FooAbilitySlice从INACTIVE状态变为BACKGROUND状态。

对应两个slice的生命周期方法回调顺序为：

FooAbilitySlice.onInactive() --> BarAbilitySlice.onStart() --> BarAbilitySlice.onActive() --> FooAbilitySlice.onBackground()

在整个流程中，MyAbility始终处于ACTIVE状态。但是，当Page被系统销毁时，其所有已实例化的AbilitySlice将联动销毁，而不仅是处于前台的AbilitySlice。



## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ability-page-lifecycle-0000000000029840)