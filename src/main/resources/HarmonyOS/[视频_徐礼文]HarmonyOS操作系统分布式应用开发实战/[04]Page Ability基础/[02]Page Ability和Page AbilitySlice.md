# Page Ability和Page Ability Slice

- [Page Ability和Page Ability Slice](#page-ability和page-ability-slice)
  - [Page Ability](#page-ability)
  - [Page Ability Slice](#page-ability-slice)
  - [Ability与Ability Slice的关系](#ability与ability-slice的关系)

## Page Ability

Ability是HarmonyOS开发的核心，首先了解一下什么是Ability？我们用生活中的例子来做一个类比，Ability就好像是一个画板，初始什么都没有，一片空白。我们可以找一张画纸进行绘画，画完的画纸夹在画板上，画板上就会有我们创作的图画可。

## Page Ability Slice

Page Ability相当于是画板，它是来承载画纸的，Page AbilitySlice就相当于画纸。我们写应用的UI，写完后通过Slice加载布局，就相当于在画纸上绘画完成后把画纸放在画板上进行展示。

## Ability与Ability Slice的关系

Ability与Ability Slice的关系示例：

![image-20220403141858363](../../../repository/image-20220403141858363.png)