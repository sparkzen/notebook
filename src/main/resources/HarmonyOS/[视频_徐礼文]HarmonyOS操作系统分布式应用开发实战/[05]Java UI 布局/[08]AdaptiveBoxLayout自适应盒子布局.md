# AdaptiveBoxLayout自适应盒子布局

- [AdaptiveBoxLayout自适应盒子布局](#adaptiveboxlayout自适应盒子布局)
  - [概览](#概览)
  - [使用示例](#使用示例)
  - [相关资料](#相关资料)

## 概览

![1653290618382](../../../repository/1653290618382.png)

![1653290638522](../../../repository/1653290638522.png)

## 使用示例

- 使用自适应布局

  ![1653290946104](../../../repository/1653290946104.png)

- 添加/删除布局规则

  ![1653291416790](../../../repository/1653291416790.png)

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-layout-adaptiveboxlayout-0000001104341118)