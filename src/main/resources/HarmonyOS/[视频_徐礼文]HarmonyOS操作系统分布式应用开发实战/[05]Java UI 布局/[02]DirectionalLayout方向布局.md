# DirectionalLayout方向布局

- [DirectionalLayout方向布局](#directionallayout方向布局)
  - [基础知识](#基础知识)
  - [排列方向orientation](#排列方向orientation)
  - [对齐方式alignment](#对齐方式alignment)
  - [相关资料](#相关资料)

## 基础知识

- DirectionalLayout的orientation属性有两个horizontal和vertical，代表横向和纵向布局。

- DirectionalLayout用于将DirectionalLayout内的一组组件（Component）按照水平或者垂直方向排布。

- DirectionalLayout有几个重要的属性：

  - 排列属性
  - 对齐方式属性
  - 权重属性

  

## 排列方向orientation

DirectionalLayout的排列方向（orientation）分为水平（horizontal）或者垂直（vertical）方向。使用orientation设置布局内组件的排列方式，默认为垂直排列。

设置布局内的组件水平排布（示例）：

![1649842069014](../../../repository/1649842069014.png)

设置布局内的组件垂直排布（示例）：

![1649842126301](../../../repository/1649842126301.png)

## 对齐方式alignment

DirectionalLayout中的组件使用layout_alignment控制自身在布局中的对齐方式。对齐方式和排列方式密切相关。

- 当排列方式为水平方向时，可选的对齐方式只有作用于垂直方向的类型（top、bottom、vertical_center、center）,其它对齐方式不会生效
- 当排列方式为垂直方向时，可选的对齐方式只有作用于水平方向的类型（left、right、start、end、horizontal_center、center）,其它对齐方式不会生效

右对齐（示例）：

![1649842468649](../../../repository/1649842468649.png)

居中对齐（示例）：

![1649842503771](../../../repository/1649842503771.png)

右侧居中对齐（示例）：

![1649842537304](../../../repository/1649842537304.png)

底部右侧对齐（示例）：

![1649842609818](../../../repository/1649842609818.png)

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-layout-directionallayout-0000001050769565)

