# PositionLayout位置布局

- [PositionLayout位置布局](#positionlayout位置布局)
  - [概览](#概览)
  - [使用示例](#使用示例)
  - [相关资料](#相关资料)

## 概览

![1653288351226](../../../repository/1653288351226.png)

## 使用示例

- 设置子元素的位置

  现有这样的布局及子元素：

  ![1653288403035](../../../repository/1653288403035.png)

  获取子元素，并设置其位置：

  ![1653288499686](../../../repository/1653288499686.png)

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-layout-positionlayout-0000001062250815)