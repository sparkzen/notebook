# DependentLayout相对布局

- [DependentLayout相对布局](#dependentlayout相对布局)
  - [简述](#简述)
  - [相关资料](#相关资料)

## 简述

DependentLayout是Java UI系统里的一种常见布局。与DirectionalLayout相比，拥有更多的排布方式，每个组件可以指定相对于其他同级元素的位置，或者指定相对于父组件的位置

- 相对于同级组件

  - end_of

  - below

  - above

  - start_of

  - left_of

  - right_of

  - ......

    基础使用示例：

    ![1652782872153](../../../repository/1652782872153.png)

    组合使用示例：

    ![1652782978538](../../../repository/1652782978538.png)

- 相对于父组件

  - align_parent_left

  - align_parent_right

  - align_parent_top

  - align_parent_bottom

  - center_in_parent

  - align_parent_start

  - align_parent_end

  - ......

    基础使用示例：

    ![1652783352579](../../../repository/1652783352579.png)

    组合使用示例：

    ![1652783403641](../../../repository/1652783403641.png)

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-layout-dependentlayout-0000001050729536)