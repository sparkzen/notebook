# StackLayout堆叠布局

- [StackLayout堆叠布局](#stacklayout堆叠布局)
  - [概览](#概览)
  - [使用示例](#使用示例)
  - [相关资料](#相关资料)

## 概览

![1653286671432](../../../repository/1653286671432.png)

## 使用示例

- 开始层叠布局

  ![1653287389837](../../../repository/1653287389837.png)

- 移动某个层到最前面

  ![1653287270624](../../../repository/1653287270624.png)

  注：tx2代表要移动的层，layout代表该层所在的层叠布局

- 对齐属性

  ![1653287713215](../../../repository/1653287713215.png)

  ![1653287795150](../../../repository/1653287795150.png)

  ![1653287873919](../../../repository/1653287873919.png)

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-layout-stacklayout-0000001060357540)