#  DirectionalLayout-weight权重设置

- [DirectionalLayout-weight权重设置](#directionallayout-weight权重设置)
  - [简述](#简述)

## 简述

- 权重（weight）就是按比例来分配组件占用父组件的大小，在水平布局下计算公式为：
  

$$
组件宽度 = \frac{组件weight}{所有组件weight之和} * 父布局可分配宽度
$$

