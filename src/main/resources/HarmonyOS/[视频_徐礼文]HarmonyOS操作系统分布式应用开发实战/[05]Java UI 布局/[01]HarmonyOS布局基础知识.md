# HarmonyOS布局基础知识

- [HarmonyOS布局基础知识](#harmonyos布局基础知识)
  - [尺寸单位与字体大小](#尺寸单位与字体大小)
  - [组件（Component）与布局（ComponentContainer）](#组件component与布局componentcontainer)
  - [布局配置LayoutConfig](#布局配置layoutconfig)
  - [代码创建布局 VS XML创建布局](#代码创建布局-vs-xml创建布局)

## 尺寸单位与字体大小

- `px`：像素。1px代表手机屏幕上的一个像素点。由于px在不同手机上的大小不同，差别较大，适配性太差，不建议使用。

  注：若不设置任何单位的话，默认采用`px`。

- `vp`：虚拟像素。虚拟像素是一种可灵活使用和缩放的单位，它与屏幕像素的关系是1vp约等于160dpi屏幕密度设备上的1px。在不同密度的设备之间，HarmonyOS会针对性的转换设备间对应的实际像素值。
- `fp`：字体像素。字体像素默认情况下与vp相同，即默认情况下`1 fp = 1 vp`。如果用户在设置中选择了更大的字体，字体的实际显示大小就会在vp的基础上乘以scale系数，即1 `fp = 1 vp * scale`。

注：为了方便开发者对尺寸长度的管理，官方提供了`AttrHelper`工具类，可实现fp、vp、px之间的相互转换。

## 组件（Component）与布局（ComponentContainer）

- **Component：**提供内容显示，是界面中所有组件的基类，开发者可以给Component设置事件处理回调来创建一个可交互的组件。Java UI框架提供了一些常用的界面元素，也可称之为组件，组件一般直接继承Component或它的子类，如Text、Image等。
- **ComponentContainer：**作为容器容纳Component或ComponentContainer对象，并对他们进行布局。Java UI框架提供了一些标准布局功能的容器，它们继承自ComponentContainer，一般以Layout结尾，如DirectionalLayout、DependentLayout等。

## 布局配置LayoutConfig

- 每种布局都根据自身特点提供LayoutConfig，供子Component设定布局属性和参数
- 通过指定布局属性可以对子Component在布局中的显示效果进行约束。例如：width、height是最基本的布局属性，它们指定了组件的大小

## 代码创建布局 VS XML创建布局

- 代码创建布局：硬编码创建布局，性能可能稍好，但可维护性差。

  示例：

  ![image-20220412233838153](../../../repository/image-20220412233838153.png)

- XML创建布局：XML创建布局。将逻辑和UI分开，维护性强。

  示例：

  ![image-20220412233906942](../../../repository/image-20220412233906942.png)
