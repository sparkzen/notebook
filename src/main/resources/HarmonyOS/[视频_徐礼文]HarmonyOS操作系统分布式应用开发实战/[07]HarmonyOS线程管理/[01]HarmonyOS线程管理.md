# HarmonyOS线程管理

- [HarmonyOS线程管理](#harmonyos线程管理)
  - [简介](#简介)
  - [TaskDispatcher简介及其实现](#taskdispatcher简介及其实现)
    - [GlobalTaskDispather](#globaltaskdispather)
    - [ParallelTaskDispather](#paralleltaskdispather)
    - [SerialTaskDispather](#serialtaskdispather)
    - [SpecTaskDispather](#spectaskdispather)
  - [TaskDispatcher的接口方法](#taskdispatcher的接口方法)
  - [相关资料](#相关资料)

## 简介

![1654509343536](../../../repository/1654509343536.png)

## TaskDispatcher简介及其实现
![1654509590986](../../../repository/1654509590986.png)

![1654509777472](../../../repository/1654509777472.png)


### GlobalTaskDispather

![1654509916698](../../../repository/1654509916698.png)

### ParallelTaskDispather

![1654509941051](../../../repository/1654509941051.png)

### SerialTaskDispather

![1654510096129](../../../repository/1654510096129.png)

### SpecTaskDispather

![1654510134657](../../../repository/1654510134657.png)

## TaskDispatcher的接口方法

![1654510225528](../../../repository/1654510225528.png)

## 相关资料

- [官网 - 概览](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/thread-mgmt-overview-0000000000032127)
- [官网 - 开发指导](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/thread-mgmt-guidelines-0000000000032130)