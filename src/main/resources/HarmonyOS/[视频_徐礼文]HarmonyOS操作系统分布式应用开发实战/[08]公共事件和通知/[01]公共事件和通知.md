# 公共事件和通知

- [公共事件和通知](#公共事件和通知)
  - [概述](#概述)
  - [约束与限制](#约束与限制)
  - [使用介绍&接口说明](#使用介绍接口说明)
    - [公共事件](#公共事件)
      - [场景介绍](#场景介绍)
      - [发布公共事件](#发布公共事件)
        - [发布无序的公共事件](#发布无序的公共事件)
        - [发布携带权限的公共事件](#发布携带权限的公共事件)
        - [发布有序的公共事件](#发布有序的公共事件)
        - [发布粘性公共事件](#发布粘性公共事件)
      - [订阅公共事件](#订阅公共事件)
      - [退订公共事件](#退订公共事件)
    - [通知](#通知)
      - [场景介绍](#场景介绍-1)
      - [NotificationSlot](#notificationslot)
      - [NotificationRequest](#notificationrequest)
      - [NotificationHelper](#notificationhelper)
      - [创建NotificationSlot](#创建notificationslot)
      - [发布通知](#发布通知)
    - [IntentAgent](#intentagent)
      - [场景介绍](#场景介绍-2)
      - [获取IntentAgent示例](#获取intentagent示例)
      - [通知中添加IntentAgent示例](#通知中添加intentagent示例)
      - [主动激发IntentAgent示例](#主动激发intentagent示例)
  - [相关资料](#相关资料)

## 概述

HarmonyOS通过CES（Common Event Service，公共事件服务）为应用程序提供订阅、发布、退订公共事件的能力，通过ANS（Advanced Notification Service，即通知增强服务）系统服务来为应用程序提供发布通知的能力

- 公共事件可分为**系统公共事件**和**自定义公共事件**

  - 系统公共事件：系统将收集到的事件信息，根据系统策略发送给订阅该事件的用户程序。 公共事件包括：终端设备用户可感知的亮灭屏事件，以及系统关键服务发布的系统事件（例如：USB插拔，网络连接，系统升级）等

  - 自定义公共事件：应用自定义一些公共事件用来处理业务逻辑

- 通知提供应用的即时消息或通信消息，用户可以直接删除或点击通知触发进一步的操作

- `IntentAgent`封装了一个指定行为的Intent，可以通过`IntentAgent`启动Ability和发布公共事件

## 约束与限制

公共事件的约束与限制

- 目前公共事件仅支持动态订阅。部分系统事件需要具有指定的权限，具体的权限见API参考
- 目前公共事件订阅不支持多用户
- ThreadMode表示线程模型，目前仅支持HANDLER模式，即在当前UI线程上执行回调函数
- deviceId用来指定订阅本地公共事件还是远端公共事件。deviceId为null、空字符串或本地设备deviceId时，表示订阅本地公共事件，否则表示订阅远端公共事件

通知的约束与限制

- 通知目前支持六种样式：普通文本、长文本、图片、社交、多行文本和媒体样式。创建通知时必须包含一种样式
- 通知支持快捷回复

## 使用介绍&接口说明

### 公共事件

#### 场景介绍

每个应用都可以订阅自己感兴趣的公共事件，订阅成功后且公共事件发布后，系统会把其发送给应用。这些公共事件可能来自系统、其他应用和应用自身。HarmonyOS提供了一套完整的API，支持用户订阅、发布和接收公共事件。发布公共事件需要借助CommonEventData对象，接收公共事件需要继承CommonEventSubscriber类并实现onReceiveEvent回调函数

![1655106461815](../../../repository/1655106461815.png)

- `CommonEventData`：用于发布公共事件
- `CommonEventSubscriber`：订阅公共事件（，需要实现其`onReceiveEvent`方法）

#### 发布公共事件

开发者可以发布四种公共事件：无序的公共事件、带权限的公共事件、有序的公共事件、粘性的公共事件。

##### 发布无序的公共事件

构造CommonEventData对象，设置Intent，通过构造operation对象把需要发布的公共事件信息传入intent对象。然后调用 CommonEventManager.publishCommonEvent(CommonEventData) 接口发布公共事件。

```java
try {
    Intent intent = new Intent();   
    Operation operation = new Intent.OperationBuilder()
            .withAction("com.my.test") // 设置自定义事件的标识符
            .build();
    intent.setOperation(operation);
    CommonEventData eventData = new CommonEventData(intent);
    CommonEventManager.publishCommonEvent(eventData); 
    HiLog.info(LABEL_LOG, "Publish succeeded"); 
} catch (RemoteException e) {
    HiLog.error(LABEL_LOG, "Exception occurred during publishCommonEvent invocation."); 
}
```

##### 发布携带权限的公共事件

构造CommonEventPublishInfo对象，设置订阅者的权限。

- 订阅者在config.json中申请所需的权限，各字段含义详见[权限申请字段说明](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/security-permissions-guidelines-0000000000029886#ZH-CN_TOPIC_0000001072906209__table73291742539)。

  > 说明：非系统已定义的权限，需要先在config.json中自定义，才可以申请使用。详见[权限定义字段说明](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/security-permissions-guidelines-0000000000029886#ZH-CN_TOPIC_0000001072906209__table16180174018340)

  示例一

  ```json
  "reqPermissions": [
    {
      "name": "com.example.MyApplication.permission",
      "reason": "Obtain the required permission",
      "usedScene": {
        "ability": [
          ".MainAbility"
        ],
        "when": "inuse"
      }
    }, 
    {
      ...
    }
  ]
  ```

  示例二

  ![1655108587890](../../../repository/1655108587890.png)

- 发布带权限的公共事件示例代码如下：

  ```java
  Intent intent = new Intent();
  Operation operation = new Intent.OperationBuilder()
          .withAction("com.my.test")
          .build();
  intent.setOperation(operation);
  CommonEventData eventData = new CommonEventData(intent);
  CommonEventPublishInfo publishInfo = new CommonEventPublishInfo();
  String[] permissions = {"com.example.MyApplication.permission"};
  publishInfo.setSubscriberPermissions(permissions); // 设置权限
  try {   
      CommonEventManager.publishCommonEvent(eventData, publishInfo); 
      HiLog.info(LABEL_LOG, "Publish succeeded"); 
  } catch (RemoteException e) {
      HiLog.error(LABEL_LOG, "Exception occurred during publishCommonEvent invocation."); 
  }
  ```

##### 发布有序的公共事件

构造CommonEventPublishInfo对象，通过setOrdered(true)指定公共事件属性为有序公共事件，也可以指定一个最后的公共事件接收者

```java
CommonEventSubscriber resultSubscriber = new MyCommonEventSubscriber();
CommonEventPublishInfo publishInfo = new CommonEventPublishInfo();
publishInfo.setOrdered(true); // 设置属性为有序公共事件(即：有序体现在，串行的通知订阅者，订阅者收到通知是按优先级顺序的)
try {   
    CommonEventManager.publishCommonEvent(eventData, publishInfo, resultSubscriber); // 指定resultSubscriber为有序公共事件最后一个接收者。
} catch (RemoteException e) {
    HiLog.error(LABEL_LOG, "Exception occurred during publishCommonEvent invocation."); 
}
```

##### 发布粘性公共事件

构造CommonEventPublishInfo对象，通过setSticky(true)指定公共事件属性为粘性公共事件

> 何为粘性公共事件？
>
> 一般的，订阅者只能收到从订阅后开始，发布者发布的事件；粘性事件，使得后订阅的订阅者，可以收到订阅之前，发布者发布的事件

1. 发布者首先在config.json中申请发布粘性公共事件所需的权限，各字段含义详见[权限申请字段说明](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/security-permissions-guidelines-0000000000029886#ZH-CN_TOPIC_0000001072906209__table73291742539)

   ```json
   {
       "reqPermissions": [{
           "name": "ohos.permission.COMMONEVENT_STICKY",
           "reason": "Obtain the required permission",
           "usedScene": {
              "ability": [
               ".MainAbility"
              ],
              "when": "inuse"
           }
       }, {
       ...
       }]
   }
   ```

2. 发布粘性公共事件

   ```java
   CommonEventPublishInfo publishInfo = new CommonEventPublishInfo();
   publishInfo.setSticky(true); // 设置属性为粘性公共事件
   try {   
       CommonEventManager.publishCommonEvent(eventData, publishInfo); 
   } catch (RemoteException e) {
       HiLog.error(LABEL, "Exception occurred during publishCommonEvent invocation."); 
   }
   ```

#### 订阅公共事件

1. 创建CommonEventSubscriber派生类，在onReceiveEvent()回调函数中处理公共事件

   > 说明：此处不能执行耗时操作，否则会阻塞UI线程，产生用户点击没有反应等异常

   ```java
   class MyCommonEventSubscriber extends CommonEventSubscriber { 
       MyCommonEventSubscriber(CommonEventSubscribeInfo info) { 
           super(info);   
       }
       @Override 
       public void onReceiveEvent(CommonEventData commonEventData) {
       } 
   }
   ```

2. 构造MyCommonEventSubscriber对象，调用CommonEventManager.subscribeCommonEvent()接口进行订阅

   ```java
   String event = "com.my.test";
   MatchingSkills matchingSkills = new MatchingSkills();
   matchingSkills.addEvent(event); // 自定义事件
   matchingSkills.addEvent(CommonEventSupport.COMMON_EVENT_SCREEN_ON); // 亮屏事件
   CommonEventSubscribeInfo subscribeInfo = new CommonEventSubscribeInfo(matchingSkills);
   MyCommonEventSubscriber subscriber = new MyCommonEventSubscriber(subscribeInfo);
   try {
       CommonEventManager.subscribeCommonEvent(subscriber); 
   } catch (RemoteException e) {
       HiLog.error(LABEL, "Exception occurred during subscribeCommonEvent invocation."); 
   }
   ```

   如果订阅拥有指定权限应用发布的公共事件，发布者需要在config.json中申请权限，各字段含义详见[权限申请字段说明](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/security-permissions-guidelines-0000000000029886#ZH-CN_TOPIC_0000001072906209__table73291742539)

   ```json
   "reqPermissions": [
       {
           "name": "ohos.abilitydemo.permission.PROVIDER",
           "reason": "Obtain the required permission",
           "usedScene": {
               "ability": ["com.hmi.ivi.systemsetting.MainAbility"],
               "when": "inuse"
           }
       }
   ]
   ```

   如果订阅的公共事件是有序的，可以调用setPriority(）指定优先级

   ```java
   // 设置查找公共事件的条件
   String event = "com.my.test";
   MatchingSkills matchingSkills = new MatchingSkills();
   matchingSkills.addEvent(event); // 自定义事件
   
   CommonEventSubscribeInfo subscribeInfo = new CommonEventSubscribeInfo(matchingSkills);
   subscribeInfo.setPriority(100); // 设置优先级，优先级取值范围[-1000，1000]，值默认为0。
   MyCommonEventSubscriber subscriber = new MyCommonEventSubscriber(subscribeInfo);
   try {
        CommonEventManager.subscribeCommonEvent(subscriber); 
   } catch (RemoteException e) {
        HiLog.error(LABEL, "Exception occurred during subscribeCommonEvent invocation."); 
   }
   ```

3. 针对在onReceiveEvent中不能执行耗时操作的限制，可以使用CommonEventSubscriber的goAsyncCommonEvent()来实现异步操作，函数返回后仍保持该公共事件活跃，且执行完成后必须调用AsyncCommonEventResult .finishCommonEvent()来结束

   ```java
   EventRunner runner = EventRunner.create(); // EventRunner创建新线程，将耗时的操作放到新的线程上执行
   MyEventHandler myHandler = new MyEventHandler(runner); // MyEventHandler为EventHandler的派生类，在不同线程间分发和处理事件和Runnable任务
   @Override
   public void onReceiveEvent(CommonEventData commonEventData){
       final AsyncCommonEventResult result = goAsyncCommonEvent();
   
       Runnable task = new Runnable() {
           @Override
           public void run() {
               ........         // 待执行的操作，由开发者定义
               result.finishCommonEvent(); // 调用finish结束异步操作
           }
       };
       myHandler.postTask(task);
   } 
   ```

#### 退订公共事件

在Ability的onStop()中调用CommonEventManager.unsubscribeCommonEvent()方法来退订公共事件。调用后，之前订阅的所有公共事件均被退订

```java
try { 
    CommonEventManager.unsubscribeCommonEvent(subscriber);
 } catch (RemoteException e) {
    HiLog.error(LABEL, "Exception occurred during unsubscribeCommonEvent invocation.");
 }
```

### 通知

#### 场景介绍

HarmonyOS提供了通知功能，即在一个应用的UI界面之外显示的消息，主要用来提醒用户有来自该应用中的信息。当应用向系统发出通知时，它将先以图标的形式显示在通知栏中，用户可以下拉通知栏查看通知的详细信息。常见的使用场景：

- 显示接收到短消息、即时消息等。
- 显示应用的推送消息，如广告、版本更新等。
- 显示当前正在进行的事件，如播放音乐、导航、下载等。

![1655109318113](../../../repository/1655109318113.png)

#### NotificationSlot

NotificationSlot可以对提示音、振动、重要级别等进行设置。一个应用可以创建一个或多个NotificationSlot，在发布通知时，通过绑定不同的NotificationSlot，实现不同用途。

> 说明
>
> NotificationSlot需要先通过NotificationHelper的addNotificationSlot(NotificationSlot)方法发布后，通知才能绑定使用；所有绑定该NotificationSlot的通知在发布后都具备相应的特性，对象在创建后，将无法更改这些设置，对于是否启动相应设置，用户有最终控制权。
>
> 不指定NotificationSlot时，当前通知会使用默认的NotificationSlot，默认的NotificationSlot优先级为LEVEL_DEFAULT。

| 接口名                                              | 描述                                                     |
| --------------------------------------------------- | -------------------------------------------------------- |
| NotificationSlot(String id, String name, int level) | 构造NotificationSlot。                                   |
| setLevel(int level)                                 | 设置NotificationSlot的级别。                             |
| setName(String name)                                | 设置NotificationSlot的命名。                             |
| setDescription(String description)                  | 设置NotificationSlot的描述信息。                         |
| enableBypassDnd(boolean bypassDnd)                  | 设置是否绕过系统的免打扰模式。                           |
| setEnableVibration(boolean vibration)               | 设置收到通知时是否使能振动。                             |
| setEnableLight(boolean isLightEnabled)              | 设置收到通知时是否开启呼吸灯，前提是当前硬件支持呼吸灯。 |
| setLedLightColor(int color)                         | 设置收到通知时的呼吸灯颜色。                             |
| setSlotGroup(String groupId)                        | 绑定当前NotificationSlot到一个NotificationSlot组。       |

NotificationSlot的级别目前支持如下几种， 由低到高：

- LEVEL_NONE： 表示通知不发布
- LEVEL_MIN：表示通知可以发布，但不在状态栏显示，不自动弹出，无提示音；该级别不适用于前台服务的场景
- LEVEL_LOW：表示通知发布后在状态栏显示，不自动弹出，无提示音
- LEVEL_DEFAULT：表示通知发布后在状态栏显示，不自动弹出，触发提示音
- LEVEL_HIGH：表示通知发布后在状态栏显示，自动弹出，触发提示音

#### NotificationRequest

NotificationRequest用于设置具体的通知对象，包括设置通知的属性，如：通知的分发时间、小图标、大图标、自动删除等参数，以及设置具体的通知类型，如普通文本、长文本等。

| 接口名                                                      | 描述                                                         |
| ----------------------------------------------------------- | ------------------------------------------------------------ |
| NotificationRequest()                                       | 构建一个通知。                                               |
| NotificationRequest(int notificationId)                     | 构建一个通知，指定通知的id。通知的Id在应用内具有唯一性，如果不指定，默认为0。 |
| setNotificationId(int notificationId)                       | 设置当前通知id。                                             |
| setAutoDeletedTime(long time)                               | 设置通知自动取消的时间戳。                                   |
| setContent(NotificationRequest.NotificationContent content) | 设置通知的具体内容。                                         |
| setDeliveryTime(long deliveryTime)                          | 设置通知分发的时间戳。                                       |
| setSlotId(String slotId)                                    | 设置通知的NotificationSlot id。                              |
| setTapDismissed(boolean tapDismissed)                       | 设置通知在用户点击后是否自动取消。                           |
| setLittleIcon(PixelMap smallIcon)                           | 设置通知的小图标，在通知左上角显示。                         |
| setBigIcon(PixelMap bigIcon)                                | 设置通知的大图标，在通知的右边显示。                         |
| setGroupValue(String groupValue)                            | 设置分组通知，相同分组的通知在通知栏显示时，将会折叠在一组应用中显示。 |
| addActionButton(NotificationActionButton actionButton)      | 设置通知添加通知ActionButton。                               |
| setIntentAgent(IntentAgent agent)                           | 设置通知承载指定的IntentAgent，在通知中实现即将触发的事件。  |

具体的通知类型：

目前支持六种类型，包括`普通文本NotificationNormalContent`、`长文本NotificationLongTextContent`、`图片NotificationPictureContent`、`多行NotificationMultiLineContent`、`社交NotificationConversationalContent`、`媒体NotificationMediaContent`。

| 类名                              | 接口名                                                  | 描述                                                 |
| --------------------------------- | ------------------------------------------------------- | ---------------------------------------------------- |
| NotificationNormalContent         | setTitle(String title)                                  | 设置通知标题。                                       |
| NotificationNormalContent         | setText(String text)                                    | 设置通知内容。                                       |
| NotificationNormalContent         | setAdditionalText(String additionalText)                | 设置通知次要内容，是对通知内容的补充。               |
| NotificationPictureContent        | setBriefText(String briefText)                          | 设置通知概要内容，是对通知内容的总结。               |
| NotificationPictureContent        | setExpandedTitle(String expandedTitle)                  | 设置附加图片的通知展开时的标题。                     |
| NotificationPictureContent        | setBigPicture(PixelMap bigPicture)                      | 设置通知的图片内容，附加在setText(String text)下方。 |
| NotificationLongTextContent       | setLongText(String longText)                            | 设置通知的长文本。                                   |
| NotificationConversationalContent | setConversationTitle(String conversationTitle)          | 设置社交通知的标题。                                 |
| NotificationConversationalContent | addConversationalMessage(ConversationalMessage message) | 通知添加一条消息。                                   |
| NotificationMultiLineContent      | addSingleLine(String line)                              | 在当前通知中添加一行文本。                           |
| NotificationMediaContent          | setAVToken(AVToken avToken)                             | 将媒体通知绑定指定的AVToken。                        |
| NotificationMediaContent          | setShownActions(int[] actions)                          | 设置媒体通知待展示的按钮。                           |

>  说明：通知发布后，通知的设置不可修改。如果下次发布通知使用相同的id，就会更新之前发布的通知

#### NotificationHelper

NotificationHelper封装了发布、更新、删除通知等静态方法。

| 接口名                                                       | 描述                                         |
| ------------------------------------------------------------ | -------------------------------------------- |
| publishNotification(NotificationRequest request)             | 发布一条通知。                               |
| publishNotification(String tag, NotificationRequest request) | 发布一条带TAG的通知。                        |
| cancelNotification(int notificationId)                       | 取消指定的通知。                             |
| cancelNotification(String tag, int notificationId)           | 取消指定的带TAG的通知。                      |
| cancelAllNotifications()                                     | 取消之前发布的所有通知。                     |
| addNotificationSlot(NotificationSlot slot)                   | 创建一个NotificationSlot。                   |
| getNotificationSlot(String slotId)                           | 获取NotificationSlot。                       |
| removeNotificationSlot(String slotId)                        | 删除一个NotificationSlot。                   |
| getActiveNotifications()                                     | 获取当前应用发的活跃通知。                   |
| getActiveNotificationNums()                                  | 获取系统中当前应用发的活跃通知的数量。       |
| setNotificationBadgeNum(int num)                             | 设置通知的角标。                             |
| setNotificationBadgeNum()                                    | 设置当前应用中活跃状态通知的数量在角标显示。 |

#### 创建NotificationSlot

NotificationSlot可以设置公共通知的震动，重要级别等，并通过调用NotificationHelper.addNotificationSlot()发布NotificationSlot对象

```java
NotificationSlot slot = new NotificationSlot("slot_001", "slot_default", NotificationSlot.LEVEL_MIN); // 创建notificationSlot对象
slot.setDescription("NotificationSlotDescription");
slot.setEnableVibration(true); // 设置振动提醒
slot.setEnableLight(true); // 设置开启呼吸灯提醒
slot.setLedLightColor(Color.RED.getValue());// 设置呼吸灯的提醒颜色
try {
   NotificationHelper.addNotificationSlot(slot);
} catch (RemoteException ex) {
   HiLog.error(LABEL, "Exception occurred during addNotificationSlot invocation.");
}
```

#### 发布通知

1. 构建NotificationRequest对象，应用发布通知前，通过NotificationRequest的setSlotId()方法与NotificationSlot绑定，使该通知在发布后都具备该对象的特征。

   ```java
   int notificationId = 1;
   NotificationRequest request = new NotificationRequest(notificationId);
   request.setSlotId(slot.getId()); // 设置通知的配置
   ```

2. 调用setContent()设置通知的内容。

   ```java
   String title = "title";
   String text = "There is a normal notification content.";
   NotificationNormalContent content = new NotificationNormalContent();
   content.setTitle(title)
          .setText(text);
   NotificationRequest.NotificationContent notificationContent = new NotificationRequest.NotificationContent(content);
   request.setContent(notificationContent); // 设置通知的内容
   
   // --------------------- 基础配置
   // Image image = new Image(this);
   // image.setPixelMap(ResourceTable.Media_1);
   // request.setLittleIcon(image.getPixelMap()); // 设置小图标
   // request.setBigIcon(image.getPixelMap()); // 设置大图标
   // request.setShowDeliveryTime(true); // 设置显示通知的发布时间
   
   // --------------------- 点击通知栏，跳转至对应页面
   //List<Intent> intents = new ArrayList<>();
   //Intent skipToIntent = new Intent();
   //Operation operation = new Intent.OperationBuilder().withBundleName(getBundleName())
   //        .withAbilityName(MainAbility.class.getName())
   //        .withDeviceId("")
   //        .build();
   //skipToIntent.setOperation(operation);
   //intents.add(skipToIntent);
   //
   //IntentAgentInfo intentAgentInfo = new IntentAgentInfo(
   //        100, // 使用者定义的一个私有值
   //        IntentAgentConstant.OperationType.START_ABILITY, // 操作类型
   //        IntentAgentConstant.Flags.UPDATE_PRESENT_FLAG,
   //        intents, // 跳转列表
   //        new IntentParams() // intent的参数
   //        
   //);
   //IntentAgent intentAgent = IntentAgentHelper.getIntentAgent(this, intentAgentInfo);
   //request.setIntentAgent(intentAgent); // 设置跳转配置
   ```

3. 调用publishNotification()发布通知。

   ```java
   try {
      NotificationHelper.publishNotification(request);
   } catch (RemoteException ex) {
      HiLog.error(LABEL, "Exception occurred during publishNotification invocation.");
   }
   ```

**取消通知**

取消通知分为取消指定单条通知和取消所有通知，应用只能取消自己发布的通知。

- 调用cancelNotification()取消指定的单条通知。

  ```java
  int notificationId = 1;
  try {
      NotificationHelper.cancelNotification(notificationId);
  } catch (RemoteException ex) {
      HiLog.error(LABEL, "Exception occurred during cancelNotification invocation.");
  }
  ```

- 调用cancelAllNotifications()取消所有通知。

  ```java
  try {
      NotificationHelper.cancelAllNotifications();
  } catch (RemoteException ex) {
      HiLog.error(LABEL, "Exception occurred during cancelAllNotifications invocation.");
  }
  ```

### IntentAgent

#### 场景介绍

IntentAgent封装了一个指定行为的[Intent](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ability-intent-0000000000038799)，可以通过triggerIntentAgent接口主动触发，也可以与通知绑定被动触发。具体的行为包括：启动Ability和发布公共事件。例如：收到通知后，在点击通知后跳转到一个新的Ability，不点击则不会触发。

![1655111275753](../../../repository/1655111275753.png)

#### 获取IntentAgent示例

```java
// 指定要启动的Ability的BundleName和AbilityName字段
// 将Operation对象设置到Intent中
Operation operation = new Intent.OperationBuilder()
        .withDeviceId("")
        .withBundleName("com.testintentagent")
        .withAbilityName("com.testintentagent.entry.IntentAgentAbility")
        .build();
intent.setOperation(operation);
List<Intent> intentList = new ArrayList<>();
intentList.add(intent);
// 定义请求码
int requestCode = 200;
// 设置flags
List<IntentAgentConstant.Flags> flags = new ArrayList<>();
flags.add(IntentAgentConstant.Flags.UPDATE_PRESENT_FLAG);
// 指定启动一个有页面的Ability
IntentAgentInfo paramsInfo = new IntentAgentInfo(requestCode, IntentAgentConstant.OperationType.START_ABILITY, flags, intentList, null);
// 获取IntentAgent实例
IntentAgent agent = IntentAgentHelper.getIntentAgent(this, paramsInfo);
```

#### 通知中添加IntentAgent示例

```java
int notificationId = 1;
NotificationRequest request = new NotificationRequest(notificationId);
String title = "title";
String text = "There is a normal notification content.";
NotificationRequest.NotificationNormalContent content = new NotificationRequest.NotificationNormalContent();
content.setTitle(title)
       .setText(text);
NotificationContent notificationContent = new NotificationContent(content);
request.setContent(notificationContent); // 设置通知的内容
request.setIntentAgent(agent); // 设置通知的IntentAgent
```

#### 主动激发IntentAgent示例

```java
int code = 100;
IntentAgentHelper.triggerIntentAgent(this, agent, null, null, new TriggerInfo(null, null, null, code));
```

## 相关资料

- [官网 - 概述](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ability-notification-fwk-overview-0000000000029862)
- [官网 - 公共事件开发指导](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ability-notification-fwk-common-event-0000000000029871)
- [官网 - 通知开发指导](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ability-notification-fwk-notification-0000000000038804)
- [官网 - IntentAgent开发指导](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ability-notification-fwk-intentagent-0000001050764907)

