# Piker组件

- [Piker组件](#piker组件)
  - [简述](#简述)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 简述

Picker提供了滑动选择器，允许用户从预定义范围中进行选择。

Picker的自有XML属性见下表：

| 属性名称                          | 中文描述                                                     | 取值        | 取值说明                                                     | 使用案例                                                     |
| --------------------------------- | ------------------------------------------------------------ | ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| element_padding                   | 文本和Element之间的间距Element必须通过setElementFormatter接口配置 | float类型   | 表示间距尺寸的float类型。可以是浮点数值，其默认单位为px；也可以是带px/vp/fp单位的浮点数值；也可以引用float资源。 | ohos:element_padding="30"ohos:element_padding="16vp"ohos:element_padding="$float:padding" |
| max_value                         | 最大值                                                       | integer类型 | 可直接设置整型数值，也可以引用integer资源。                  | ohos:max_value="10"ohos:max_value="$integer:value"           |
| min_value                         | 最小值                                                       | integer类型 | 可直接设置整型数值，也可以引用integer资源。                  | ohos:min_value="1"ohos:min_value="$integer:value"            |
| value                             | 当前值                                                       | integer类型 | 可直接设置整型数值，也可以引用integer资源。                  | ohos:value="5"ohos:value="$integer:value"                    |
| normal_text_color                 | 未选中的文本颜色                                             | color类型   | 可直接设置色值，也可以引用color资源。                        | ohos:normal_text_color="#A8FFFFFF"ohos:normal_text_color="$color:black" |
| normal_text_size                  | 未选中的文本大小                                             | float类型   | 表示字体大小的float类型。可以是浮点数值，其默认单位为px；也可以是带px/vp/fp单位的浮点数值；也可以引用float资源。 | ohos:normal_text_size="30"ohos:normal_text_size="16fp"ohos:normal_text_size="$float:size_value" |
| selected_text_color               | 选中的文本颜色                                               | color类型   | 可直接设置色值，也可以引用color资源。                        | ohos:selected_text_color="#A8FFFFFF"ohos:selected_text_color="$color:black" |
| selected_text_size                | 选中的文本大小                                               | float类型   | 表示字体大小的float类型。可以是浮点数值，其默认单位为px；也可以是带px/vp/fp单位的浮点数值；也可以引用float资源。 | ohos:selected_text_size="30"ohos:selected_text_size="16fp"ohos:selected_text_size="$float:size_value" |
| selector_item_num                 | 显示的项目数量                                               | integer类型 | 可直接设置整型数值，也可以引用integer资源。                  | ohos:selector_item_num="10"ohos:selector_item_num="$integer:num" |
| selected_normal_text_margin_ratio | 已选文本边距与常规文本边距的比例                             | float类型   | 可直接设置浮点数值，也可以引用float浮点数资源。取值需>0.0f，默认值为1.0f 。 | ohos:selected_normal_text_margin_ratio="0.5"ohos:selected_normal_text_margin_ratio="$float:ratio" |
| shader_color                      | 着色器颜色                                                   | color类型   | 可直接设置色值，也可以引用color资源。                        | ohos:shader_color="#A8FFFFFF"ohos:shader_color="$color:black" |
| top_line_element                  | 选中项的顶行                                                 | Element类型 | 可直接配置色值，也可以引用color资源或引用media/graphic下的图片资源。 | ohos:top_line_element="#FFFFFFFF"ohos:top_line_element="$color:black"ohos:top_line_element="$media:media_src"ohos:top_line_element="$graphic:graphic_src" |
| bottom_line_element               | 选中项的底线                                                 | Element类型 | 可直接配置色值，也可以引用color资源或引用media/graphic下的图片资源。 | ohos:bottom_line_element="#FFFFFFFF"ohos:bottom_line_element="$color:black"ohos:bottom_line_element="$media:media_src"ohos:bottom_line_element="$graphic:graphic_src" |
| wheel_mode_enabled                | 选择轮是否循环显示数据                                       | boolean类型 | 可直接设置true/false，也可以引用boolean资源。                | ohos:wheel_mode_enabled="true"ohos:wheel_mode_enabled="$boolean:true" |

## 示例说明

- 图片

  ![2](../../../repository/2-1654152757663.gif)

- ability_piker.xml部分

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <Picker
          ohos:id="$+id:picker1"
          ohos:height="300vp"
          ohos:width="200vp"
          ohos:background_element="#fc2"
          ohos:normal_text_color="#fff"
          ohos:normal_text_size="30fp"
          ohos:selected_text_color="#f00"
          ohos:selected_text_size="60fp"
          ohos:min_value="0"
          ohos:max_value="6"
          />
  
  </DirectionalLayout>
  ```

- java部分

  ```java
  import com.example.javabasecomponent.ResourceTable;
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.components.Picker;
  
  public class PikerAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_piker);
          
          Picker picker1 = findComponentById(ResourceTable.Id_picker1);
          // 选择监听事件
          picker1.setValueChangedListener(new Picker.ValueChangedListener() {
              @Override
              public void onValueChanged(Picker picker, int selectedValueIndex, int lastValueIndex) {
                  System.out.println("当前选中的值的index：" + selectedValueIndex);
                  System.out.println("上次选中的值的index(即：滑开的值的index)：" + lastValueIndex);
              }
          });
      
          // 自定义展示内容
          String[] displayedDataArr = {"星期一", "星期二", "星期三", "星期四", "星期五", "星期六", "星期日"};
          picker1.setDisplayedData(displayedDataArr);
          picker1.setFormatter(index -> displayedDataArr[index]);
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
  }
  
  ```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-picker-0000001059807909)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)