# Button组件

- [Button组件](#button组件)
  - [概述](#概述)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 概述

Button组件继承自Text组件（`public class Button extends Text`），所以Text组件有的能力，Button都是有的

## 示例说明

- 按钮

  ![1653637750607](../../../repository/1653637750607.png)

- （上述按钮的）xml实现

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <!--   普通按钮 -->
      <Text
          ohos:id="$+id:button0"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="#fc0"
          ohos:layout_alignment="horizontal_center"
          ohos:margin="10vp"
          ohos:text="按钮0"
          ohos:text_alignment="center"
          ohos:text_color="#fff"
          ohos:text_size="40vp"
          />
  
      <!--   圆角按钮 -->
      <Text
          ohos:id="$+id:button1"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="$graphic:background_ability_button1"
          ohos:layout_alignment="horizontal_center"
          ohos:margin="10vp"
          ohos:text="按钮1"
          ohos:text_alignment="center"
          ohos:text_color="#fff"
          ohos:text_size="40vp"
          />
  
      <!--   椭圆按钮 -->
      <Text
          ohos:id="$+id:button2"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="$graphic:background_ability_button2"
          ohos:layout_alignment="horizontal_center"
          ohos:margin="10vp"
          ohos:text="按钮2"
          ohos:text_alignment="center"
          ohos:text_color="#fff"
          ohos:text_size="40vp"
          />
  
      <!--   圆形按钮 -->
      <Text
          ohos:id="$+id:button3"
          ohos:height="100vp"
          ohos:width="100vp"
          ohos:background_element="$graphic:background_ability_button3"
          ohos:layout_alignment="horizontal_center"
          ohos:margin="10vp"
          ohos:text="按钮3"
          ohos:text_alignment="center"
          ohos:text_color="#fff"
          ohos:text_size="40vp"
          />
  
      <!--   按钮前带图标 -->
      <Text
          ohos:id="$+id:button4"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="#f00"
          ohos:element_left="$media:icon"
          ohos:element_padding="5vp"
          ohos:layout_alignment="horizontal_center"
          ohos:margin="10vp"
          ohos:text="按钮4"
          ohos:text_alignment="left|center"
          ohos:text_color="#fff"
          ohos:text_size="40vp"
          />
  
      <!-- 纯图片按钮 -->
      <Text
          ohos:id="$+id:button5"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:element_left="$media:icon"
          ohos:margin="10vp"
          />
  
  </DirectionalLayout>
  ```


## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-button-0000001051009585)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)