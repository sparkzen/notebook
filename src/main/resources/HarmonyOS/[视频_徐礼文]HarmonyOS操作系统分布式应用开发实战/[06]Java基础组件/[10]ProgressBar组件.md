# ProgressBar组件

- [ProgressBar组件](#progressbar组件)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 示例说明

- 图片

  ![1654055308979](../../../repository/1654055308979.png)

- （上述图片的）ability_progress_bar.xml实现

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:background_element="#3cc"
      ohos:orientation="vertical">
      <!--
        orientation: 设置方向
        height/width: 设置纵向/横向进度条的长度
        progress_width: 设置进度条的宽度
        min/max: 设置进度条所代表的的最小最大值
        progress: 设置进度条当前进度的值
        progress_element: 设置进度条的颜色
        background_instruct_element: 设置进度条的底色
        divider_lines_enabled: 分割线
        通过代码progressBar.setDividerLineColor(Color.MAGENTA): 设置分割线的颜色
        divider_lines_number: 指定分割成多少段
        progress_hint_text: 设置进度条提示
        progress_hint_text_size: 设置progress_hint_text的颜色
        progress_hint_text_alignment: 设置progress_hint_text对齐方式
       -->
      <ProgressBar
          ohos:id="$+id:progressBar2"
          ohos:height="280vp"
          ohos:width="280vp"
          ohos:orientation="horizontal"
          ohos:progress_width="30vp"
          ohos:min="0"
          ohos:max="100"
          ohos:progress="50"
          ohos:progress_element="red"
          ohos:background_instruct_element="green"
          ohos:divider_lines_enabled="true"
          ohos:divider_lines_number="5"
          ohos:progress_hint_text="50%"
          ohos:progress_hint_text_size="30fp"
          ohos:progress_hint_text_alignment="center"
          />
  
  </DirectionalLayout>
  ```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-progressbar-0000001060379891)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)