# ScrollView组件

- [ScrollView组件](#scrollview组件)
  - [简述](#简述)
    - [支持的XML属性](#支持的xml属性)
  - [示例说明](#示例说明)
    - [示例一](#示例一)
    - [示例二](#示例二)
  - [相关资料](#相关资料)

## 简述

ScrollView是一种带滚动功能的组件，它采用滑动的方式在有限的区域内显示更多的内容

### 支持的XML属性

ScrollView的共有XML属性继承自：[StackLayout](../[05]Java UI 布局/[06]StackLayout堆叠布局.md)

ScrollView的自有XML属性见下表：

| 属性名称       | 中文描述     | 取值        | 取值说明                                        | 使用案例                                                     |
| -------------- | ------------ | ----------- | ----------------------------------------------- | ------------------------------------------------------------ |
| match_viewport | 是否拉伸匹配 | boolean类型 | 可以直接设置true/false，也可以引用boolean资源。 | ohos:match_viewport="true"ohos:match_viewport="$boolean:true" |
| rebound_effect | 回弹效果     | boolean类型 | 可以直接设置true/false，也可以引用boolean资源。 | ohos:rebound_effect="true"ohos:rebound_effect="$boolean:true" |

## 示例说明

### 示例一

- 图片

  ![1](../../../repository/1-1654154760289.gif)

- xml部分

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <!--  文字示例  -->
      <ScrollView
          ohos:height="match_content"
          ohos:width="match_content"
          ohos:padding="20vp"
          ohos:rebound_effect="true"
          >
  
          <Text
              ohos:height="match_content"
              ohos:width="match_content"
              ohos:multiple_lines="true"
              ohos:text="$string:ren_ming_ru_cao_jian"
              ohos:text_size="20fp"
              />
      </ScrollView>
      
  </DirectionalLayout>
  ```

### 示例二

- 图片

  ![2](../../../repository/2-1654154766611.gif)

- xml部分

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
      
      <!--  图片示例  -->
      <ScrollView
          ohos:height="match_content"
          ohos:width="match_content"
          ohos:rebound_effect="true"
          >
          <!--        ohos:visibility="hide">-->
  
          <DirectionalLayout
              ohos:height="match_content"
              ohos:width="match_content"
              ohos:alignment="vertical_center"
              ohos:orientation="vertical">
  
              <Image
                  ohos:height="280vp"
                  ohos:width="280vp"
                  ohos:background_element="#f3d"
                  ohos:image_src="$media:1"
                  ohos:margin="10vp"
                  ohos:padding="5vp"/>
  
              <Image
                  ohos:height="280vp"
                  ohos:width="280vp"
                  ohos:background_element="#f3d"
                  ohos:image_src="$media:1"
                  ohos:margin="10vp"
                  ohos:padding="5vp"/>
  
              <Image
                  ohos:height="280vp"
                  ohos:width="280vp"
                  ohos:background_element="#f3d"
                  ohos:image_src="$media:1"
                  ohos:margin="10vp"
                  ohos:padding="5vp"/>
  
              <Image
                  ohos:height="280vp"
                  ohos:width="280vp"
                  ohos:background_element="#f3d"
                  ohos:image_src="$media:1"
                  ohos:margin="10vp"
                  ohos:padding="5vp"/>
  
          </DirectionalLayout>
      </ScrollView>
  
  </DirectionalLayout>
  ```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-scrollview-0000001060965602)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)