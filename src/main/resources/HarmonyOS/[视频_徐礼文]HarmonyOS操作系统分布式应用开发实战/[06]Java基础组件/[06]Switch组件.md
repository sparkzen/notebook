# Switch组件

- [Switch组件](#switch组件)
  - [示例说明](#示例说明)
  - [补充](#补充)
  - [相关资料](#相关资料)

## 示例说明

- 图片

  ![2](../../../repository/2-1653889178369.gif)

- （上述图片的）ability_switch.xml实现

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <!--
          text_state_off： 关闭的显示文案
          text_state_on： 开启的显示文案
          thumb_element： 设置滑块下面的背景
          track_element: 设置滑块
       -->
      <Switch
          ohos:id="$+id:switch1"
          ohos:height="100vp"
          ohos:width="200vp"
          ohos:text_color="#fff"
          ohos:text_size="30fp"
          ohos:text_state_off="OFF"
          ohos:text_state_on="ON"
          ohos:thumb_element="#3dc"
          ohos:track_element="#4c3"
          />
  
  </DirectionalLayout>
  ```

## 补充

- 设置监听事件

  ```java
  import com.example.javabasecomponent.ResourceTable;
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.components.AbsButton;
  import ohos.agp.components.Switch;
  
  public class SwitchAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_switch);
          
          Switch switch1 = findComponentById(ResourceTable.Id_switch1);
          // 设置监听
          switch1.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
              @Override
              public void onCheckedChanged(AbsButton absButton, boolean state) {
                  System.err.println(state);
              }
          });
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
  }
  ```

  

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-switch-0000001060806006)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)