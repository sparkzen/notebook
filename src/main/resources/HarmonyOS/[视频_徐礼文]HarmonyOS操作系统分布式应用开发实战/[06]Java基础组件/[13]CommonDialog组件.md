# CommonDialog组件

- [CommonDialog组件](#commondialog组件)
  - [简述](#简述)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 简述

CommonDialog是一种在弹出框消失之前，用户无法操作其他界面内容的对话框。通常用来展示用户当前需要的或用户必须关注的信息或操作。对话框的内容通常是不同组件进行组合布局，如：文本、列表、输入框、网格、图标或图片，常用于选择或确认信息

![1654076315329](../../../repository/1654076315329.png)

## 示例说明

- 图片

  ![2](../../../repository/2-1654076752149.gif)

- ability_common_dialog.xml部分

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <Button
          ohos:id="$+id:button8"
          ohos:height="60vp"
          ohos:width="300vp"
          ohos:text_size="20fp"
          ohos:text="确认消息(点击弹出CommonDialog)"
          ohos:background_element="#f00"
          />
  
  </DirectionalLayout>
  ```

- java部分

  ```java
  import com.example.javabasecomponent.ResourceTable;
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.components.Button;
  import ohos.agp.components.Component;
  import ohos.agp.components.ComponentContainer;
  import ohos.agp.components.LayoutScatter;
  import ohos.agp.components.Text;
  import ohos.agp.window.dialog.CommonDialog;
  import ohos.agp.window.dialog.IDialog;
  
  import java.util.Random;
  
  public class CommonDialogAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_common_dialog);
          
          Button button8 = findComponentById(ResourceTable.Id_button8);
          button8.setClickedListener(new Component.ClickedListener() {
              @Override
              public void onClick(Component component) {
                  if (new Random().nextBoolean()) {
                      showTip1("hello msg~");
                  } else {
                      showTip2("君不见黄河之水天上来，奔流到海不复回。君不见高堂明镜悲白发，朝如青丝暮成雪。");
                  }
              }
          
          });
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
      
      
      /**
       * 简单的弹框
       */
      private void showTip1(String msg) {
          CommonDialog dialog = new CommonDialog(getContext());
          dialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
          dialog.setTitleText("通知");
          dialog.setContentText(msg);
          dialog.setButton(IDialog.BUTTON3, "确认", (iDialog, i) -> iDialog.destroy());
          dialog.show();
      }
      
      /**
       * 复杂的弹框（自定义样式）
       * <p>
       * setTitleCustomComponent setContentCustomComponent
       */
      private void showTip2(String msg) {
          CommonDialog dialog = new CommonDialog(getContext());
          
          // 组件
          Component container = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_common_dialog_custom
                  , null, false);
          Text text4 = container.findComponentById(ResourceTable.Id_text4);
          text4.setText(msg);
          
          Text confirmButton9 = container.findComponentById(ResourceTable.Id_button9);
          confirmButton9.setClickedListener(new Component.ClickedListener() {
              @Override
              public void onClick(Component component) {
                  dialog.destroy();
              }
          });
          Text cancelButton10 = container.findComponentById(ResourceTable.Id_button10);
          cancelButton10.setClickedListener(new Component.ClickedListener() {
              @Override
              public void onClick(Component component) {
                  dialog.destroy();
              }
          });
      
          /// -> 确认框
          // 自定义标题栏
          // dialog.setTitleCustomComponent();
          dialog.setTitleText("看！美女！");
          // 自定义内容栏
          dialog.setContentCustomComponent(container);
          dialog.setSize(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
          dialog.show();
      }
  }
  ```

- common_dialog_custom.xml部分

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  
  <DependentLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:id="$+id:custom_container"
      ohos:height="match_content"
      ohos:width="300vp"
      ohos:background_element="#F5F5F5">
  
      <Image
          ohos:id="$+id:image9"
          ohos:height="150vp"
          ohos:width="280vp"
          ohos:clip_alignment="center"
          ohos:horizontal_center="true"
          ohos:image_src="$media:1"/>
  
      <Text
          ohos:id="$+id:text4"
          ohos:height="match_content"
          ohos:width="100vp"
          ohos:below="$id:image9"
          ohos:text="123"
          ohos:text_size="30fp"
          ohos:text_color="red"
          ohos:truncation_mode="ellipsis_at_end"
          />
  
      <Button
          ohos:id="$+id:button9"
          ohos:height="match_content"
          ohos:width="match_parent"
          ohos:background_element="#FF9912"
          ohos:below="$id:text4"
          ohos:margin="8vp"
          ohos:padding="8vp"
          ohos:text="确认"
          ohos:text_color="green"
          ohos:text_size="18vp"/>
  
      <Button
          ohos:id="$+id:button10"
          ohos:height="match_content"
          ohos:width="match_parent"
          ohos:background_element="#FF9912"
          ohos:below="$id:button9"
          ohos:margin="8vp"
          ohos:padding="8vp"
          ohos:text="取消"
          ohos:text_color="yellow"
          ohos:text_size="18vp"/>
  </DependentLayout>
  ```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-commondialog-0000001150874228#section53441301914)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)