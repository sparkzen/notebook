# TabList组件

- [TabList组件](#tablist组件)
  - [简述](#简述)
    - [支持的XML属性](#支持的xml属性)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 简述

Tablist可以实现多个页签栏的切换，Tab为某个页签。子页签通常放在内容区上方，展示不同的分类。页签名称应该简洁明了，清晰描述分类的内容

### 支持的XML属性

Tablist的共有XML属性继承自：[ScrollView](./[15]ScrollView组件.md)

Tablist的自有XML属性见下表：

| 属性名称                      | 中文描述                           | 取值                                  | 取值说明                                                     | 使用案例                                                     |
| ----------------------------- | ---------------------------------- | ------------------------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
| fixed_mode                    | 固定所有页签并同时显示             | boolean类型                           | 可以直接设置true/false，也可以引用boolean资源。              | ohos:fixed_mode="true"ohos:fixed_mode="$boolean:true_tag"    |
| orientation                   | 页签排列方向                       | horizontal                            | 表示水平排列。                                               | ohos:orientation="horizontal"                                |
| vertical                      | 表示垂直排列。                     | ohos:orientation="vertical"           |                                                              |                                                              |
| normal_text_color             | 未选中的文本颜色                   | color类型                             | 可以直接设置色值，也可以引用color资源。                      | ohos:normal_text_color="#FFFFFFFF"ohos:normal_text_color="$color:black" |
| selected_text_color           | 选中的文本颜色                     | color类型                             | 可以直接设置色值，也可以引用color资源。                      | ohos:selected_text_color="#FFFFFFFF"ohos:selected_text_color="$color:black" |
| selected_tab_indicator_color  | 选中页签的颜色                     | color类型                             | 可以直接设置色值，也可以引用color资源。                      | ohos:selected_tab_indicator_color="#FFFFFFFF"ohos:selected_tab_indicator_color="$color:black" |
| selected_tab_indicator_height | 选中页签的高度                     | float类型                             | 表示尺寸的float类型。可以是浮点数值，其默认单位为px；也可以是带px/vp/fp单位的浮点数值；也可以引用float资源。 | ohos:selected_tab_indicator_height="100"ohos:selected_tab_indicator_height="20vp"ohos:selected_tab_indicator_height="$float:size_value" |
| tab_indicator_type            | 页签指示类型                       | invisible                             | 表示选中的页签无指示标记。                                   | ohos:tab_indicator_type="invisible"                          |
| bottom_line                   | 表示选中的页签通过底部下划线标记。 | ohos:tab_indicator_type="bottom_line" |                                                              |                                                              |
| left_line                     | 表示选中的页签通过左侧分割线标记。 | ohos:tab_indicator_type="left_line"   |                                                              |                                                              |
| oval                          | 表示选中的页签通过椭圆背景标记。   | ohos:tab_indicator_type="oval"        |                                                              |                                                              |
| tab_length                    | 页签长度                           | float类型                             | 表示尺寸的float类型。可以是浮点数值，其默认单位为px；也可以是带px/vp/fp单位的浮点数值；也可以引用float资源。 | ohos:tab_length="100"ohos:tab_length="20vp"ohos:tab_length="$float:size_value" |
| tab_margin                    | 页签间距                           | float类型                             | 表示尺寸的float类型。可以是浮点数值，其默认单位为px；也可以是带px/vp/fp单位的浮点数值；也可以引用float资源。 | ohos:tab_margin="100"ohos:tab_margin="20vp"ohos:tab_margin="$float:size_value" |
| text_alignment                | 文本对齐方式                       | left                                  | 表示文本靠左对齐。                                           | 可以设置取值项如表中所列，也可以使用“\|”进行多项组合。ohos:text_alignment="center"ohos:text_alignment="top\|left" |
| top                           | 表示文本靠顶部对齐。               |                                       |                                                              |                                                              |
| right                         | 表示文本靠右对齐。                 |                                       |                                                              |                                                              |
| bottom                        | 表示文本靠底部对齐。               |                                       |                                                              |                                                              |
| horizontal_center             | 表示文本水平居中对齐。             |                                       |                                                              |                                                              |
| vertical_center               | 表示文本垂直居中对齐。             |                                       |                                                              |                                                              |
| center                        | 表示文本居中对齐。                 |                                       |                                                              |                                                              |
| start                         | 表示文本靠起始端对齐。             |                                       |                                                              |                                                              |
| end                           | 表示文本靠结尾端对齐。             |                                       |                                                              |                                                              |
| text_size                     | 文本大小                           | float类型                             | 表示尺寸的float类型。可以是浮点数值，其默认单位为px；也可以是带px/vp/fp单位的浮点数值；也可以引用float资源。 | ohos:text_size="100"ohos:text_size="16fp"ohos:text_size="$float:size_value" |

## 示例说明

- 图片

  ![2](../../../repository/2-1654157316889.gif)

- xml部分

  ```xml
  ![2](../../../../../../../../360MoveData/Users/邓沙利文/Desktop/2.gif<?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:orientation="vertical">
  
      <TabList
          ohos:id="$+id:tabList1"
          ohos:background_element="#fcc"
          ohos:text_alignment="center"
          ohos:layout_alignment="center"
          ohos:tab_margin="5vp"
          ohos:padding="5vp"
          ohos:tab_length="60vp"
          ohos:text_size="18vp"
          ohos:height="50vp"
          ohos:width="match_parent"
  
          ohos:normal_text_color="#fff"
          ohos:selected_text_color="#f00"
          ohos:selected_tab_indicator_color="#f00"
          ohos:selected_tab_indicator_height="2vp"
          />
  
  </DirectionalLayout>
  ```

- java部分

  ```java
  import com.example.javabasecomponent.ResourceTable;
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.components.TabList;
  
  public class TabListAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_tab_list);
      
      
          String[] tabInfos = {"相册", "视频", "音乐", "电视", "新闻", "电影", "体育", "旅游"};
          TabList tabList = findComponentById(ResourceTable.Id_tabList1);
          // 添加tab
          for (String tabInfo : tabInfos) {
              TabList.Tab tab = tabList.new Tab(getContext());
              tab.setText(tabInfo);
              tabList.addTab(tab);
          }
          
          
          // 设置选中tab
          tabList.selectTabAt(2);
          //tabList.selectTab();
          
          tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
              @Override
              public void onSelected(TabList.Tab tab) {
                  // 当某个Tab从未选中状态变为选中状态时的回调
                  System.err.println("tab" + tab.getText() + "被选中了");
              }
      
              @Override
              public void onUnselected(TabList.Tab tab) {
                  // 当某个Tab从选中状态变为未选中状态时的回调
                  System.err.println("tab" + tab.getText() + "被取消选中了");
              }
      
              @Override
              public void onReselected(TabList.Tab tab) {
                  // 当某个Tab已处于选中状态，再次被点击时的状态回调
                  System.err.println("tab" + tab.getText() + "被再次选中了");
              }
          });
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
  }
  ```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-tablist-tab-0000001062229749)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)