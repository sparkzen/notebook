# PageSlider组件

- [PageSlider组件](#pageslider组件)
  - [简述](#简述)
    - [支持的XML属性](#支持的xml属性)
  - [示例说明](#示例说明)
    - [示例一](#示例一)
    - [示例二](#示例二)
  - [相关资料](#相关资料)

## 简述

PageSlider是用于页面之间切换的组件，它通过响应滑动事件完成页面间的切换。

### 支持的XML属性

PageSlider无自有的XML属性，共有XML属性继承自：[StackLayout](../[05]Java UI 布局/[06]StackLayout堆叠布局.md)

## 示例说明

### 示例一

- 图片

  ![2](../../../repository/2-1654161366474.gif)

- xml部分

  - ability_page_slider.xml

    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <DirectionalLayout
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:alignment="center"
        ohos:orientation="vertical">
    
        <PageSlider
            ohos:id="$+id:pageSlider1"
            ohos:background_element="#808080"
            ohos:height="match_parent"
            ohos:width="match_parent" />
    
    </DirectionalLayout>
    ```

  - page_slider1.xml

    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <DirectionalLayout
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:background_element="#f0c"
        ohos:margin="30vp"
        ohos:orientation="vertical">
    
    
        <Text
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:text_alignment="center"
            ohos:text="Page1"
            ohos:text_size="50fp"
            />
    </DirectionalLayout>
    ```

    注：page_slider2.xml、page_slider3.xml与page_slider1.xml差不多，就不贴出来了

- java部分

  ```
  import com.example.javabasecomponent.ResourceTable;
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.components.AttrHelper;
  import ohos.agp.components.Component;
  import ohos.agp.components.ComponentContainer;
  import ohos.agp.components.DirectionalLayout;
  import ohos.agp.components.LayoutScatter;
  import ohos.agp.components.PageSlider;
  import ohos.agp.components.PageSliderProvider;
  
  import java.util.ArrayList;
  import java.util.List;
  
  public class PageSliderAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_page_slider);
          
          // step1. 获取PageSlider
          PageSlider pageSlider = findComponentById(ResourceTable.Id_pageSlider1);
          
          // step2. 添加PageSlider相关属性
          pageSlider.setReboundEffect(true);
          pageSlider.setPageMargin(AttrHelper.vp2px(100, AttrHelper.getDensity(getContext())));
          
          // step3. 获取页面
          List<Component> pageList = new ArrayList<>(8);
          pageList.add(getComponent(ResourceTable.Layout_page_slider1));
          pageList.add(getComponent(ResourceTable.Layout_page_slider2));
          pageList.add(getComponent(ResourceTable.Layout_page_slider3));
          
          // step4. 为PageSlider添加页面数据
          pageSlider.setProvider(new PageSliderProvider() {
              @Override
              public int getCount() {
                  // 获取可用视图的数量
                  return pageList.size();
              }
              
              @Override
              public Object createPageInContainer(ComponentContainer componentContainer, int index) {
                  // 在指定位置创建页面
                  componentContainer.addComponent(pageList.get(index));
                  return pageList.get(index);
              }
              
              @Override
              public void destroyPageFromContainer(ComponentContainer componentContainer, int index, Object o) {
                  // 销毁容器中的指定页面
                  componentContainer.removeComponent(pageList.get(index));
                  // 这里pageList.get(index)和o相等
                  // componentContainer.removeComponent((Component)o);
              }
              
              @Override
              public boolean isPageMatchToObject(Component component, Object o) {
                  // 视图是否关联指定对象
                  return component == o;
              }
          });
      }
      
      private DirectionalLayout getComponent(int resId) {
          return (DirectionalLayout)LayoutScatter.getInstance(getContext()).parse(resId, null, false);
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
  }
  ```

### 示例二

- 图片

  ![2](../../../repository/2-1654164885544.gif)

- xml部分

  - ability_page_slider.xml

    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <DirectionalLayout
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:alignment="center"
        ohos:orientation="vertical">
    
        <TabList
            ohos:id="$+id:tabList2"
            ohos:background_element="#fcc"
            ohos:text_alignment="center"
            ohos:layout_alignment="center"
            ohos:tab_margin="5vp"
            ohos:padding="5vp"
            ohos:tab_length="60vp"
            ohos:text_size="18vp"
            ohos:height="50vp"
            ohos:width="match_parent"
    
            ohos:normal_text_color="#fff"
            ohos:selected_text_color="#f00"
            ohos:selected_tab_indicator_color="#f00"
            ohos:selected_tab_indicator_height="2vp"
            />
    
        <PageSlider
            ohos:id="$+id:pageSlider1"
            ohos:background_element="#808080"
            ohos:height="match_parent"
            ohos:width="match_parent" />
    
    </DirectionalLayout>
    ```

  - page_slider1.xml

    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <DirectionalLayout
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:background_element="#f0c"
        ohos:margin="30vp"
        ohos:orientation="vertical">
    
    
        <Text
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:text_alignment="center"
            ohos:text="Page1"
            ohos:text_size="50fp"
            />
    </DirectionalLayout>
    ```

    注：page_slider2.xml、page_slider3.xml与page_slider1.xml差不多，就不贴出来了

- java部分

  ```java
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.components.AttrHelper;
  import ohos.agp.components.Component;
  import ohos.agp.components.ComponentContainer;
  import ohos.agp.components.DirectionalLayout;
  import ohos.agp.components.LayoutScatter;
  import ohos.agp.components.PageSlider;
  import ohos.agp.components.PageSliderProvider;
  import ohos.agp.components.TabList;
  
  import java.util.ArrayList;
  import java.util.List;
  
  public class PageSliderAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_page_slider);
          
          // step0. 组装TabList
          String[] tabInfos = {"相册", "视频", "音乐"};
          TabList tabList = findComponentById(ResourceTable.Id_tabList2);
          // 添加tab
          for (String tabInfo : tabInfos) {
              TabList.Tab tab = tabList.new Tab(getContext());
              tab.setText(tabInfo);
              tabList.addTab(tab);
          }
          tabList.selectTabAt(0);// 设置选中tab
       
          // step1. 获取PageSlider
          PageSlider pageSlider = findComponentById(ResourceTable.Id_pageSlider1);
          
          // step2. 添加PageSlider相关属性
          pageSlider.setReboundEffect(true);
          pageSlider.setPageMargin(AttrHelper.vp2px(100, AttrHelper.getDensity(getContext())));
          
          // step3. 获取页面
          List<Component> pageList = new ArrayList<>(8);
          pageList.add(getComponent(ResourceTable.Layout_page_slider1));
          pageList.add(getComponent(ResourceTable.Layout_page_slider2));
          pageList.add(getComponent(ResourceTable.Layout_page_slider3));
          
          // step4. 为PageSlider添加页面数据
          pageSlider.setProvider(new PageSliderProvider() {
              @Override
              public int getCount() {
                  // 获取可用视图的数量
                  return pageList.size();
              }
              
              @Override
              public Object createPageInContainer(ComponentContainer componentContainer, int index) {
                  // 在指定位置创建页面
                  componentContainer.addComponent(pageList.get(index));
                  return pageList.get(index);
              }
              
              @Override
              public void destroyPageFromContainer(ComponentContainer componentContainer, int index, Object o) {
                  // 销毁容器中的指定页面
                  componentContainer.removeComponent(pageList.get(index));
                  // 这里pageList.get(index)和o相等
                  // componentContainer.removeComponent((Component)o);
              }
              
              @Override
              public boolean isPageMatchToObject(Component component, Object o) {
                  // 视图是否关联指定对象
                  return component == o;
              }
          });
          
          // step5. 监听PageSlider的切换时间
          pageSlider.addPageChangedListener(new PageSlider.PageChangedListener() {
      
              // 页面滑动时
              @Override
              public void onPageSliding(int i, float v, int i1) {
          
              }
      
              // 页面状态改变时
              @Override
              public void onPageSlideStateChanged(int i) {
          
              }
      
              // 页面被选中时
              @Override
              public void onPageChosen(int index) {
                  tabList.selectTabAt(index);
              }
          });
      
          // 监听tablist事件
          tabList.addTabSelectedListener(new TabList.TabSelectedListener() {
              // 当某个Tab从未选中状态变为选中状态时的回调
              @Override
              public void onSelected(TabList.Tab tab) {
                  pageSlider.setCurrentPage(tab.getPosition());
              }
          
              // 当某个Tab从选中状态变为未选中状态时的回调
              @Override
              public void onUnselected(TabList.Tab tab) {
              }
          
              // 当某个Tab已处于选中状态，再次被点击时的状态回调
              @Override
              public void onReselected(TabList.Tab tab) {
              }
          });
      }
      
      private DirectionalLayout getComponent(int resId) {
          return (DirectionalLayout) LayoutScatter.getInstance(getContext()).parse(resId, null, false);
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
  }
  ```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-pageslider-0000001091933258)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)