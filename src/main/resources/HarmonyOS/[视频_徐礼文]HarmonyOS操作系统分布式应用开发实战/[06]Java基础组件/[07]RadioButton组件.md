# RadioButton组件

- [RadioButton组件](#radiobutton组件)
  - [简述](#简述)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 简述

RadioButton用于多选一的操作，需要搭配RadioContainer使用，实现单选效果

![1653988941212](../../../repository/1653988941212.png)

## 示例说明

- 图片

  ![2](../../../repository/2-1653990141007.gif)

  提示：应为还没有设置RadioContainer，所以上图中的两个单选按钮还没有互斥的效果

- （上述图片的）ability_radio_button.xml实现

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <!--
      基本用法
      -->
      <RadioButton
          ohos:id="$+id:radio1"
          ohos:height="match_content"
          ohos:width="300vp"
          ohos:text="选择学习Java"
          ohos:text_size="20fp"
          ohos:margin="30vp"
          />
  
      <!--
          text_color_off: 没选中的item的字体的颜色
          text_color_on: 选中的item的字体的颜色
          check_element: 用于修改前面那个小圆点形状形状
      -->
      <RadioButton
          ohos:id="$+id:radio2"
          ohos:height="match_content"
          ohos:width="300vp"
          ohos:check_element="$graphic:radio_state"
  
          ohos:text="选择学习HarmonyOS"
          ohos:text_color_off="#d4f"
          ohos:text_color_on="#f00"
          ohos:text_size="20fp"
          />
  
  </DirectionalLayout>
  ```

- radio_state.xml辅助部分

  ```xml
  <?xml version="1.0" encoding="UTF-8" ?>
  <!-- 形状 -->
  <state-container
      xmlns:ohos="http://schemas.huawei.com/res/ohos">
  
      <!--  选中状态  -->
      <item
          ohos:element="$graphic:background_checkbox_checked"
          ohos:state="component_state_checked"/>
  
      <!--  没选中状态  -->
      <item
          ohos:element="$graphic:background_checkbox_empty"
          ohos:state="component_state_empty"/>
  </state-container>
  ```

- background_checkbox_checked.xml辅助部分

  ```xml
  <?xml version="1.0" encoding="UTF-8" ?>
  <shape xmlns:ohos="http://schemas.huawei.com/res/ohos"
         ohos:shape="rectangle">
      <solid
          ohos:color="#007DFF"/>
      <corners
          ohos:radius="4"/>
  </shape>
  ```

- background_checkbox_empty.xml辅助部分

  ```xml
  <?xml version="1.0" encoding="UTF-8" ?>
  <shape xmlns:ohos="http://schemas.huawei.com/res/ohos"
         ohos:shape="rectangle">
      <stroke ohos:width="5"
              ohos:color="gray"/>
      <corners
          ohos:radius="4"/>
  </shape>
  ```


## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-radiobutton-0000001060647792)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)