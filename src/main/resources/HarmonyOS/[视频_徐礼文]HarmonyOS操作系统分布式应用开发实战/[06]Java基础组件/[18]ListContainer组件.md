# ListContainer组件

- [ListContainer组件](#listcontainer组件)
  - [简述](#简述)
    - [支持的XML属性](#支持的xml属性)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 简述

ListContainer是用来呈现连续、多行数据的组件，包含一系列相同类型的列表项。

### 支持的XML属性

ListContainer的共有XML属性继承自：[Component](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-common-xml-0000001138483639)

ListContainer的自有XML属性见下表：

| 属性名称       | 中文描述           | 取值                        | 取值说明                                        | 使用案例                                                     |
| -------------- | ------------------ | --------------------------- | ----------------------------------------------- | ------------------------------------------------------------ |
| rebound_effect | 开启/关闭回弹效果  | boolean类型                 | 可以直接设置true/false，也可以引用boolean资源。 | ohos:rebound_effect="true"ohos:rebound_effect="$boolean:true" |
| shader_color   | 着色器颜色         | color类型                   | 可以直接设置色值，也可以引用color资源。         | ohos:shader_color="#A8FFFFFF"ohos:shader_color="$color:black" |
| orientation    | 列表项排列方向     | horizontal                  | 表示水平方向列表。                              | ohos:orientation="horizontal"                                |
| vertical       | 表示垂直方向列表。 | ohos:orientation="vertical" |                                                 |                                                              |

## 示例说明

- 图片

  ![2](../../../repository/2-1654495473571.gif)

- xml部分

  - ability_list_container.xml

    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <DirectionalLayout
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:alignment="center"
        ohos:orientation="vertical">
    
        <ListContainer
            ohos:id="$+id:listContainer1"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:background_element="#fcc"
            ohos:rebound_effect="true"
            />
    
    </DirectionalLayout>
    ```

  - list_container_item.xml

    ```xml
    <?xml version="1.0" encoding="utf-8"?>
    <DirectionalLayout
        xmlns:ohos="http://schemas.huawei.com/res/ohos"
        ohos:height="match_content"
        ohos:width="match_parent"
        ohos:orientation="vertical">
    
    
        <Text
            ohos:id="$+id:item"
            ohos:height="80vp"
            ohos:width="match_parent"
            ohos:text_alignment="left"
            ohos:text_size="20fp"
            />
    
    </DirectionalLayout>
    ```

- java部分

  ```java
  import com.example.javabasecomponent.ResourceTable;
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.components.BaseItemProvider;
  import ohos.agp.components.Component;
  import ohos.agp.components.ComponentContainer;
  import ohos.agp.components.LayoutScatter;
  import ohos.agp.components.ListContainer;
  import ohos.agp.components.Text;
  
  import java.util.ArrayList;
  import java.util.List;
  
  public class ListContainerAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_list_container);
          
          // step1. 获取组件
          ListContainer listContainer1 = findComponentById(ResourceTable.Id_listContainer1);
          
          // 模拟数据
          List<SampleItem> itemList = getDataList();
          
          // step2. 设置ListContainer数据源
          listContainer1.setItemProvider(new BaseItemProvider() {
              // 返回填充的表项个数
              @Override
              public int getCount() {
                  return itemList.size();
              }
              
              // 根据position返回对应的数据
              @Override
              public Object getItem(int index) {
                  return itemList.get(index);
              }
              
              // 返回某一项的id
              @Override
              public long getItemId(int id) {
                  return id;
              }
              
              /**
               * 根据position返回对应的界面组件
               *
               * @param index 组件在数组集中的位置
               * @param component 要重用的上一个组件。如果没有此类组件，则此参数可以为null
               * @param componentContainer 与要获取的组件匹配的父组件
               *
               * @return 组件
               */
              @Override
              public Component getComponent(int index, Component component, ComponentContainer componentContainer) {
                  Component tmpComponent;
                  if (component == null) {
                      tmpComponent =
                              LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_list_container_item,
                                      null, false);
                  } else {
                      tmpComponent = component;
                  }
                  Text text = tmpComponent.findComponentById(ResourceTable.Id_item);
                  text.setText(itemList.get(index).getName());
                  return tmpComponent;
              }
          });
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
      
      private List<SampleItem> getDataList() {
          List<SampleItem> list = new ArrayList<>();
          for (int i = 0; i < 10; i++) {
              list.add(new SampleItem("item - " + i));
          }
          return list;
      }
      
  }
  
  class SampleItem {
      private String name;
      
      public SampleItem(String name) {
          this.name = name;
      }
      
      public String getName() {
          return name;
      }
      
      public void setName(String name) {
          this.name = name;
      }
  }
  ```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-listcontainer-0000001060007847)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)