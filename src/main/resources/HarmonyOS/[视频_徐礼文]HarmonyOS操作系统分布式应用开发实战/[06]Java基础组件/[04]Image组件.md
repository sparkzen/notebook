# Image组件

- [Image组件](#image组件)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 示例说明

- 图片

  ![1653640196303](../../../repository/1653640196303.png)

- （上述图片的）xml实现

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <AdaptiveBoxLayout
          ohos:height="match_parent"
          ohos:width="match_parent">
  
          <!-- 小图片显示在大框中 -->
          <Image
              ohos:id="$+id:image1"
              ohos:height="120vp"
              ohos:width="120vp"
              ohos:background_element="$graphic:background_ability_image"
              ohos:image_src="$media:icon"
              ohos:margin="5vp"
              />
  
  
          <!-- 大图片显示在小框中（只能显示一部分） -->
          <Image
              ohos:id="$+id:image2"
              ohos:height="120vp"
              ohos:width="120vp"
              ohos:background_element="$graphic:background_ability_image"
              ohos:image_src="$media:1"
              ohos:margin="5vp"
              />
  
          <!--
            大图片显示在小框中(scale_mode -> zoom等比缩放)
            zoom_center： 等比缩放，放在框的中间
            zoom_start： 等比缩放，放在框的顶部
            zoom_end： 等比缩放，放在框的底部
            注：inside模式效果等同于zoom_center
          -->
          <Image
              ohos:id="$+id:image3"
              ohos:height="120vp"
              ohos:width="120vp"
              ohos:background_element="$graphic:background_ability_image"
              ohos:image_src="$media:1"
              ohos:margin="5vp"
              ohos:scale_mode="zoom_center"
              />
  
          <!--
            大图片显示在小框中(stretch -> 填充， 可能导致变形)
          -->
          <Image
              ohos:id="$+id:image4"
              ohos:height="120vp"
              ohos:width="120vp"
              ohos:background_element="$graphic:background_ability_image"
              ohos:image_src="$media:1"
              ohos:margin="5vp"
              ohos:scale_mode="stretch"
              />
  
          <!--
            大图片显示在小框中(剪切模式 -> 剪切图片的一部分进行放入)
            ohos:scale_mode="clip_center"  剪切中间
            ohos:clip_alignment="bottom"   剪切底部
            ......
          -->
          <Image
              ohos:id="$+id:image5"
              ohos:height="120vp"
              ohos:width="120vp"
              ohos:background_element="$graphic:background_ability_image"
              ohos:clip_alignment="center"
              ohos:image_src="$media:1"
              ohos:margin="5vp"
              />
  
          <!-- 缩放 -->
          <Image
              ohos:id="$+id:image6"
              ohos:height="120vp"
              ohos:width="120vp"
              ohos:background_element="$graphic:background_ability_image"
              ohos:image_src="$media:1"
              ohos:margin="5vp"
              ohos:scale_x="0.5"
              />
  
          <!-- 透明度 alpha设置 -->
          <Image
              ohos:id="$+id:image7"
              ohos:background_element="$graphic:background_ability_image"
              ohos:image_src="$media:1"
              ohos:margin="5vp"
              ohos:scale_mode="clip_center"
              ohos:alpha="0.5"
              ohos:height="120vp"
              ohos:width="120vp"
              />
  
          <!-- 设置图片圆角 （需要配合代码） -->
          <Image
              ohos:id="$+id:image8"
              ohos:background_element="$graphic:background_ability_image"
              ohos:image_src="$media:1"
              ohos:margin="5vp"
              ohos:scale_mode="clip_center"
              ohos:height="200vp"
              ohos:width="200vp"
              />
      </AdaptiveBoxLayout>
  </DirectionalLayout>
  
  ```

- java辅助部分

  > 提示：在使用idea预览时，如果效果部分涉及到使用java辅助，那么需要现在idea里面打开对应的AbilitySlice文件，然后点击右侧的preview预览，java代码部分的辅助效果才会生效

  ```java
  import com.example.javabasecomponent.ResourceTable;
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.components.AttrHelper;
  import ohos.agp.components.Component;
  import ohos.agp.components.Image;
  
  public class ImageAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_image);
      
          Image image8 = findComponentById(ResourceTable.Id_image8);
          image8.setCornerRadius(AttrHelper.vp2px(100, getContext()));
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
  }
  ```


## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-image-0000001058976860)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)

