# RoundProgressBar组件

- [RoundProgressBar组件](#roundprogressbar组件)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 示例说明

- 图片

  ![1654062897974](../../../repository/1654062897974.png)

- （上述图片的）ability_round_progress_bar.xml实现

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:background_element="#3cc"
      ohos:orientation="vertical">
  
      <!--
          width/height: 设置进度条形状的宽/高
          progress_width: 设置进度条的宽度
          progress_hint_text: 设置进度条提示
          progress_hint_text_size: 设置progress_hint_text的颜色
          progress_hint_text_alignment: 设置progress_hint_text对齐方式
          progress_color: 设置进度条的颜色
          progress: 设置进度条当前进度的值
          min/max: 设置进度条所代表的的最小最大值
          start_angle: 设置从多少度开始
          max_angle: 设置最大角度
        -->
      <RoundProgressBar
          ohos:id="$+id:roundProgressBar1"
          ohos:height="250vp"
          ohos:width="250vp"
          ohos:max="100"
          ohos:max_angle="270"
          ohos:min="0"
          ohos:progress="15"
          ohos:progress_color="#f00"
          ohos:progress_hint_text="30%"
          ohos:progress_hint_text_alignment="center"
          ohos:progress_hint_text_size="60fp"
          ohos:progress_width="30vp"
          ohos:start_angle="15"
          />
  
  </DirectionalLayout>
  ```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-roundprogressbar-0000001062558057)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)