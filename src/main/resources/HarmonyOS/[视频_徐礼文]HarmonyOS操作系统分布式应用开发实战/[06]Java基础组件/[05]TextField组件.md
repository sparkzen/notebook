# TextField组件

- [TextField组件](#textfield组件)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 示例说明

- 图片

  ![2](../../../repository/2.gif)

  注：浅蓝色的原点不是绘制出来的，是自带的光标显示效果

  注：ide观察TextField的效果需要启动设备模拟器，preview是没法看TextField的效果的

- （上述图片的）ability_text_field.xml实现

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
      <!--
          background_element: 背景
          margin: 外边距
          padding: 内边距
          text_size: 字体大小
          text_alignment: 对齐方式
          text_color: 字体颜色
          text_input_type: 输入类型
          basement: 设置底部元素（下划线、图片什么之类的）
          hint: 提示信息（占位符）
      -->
      <TextField
          ohos:height="100vp"
          ohos:width="match_parent"
          ohos:background_element="$graphic:background_ability_text_field"
          ohos:basement="#3c3"
          ohos:hint="输入密码"
          ohos:margin="20vp"
          ohos:padding="20vp"
          ohos:text_alignment="top"
          ohos:text_color="#fff"
          ohos:text_input_type="pattern_password"
          ohos:text_size="30fp"
          />
  
  </DirectionalLayout>
  ```

- background_ability_text_field.xml辅助部分

  ```xml
  <?xml version="1.0" encoding="UTF-8" ?>
  <!-- 形状 -->
  <shape
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:shape="rectangle">
  
      <!--  颜色  -->
      <solid
          ohos:color="#cce"/>
  
      <!--  圆角  -->
      <corners ohos:radius="20vp"/>
  </shape>
  ```


## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-textfield-0000001061125582)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)