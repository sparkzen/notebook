# CheckBox组件

- [CheckBox组件](#checkbox组件)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 示例说明

- 图片

  ![2](../../../repository/2-1653993876374.gif)

- （上述图片的）ability_check_box.xml实现

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center|left"
      ohos:background_element="#ccc"
      ohos:orientation="vertical"
      >
  
      <Text
          ohos:height="match_content"
          ohos:width="match_content"
          ohos:text="你最想学习什么开发？"
          ohos:text_size="30fp"
          ohos:text_color="green"
          ohos:padding="5vp"
          ohos:margin="20vp"
          />
  
      <Text
          ohos:id="$+id:resultShow1"
          ohos:height="match_content"
          ohos:width="match_content"
          ohos:text_size="30fp"
          ohos:text_color="red"
          ohos:padding="5vp"
          ohos:margin="20vp"
          />
  
      <!--  基础使用  -->
      <Checkbox
          ohos:id="$+id:checkBox1"
          ohos:height="match_content"
          ohos:width="match_content"
          ohos:text="IOS App"
          ohos:text_size="30fp"
          ohos:margin="20vp"
          />
  
      <!--
          text_color_off: 没选中的item的字体的颜色
          text_color_on: 选中的item的字体的颜色
          check_element: 用于修改前面那个小圆点形状形状
      -->
      <Checkbox
          ohos:id="$+id:checkBox2"
          ohos:height="match_content"
          ohos:width="match_content"
          ohos:check_element="$graphic:check_state"
          ohos:text="HarmonyOS App"
          ohos:text_color_off="red"
          ohos:text_color_on="blue"
          ohos:text_size="30fp"
          ohos:margin="20vp"
          />
  
      <Checkbox
          ohos:id="$+id:checkBox3"
          ohos:height="match_content"
          ohos:width="match_content"
          ohos:bottom_margin="20vp"
          ohos:text="Android App"
          ohos:text_size="30fp"
          ohos:margin="20vp"
          />
  
  </DirectionalLayout>
  ```

- java辅助部分

  ```java
  import com.example.javabasecomponent.ResourceTable;
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.components.AbsButton;
  import ohos.agp.components.Checkbox;
  import ohos.agp.components.Component;
  import ohos.agp.components.Text;
  
  import java.util.HashSet;
  
  public class CheckBoxAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_check_box);
          Checkbox checkBox1 = findComponentById(ResourceTable.Id_checkBox1);
          Checkbox checkBox2 = findComponentById(ResourceTable.Id_checkBox2);
          Checkbox checkBox3 = findComponentById(ResourceTable.Id_checkBox3);
          Text resultShow1 = findComponentById(ResourceTable.Id_resultShow1);
          HashSet<String> selectedSet = new HashSet<>();
          
          
      
          checkBox1.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
              @Override
              public void onCheckedChanged(AbsButton absButton, boolean checked) {
                  String text = absButton.getText();
                  if (checked) {
                      // 选中
                      selectedSet.add(text);
                  } else {
                      // 未选中
                      selectedSet.remove(text);
                  }
                  // 展示结果
                  resultShow1.setText(selectedSet.toString());
              }
          });
      
          checkBox2.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
              @Override
              public void onCheckedChanged(AbsButton absButton, boolean checked) {
                  String text = absButton.getText();
                  if (checked) {
                      // 选中
                      selectedSet.add(text);
                  } else {
                      // 未选中
                      selectedSet.remove(text);
                  }
                  // 展示结果
                  resultShow1.setText(selectedSet.toString());
              }
          });
      
          checkBox3.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
              @Override
              public void onCheckedChanged(AbsButton absButton, boolean checked) {
                  String text = absButton.getText();
                  if (checked) {
                      // 选中
                      selectedSet.add(text);
                  } else {
                      // 未选中
                      selectedSet.remove(text);
                  }
                  // 展示结果
                  resultShow1.setText(selectedSet.toString());
              }
          });
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
  }
  ```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-checkbox-0000001060487804)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)