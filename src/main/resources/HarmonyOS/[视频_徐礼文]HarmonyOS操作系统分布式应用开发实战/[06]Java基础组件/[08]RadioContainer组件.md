# RadioContainer组件

- [RadioContainer组件](#radiocontainer组件)
  - [简述](#简述)
  - [示例说明](#示例说明)
  - [补充](#补充)
  - [相关资料](#相关资料)

## 简述

RadioContainer组件主要是用来实现多个RadioButton互斥效果的

## 示例说明

- 图片

  ![2](../../../repository/2-1653991806384.gif)

- （上述图片的）ability_radio_container.xml实现

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <!--
      alignment: 设置对齐
      horizontal: 设置单选按钮的布局
      -->
      <RadioContainer
          ohos:id="$+id:radioContainer1"
          ohos:height="match_parent"
          ohos:width="match_parent"
          ohos:alignment="center"
          ohos:orientation="horizontal"
          >
  
  
          <RadioButton
              ohos:id="$+id:radio3"
              ohos:height="match_content"
              ohos:width="match_content"
              ohos:margin="10vp"
              ohos:text="选择学习Java"
              ohos:text_alignment="left"
              ohos:text_color_off="green"
              ohos:text_color_on="red"
              ohos:text_size="20fp"
              />
  
          <RadioButton
              ohos:id="$+id:radio4"
              ohos:height="match_content"
              ohos:width="match_content"
              ohos:margin="10vp"
              ohos:text="选择学习Python"
              ohos:text_alignment="left"
              ohos:text_color_off="green"
              ohos:text_color_on="red"
              ohos:text_size="20fp"
              />
      </RadioContainer>
  </DirectionalLayout>
  ```

## 补充

我们可以给RadioContainer加相关的事件

```java
import com.example.javabasecomponent.slice.RadioContainerAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.RadioContainer;

public class RadioContainerAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(RadioContainerAbilitySlice.class.getName());
    
        RadioContainer radioContainer1 = findComponentById(ResourceTable.Id_radioContainer1);
        /*
         * radioContainer1.mark(0); // 根据索引值设置指定RadioButton为选定状态
         * radioContainer1.cancelMarks();; // 清除RadioContainer中所有RadioButton的选定状态
         */
        radioContainer1.setMarkChangedListener(new RadioContainer.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(RadioContainer radioContainer, int index) {
                System.err.println("您选中了第" + (index + 1) + "个单选项！");
            }
        });
    }
}
```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-radiocontainer-0000001063470429)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)