# Text组件

- [Text组件](#text组件)
  - [概览](#概览)
  - [基础设置](#基础设置)
  - [高级设置](#高级设置)
  - [相关资料](#相关资料)

## 概览

![1653293165073](../../../repository/1653293165073.png)

## 基础设置

- 背景设置 - 示例1

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <Text
          ohos:id="$+id:t0"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="#3dd"
          ohos:layout_alignment="horizontal_center"
          />
  </DirectionalLayout>
  ```

  ![1653621366802](../../../repository/1653621366802.png)

- 背景设置 - 示例2

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <Text
          ohos:id="$+id:t1"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="$graphic:background_ability_text1"
          ohos:layout_alignment="horizontal_center"
          />
  </DirectionalLayout>
  ```

  其中，background_ability_text1对应的xml文件为

  ```xml
  <?xml version="1.0" encoding="UTF-8" ?>
  <shape
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:shape="rectangle">
  
      <!--  设置背景色  -->
      <solid
          ohos:color="#3c3"/>
      <!--  设置圆角  -->
      <corners ohos:radius="30vp"/>
      <!--  设置边框  -->
      <stroke
          ohos:width="6vp"
          ohos:color="red"/>
  </shape>
  ```

  ![1653621394809](../../../repository/1653621394809.png)

- 文字设置 - 示例1

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <Text
          ohos:id="$+id:t1"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="$graphic:background_ability_text1"
          ohos:layout_alignment="horizontal_center"
          ohos:text="你好！"
          ohos:text_alignment="center"
          ohos:text_size="30fp"
          ohos:text_color="#fff"
          />
  </DirectionalLayout>
  ```

  ![1653621731809](../../../repository/1653621731809.png)

- 文字设置 - 示例2

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <Text
          ohos:id="$+id:t1"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="$graphic:background_ability_text1"
          ohos:layout_alignment="horizontal_center"
          ohos:text="你好！"
          ohos:left_padding="20vp"
          ohos:top_padding="20vp"
          ohos:text_alignment="top"
          ohos:text_size="30fp"
          ohos:text_color="red"
          />
  </DirectionalLayout>
  ```

  ![1653622001440](../../../repository/1653622001440.png)

## 高级设置

- truncation_mode截断显示 - 示例1

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <Text
          ohos:id="$+id:t1"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="$graphic:background_ability_text1"
          ohos:layout_alignment="horizontal_center"
          ohos:text="黄河之水天上来，奔流到海不复回~"
          ohos:left_padding="20vp"
          ohos:top_padding="20vp"
          ohos:text_alignment="top"
          ohos:text_size="30fp"
          ohos:text_color="red"
          ohos:truncation_mode="ellipsis_at_middle"
          />
  </DirectionalLayout>
  ```

  ![1653622332480](../../../repository/1653622332480.png)

- multiple_lines多行显示 - 示例1

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <Text
          ohos:id="$+id:t1"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="$graphic:background_ability_text1"
          ohos:layout_alignment="horizontal_center"
          ohos:text="黄河之水天上来，奔流到海不复回~"
          ohos:left_padding="20vp"
          ohos:top_padding="20vp"
          ohos:text_alignment="top"
          ohos:text_size="30fp"
          ohos:text_color="red"
          ohos:multiple_lines="true"
          />
  </DirectionalLayout>
  ```

  ![1653622431145](../../../repository/1653622431145.png)

- multiple_lines多行显示 - 示例2

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <!--  truncation_mode：设置截断显示模式  -->
      <!--  multiple_lines：设置多行显示  -->
      <!--  max_text_lines：设置最大显示行数  -->
      <Text
          ohos:id="$+id:t1"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="$graphic:background_ability_text1"
          ohos:layout_alignment="horizontal_center"
          ohos:left_padding="20vp"
          ohos:max_text_lines="2"
          ohos:multiple_lines="true"
          ohos:text="黄河之水天上来，奔流到海不复回~头昏脑涨腿抽筋~"
          ohos:text_alignment="top"
          ohos:text_color="red"
          ohos:text_size="30fp"
          ohos:top_padding="20vp"
          ohos:truncation_mode="ellipsis_at_end"
          />
  </DirectionalLayout>
  ```

  ![1653622666905](../../../repository/1653622666905.png)

- 滚动设置 - 示例

  xml部分

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <Text
          ohos:id="$+id:text2"
          ohos:height="100vp"
          ohos:width="300vp"
          ohos:background_element="#33c"
          ohos:layout_alignment="horizontal_center"
          ohos:left_padding="20vp"
          ohos:text="黄河之水天上来，奔流到海不复回~头昏脑涨腿抽筋~"
          ohos:text_alignment="top"
          ohos:text_color="red"
          ohos:text_size="30fp"
          ohos:top_padding="20vp"
          />
  </DirectionalLayout>
  ```

  java辅助部分

  > 提示：在使用idea预览时，如果效果部分涉及到使用java辅助，那么需要现在idea里面打开对应的AbilitySlice文件，然后点击右侧的preview预览，java代码部分的辅助效果才会生效

  ```java
  import com.example.javabasecomponent.ResourceTable;
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.components.Text;
  
  public class TextAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_text1);
      
          Text text2 = findComponentById(ResourceTable.Id_text2);
          text2.setTruncationMode(Text.TruncationMode.AUTO_SCROLLING);
          text2.setAutoScrollingCount(Text.AUTO_SCROLLING_FOREVER);
          text2.startAutoScrolling();
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
  }
  ```

  ![1](../../../repository/1.gif)

- element_xxx设置元素 - 示例

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <Text
          ohos:id="$+id:text3"
          ohos:height="300vp"
          ohos:width="300vp"
          ohos:background_element="#5dc"
          ohos:layout_alignment="horizontal_center"
          ohos:left_padding="20vp"
          ohos:text="看一看"
          ohos:text_alignment="center"
          ohos:text_color="red"
          ohos:text_size="30fp"
          ohos:margin="15vp"
  
          ohos:element_left="$media:icon"
          ohos:element_right="#f00"
          ohos:element_top="#f00"
          ohos:element_bottom="#ccc"
          />
  </DirectionalLayout>
  ```

  ![1653634865801](../../../repository/1653634865801.png)

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-text-0000001050729534)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)