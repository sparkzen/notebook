# WebView组件

- [WebView组件](#webview组件)
  - [简述](#简述)
  - [加载在线网页、加载本地html示例说明](#加载在线网页加载本地html示例说明)
  - [相关资料](#相关资料)

## 简述

WebView提供在应用中集成Web页面的能力。

在使用WebView时需要配置应用的网络权限。打开"entry > src > main > config.json"，并添加如下配置：

```xml
{
  ...
  "module": {
    ...
    "reqPermissions": [
      {
        "name": "ohos.permission.INTERNET"
      }
    ],
    ...
  }
}
```

## 加载在线网页、加载本地html示例说明

- demo1图片

  ![2](../../../repository/2-1654498456999.gif)

- demo2图片

  ![2](../../../repository/2-1654500439423.gif)

- xml部分

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <ohos.agp.components.webengine.WebView
          ohos:id="$+id:webView1"
          ohos:height="match_parent"
          ohos:width="match_parent"
          >
  
      </ohos.agp.components.webengine.WebView>
  
  </DirectionalLayout>
  ```

- 本地资源文件

  ![1](../../../repository/1-1654500434163.png)

  其中html内容如下：

  ```html
  <!doctype html>
  <html lang="cn">
      <head>
          <meta charset="utf-8">
          <title>关于我</title>
      </head>
      <body>
          <img src="1.jpeg" width="100%" height="100%"/>
      </body>
  </html>
  ```

- java部分

  ```java
  import com.example.javabasecomponent.ResourceTable;
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.components.webengine.ResourceError;
  import ohos.agp.components.webengine.ResourceRequest;
  import ohos.agp.components.webengine.ResourceResponse;
  import ohos.agp.components.webengine.WebAgent;
  import ohos.agp.components.webengine.WebView;
  import ohos.agp.utils.TextTool;
  import ohos.agp.window.dialog.ToastDialog;
  import ohos.global.resource.Resource;
  import ohos.media.image.PixelMap;
  import ohos.utils.net.Uri;
  
  import java.io.IOException;
  import java.net.URLConnection;
  
  public class WebViewAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_web_view);
          // 加载url
          //demo1();
          // 加载本地html
          demo2();
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
      
      private void demo1() {
          WebView webView1 = findComponentById(ResourceTable.Id_webView1);
          // 允许WebView执行JavaScript
          webView1.getWebConfig().setJavaScriptPermit(true);
          webView1.load("https://www.baidu.com");
          // 通过WebAgent，可以在WebView加载网页的生命周期中做一些操作
          webView1.setWebAgent(new WebAgent() {
              @Override
              public void onLoadingPage(WebView webView, String url, PixelMap icon) {
                  // 页面开始加载时自定义处理
                  ToastDialog toastDialog = new ToastDialog(getContext());
                  toastDialog.setText("page loading ......");
                  toastDialog.show();
                  super.onLoadingPage(webView, url, icon);
              }
              
              @Override
              public void onPageLoaded(WebView webView, String url) {
                  // 页面加载结束后自定义处理
                  super.onPageLoaded(webView, url);
              }
              
              @Override
              public void onLoadingContent(WebView webView, String url) {
                  // 加载资源时自定义处理
                  super.onLoadingContent(webView, url);
              }
              
              @Override
              public void onError(WebView webView, ResourceRequest request, ResourceError error) {
                  // 发生错误时自定义处理
                  super.onError(webView, request, error);
              }
          });
      }
      
      private void demo2() {
          WebView webView1 = findComponentById(ResourceTable.Id_webView1);
          // 允许WebView执行JavaScript
          webView1.getWebConfig().setJavaScriptPermit(true);
          webView1.load("https://com.example.javabasecomponent/rawfile/example.html");
          // 通过WebAgent，可以在WebView加载网页的生命周期中做一些操作
          webView1.setWebAgent(new WebAgent() {
              // 处理本地资源文件访问
              @Override
              public ResourceResponse processResourceRequest(WebView webView, ResourceRequest request) {
                  Uri requestUrl = request.getRequestUrl();
                  // 判断bundle是否合法
                  String authority = "com.example.javabasecomponent";
                  if (authority.equals(requestUrl.getDecodedAuthority())) {
                      // 判断rawfile
                      String decodedPath = requestUrl.getDecodedPath();
                      if (TextTool.isNullOrEmpty(decodedPath)) {
                          return super.processResourceRequest(webView, request);
                      }
                      if (decodedPath.startsWith("/rawfile/")) {
                          decodedPath = decodedPath.substring("/rawfile/".length());
                      }
                      String rawfilePath = "entry/resources/rawfile/" + decodedPath;
      
                      String mineType = URLConnection.guessContentTypeFromName(rawfilePath);
                      try {
                          Resource resource = getResourceManager().getRawFileEntry(rawfilePath).openRawFile();
                          return new ResourceResponse(mineType, resource, null);
                      } catch (IOException e) {
                          throw new RuntimeException(e);
                      }
                  }
                  return super.processResourceRequest(webView, request);
              }
          });
      }
  }
  ```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-webview-0000001092715158)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)