# ToastDialog组件

- [ToastDialog组件](#toastdialog组件)
  - [简述](#简述)
  - [示例说明](#示例说明)
  - [相关资料](#相关资料)

## 简述

ToastDialog是在窗口上方弹出的对话框，是通知操作的简单反馈。ToastDialog会在一段时间后消失，在此期间，用户还可以操作当前窗口的其他组件

![1654063473748](../../../repository/1654063473748.png)

## 示例说明

- 图片

  ![2](../../../repository/2-1654070528637.gif)

- ability_toast_dialog.xml部分

  ```xml
  <?xml version="1.0" encoding="utf-8"?>
  <DirectionalLayout
      xmlns:ohos="http://schemas.huawei.com/res/ohos"
      ohos:height="match_parent"
      ohos:width="match_parent"
      ohos:alignment="center"
      ohos:orientation="vertical">
  
      <Button
          ohos:id="$+id:button7"
          ohos:height="60vp"
          ohos:width="300vp"
          ohos:text_size="20fp"
          ohos:text="弹出消息(点击弹出ToastDialog)"
          ohos:background_element="#f00"
          />
  
  </DirectionalLayout>
  ```
  
- java部分

  ```java
  import com.example.javabasecomponent.ResourceTable;
  import ohos.aafwk.ability.AbilitySlice;
  import ohos.aafwk.content.Intent;
  import ohos.agp.colors.RgbColor;
  import ohos.agp.components.AttrHelper;
  import ohos.agp.components.Component;
  import ohos.agp.components.ComponentContainer;
  import ohos.agp.components.Text;
  import ohos.agp.components.element.ShapeElement;
  import ohos.agp.utils.LayoutAlignment;
  import ohos.agp.window.dialog.ToastDialog;
  
  public class ToastDialogAbilitySlice extends AbilitySlice {
      @Override
      public void onStart(Intent intent) {
          super.onStart(intent);
          super.setUIContent(ResourceTable.Layout_ability_toast_dialog);
          
          Component button7 = findComponentById(ResourceTable.Id_button7);
          button7.setClickedListener(new Component.ClickedListener() {
              @Override
              public void onClick(Component component) {
                  showTip1("hello msg~");
                  showTip2("君不见黄河之水天上来，奔流到海不复回。君不见高堂明镜悲白发，朝如青丝暮成雪。");
              }
              
          });
      }
      
      @Override
      public void onActive() {
          super.onActive();
      }
      
      @Override
      public void onForeground(Intent intent) {
          super.onForeground(intent);
      }
      
      /**
       * 基础用法
       */
      private void showTip1(String msg) {
          // 创建ToastDialog
          ToastDialog toastDialog = new ToastDialog(getContext());
          // 设置弹出的消息
          toastDialog.setText(msg);
          // 设置对齐
          toastDialog.setAlignment(LayoutAlignment.CENTER);
          // 是否可以被拖动
          toastDialog.setMovable(true);
          // 设置自动关闭（默认即为true）
          toastDialog.setAutoClosable(false);
          // 设置3000ms后消失
          toastDialog.setDuration(30000);
          // 展示
          toastDialog.show();
      }
      
      /**
       * 升级用法
       */
      private void showTip2(String msg) {
          /// -> 创建一个组件
          Text text = new Text(getContext());
          // 字体大小
          text.setTextSize(AttrHelper.fp2px(20, AttrHelper.getDensity(getContext())));
          // 多行显示
          text.setMultipleLine(true);
          // 设置内边距
          text.setPadding(
                  AttrHelper.fp2px(15, AttrHelper.getDensity(getContext())),
                  AttrHelper.fp2px(15, AttrHelper.getDensity(getContext())),
                  AttrHelper.fp2px(15, AttrHelper.getDensity(getContext())),
                  AttrHelper.fp2px(15, AttrHelper.getDensity(getContext()))
          );
          ShapeElement shapeElement = new ShapeElement();
          shapeElement.setRgbColor(new RgbColor(0, 255, 255));
          shapeElement.setCornerRadius(AttrHelper.fp2px(20, AttrHelper.getDensity(getContext())));
          // 设置背景
          text.setBackground(shapeElement);
          // 设置宽高
          text.setWidth(ComponentContainer.LayoutConfig.MATCH_CONTENT);
          text.setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT);
          // 文案
          text.setText(msg);
      
          
          /// -> 创建ToastDialog
          ToastDialog toastDialog = new ToastDialog(getContext());
          // 设置弹出的消息
          toastDialog.setComponent(text);
          // 设置对齐
          toastDialog.setAlignment(LayoutAlignment.BOTTOM);
          // 是否可以被拖动
          toastDialog.setMovable(true);
          // 设置自动关闭（默认即为true）
          toastDialog.setAutoClosable(false);
          // 设置3000ms后消失
          toastDialog.setDuration(30000);
          // 展示
          toastDialog.show();
      }
  }
  ```

## 相关资料

- [官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ui-java-component-toastdialog-0000001060125963)

- [demo代码](https://gitee.com/JustryDeng/shared-files/raw/master/HarmonyOS/JavaBaseComponent.rar)