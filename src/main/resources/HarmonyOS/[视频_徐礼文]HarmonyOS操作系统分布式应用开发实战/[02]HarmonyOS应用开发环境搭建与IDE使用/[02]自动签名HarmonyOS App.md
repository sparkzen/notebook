# 自动签名HarmonyOS App


- [自动签名HarmonyOS App](#自动签名harmonyos-app)
  - [背景说明](#背景说明)
  - [自动签名HarmonyOS App](#自动签名harmonyos-app-1)
    - [第一步：在AppGallery Connect中添加对应的App](#第一步在appgallery-connect中添加对应的app)
    - [第二步：添加SDK](#第二步添加sdk)
    - [第三步：连接启动真机](#第三步连接启动真机)
    - [第四步：在DevEco Studio中设置自动签名](#第四步在deveco-studio中设置自动签名)
    - [第五步：运行项目，测试观察一下](#第五步运行项目测试观察一下)


## 背景说明

> 提示：自动签名见[官网](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ohos-auto-configuring-signature-information-0000001271659465)

鸿蒙应用/鸿蒙服务在真机设备上运行，需要提前为鸿蒙应用/鸿蒙服务进行签名，否者在真机上跑不起来。

DevEco Studio为开发者提供了自动化签名方案（当然，如果你要进行手动签名也是可以的，官网[手动签名](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/ohos-manual-configuring-signature-information-0000001227179436)）。

## 自动签名HarmonyOS App

### 第一步：在[AppGallery Connect](https://developer.huawei.com/consumer/cn/service/josp/agc/index.html#/)中添加对应的App

> 提示：应用（或工程）app是挂在项目（project）下的，所以创建app时，先要选所选项目，如果没有项目，那么需要新建项目。

1. 选择项目
   ![1648719952713](../../../repository/1648719952713.png)

   ![1648720024116](../../../repository/1648720024116.png)
   注：其实也可以先创建应用，然后再在项目中把该应用加入项目也可。

2. 添加应用
   ![1648720070900](../../../repository/1648720070900.png)
   ![1648720533753](../../../repository/1648720533753.png)

### 第二步：添加AGC SDK（可跳过）

> 注：自动签名服务，不需要AGC SDK，可跳过。
>
> 注：部分AGC服务（即：AppGallery Connect服务）提供了集成到本地的AGC SDK，在使用此类服务前需要将AGC SDK集成到您的开发环境。如果你要使用以下服务，那么你需要添加AGC SDK：
>
> - 崩溃服务
> - 远程配置
> - App Linking
> - 认证服务
> - 云函数
> - 应用内消息
> - ......

- 第一步：添加配置文件
 ![1648721258659](../../../repository/1648721258659.png)
  执行效果如图：
  ![1648721494481](../../../repository/1648721494481.png)

- 第二步：添加SDK
  在Gradle文件中设置AppGallery Connect的Gradle插件以及AppGallery Connect SDK基础包：

  1. 设置项目级 build.gradle

     ```groovy
     allprojects {
         repositories {
                 // Add the Maven address.
                 maven {url 'https://developer.huawei.com/repo/'}
         }
     }
     ...
     buildscript{
         repositories {
             // Add the Maven address.
             maven { url 'https://developer.huawei.com/repo/' }
         }
         dependencies {
             // Add dependencies.
             classpath 'com.huawei.agconnect:agcp-harmony:1.0.0.300'
         }
     }
     ```

     执行效果如图：
     ![1648721711537](../../../repository/1648721711537.png)

  2. 置模块级 build.gradle

     ```groovy
     dependencies {
         // Add dependencies.
         implementation 'com.huawei.agconnect:agconnect-core-harmony:1.0.0.300'
     }
     ...
     // Add the information to the bottom of the file.
     apply plugin: 'com.huawei.agconnect'
     ```

     执行效果如图：
     ![1648721819497](../../../repository/1648721819497.png)

### 第三步：连接启动真机

> 提示：只有在连接真机的时候才会使用到签名；而使用DevEco Studio的自动签名的前提是，连上真机

连接上真机后的样子（示例）：

![image-20220331234622853](../../../repository/image-20220331234622853.png)

注：真机又可分为**本地真机**和**远程真机**：

- 本地真机：通过USB连接进电脑的真实鸿蒙设备（需要设备打开开发者模式）
- 远程真机：由华为提供的用于测试的远程真机，在`Tools` > `Device Manager` > `Remote Device`
  ![image-20220331221955481](../../../repository/image-20220331221955481.png)

### 第四步：在DevEco Studio中设置自动签名

>  **自动签名打开位置：**
>
> - 应用级：`File` **>** `Project Structure` > `Project` > `Signing Configs`
> - 模块级：`File` **>** `Project Structure` > `Modules` > `Signing Configs`

![image-20220331222532232](../../../repository/image-20220331222532232.png)

### 第五步：运行项目，测试观察一下

![image-20220331234703429](../../../repository/image-20220331234703429.png)

![image-20220401001620952](../../../repository/image-20220401001620952.png)

由此可见，成功了。
