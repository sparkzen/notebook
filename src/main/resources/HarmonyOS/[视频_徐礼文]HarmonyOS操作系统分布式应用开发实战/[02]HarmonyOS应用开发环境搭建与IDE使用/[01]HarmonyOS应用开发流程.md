# HarmonyOS应用开发流程

- [HarmonyOS应用开发流程](#harmonyos应用开发流程)
  - [HarmonyOS应用开发流程](#harmonyos应用开发流程-1)


## HarmonyOS应用开发流程

> **提示：**官网见[HarmonyOS应用开发流程](https://developer.harmonyos.com/cn/docs/documentation/doc-guides/tools_overview-0000001053582387)。

![1648716792316](../../../repository/1648716792316.png)

提示：开发准备、开发应用这两步的具体实施步骤，可按照**resources/HarmonyOS/[01]搭建HarmonyOS开发环境.md**来。