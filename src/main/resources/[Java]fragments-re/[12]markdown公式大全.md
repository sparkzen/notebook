markdown公式大全

- [分数](#分数)
- [上下标](#上下标)
- [相关资料](#相关资料)

## 分数

|                       算式                        | markdown公式 |
| :-----------------------------------------------: | :----------: |
| ![1652780441189](../repository/1652780441189.png) | \frac{a}{b}  |

>  更多公式见[here](https://blog.csdn.net/konglongdanfo1/article/details/85204312)

## 上下标

| 功能 | markdown公式 |
| :--: | :----------: |
| 上标 |      ^       |
|      |      _       |

注：默认情况下，上下标符号仅仅对下一个字符作用。一组字符使用`{}`包裹起来的内容，示例：
$$
X_{y} * Y^{2x} = 1
$$


## 相关资料

- [markdown公式符号大全](https://blog.csdn.net/konglongdanfo1/article/details/85204312)