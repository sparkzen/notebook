# swagger对特定请求统一追加指定请求头

- [swagger对特定请求统一追加指定请求头](#swagger对特定请求统一追加指定请求头)
  - [简述](#简述)
  - [示例说明](#示例说明)

## 简述

- **第一步：**通过`springfox.documentation.spring.web.plugins.Docket#securitySchemes`设置"变量"

  注：这步设置的变量，只是一个变量名，以及对应的heaer的key，header的value是用户在swagger界面输入的

- **第二步：**通过`springfox.documentation.spring.web.plugins.Docket#securityContexts`引用"变量"

  注：引入变量，就相当于引用了第一步对应设置的heaer的key以及用户输入的header的value，若当前请求url匹配securityContext的设置，那么对应的key-value将会自动追加请这次请求的请求头中

## 示例说明

> 注意观察：`.securitySchemes(xxx)`与`.securityContexts(xxx)`的设置

> 注：本人用得为swagger版本为：
>
> ```java
> <dependency>
>     <groupId>io.springfox</groupId>
>     <artifactId>springfox-boot-starter</artifactId>
>     <version>3.0.0</version>
> </dependency>
> ```

```java
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZonedDateTime;

@Configuration
@EnableSwagger2
public class Swagger2Config {
    
    @Bean
    public Docket configSpringfoxDocketForAll() {
        return new Docket(DocumentationType.SWAGGER_2)
        		.directModelSubstitute(LocalDateTime.class, String.class) 
                .directModelSubstitute(LocalDate.class, String.class)
                .directModelSubstitute(LocalTime.class, String.class)
                .directModelSubstitute(ZonedDateTime.class, String.class)
                // 设置一些通用变量， 如这里的Authorization
                .securitySchemes(Lists.newArrayList(new ApiKey("myKey1", "Authorization", "header")))
                // 配置在哪些请求时，额外引用哪些securitySchemes变量
        		.securityContexts(Lists.newArrayList(securityContext()))
                .produces(Sets.newHashSet("application/json","application/x-www-form-urlencoded","*/*"))
                .consumes(Sets.newHashSet("application/json","application/x-www-form-urlencoded","*/*"))
                .protocols(Sets.newHashSet("http"))
                .forCodeGeneration(true)
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.ideaaedi.smart.pension.basic.controller"))
                .build()
                .apiInfo(apiInfo());
    }
    
    
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Safe RESTful APIs")
                .description("服务端后台接口说明文档")
                .version("1.0")
                .build();
    }
    
    private SecurityContext securityContext() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "description");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return SecurityContext.builder()
                // 设置引用securitySchemes中name为myKey1的配置
                .securityReferences(
                    Lists.newArrayList(new SecurityReference("myKey1", authorizationScopes))
                )
                .build();
    }

}
```



