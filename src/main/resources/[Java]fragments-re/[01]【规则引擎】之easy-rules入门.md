# 【规则引擎】之easy-rules入门

- [【规则引擎】之easy-rules入门](#规则引擎之easy-rules入门)
  - [<font  face="幼圆" color = "#86CA5E" >背景</font>](#font--face幼圆-color--86ca5e-背景font)
  - [<font  face="幼圆" color = "#86CA5E" >easy rules的依赖</font>](#font--face幼圆-color--86ca5e-easy-rules的依赖font)
  - [<font  face="幼圆" color = "#86CA5E" >easy rules的使用步骤</font>](#font--face幼圆-color--86ca5e-easy-rules的使用步骤font)
  - [<font  face="幼圆" color = "#86CA5E" >easy rules中的规则</font>](#font--face幼圆-color--86ca5e-easy-rules中的规则font)
  - [<font  face="幼圆" color = "#86CA5E" >easy rules中的规则引擎</font>](#font--face幼圆-color--86ca5e-easy-rules中的规则引擎font)
  - [<font  face="幼圆" color = "#86CA5E" >监听器</font>](#font--face幼圆-color--86ca5e-监听器font)
  - [<font  face="幼圆" color = "#86CA5E" >相关资料</font>](#font--face幼圆-color--86ca5e-相关资料font)

---

## <font  face="幼圆" color = "#86CA5E" >背景</font>

&emsp;&emsp;在写业务逻辑时，我们不可避免的会使用到if这样的语法；对于特别复杂的业务场景，那么就可能会写很多"并联"或"串联"的if，冗余又不好管理；此时，我们不妨引入规则引擎。在我看来，规则引擎中的"规则即对应if"，规则引擎中的"引擎则是统筹管理这些if"的大脑。

&emsp;&emsp;当下规则引擎有很多，如DROOLS、OPENL TABLETS、EASY RULES、RULEBOOK等等，其中DROOLS的功能强大且完善，社区也非常活跃，但是入门门槛相对较高、框架较重，如果项目中if的业务场景非常多，那么不妨采用DROOLS。但是实际上，很多项目只是对规则引擎有一些基本的需求罢了，此时，可以考虑采用EASY RULES，门槛低、使用友好、社区也是相对活跃的。

## <font  face="幼圆" color = "#86CA5E" >easy rules的依赖</font>

<font face="幼圆" >**提示**</font>**：** 我们一般只需要前两个就行了。

```xml
<!-- https://mvnrepository.com/artifact/org.jeasy/easy-rules-core -->
<dependency>
    <groupId>org.jeasy</groupId>
    <artifactId>easy-rules-core</artifactId>
    <version>4.1.0</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.jeasy/easy-rules-support -->
<dependency>
    <groupId>org.jeasy</groupId>
    <artifactId>easy-rules-support</artifactId>
    <version>4.1.0</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.jeasy/easy-rules-mvel -->
<dependency>
    <groupId>org.jeasy</groupId>
    <artifactId>easy-rules-mvel</artifactId>
    <version>4.1.0</version>
</dependency>

<!-- https://mvnrepository.com/artifact/org.jeasy/easy-rules-spel -->
<dependency>
    <groupId>org.jeasy</groupId>
    <artifactId>easy-rules-spel</artifactId>
    <version>4.1.0</version>
</dependency>
```

## <font  face="幼圆" color = "#86CA5E" >easy rules的使用步骤</font>

![1](../repository/1-16278299687812.png)

注：规则引擎执行规则时，是单线程执行的。

## <font  face="幼圆" color = "#86CA5E" >easy rules中的规则</font>

- <font  face="幼圆" color = "#FFBB66">**规则的分类**</font>
  
  easy rules中，规则可分为简单规则和复合规则。其中复合规则由多个简单规则组合而成。
  
  ![在这里插入图片描述](../repository/20210621233920896.png)
  
- <font  face="幼圆" color = "#FFBB66">**规则相关参数简介**</font>
  - Rule：规则。
    - Rule.name：规则名称。
    - Rule.description：规则描述。
    - Rule.priority：规则执行优先级。
  - Facts：因子。一种键值对存储结构，用于存储、传递相关数据。
    - Fact：代表Facts中的某一个键值对。
  - Conditon：判断条件。用于判断是否满足触发action的条件。
  - Action：当条件成立时，执行action中的逻辑。

- <font  face="幼圆" color = "#FFBB66">**复合规则(组合规则)简介**</font>
  - UnitRuleGroup： 当且仅当group中所有rule的condition都成立时，才执行group中所有rule的action（，否者一个action都不会执行）。
  - ActivationRuleGroup：执行第一个condition成立的rule的action（，剩下的rule将被忽略）。
  - ConditionalRuleGroup：当前group中，是否正常往下走流程，取决于优先级最高的rule的condition是否成立，若成立，则继续往下走此group的流程；若不成立，忽略剩下的rule。
  
    注：group中rule的优先级，取决于rule的priority；若两个rule之间的priority相同，则优先级取决于这两个rule的自然排序结果。

- <font  face="幼圆" color = "#FFBB66">**规则的编写**</font>
  
  - <font  face="幼圆">**方式一（推荐）：通过注解编写规则。** </font>
  
  ![在这里插入图片描述](../repository/20210621235045133.png)
  
    - <font  face="幼圆">**方式二（推荐）：通过Builder模式编写规则。** </font>
  
  ![在这里插入图片描述](../repository/20210621235054324.png)
  
  - <font  face="幼圆">**方式三：通过MVEL（MVFLEX Expression Language)编写规则。**</font> 
  
  ![在这里插入图片描述](../repository/20210621235105563.png)
  
  - <font  face="幼圆">**方式四：通过SpEL（Spring Expression Language)编写规则。** </font>
  
  ![在这里插入图片描述](../repository/20210621235112858.png)

## <font  face="幼圆" color = "#86CA5E" >easy rules中的规则引擎</font>

&emsp;&emsp;规则引擎主要用于将(各个相对独立的)规则进行统筹（如：定义规则与规则之间的关系等）。
- <font  face="幼圆" color = "#FFBB66">**规则引擎的分类**</font>

  ![在这里插入图片描述](../repository/20210621235552869.png)

  

  

- <font  face="幼圆" color = "#FFBB66">**规则引擎的使用**</font>

  ![在这里插入图片描述](../repository/20210621235602630.png)


- <font  face="幼圆" color = "#FFBB66">**规则引擎参数RulesEngineParameters**</font>
	
	&emsp;&emsp;在规则引擎中的各个rule是相对独立的，即便执行时某个rule出错了，也不会影响其它rule的执行。但是在很多时候，我们反而需要的是规则之间互相影响。规则引擎就是通过RulesEngineParameters来控制rule之间的关系的。
	
	|            参数             |                             描述                             |      默认值       |
	| :-------------------------: | :----------------------------------------------------------: | :---------------: |
	|   skipOnFirstAppliedRule    | 当出现第一个执行成功了的(rule的)action时，跳过(不执行)剩下rule的action |       false       |
	|    skipOnFirstFailedRule    | 当出现第一个执行报错了的(rule的)action时，跳过(不执行)剩下rule的action |       false       |
	| skipOnFirstNonTriggeredRule | 当最高优先级的rule的condition结果为false时，跳过(不执行)所有的rule |       false       |
	|      priorityThreshold      | 规则引擎只执行优先级(rule#priority)不低于此参数值的rule<br/>注：priority越小，优先级越高。 | Integer.MAX_VALUE |

## <font  face="幼圆" color = "#86CA5E" >监听器</font>

- <font  face="幼圆" color = "#FFBB66">**规则监听器**</font>
    
    <font  face="幼圆">**提示**</font>：各方法的执行时机，见下文。
    
    ![在这里插入图片描述](../repository/20210622000054695.png)


- <font  face="幼圆" color = "#FFBB66">**规则引擎监听器**</font>
    
    <font  face="幼圆">**提示**</font>：各方法的执行时机，见下文。
    
    ![在这里插入图片描述](../repository/20210622000048249.png)


- <font  face="幼圆" color = "#FFBB66">**监听器的执行时机**</font>

  ![在这里插入图片描述](../repository/20210622000110335.png)

---

## <font  face="幼圆" color = "#86CA5E" >相关资料</font>

- [官网](https://github.com/j-easy/easy-rules)
