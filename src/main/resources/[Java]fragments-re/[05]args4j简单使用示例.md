# args4j简单使用示例

- [args4j简单使用示例](#args4j简单使用示例)
  - [<font  face="幼圆" color = "#3399EA">第一步：引入依赖</font>](#font--face幼圆-color--3399ea第一步引入依赖font)
  - [<font  face="幼圆" color = "#3399EA">第二步：封装参数对象</font>](#font--face幼圆-color--3399ea第二步封装参数对象font)
  - [<font  face="幼圆" color = "#3399EA">第三步：在main方法中进行解析</font>](#font--face幼圆-color--3399ea第三步在main方法中进行解析font)
  - [<font  face="幼圆" color = "#3399EA">测试验证</font>](#font--face幼圆-color--3399ea测试验证font)
  - [<font  face="幼圆" color = "#3399EA">相关资料</font>](#font--face幼圆-color--3399ea相关资料font)

---

## <font  face="幼圆" color = "#3399EA">第一步：引入依赖</font>

```xml
<dependency>
    <groupId>args4j</groupId>
    <artifactId>args4j</artifactId>
    <version>2.33</version>
</dependency>
```

## <font  face="幼圆" color = "#3399EA">第二步：封装参数对象</font>

```java
import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.BooleanOptionHandler;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * (non-javadoc)
 *
 * @author JustryDeng
 * @since 2021/10/14 0:41:06
 */
@SuppressWarnings("all")
public class ArgsInfo {
    
    /**
     * 此字段主要用于容纳可能被多解析出来的无效的指令
     */
    @Argument
    private List<String> arguments = new ArrayList<>();
    
    /** *************************** 通过{@link Option}注解，将启动指令中的参数与此模型的字段关联起来 *************************** */
    
    @Option(name = "-name", required = true, usage = "我是-name的说明")
    private String name;
    
    @Option(name = "-age", usage = "我是-age的说明")
    private int age = -1;
    
    @Option(name = "-student", usage = "我是-student的说明", handler = BooleanOptionHandler.class)
    private boolean student = false;
    
    @Option(name = "-hobby", aliases = {"-love", "-like"}, usage = "我是-hobby的说明")
    private String hobby = "defaultHobby";
    
    @Option(name = "-gender", usage = "我是-gender的说明")
    private GenderEnum gender = GenderEnum.WOMAN;
    
    @Option(name = "-avatar", usage = "我是-avatar的说明")
    private File avatar = new File("D:/defaultDir/");
    
    public enum GenderEnum {
        MAN, WOMAN
    }
    
    @Override
    public String toString() {
        return "ArgsInfo{" +
                "arguments=" + arguments +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", student=" + student +
                ", hobby='" + hobby + '\'' +
                ", gender=" + gender +
                ", avatar=" + avatar +
                '}';
    }
}
```

## <font  face="幼圆" color = "#3399EA">第三步：在main方法中进行解析</font>

```java
import com.example.args4jdemo.model.ArgsInfo;
import com.ideaaedi.commonds.exception.ExceptionUtil;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SuppressWarnings("all")
@SpringBootApplication
public class Args4jDemoApplication {
    
    public static void main(String[] args) {
        // 创建一个对象，这个对象中被@Option注解标注了的字段即为想要解析到的字段
        ArgsInfo argsInfo = new ArgsInfo();
        // 创建解析器
        CmdLineParser cmdLineParser = new CmdLineParser(argsInfo);
        try {
            // 进行解析
            cmdLineParser.parseArgument(args);
            // 打印解析结果
            System.out.println(argsInfo);
        } catch (CmdLineException e) {
            ///e.printStackTrace();
            System.out.println(ExceptionUtil.getStackTraceMessage(e));
            // 打印使用说明
            new CmdLineParser(new ArgsInfo()).printUsage(System.out);
        }
    }
}
```

## <font  face="幼圆" color = "#3399EA">测试验证</font>

- 正常测试

  执行`java -jar args4j-demo-0.0.1-SNAPSHOT.jar -name 邓沙利文 -age 18 -love eating -student false -gender MAN -avatar D:/avatar.jpg`，可看到控制台输出：

  ```bash
  ArgsInfo{arguments=[false], name='邓沙利文', age=18, student=true, hobby='eating', gender=MAN, avatar=D:\avatar.jpg}
  ```

- 异常测试

  执行`java -jar args4j-demo-0.0.1-SNAPSHOT.jar`，可看到控制台输出：

  ```bash
  org.kohsuke.args4j.CmdLineException: Option "-name" is required
  	at org.kohsuke.args4j.CmdLineParser.checkRequiredOptionsAndArguments(CmdLineParser.java:588)
  	at org.kohsuke.args4j.CmdLineParser.parseArgument(CmdLineParser.java:534)
  	at com.example.args4jdemo.Args4jDemoApplication.main(Args4jDemoApplication.java:20)
  	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
  	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
  	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
  	at java.lang.reflect.Method.invoke(Method.java:498)
  	at org.springframework.boot.loader.MainMethodRunner.run(MainMethodRunner.java:49)
  	at org.springframework.boot.loader.Launcher.launch(Launcher.java:108)
  	at org.springframework.boot.loader.Launcher.launch(Launcher.java:58)
  	at org.springframework.boot.loader.JarLauncher.main(JarLauncher.java:88)
  
   -age N                    : 我是-age的说明 (default: -1)
   -avatar FILE              : 我是-avatar的说明 (default: D:\defaultDir)
   -gender [MAN | WOMAN]     : 我是-gender的说明 (default: WOMAN)
   -hobby (-love, -like) VAL : 我是-hobby的说明 (default: defaultHobby)
   -name VAL                 : 我是-name的说明
   -student                  : 我是-student的说明 (default: false)
  ```

---

## <font  face="幼圆" color = "#3399EA">相关资料</font>

- [`官网`](https://github.com/kohsuke/args4j)