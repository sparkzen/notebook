# thumbnailator图片处理

---
- [thumbnailator图片处理](#thumbnailator图片处理)

  - [<font color = "#86CA5E" face="幼圆">准备工作 - 引入thumbnailator依赖</font>](#font-color--86ca5e-face幼圆准备工作---引入thumbnailator依赖font)
  - [<font  face="幼圆" color = "#86CA5E">thumbnailator图片处理(简单演示常用操作)</font>](#font--face幼圆-color--86ca5ethumbnailator图片处理简单演示常用操作font)

  - [<font  face="幼圆" color = "#86CA5E" >相关资料</font>](#font--face幼圆-color--86ca5e-相关资料font)
---

### <font color = "#86CA5E" face="幼圆">准备工作 - 引入thumbnailator依赖</font>

```xml
<!-- https://mvnrepository.com/artifact/net.coobird/thumbnailator -->
<dependency>
    <groupId>net.coobird</groupId>
    <artifactId>thumbnailator</artifactId>
    <version>0.4.13</version>
</dependency>
```

---
### <font  face="幼圆" color = "#86CA5E">thumbnailator图片处理(简单演示常用操作)</font>
- <font  face="幼圆" color = "#FFBB66">**按size等比缩放**</font>
  
  - 代码
  
    ![在这里插入图片描述](../repository/20210226231842458.png)
  
  - 生成的图片
  
    ![在这里插入图片描述](../repository/20210226231858118.png)
  
    
  
- <font  face="幼圆" color = "#FFBB66">**按scale等比缩放**</font>
  - 代码
  
    ![在这里插入图片描述](../repository/20210226231908848.png)
  
  - 生成的图片
  
    ![在这里插入图片描述](../repository/2021022623192230.png)
  
- <font  face="幼圆" color = "#FFBB66">**按size强制缩放到指定的宽和高**</font>
  - 代码
    
    ![在这里插入图片描述](../repository/20210226231933328.png)
    
  - 生成的图片
  
    ![在这里插入图片描述](../repository/20210226231944260.png)
  
- <font  face="幼圆" color = "#FFBB66">**水印**</font>
  
  - 代码
  ![在这里插入图片描述](../repository/20210226231953440.png)
  - 生成的图片
  ![在这里插入图片描述](../repository/2021022623200565.png)
  
- <font  face="幼圆" color = "#FFBB66">**旋转**</font>
  
  - 代码
  
    ![在这里插入图片描述](../repository/20210226232017897.png)
  
  - 生成的图片
  
    ![在这里插入图片描述](../repository/20210226232029999.png)
  
- <font  face="幼圆" color = "#FFBB66">**自动命名文件**</font>
  
  - 代码
  
    ![在这里插入图片描述](../repository/20210226232042672.png)
  
  - 生成的图片
  
    ![在这里插入图片描述](../repository/20210226232053161.png)
  
    
  
- <font  face="幼圆" color = "#FFBB66">**获得OutputStream**</font>

  ![在这里插入图片描述](../repository/20210226232102917.png)

- <font  face="幼圆" color = "#FFBB66">**获得BufferedImage**</font>

  ![在这里插入图片描述](../repository/20210226232116595.png)

---

## <font  face="幼圆" color = "#86CA5E" >相关资料</font>

- [官网](https://github.com/coobird/thumbnailator)

