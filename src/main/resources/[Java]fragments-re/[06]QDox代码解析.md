# QDox代码解析

- [QDox代码解析](#qdox代码解析)
  - [<font  face="幼圆" color = "#3399EA">简介</font>](#font--face幼圆-color--3399ea简介font)
  - [<font  face="幼圆" color = "#3399EA">maven坐标</font>](#font--face幼圆-color--3399eamaven坐标font)
  - [<font  face="幼圆" color = "#3399EA">解析被类加载器加载的class</font>](#font--face幼圆-color--3399ea解析被类加载器加载的classfont)
  - [<font  face="幼圆" color = "#3399EA">解析源码</font>](#font--face幼圆-color--3399ea解析源码font)
  - [<font  face="幼圆" color = "#3399EA">相关资料</font>](#font--face幼圆-color--3399ea相关资料font)

---

## <font  face="幼圆" color = "#3399EA">简介</font>

`QDox`是一种高速、小巧的解析器。主要用于**解析java源码**，也能用于**解析被类加载器加载了的class**。

## <font  face="幼圆" color = "#3399EA">maven坐标</font>

```xml
<dependency>
    <groupId>com.thoughtworks.qdox</groupId>
    <artifactId>qdox</artifactId>
    <version>2.0.0</version>
</dependency>
```

## <font  face="幼圆" color = "#3399EA">解析被类加载器加载的class</font>

`QDox`默认解析当前类加载器加载了的class。如果你有外部class或者jar想要被纳入`QDox`解析，你可以：

- 方式一：用当前类加载器将外部class或者jar加载进来
- 方式二：创建一个类加载器，然后用这个类加载器加载外部class或者jar，然后将这个类加载器添加至`QDox`
- 方式三：......

示例：

```java
// 使用自定义的类加载器加载外部jar或class（注：示例中这个自定义的类加载器不是QDox自带的，本人使用的是其它工具包下的类）
LoadJarClassHelper loadJarClassHelper = new LoadJarClassHelper();
loadJarClassHelper.loadNormalJar(
        Lists.newArrayList(new File("E:\\Maven\\Repository\\com\\idea-aedi\\component-dump-class\\2.6"
                + ".0\\component-dump-class-2.6.0.jar")), false, null, null);
URLClassLoader classLoader = loadJarClassHelper.getClassLoader();

// QDox构建器
JavaProjectBuilder javaProjectBuilder = new JavaProjectBuilder();
javaProjectBuilder.setEncoding(StandardCharsets.UTF_8.name());
// 将自定义的类加载器添加进QDox
javaProjectBuilder.addClassLoader(classLoader);

// 获取加载进来的外部jar或class总额类的信息
JavaClass javaClass = javaProjectBuilder.getClassByName("com.ideaaedi.component.dump.NonExitClassFileTransformerExecutor");
System.err.println(javaClass.getFields());
```

## <font  face="幼圆" color = "#3399EA">解析源码</font>

`QDox`是一种高速、小巧的解析器。主要用于解析Java源码，也能用于解析被类加载器加载了的class。

示例：

```java
// QDox构建器
JavaProjectBuilder javaProjectBuilder = new JavaProjectBuilder();
javaProjectBuilder.setEncoding(StandardCharsets.UTF_8.name());


// 通过addSource等相关方法， 将源码添加进QDox中 (注：可以多次添加)
javaProjectBuilder.addSourceTree(new File("E:\\Git\\Repository\\components\\component-decompile\\src\\main\\java\\"));


// 获取解析出来的JavaClass对象
javaProjectBuilder.getClasses().forEach(javaClass -> {
    /*
     * 获取信息
     */
    System.err.println("包名\t" + javaClass.getPackageName());
    System.err.println("全类名\t" + javaClass.getFullyQualifiedName());
    System.err.println("注释\t" + javaClass.getComment());
    System.err.println("字段\t" + javaClass.getFields());
    System.err.println("方法\t" + javaClass.getMethods());
    System.err.println("源码\t" + javaClass.getSource());
    System.err.println("注释上的标签\t" + javaClass.getTags()); // 如注释上的@author JustryDeng，author即为标签名，JustryDeng即为标签值
    // .....
    
    /*
     * 判断信息
     */
    System.err.println("是否是抽象类\t" + javaClass.isAbstract());
    System.err.println("是否是注解\t" + javaClass.isAnnotation());
    System.err.println("是否是枚举\t" + javaClass.isEnum());
    // .....
}
```

---

## <font  face="幼圆" color = "#3399EA">相关资料</font>

- [`官网`](https://github.com/paul-hammant/qdox)