# idea GUI Form

- [idea GUI Form](#idea-gui-form)
  - [简述](#简述)
  - [准备工作](#准备工作)
  - [使用示例](#使用示例)
    - [第一步：创建类及对应的form文件](#第一步创建类及对应的form文件)
    - [第二步：给From中的组件JPanel起一个字段名（，否者下面在生成main方法时会报错）](#第二步给from中的组件jpanel起一个字段名否者下面在生成main方法时会报错)
    - [第三步：拖动组件，在画板中完成UI](#第三步拖动组件在画板中完成ui)
    - [第四步：给组件添加监听](#第四步给组件添加监听)
    - [第五步：生成main方法](#第五步生成main方法)
    - [第六步：运行main方法，（idea自动）生成GUI对应源码](#第六步运行main方法idea自动生成gui对应源码)
    - [第七步：将项目打成可执行jar包，以便使用](#第七步将项目打成可执行jar包以便使用)
  - [相关资料](#相关资料)

---

## 简述

`GUI Form`是IntelliJ IDEA提供的快速创建GUI界面的功能，通过拖拽组装组件、自动生成代码的方式完成GUI界面的绘制。

## 准备工作

- 设置GUI代码生成的位置为source code，打开`File | Settings | Editor | GUI Designer`，并设置

  ![image-20211211083531449](../repository/image-20211211083531449.png)

- 引入（自动生成的GUI源码需要的）依赖

  ```xml
  <dependency>
      <groupId>com.intellij</groupId>
      <artifactId>forms_rt</artifactId>
      <version>7.0.3</version>
  </dependency>
  ```

## 使用示例

### 第一步：创建类及对应的form文件

![image-20211211084140309](../repository/image-20211211084140309.png)

![image-20211211084305534](../repository/image-20211211084305534.png)

![image-20211211085419685](../repository/image-20211211085419685.png)

### 第二步：给From中的组件JPanel起一个字段名（，否者下面在生成main方法时会报错）

![image-20211211084922065](../repository/image-20211211084922065.png)



### 第三步：拖动组件，在画板中完成UI

![image-20211211085855741](../repository/image-20211211085855741.png)

![image-20211211090107606](../repository/image-20211211090107606.png)



### 第四步：给组件添加监听

![image-20211211090301236](../repository/image-20211211090301236.png)

![image-20211211090506726](../repository/image-20211211090506726.png)

![image-20211211090741697](../repository/image-20211211090741697.png)

示例：

```java
public class DemoGUI {
    private JPanel jPanel;
    private JTextField jTextField;
    private JLabel jLable;
    private JButton button;
    
    public DemoGUI() {
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String inputText = jTextField.getText();
                
                /// 弹出框
                JDialog jDialog = new JDialog();
                // 设置相对位置. null代表位于屏幕居中
                jDialog.setLocationRelativeTo(null);
                // 设置标题
                jDialog.setTitle("Information");
                // 设置可见性
                jDialog.setVisible(true);
                // 设置大小
                jDialog.setSize(200, 80);
                // 设置弹出框图标
                try {
                    InputStream inputStream = Objects.requireNonNull(DemoGUI.class.getResourceAsStream("/information.png"));
                    jDialog.setIconImage(ImageIO.read(inputStream));
                } catch (Exception exc) {
                    throw new UndeclaredThrowableException(exc);
                }
    
                // 给弹出框面板添加组件
                Container contentPane = jDialog.getContentPane();
                contentPane.add(new JLabel("hello~ " + inputText));
            }
        });
    }
```

### 第五步：生成main方法

![image-20211211092716958](../repository/image-20211211092716958.png)

![image-20211213205231100](../repository/image-20211213205231100.png)

### 第六步：运行main方法，（idea自动）生成GUI对应源码

**生成源码：**

![image-20211211093436670](../repository/image-20211211093436670.png)

> 提示：如果你想修改生成的GUI代码（即：你想避免每次运行main方法时都生成GUI代码），你只需要使java类没有对应的form文件即可：
>
> - 你可以在首次生成GUI源码后，删除掉对应的form文件，然后再修改GUI源码即可。
> - 你也可以在首次生成GUI源码后，直接复制一个新的java类出来（只复制java类不复制对应的form文件），然后再修改GUI源码即可。



**观察效果：**

![image-20211211094419095](../repository/image-20211211094419095.png)

点击【确定】，弹出新的对话框：

![image-20211211094453828](../repository/image-20211211094453828.png)



### 第七步：将项目打成可执行jar包，以便使用

- 在pom中添加打包插件maven-shade-plugin

  ```xml
  <?xml version="1.0" encoding="UTF-8"?>
  <project xmlns="http://maven.apache.org/POM/4.0.0"
           xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
           xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
      <modelVersion>4.0.0</modelVersion>
  
      <groupId>org.example</groupId>
      <artifactId>gui-form</artifactId>
      <version>1.0.0</version>
  
      <properties>
          <maven.compiler.source>8</maven.compiler.source>
          <maven.compiler.target>8</maven.compiler.target>
          <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
      </properties>
  
      <dependencies>
          <!-- https://mvnrepository.com/artifact/com.intellij/forms_rt -->
          <dependency>
              <groupId>com.intellij</groupId>
              <artifactId>forms_rt</artifactId>
              <version>7.0.3</version>
          </dependency>
      </dependencies>
  
  
      <build>
          <resources>
              <resource>
                  <directory>src/main/resources</directory>
                  <includes>
                      <!--包含文件夹以及子文件夹下所有资源-->
                      <include>**/*.*</include>
                  </includes>
              </resource>
          </resources>
  
          <plugins>
              <plugin>
                  <groupId>org.apache.maven.plugins</groupId>
                  <artifactId>maven-shade-plugin</artifactId>
                  <version>3.2.4</version>
                  <configuration>
                      <transformers>
                          <transformer implementation="org.apache.maven.plugins.shade.resource.ManifestResourceTransformer">
                              <!-- main类 -->
                              <mainClass>com.example.guiform.DemoGUI</mainClass>
                          </transformer>
                      </transformers>
                  </configuration>
                  <executions>
                      <execution>
                          <phase>package</phase>
                          <goals>
                              <goal>shade</goal>
                          </goals>
                      </execution>
                  </executions>
              </plugin>
          </plugins>
      </build>
  
  </project>
  ```

- 打成jar包并运行

![image-20211211095657620](../repository/image-20211211095657620.png)

![image-20211211095714452](../repository/image-20211211095714452.png)

---

## 相关资料

- [**`demo代码下载`**](https://gitee.com/JustryDeng/shared-files/raw/master/gui-form.rar)
