# SPI

- [SPI](#spi)
  - [简介](#简介)
  - [使用方式](#使用方式)
  - [相关资料](#相关资料)

---

## 简介

SPI ，全称为Service Provider Interface，是一种服务发现机制。它通过读取ClassPath路径下指定的文件，并根据文件内容自动加载文件里所定义的类。JDK、SpringBoot、Dubbo都对此机制有自己的扩展实现。

## 使用方式

> 提示：本文以JDK的SPI机制为例进行说明。

第一步：定义需要的接口。

![image-20211124204732427](../repository/image-20211124204732427.png)

第二步：在`classpath`路径下的`META-INF/services`目录下，创建该接口全类名文件，内容写该接口的实现，多个实现换行即可。

**注：** 在使用时，谁依赖当前项目，谁来进行步骤二。

![image-20211124205022489](../repository/image-20211124205022489.png)

第三步：在代码中使用SPI。

- 方式一：使用`sun.misc.Service#providers`获取

  ```java
  // 获取SpiService的实现
  Iterator<SpiService> providers = sun.misc.Service.providers(SpiService.class);
  while (providers.hasNext()) {
      SpiService ser = providers.next();
      // 调用方法
      ser.execute();
  }
  ```

- 方式二：使用`java.util.ServiceLoader#load`获取

  ```java
  // 获取SpiService的实现
  ServiceLoader<SpiService> load = java.util.ServiceLoader.load(SpiService.class);
  Iterator<SpiService> iterator = load.iterator();
  while (iterator.hasNext()) {
      SpiService ser = iterator.next();
      // 调用方法
      ser.execute();
  }
  ```

---

## 相关资料

- [`https://www.jianshu.com/p/3a3edbcd8f24`](https://www.jianshu.com/p/3a3edbcd8f24)

