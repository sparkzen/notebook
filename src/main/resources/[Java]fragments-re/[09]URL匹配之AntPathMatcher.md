# URL匹配之AntPathMatcher

- [URL匹配之AntPathMatcher](#url匹配之antpathmatcher)
  - [背景说明](#背景说明)
  - [Ant基础通配符简介](#ant基础通配符简介)
  - [AntPathMatcher常用方法介绍及示例](#antpathmatcher常用方法介绍及示例)
    - [常用的构造方法](#常用的构造方法)
    - [extractUriTemplateVariables(String pattern, String path)：根据pattern的规则，从path中抽取对应的变量值](#extracturitemplatevariablesstring-pattern-string-path根据pattern的规则从path中抽取对应的变量值)
    - [isPattern(String str)：判断str是否可以作为一个pattern匹配器](#ispatternstring-str判断str是否可以作为一个pattern匹配器)
    - [match(String pattern, String path)：path是否完全匹配pattern](#matchstring-pattern-string-pathpath是否完全匹配pattern)
    - [matchStart(String pattern, String path)：pattern的前面一部分是否就能匹配上整个path](#matchstartstring-pattern-string-pathpattern的前面一部分是否就能匹配上整个path)

## 背景说明

`Ant`匹配模式是当下（2022-03-18）最火的对`URL`进行匹配的模式。很多框架都采用`Ant`对`URL`进行匹配，如Spring框架中的`org.springframework.util.AntPathMatcher`。

![image-20220318171118650](../repository/image-20220318171118650.png)

## Ant基础通配符简介

| 通配符 |       介绍        | 示例说明                                                     |
| :----: | :---------------: | :----------------------------------------------------------- |
|   ?    |  **匹配1个字符**  | 模板：`/jd/a?c`<br />匹配示例：`/jd/abc`<br />不匹配示例1：`/jd/ac`<br />不匹配示例2：`/jd/axyzc` |
|   *    | **匹配>=0个字符** | 模板：`/jd/a*c`<br />匹配示例1：`/jd/ac`<br />匹配示例2：`/jd/abc`<br />匹配示例3：`/jd/axyzc`<br />不匹配示例1：`/jd/a`<br />不匹配示例2：`/jd/xc`<br />不匹配示例3：`/jk/ac` |
|   **   | **匹配>=0个目录** | 示例1：`/**/abc.html`匹配所有以`abc.html`结尾的路径<br />示例2：`/abc/**`匹配所有以`/abc/`打头的路径<br />示例3：`abc**`匹配所有以`abc`打头的路径<br />示例4：`/xyz/**/qwer`匹配所有以`/xyz/`打头并以`/qwer`结尾的路径 |
| ...... |      ......       | ......                                                       |



## AntPathMatcher常用方法介绍及示例

> 提示：以下内容基于spring 5.3.12

### 常用的构造方法

- 示例

  ```java
  // 无参构造，默认以 "/" 作为路径分隔符
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  // 也可以指定路径分隔符
  antPathMatcher = new AntPathMatcher("-");
  ```

### extractUriTemplateVariables(String pattern, String path)：根据pattern的规则，从path中抽取对应的变量值

> 当pattern与path不匹配时，会报错

- 示例一

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  Map<String, String> map = antPathMatcher.extractUriTemplateVariables("/root/{a}/{b}/{c}/{d}.html", "/root/aa/bb/cc/xyz.html");
  map.forEach((k, v) -> System.out.println(k + "=" + v));
  ```

  输出

  ```
  a=aa
  b=bb
  c=cc
  d=xyz
  ```

- 示例二

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  Map<String, String> map = antPathMatcher.extractUriTemplateVariables("/root/{a}/{b}/{c}", "/root/aa/bb/cc/xyz.html");
  map.forEach((k, v) -> System.out.println(k + "=" + v));
  ```

  输出

  ```
  Exception in thread "main" java.lang.IllegalStateException: Pattern "/root/{a}/{b}/{c}" is not a match for "/root/aa/bb/cc/xyz.html"
  	at org.springframework.util.AntPathMatcher.extractUriTemplateVariables(AntPathMatcher.java:517)
  	at com.example.springbootdemo.MainApplication.main(MainApplication.java:17)
  ```

### isPattern(String str)：判断str是否可以作为一个pattern匹配器

> 注：当str是一个不带任何通配符（如：`?`、`*`、`**`、`{}`等）的字符串，即：str是一个准确的字符串时。此方法返回false，该方法认为：既然pattern是一个不带通配符的明确字符串，那么你直接和要匹配的path进行相等比较即可

- 示例一

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  boolean isPattern = antPathMatcher.isPattern("/aa/bb/cc/xyz.html");
  System.out.println(isPattern);
  ```

  输出

  ```
  false
  ```

- 示例二

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  boolean isPattern = antPathMatcher.isPattern("/aa/*/cc/xyz.html");
  System.out.println(isPattern);
  ```

  输出

  ```
  true
  ```

- 示例三

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  boolean isPattern = antPathMatcher.isPattern("/aa/**/xyz.html");
  System.out.println(isPattern);
  ```

  输出

  ```
  true
  ```

- 示例四

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  boolean isPattern = antPathMatcher.isPattern("/aa/?/xyz.html");
  System.out.println(isPattern);
  ```

  输出

  ```
  true
  ```

- 示例五

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  boolean isPattern = antPathMatcher.isPattern("/aa/{qwer}/xyz.html");
  System.out.println(isPattern);
  ```

  输出

  ```
  true
  ```

### match(String pattern, String path)：path是否完全匹配pattern

- 示例一（示例统配符`?`）

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  // 输出：true
  System.out.println(antPathMatcher.match("/jd/a?c", "/jd/abc"));
  // 输出：false
  System.out.println(antPathMatcher.match("/jd/a?c", "/jd/ac"));
  // 输出：false
  System.out.println(antPathMatcher.match("/jd/a?c", "/jd/axyzc"));
  ```

- 示例二（示例统配符`*`）

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  // 输出：true
  System.out.println(antPathMatcher.match("/jd/a*c", "/jd/ac"));
  // 输出：true
  System.out.println(antPathMatcher.match("/jd/a*c", "/jd/abc"));
  // 输出：true
  System.out.println(antPathMatcher.match("/jd/a*c", "/jd/axyzc"));
  // 输出：false
  System.out.println(antPathMatcher.match("/jd/a*c", "/jd/a"));
  // 输出：false
  System.out.println(antPathMatcher.match("/jd/a*c", "/jd/xc"));
  // 输出：false
  System.out.println(antPathMatcher.match("/jd/a*c", "/jk/ac"));
  ```

- 示例三（示例统配符`**`）

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  //true
  //false
  //false
  System.out.println(antPathMatcher.match("**", ""));
  System.out.println(antPathMatcher.match("**", "/"));
  System.out.println(antPathMatcher.match("**", "////"));
  
  //false
  //true
  //true
  System.out.println(antPathMatcher.match("/**", ""));
  System.out.println(antPathMatcher.match("/**", "/"));
  System.out.println(antPathMatcher.match("/**", "////"));
  
  //true
  //false
  //false
  System.out.println(antPathMatcher.match("**/", ""));
  System.out.println(antPathMatcher.match("**/", "/"));
  System.out.println(antPathMatcher.match("**/", "////"));
  
  //false
  //true
  //true
  System.out.println(antPathMatcher.match("/**/", ""));
  System.out.println(antPathMatcher.match("/**/", "/"));
  System.out.println(antPathMatcher.match("/**/", "////"));
  
  //true
  //true
  //true
  //true
  //false
  //false
  System.out.println(antPathMatcher.match("/**/abc.html", "/abc.html"));
  System.out.println(antPathMatcher.match("/**/abc.html", "//abc.html"));
  System.out.println(antPathMatcher.match("/**/abc.html", "/1/2/3/4/5/abc.html"));
  System.out.println(antPathMatcher.match("/**/abc.html", "/a/b/c//jd/ac/abc.html"));
  System.out.println(antPathMatcher.match("/**/abc.html", "/a/abc"));
  System.out.println(antPathMatcher.match("/**/abc.html", "/a/bc.html"));
  ```

- 示例四（示例占位符`{}`）

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  //true
  //false
  //false
  System.out.println(antPathMatcher.match("a/{b}/c", "a/b0/c"));
  System.out.println(antPathMatcher.match("a/{b}/c", "a/b1/b2/b3/c"));
  System.out.println(antPathMatcher.match("a/{b}", "a/b1/b2/b3/c"));
  ```

### matchStart(String pattern, String path)：pattern的前面一部分是否就能匹配上整个path

> 注：若path与pattern完全匹配，那么也算：pattern的前面一部分匹配上了整个path
>
> 注：可理解为，path是pattern的一个子集

- 示例

  ```java
  AntPathMatcher antPathMatcher = new AntPathMatcher();
  //false
  //true
  //true
  System.out.println(antPathMatcher.matchStart("a/{b}", "a/b0/c"));
  System.out.println(antPathMatcher.matchStart("a/{b}/c", "a/b0/c"));
  System.out.println(antPathMatcher.matchStart("a/{b}/c/d", "a/b0/c"));
  
  //false
  //true
  //true
  System.out.println(antPathMatcher.matchStart("a/?", "a/b/c"));
  System.out.println(antPathMatcher.matchStart("a/?/c", "a/b/c"));
  System.out.println(antPathMatcher.matchStart("a/?/c/d", "a/b/c"));
  
  //false
  //true
  //true
  System.out.println(antPathMatcher.matchStart("a/*", "a/b0/c"));
  System.out.println(antPathMatcher.matchStart("a/*/c", "a/b0/c"));
  System.out.println(antPathMatcher.matchStart("a/*/c/d", "a/b0/c"));
  
  //true
  //true
  //true
  System.out.println(antPathMatcher.matchStart("a/**", "a/b0/c"));
  System.out.println(antPathMatcher.matchStart("a/**/c", "a/b0/c"));
  System.out.println(antPathMatcher.matchStart("a/**/c/d", "a/b0/c"));
  ```

