

# Solidity的数据类型

- [Solidity的数据类型](#solidity的数据类型)
  - [基本类型](#基本类型)
    - [整型int和uint](#整型int和uint)
    - [布尔型bool](#布尔型bool)
    - [字符串类型string](#字符串类型string)
    - [字节类型byte](#字节类型byte)
  - [复合类型](#复合类型)
    - [地址类型adress](#地址类型adress)
    - [定长字节数组](#定长字节数组)
    - [变长字节数组](#变长字节数组)
  - [相关资料](#相关资料)

---

## 基本类型

### 整型int和uint

智能合约运行在以太坊EVM中，存储到全区节点里，空间能省就省，所以整型按照所占空间的不同，又分了很多种。

- 有符号整型int

  > int、int8、int16、int24...int256

- 无符号整型uint

  > uint、uint8、uint16、uint24...uint256

### 布尔型bool

### 字符串类型string

### 字节类型byte

## 复合类型

### 地址类型adress

地址类型是Solidity特有的数据类型，它对应了以太坊的账户地址。

### 定长字节数组

> bytes1、bytes2、bytes3......bytes132

### 变长字节数组

> bytes[length]，如：bytes[16]

----

## 相关资料

- **《Go语言区块链应用开发从入门到精通》** **高野** 编著
