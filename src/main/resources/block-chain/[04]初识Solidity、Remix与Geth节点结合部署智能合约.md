

# 初识Solidity、Remix与Geth节点结合部署智能合约

- [初识Solidity、Remix与Geth节点结合部署智能合约](#初识solidityremix与geth节点结合部署智能合约)
  - [初识Solidity](#初识solidity)
  - [Remix与Geth节点结合部署智能合约](#remix与geth节点结合部署智能合约)
    - [第一步：访问`http://remix.ethereum.org/`，进入以太坊智能合约在线IDE工具Remix](#第一步访问httpremixethereumorg进入以太坊智能合约在线ide工具remix)
    - [第二步：创建并编写sol文件](#第二步创建并编写sol文件)
    - [第三步：编译](#第三步编译)
    - [第四步：部署](#第四步部署)
  - [相关资料](#相关资料)

---

## 初识Solidity

目前支持智能合约的区块链平台很多，智能合约的开发语言也有多种选择。以太坊智能合约的开发语言主要采用Solidity，（因为以太坊是第一个诞生智能合约的区块链平台，后续区块链平台大多都借鉴了它的智能合约开发环境，所以）目前Solidity是多数主流区块链平台所采用的智能合约开发语言。

示例说明：

下面代码的功能是将`Msg`值初始化为`hello`

```soli
pragma solidity^0.6.0;
contract hello {
    string public Msg;
    constructor () public {
        Msg = "hello";
    }
}
```

- `pragma：Solidity`的编译控制指令

- `^0.6.0`：指定使用0.6.0版本的编译器对改代码进行编码

  注：也可以指定编译器版本的范围，如：`pragma solidity > 0.4.99 < 0.6.0;`

- `contract`：定义智能合约的关键字

- `hello`：自定义的智能合约名称

- `string public Msg:`：类似于Java中的实例变量

- `constructor`：智能合约的构造函数。**智能合约在被部署时，其构造函数会被调用**

## Remix与Geth节点结合部署智能合约

### 第一步：访问`http://remix.ethereum.org/`，进入以太坊智能合约在线IDE工具Remix

![image-20210818001104811](../repository/image-20210818001104811.png)

### 第二步：创建并编写sol文件

![image-20210818002202266](../repository/image-20210818002202266.png)

### 第三步：编译

![image-20210818002700908](../repository/image-20210818002700908.png)

- ①切换至编译页
- ②选择编译器版本
- ③选择语言
- ④选择虚拟机版本（上图选择的是拜占庭版虚拟机）
- ⑤右侧选择要编译的sol文件，然后点击此处进行编译
- ⑥查看编译后的统计结果（这里有一个警告）

### 第四步：部署

> 部署的方式有三种：
>
> ![image-20210818003644095](../repository/image-20210818003644095.png)
>
> - `JavaScript VM`：Remix内置的虚拟机，运行速度快，无需挖矿，测试方便。
> - `Injected Web3`：单击时会连接浏览器安装的Metamask插件（即：以太坊浏览器钱包插件），很多时候，我们都是通过Metamask钱包将合约部署到主网或测试网。
> - `Web3 Provider`：单机时，将代表要连接某个以太坊节点，需要指定Geth的连接池。

**提示：** 本步骤示例演示以`Web3 Provider`的方式进行部署。

- 第一步：启动Geth私网节点

  ```bash
  root@ubuntu:~# geth --datadir ./data --networkid 18 --port 30303 --rpc --rpcport 8545 --rpcapi 'db,net,eth,web3,personal' --rpcaddr 192.168.19.129 --rpccorsdomain '*' --gasprice 0 --allow-insecure-unlock console 2> 1.log
  Welcome to the Geth JavaScript console!
  
  instance: Geth/v1.9.10-stable-58cf5686/linux-amd64/go1.13.6
  at block: 0 (Wed, 31 Dec 1969 16:00:00 PST)
   datadir: /root/data
   modules: admin:1.0 debug:1.0 eth:1.0 ethash:1.0 miner:1.0 net:1.0 personal:1.0 rpc:1.0 txpool:1.0 web3:1.0
  
  > 
  ```
  
- 第二步：调用`personal.newAccount("密码")`创建账户并设置密码

  **提示：** 若已存在账户，那么可跳过此步骤。

  ```bash
  > personal.newAccount("123")
  "0xf868d8bf6eecf8af25f76fa620e89a280bf3c74f"
  > 
  ```
  
  - `personal`：远程api"类"
  - `newAccount`：远程api"类"中的"方法"
  - `123`："方法"的参数
  - `0xf868d8bf6eecf8af25f76fa620e89a280bf3c74f`：得到的账户地址
  
- 第三步：解锁账户

  **提示：** 对于Geth内的账户，需要解锁后才可使用

  ```bash
  > eth.accounts
  ["0xf868d8bf6eecf8af25f76fa620e89a280bf3c74f"]
  > acc0=eth.accounts[0]
  "0xf868d8bf6eecf8af25f76fa620e89a280bf3c74f"
  > personal.unlockAccount(acc0,"123")
  true
  > 
  ```
  
  - `eth.accounts`：查看所有账户
  - `acc0=eth.accounts[0]`：令eth.accounts[0]账户为默认账户
  - `personal.unlockAccount(acc0,"123")`：解锁acco所在表的账户，此时需要输入早该账户的密码
  
- 第四步：返回Remix界面，选择`Web3 Provider`部署。

  ![image-20210830012221298](../repository/image-20210830012221298.png)
  
  ![image-20210830012506682](../repository/image-20210830012506682.png)
  
  ![image-20210830012506682](../repository/image-20210904212920759.png)
  
- 第五步：启动挖矿，完成交易。

  > 区块链的每次智能合约的执行都是一次交易，这个交易需要挖矿才能被打包到区块中，只有被打包到区块中，才代表交易执行成功了。需要启动挖矿，这也是区块链平台的最大特点。
  >
  > 合约要想跑，挖矿少不了。

  ![image-20210904214248861](../repository/image-20210904214248861.png)

- 第六步：验证一下。

  > 提示：因为挖矿存在一定的概率问题，所以上一步执行后进行这一步验证时，可能需要等一会儿才会出来部署成功的结果（即：可能需要等一会儿才会出来交易的结果），挖矿给力就比较快，挖矿不给力就等得久。
  >
  > 注：本人等了约10分钟。

  ![image-20210904215817446](../repository/image-20210904215817446.png)

----

## 相关资料

- **《Go语言区块链应用开发从入门到精通》** **高野** 编著
- [`https://geth.ethereum.org/docs/install-and-build/installing-geth`](https://geth.ethereum.org/docs/install-and-build/installing-geth)
