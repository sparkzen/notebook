# Jenkins（结合Docker）任务（Jenkins与Docker位于不同机器上）

- [Jenkins（结合Docker）任务（Jenkins与Docker位于不同机器上）](#jenkins结合docker任务jenkins与docker位于不同机器上)
  - [环境说明](#环境说明)
  - [准备工作](#准备工作)
    - [准备运行jar包的容器](#准备运行jar包的容器)
    - [准备Jenkins插件Push Over SSH](#准备jenkins插件push-over-ssh)
  - [Jenkins结合Docker（处于不同机器上的集成）示例](#jenkins结合docker处于不同机器上的集成示例)

## 环境说明

| IP地址      | 系统版本 | 安装软件 | 用途                                                         | 目的                                        |
| ----------- | -------- | -------- | ------------------------------------------------------------ | ------------------------------------------- |
| 10.8.109.60 | CentOS7  | Jenkins  | 从SVN上拉取（更新）源代码；maven打包；将jar包发送到10.8.109.76 | 测试Jenkins与Docker处于不同机器时的集成使用 |
| 10.8.109.76 | CentOS7  | Docker   | 根据10.8.109.60发送过来的jar包，运行容器（启动jar包）        |                                             |

注：为了避免不必要的麻烦，此两台机器的防火墙都是关闭了的；如果不想关闭防火墙，那么只开放需要用到的端口也行。

## 准备工作

### 准备运行jar包的容器

> 提示：此准备步骤的目的是，ready一个docker环境；如果你已具备自己的docker环境，那么可跳过此步骤。

- 第一步：从Docker官方仓库搜索镜像`docker search justrydeng`

  ![2022-04-28-001](../../repository/2022-04-28-001.png)

  注：此镜像是本人制作并上传到仓库中的；所以如果想自己做同样功能的镜像，那么可以参考docker相关笔记`在Docker中部署运行jar示例`

- 第二步：`docker pull justrydeng/common-run-jar`抓取（下载）此镜像

  ![2022-04-28-002](../../repository/2022-04-28-002.png)

- 第三步：以该镜像为基础，根据项目端口，定制化生成docker容器

  `docker run -p 8080:8080 --name jenkins-docker-test -d justrydeng/common-run-jar`

  ![2022-04-28-003](../../repository/2022-04-28-003.png)

  注：因为知道要运行的jar包需要的端口为docker中的8080，所以本人将docker中的8080端口，与宿主机的端口（这里干脆也8080）映射起来。其中，指令-p 8080/8080里，前面的8080为宿主机端口，后面的8080为容器端口

  此时，我们已经创建了一个名为jenkins-docker-test的容器了。如果此时我们只想在docker中运行jar包的话，那么只需将xxx.jar包改名为app.jar，放入jenkins-docker-test容器的/jarAppDir/目录下，再start启动容器即可实现docker中运行jar包了

  如果需要更新jar包，那么需要：第一步，stop该容器；第二步，以新的app.jar替换容器中旧的app.jar；第三步:再次start启动该容器即可 

  **提示**：如果上述操作出现类似这样的错误提示：

  ```sh
  docker: Error response from daemon: driver failed programming external connectivity on endpoint jenkins-docker-test (26160e69b714ec8b1f77fa080fa2865a50d0fae9455ee3466088d6f1d820fa4c):  (iptables failed: iptables --wait -t nat -A DOCKER -p tcp -d 0/0 --dport 8080 -j DNAT --to-destination 172.17.0.2:8080 ! -i docker0: iptables: No chain/target/match by that name.
   (exit status 1)).
  ```

  那么需要`systemctl restart docker`重启Docker

### 准备Jenkins插件Push Over SSH

> 说明：由于涉及到了一个Linux连接另一个Linux，所以我们需要安装辅助插件

- 第一步：进入`系统管理` => `插件管理`，安装Push Over SSH插件

  ![2022-04-28-014](../../repository/2022-04-28-014.png)

  注：安装此插件时，会自动先安装Push Over；如果不自动安装Push Over的话，那么需要我们先手动安装Push Over，然后再安装Push Over SSH，否者Push Over SSH的安装可能会出问题 

  ![2022-04-28-015](../../repository/2022-04-28-015.png)

- 第二步：安装Push Over SSH后，进入`系统管理` => `系统设置`

  点击【新增】

  ![2022-04-28-016](../../repository/2022-04-28-016.png)

  点击【高级】 

  ![2022-04-28-017](../../repository/2022-04-28-017.png)

  填写连接信息 

  ![2022-04-28-018](../../repository/2022-04-28-018.png)

  提示：如果配置有SSH公钥、私钥认证的话，那么可以不选用户名、密码认证的方式；本人为了省事，直接使用了账号密码进行认证。 

- 第三步：测试一下

  ![2022-04-28-019](../../repository/2022-04-28-019.png)

  Success说明远程连接Linux配置成功了 

注：如果后面需要连接多个机器，那么需要多次进行第二步配置。

## Jenkins结合Docker（处于不同机器上的集成）示例

> 声明：下面我不会再介绍每一步的详细作用，如果想了解细节，可详见[新建Jenkins任务之基础知识（入门级示例）](./[05]新建Jenkins任务之基础知识（入门级示例）.md)

- 第一步：新建一个maven风格的任务

  ![2022-04-28-020](../../repository/2022-04-28-020.png)

- 第二步：配置各个参数

  ![2022-04-28-021](../../repository/2022-04-28-021.png)

  ![2022-04-28-022](../../repository/2022-04-28-022.png)

  注：如果这里配置指定了Local module directory目录的话，那么后面的mvn打包指令需要切换到此目录下。Push Over SSH的Source file也要对应上。 

  ![2022-04-28-023](../../repository/2022-04-28-023.png)

  【Pre Steps】此处完成项目打包 

  ![2022-04-28-024](../../repository/2022-04-28-024.png)

  【构建后操作】此处完成将新的jar包推送至其他Linux上，并编写Exec command指令，让目标Linux执行相关指令 

  ![2022-04-28-025](../../repository/2022-04-28-025.png)

  对上图说明： 

  - `Name`：是一个下拉选项，选择的是我们在前面准备工作中配置好了的Push Over   SSH机器

  - `Source file`：是Jnekins所在机器上的要进行拷贝的文件

    此处填写相对路径。相对的Base(基)路径为:Jenkin主目录下workspase目录下的任务同名文件夹中。如：Jenkins主目录为/var/lib/jenkins，任务名为【jenkins-docker-不同机器中】，那么这里Source file就应该填相对/var/lib/jenkins/workspace/jenkins-docker-不同机器中/的相对路径

  - `Remote directory`：是远程Linux上的文件夹（无则自动创建），会将Source file指向的文件（含该文件所涉及的文件夹），复制到该文件夹中；如：Source file为target/abc.jar，Remote directory为/myjar,那么拷贝后，远程Linux上该jar包的位置为/myjar/target/abc.jar

  - `Exec command`：远程Linux要执行的shell指令

  填写完成后，应用保存，构建该任务！

- 第三步：访问测试http://10.8.109.76:8080/jenkins/test?name=张三

   ![2022-04-28-026](../../repository/2022-04-28-026.png)

  修改源代码为:

  ![2022-04-28-027](../../repository/2022-04-28-027.png)

  并提交至SVN,然后在10.8.109.60上重新构建Jenkins任务，最后再次访问http://10.8.109.76:8080/jenkins/test?name=张三

  ![2022-04-28-028](../../repository/2022-04-28-028-1651135042511.png)

由此可见，Jenkins与Docker位于不同台电脑上的联合使用，演示成功！