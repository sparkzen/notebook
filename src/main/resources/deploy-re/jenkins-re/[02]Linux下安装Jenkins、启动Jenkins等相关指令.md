# Linux下安装Jenkins、启动Jenkins

- [Linux下安装Jenkins、启动Jenkins](#linux下安装jenkins启动jenkins)
  - [Jenkins的安装方式介绍](#jenkins的安装方式介绍)
  - [安装Jenkins](#安装jenkins)
    - [准备jdk](#准备jdk)
    - [准备maven（可选）](#准备maven可选)
    - [准备Ant（可选）](#准备ant可选)
    - [安装Jenkins](#安装jenkins-1)
    - [启动Jenkins](#启动jenkins)

## Jenkins的安装方式介绍

- 使用Tomcat安装Jenkins的war包(此方式较简单)
- 利用Jenkins内置的服务器安装Jenkins
  - 直接利用其内置的服务器运行Jenkins（的jar包）
  - 其它
- 其它

注：Jenkins是一个Java项目，我们当然可以运行其jar包或war包来（安装）启动它。

## 安装Jenkins

> 提示：本人习惯将相关软件安装在/opt目录下，当然你也可以安装在其它目录下

### 准备jdk

- 第一步：进入root用户

- 第二步：把Linux版本的JDK文件拷贝到/opt

- 第三步：解压

  注：如果是.gz文件，使用`tar -xzvf`命令解压；如果是.bin文件(可执行文件) 使用`./xxx.bin`执行后就自动解

- 第四步：配置环境变量

  - 第一小步：`vim /etc/profile` 

  - 第二小步：在文件的末尾加上

    ```sh
    # 本人解压出来的jdk是jdk1.8.0_191
    export JAVA_HOME=/opt/jdk1.8.0_191
    export PATH=$JAVA_HOME/bin:$PATH
    ```

    如果上述配置不行，那么可以试一下：

    ```sh
    export JAVA_HOME=/opt/jdk1.8.0_191
    export JRE_HOME=${JAVA_HOME}/jre  
    export CLASSPATH=.:${JAVA_HOME}/lib:${JRE_HOME}/lib  
    export PATH=${JAVA_HOME}/bin:$PATH 
    ```

  - 第三小步：`source /etc/profile`刷新配置，让配置生效

  - 第四小步：编写HelloWorld.java，编译、运行来验证是否成功

    编译：`javac HelloWorld.java`

    执行：`java HelloWorld`

    注：用于测试的类.java文件,代码上不应该有导包操作

### 准备maven（可选）

> 提示：很多jenkins任务都会用到maven编译打包，所以这里干脆就先安装maven好了。当然，如果你的jenkins任务会用到其它工具（如gradle）打包，那么也是需要安装对应工具的

- 第一步：[下载](http://maven.apache.org/download.cgi)maven的.gz安装包，放进Linux中，并使用`tar -xzvf`命令解压

  ![1650853338719](../../repository/1650853338719.png)

- 第二步：配置环境变量

  - 第一小步：`vim /etc/profile`

  - 第二小步：在文件的末尾加上

    ```sh
    # 本人解压出来的maven是apache-maven-3.6.0
    export M2_HOME=/opt/apache-maven-3.6.0
    export PATH=$M2_HOME/bin:$PATH
    ```

  - 第三小步：`source /etc/profile`刷新配置，让配置生效

- 第三步：使用`mvn -v`测试一下

  ![1650853466080](../../repository/1650853466080.png)

### 准备Ant（可选）

> Ant是一个跨平台的、语法清晰的、功能强大的、使用简单的构建工具。
>
> - **如果要让Jenkins可以自动构建，那么必须安装Ant**
>
>   注：如果不需要Jenkins的自动构建功能，那么可以不安装Ant

- 第一步：[下载](http://ant.apache.org)tar.gz版ant

  ![1650853584962](../../repository/1650853584962.png)

  注：本人下载的ant是apache-ant-1.10.5-bin.tar.gz版本

- 第二步：进入root用户，将该压缩包放入Linux中/opt/下，并`tar -zxvf apache-ant-1.10.5-bin.tar.gz`解压

  ![1650853669022](../../repository/1650853669022.png)

  注：解压后，原压缩包没用了，可以选择删除了

- 第三步：配置环境变量

  - 第一小步：`vim /etc/profile`

  - 第二小步：在文件的末尾加上

    ```sh
    #set Ant enviroment
    export ANT_HOME=/opt/apache-ant-1.10.5
    export PATH=${ANT_HOME}/bin:$PATH
    ```

  - 第三小步：`source /etc/profile`刷新配置，让配置生效

- 第四步：`ant -version`测试ant是否生效

  ![1650853792895](../../repository/1650853792895.png)

### 安装Jenkins

- 第一步：`rpm -qa|grep "wget"`查看当前系统是否安装有wget指令，如果没有安装，那么使用`yum -y install wget`安装

  ![1650853955005](../../repository/1650853955005.png)

- 第二步：使用`wget --no-check-certificate https://pkg.jenkins.io/redhat-stable/jenkins-2.138.2-1.1.noarch.rpm`指令，下载指定版本的Jenkins安装包到当前目录下

  ![1650853990664](../../repository/1650853990664.png)

  注：如果提示需要校验证书，那么可以使用`wget --no-check-certificate https://pkg.jenkins.io/redhat-stable/jenkins-2.138.2-1.1.noarch.rpm`不作证书校验下载。

  注：要下载什么版本的Jenkins可以去https://jenkins.io/中找。

  注：我们也可以不利用wget指令；自己下载Jenkins的安装包放进Linux后，直接安装该安装包也可以。

- 第三步：使用`sudo rpm -ih jenkins-2.138.2-1.1.noarch.rpm`安装刚才下载下来的安装包即可

  ![1650854052744](../../repository/1650854052744.png)

  注：安装成功后，会生成一系列相关的文件、文件夹，给出几个常用的

  ```sh
  /usr/lib/jenkins/jenkins.war       #WAR包 
  /etc/sysconfig/jenkins             #配置文件
  /var/log/jenkins                   #Jenkins日志文件夹、Jenkins的日志会放进这个文件夹中
  /var/lib/jenkins/                  #默认的JENKINS_HOME目录
  /etc/init.d/jenkins                #Jenkins的statup启动脚本(相比起/etc/sysconfig/jenkins只支持一些有限的配置;启动文件中则支持更多的配置、支持更加高级的配置)
  ```

  注：Jenkins默认开机启动

  注：可以将`/etc/sysconfig/jenkins`中的`JENKINS_USER="jenkins"`改为`JENKINS_USER="root"`，因为jenkins用户身份，权限比root权限小，改成root，可避免一些不必要的麻烦

  注：安装Jenkins的时候，会创建一个名为`jenkins`的用户，Jenkins执行Shell指令时，就是以该用户身份（的权限）执行的。如果你不把JENKINS_USER改为root的话，那么需要记得给jenkins用户赋对应的权限。

  踩坑示例：本人测试时，用root身份在一个文件夹中放了一个.jar包，然后后来又想用Jenkins执行shell指令，替换该jar包；Jenkins报错提示没有权限。因为原jar包是root身份创建的，而jenkins身份无法替换root身份创建的文件

### 启动Jenkins

- 第一步（首次做）：使用`vim /etc/init.d/jenkins`指令编辑Jenkins的statup启动脚本，指定JDK位置

  ![1650854228462](../../repository/1650854228462.png)

  注：因为JDK是我们自己手动解压安装的，所以需要指定jdk。

  注：使用vim/vi 时看不全，要i进入编辑时，才能看到全的。或者cat查看也能看全的。

- 第二步（首次做）：修改端口

  > Jenkins默认端口是8080，而8080是一个热门端口，为了不出现其他情况，我们最好将Jenkins的端口重新设置一下

  `vim /etc/sysconfig/jenkins`打开配置文件，修改端口为9527，并设置Jenkins运行内存

  ![1650854302080](../../repository/1650854302080.png)

- 第三步：使用`sudo service jenkins start`指令启动Jenkins

  ![1650854353967](../../repository/1650854353967.png)

  - **启动Jenkins：**`sudo service jenkins start`

  - **重启Jenkins：**

    - 方式一：访问`http://{ip}:{port}/restart`
    - 方式二：`sudo service jenkins restart`

  - **重新加载Jenkins配置信息：**`http://{ip}:{port}/reload`

  - **关闭Jenkins服务：**

    - 方式一：访问`http://{ip}:{port}/exit`

    - 方式二：查询Jenkisn进程号`ps -ef|grep jenkins`，然后`kill -9 {进程号}`

- 第四步：访问`http://{ip}:{port}`，测试一下

  > 提示：记得开放端口（或者测试时可暂时直接关闭防火墙）
  
  ![1650854702561](../../repository/1650854702561.png)