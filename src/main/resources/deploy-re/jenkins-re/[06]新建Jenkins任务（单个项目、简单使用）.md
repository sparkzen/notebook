# 新建Jenkins任务（单个项目、简单使用）

- [新建Jenkins任务（单个项目、简单使用）](#新建jenkins任务单个项目简单使用)
  - [简述](#简述)
  - [新建Jenkins任务](#新建jenkins任务)
    - [第一步：进入任务配置界面](#第一步进入任务配置界面)
    - [第二步：编写Pre Steps、Post Steps](#第二步编写pre-stepspost-steps)
    - [第三步：构建该任务](#第三步构建该任务)
    - [第四步：访问测试](#第四步访问测试)

## 简述

本文将在[上一文](./[05]新建Jenkins任务之基础知识（入门级示例）.md)的基础上，继续配置

## 新建Jenkins任务

### 第一步：进入任务配置界面

> 提示：这里我们直接进入已存在任务（上一文中创建的任务）的配置

![1650858704275](../../repository/1650858704275.png) 



 ![1650858713104](../../repository/1650858713104.png) 

### 第二步：编写Pre Steps、Post Steps

说明：在上一文中，我们只是说了明了一下这两个选项的功能；下面我们将演示如何使用这两个功能。

【**Pre Steps】**

![1650858721135](../../repository/1650858721135.png) 

给出shell文字版:

 ```sh
# 将当前位置切换至项目根目录下
cd /var/lib/jenkins/workspace/Jenkins新建maven任务测试/JenkinsTest/
# maven打包(可以将mvn clean 、 mvn install 合并到一起写)
mvn clean install
 ```

**【Post Steps】**

![1650858751842](../../repository/1650858751842.png) 

给出shell文字版:

```sh
# 切换到jar包所在目录
cd /var/apps/jenkins-test/

# 备份原来的jar包
# 首次执行时，如果因为本来就没有该jar文件而出错;那么也不会有什么影响
tar -czvf jenkins-0.0.1-SNAPSHOT.jar.`date +%Y%m%d_%H`.tar.gz jenkins-0.0.1-SNAPSHOT.jar && rm -rf jenkins-0.0.1-SNAPSHOT.jar

# 如果该jar包正在缓存中运行着，那么先kill掉原来的进程，释放端口
pid=`ps -ef|grep java |grep -v grep|grep jenkins-0.0.1-SNAPSHOT.jar |awk '{print $2}'`
# 如果第一次启动，那么pid为空，此时做一个if判断即可
if [ "$pid" != "" ];then
kill -s 9 $pid
fi

# 将打包后的jar文件移动到一个指定的目录下(注:此步骤纯粹是为了方便管理)
cp /var/lib/jenkins/workspace/Jenkins新建maven任务测试/JenkinsTest/target/jenkins-0.0.1-SNAPSHOT.jar /var/apps/jenkins-test/

# BUILD_ID=DONTKILLME可以设置，不让Jenkins杀死后台运行的线程
# SpringBoot项目,直接运行启动jar包即可
BUILD_ID=DONTKILLME nohup java -jar jenkins-0.0.1-SNAPSHOT.jar &
```

注：考虑到实际情况，这里备份是：一个小时内最多只能保留一个最新的.tar.gz。如果想保留多个备份，那么只需要把备份时间点进一步精确即可。

**修改完后，记得保存**

###  第三步：构建该任务

![1650858772727](../../repository/1650858772727.png) 

到此步位置，Jenkins为我们做的事有：

1. 从SVN更新文件到Jenkins主目录下工作空间里的指定位置下
2. 在该位置运行maven指令,生成新的jar包
3. 备份指定目录下的原来的jar包
4. 如果原来的jar包正运行着，那么kill掉其对应的进程，释放端口
5. 将新的jar包拷贝到指定目录下(注：这样做的目的是为了方便管理)， 并运行

### 第四步：访问测试

说明：

原SVN中的项目，有一个可访问的Controller：

![1650858779916](../../repository/1650858779916.png) 

jenkins构建部署该项目后，访问：

![1650858786903](../../repository/1650858786903.png) 

此时，我们再次项目修改controller代码为：

![1650858795074](../../repository/1650858795074.png) 

然后提交至SVN并重新构建该Jenkins任务，然后再次访问：

![1650858802230](../../repository/1650858802230.png) 

**由此可见，新建Jenkins任务，实现自动打包部署等功能，成功了！**