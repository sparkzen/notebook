# Jenkins简介

- [Jenkins简介](#jenkins简介)
  - [简介](#简介)
  - [Jenkins的应用场景](#jenkins的应用场景)
  - [Jenkins的特点](#jenkins的特点)
  - [相关资料](#相关资料)

## 简介

Jenkins是一个开源软件项目，是基于Java开发的一种持续集成(Continuous integration)工具，用于监控持续重复的工作，旨在提供一个开放易用的软件平台，使软件的持续集成变成可能

## Jenkins的应用场景

- 持续、自动地构建/测试 软件项目，如CruiseControl与DamageControl

- 监控一些定时执行的任务
- ......

## Jenkins的特点

- 开源免费

- 跨平台，支持所有的平台

- master/slave支持分布式的build

- web形式的可视化的管理页面

- 安装配置超级简单

- tips及时快速的帮助

- 已有的200多个插件
- ......

## 相关资料

- [官网](https://jenkins.io/)

 

 

 