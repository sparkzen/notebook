# Jenkins（结合Docker）任务（Jenkins与Docker位于同一台机器上）

- [Jenkins（结合Docker）任务（Jenkins与Docker位于同一台机器上）](#jenkins结合docker任务jenkins与docker位于同一台机器上)
  - [简述](#简述)
  - [准备工作](#准备工作)
    - [权限配置](#权限配置)
    - [准备运行jar包的容器](#准备运行jar包的容器)
  - [配置Jenkins](#配置jenkins)
    - [第一步：新建一个maven风格的任务](#第一步新建一个maven风格的任务)
    - [第二步：配置各个参数](#第二步配置各个参数)
    - [第三步：构建](#第三步构建)
    - [第四步：构建成功后，访问url](#第四步构建成功后访问url)
    - [第五步：修改测试](#第五步修改测试)

## 简述

在[上一节](./[06]新建Jenkins任务（单个项目、简单使用）.md)我们是将Jenkins打包后的jar包直接放在Linux中运行的；下面我们示例，将Jenkins获得的jar包放进同一Linux下的Docker的容器中，并运行。

## 准备工作

### 权限配置

- 从`/etc/group`文件中在nogroup中删除jenkins账号

- 将jenkins账号分别加入到root组中 `gpasswd -a root jenkins`

- 修改`/etc/default/jenkins`文件中

  ```sh
  JENKINS_USER=root
  JENKINS_GROUP=root
  ```

  重启服务器，搞定

注：可能还需要将`/var/lib/jenkins/`中用户指定为root`JENKINS_USER=root`。

### 准备运行jar包的容器

> 提示：此准备步骤的目的是，ready一个docker环境；如果你已具备自己的docker环境，那么可跳过此步骤。

- 第一步：从Docker官方仓库搜索镜像`docker search justrydeng`

  ![2022-04-28-001](../../repository/2022-04-28-001.png)

  注：此镜像是本人制作并上传到仓库中的；所以如果想自己做同样功能的镜像，那么可以参考docker相关笔记`在Docker中部署运行jar示例`

- 第二步：`docker pull justrydeng/common-run-jar`抓取（下载）此镜像

  ![2022-04-28-002](../../repository/2022-04-28-002.png)

- 第三步：以该镜像为基础，根据项目端口，定制化生成docker容器

  `docker run -p 8080:8080 --name jenkins-docker-test -d justrydeng/common-run-jar`
  
  ![2022-04-28-003](../../repository/2022-04-28-003.png)
  
  注：因为知道要运行的jar包需要的端口为docker中的8080，所以本人将docker中的8080端口，与宿主机的端口（这里干脆也8080）映射起来。其中，指令-p 8080/8080里，前面的8080为宿主机端口，后面的8080为容器端口
  
  此时，我们已经创建了一个名为jenkins-docker-test的容器了。如果此时我们只想在docker中运行jar包的话，那么只需将xxx.jar包改名为app.jar，放入jenkins-docker-test容器的/jarAppDir/目录下，再start启动容器即可实现docker中运行jar包了
  
  如果需要更新jar包，那么需要：第一步，stop该容器；第二步，以新的app.jar替换容器中旧的app.jar；第三步:再次start启动该容器即可 
  
  **提示**：如果上述操作出现类似这样的错误提示：
  
  ```sh
  docker: Error response from daemon: driver failed programming external connectivity on endpoint jenkins-docker-test (26160e69b714ec8b1f77fa080fa2865a50d0fae9455ee3466088d6f1d820fa4c):  (iptables failed: iptables --wait -t nat -A DOCKER -p tcp -d 0/0 --dport 8080 -j DNAT --to-destination 172.17.0.2:8080 ! -i docker0: iptables: No chain/target/match by that name.
   (exit status 1)).
  ```
  
  那么需要`systemctl restart docker`重启Docker

## 配置Jenkins

>  声明：下面我不会再介绍每一步的详细作用，如果想了解细节，可详见[新建Jenkins任务之基础知识（入门级示例）](./[05]新建Jenkins任务之基础知识（入门级示例）.md)

### 第一步：新建一个maven风格的任务

![2022-04-28-004](../../repository/2022-04-28-004.png)

### 第二步：配置各个参数

![2022-04-28-005](../../repository/2022-04-28-005.png)

![2022-04-28-006](../../repository/2022-04-28-006.png)

![2022-04-28-007](../../repository/2022-04-28-007.png)

给出上图中涉及到的指令，文字版：

```shell
# 将当前位置切换至项目根目录下
cd /var/lib/jenkins/workspace/jenkins-docker-同一机器中/
# maven打包(可以将mvn clean 、 mvn install 合并到一起写)
mvn clean install
```

![2022-04-28-008](../../repository/2022-04-28-008.png)

给出上图中涉及到的指令，文字版：

```shell
# stop该jar包对应的容器
docker stop jenkins-docker-test

# 将新的jar包拷贝至容器中的指令目录下，并重命名为jar.jar （注:如果该目录下本来就有jar.jar，那么原jar包会被替换）
docker cp /var/lib/jenkins/workspace/jenkins-docker-同一机器中/target/jenkins-0.0.1-SNAPSHOT.jar jenkins-docker-test:/jarAppDir/app.jar

# start该jar包对应的容器即可
docker start jenkins-docker-test
```

保存

### 第三步：构建

![2022-04-28-009](../../repository/2022-04-28-009.png)

### 第四步：构建成功后，访问url

访问http://10.8.109.60:8080/jenkins/test?name=JustryDeng

![2022-04-28-010](../../repository/2022-04-28-010.png)

给出controller对应方法：

![2022-04-28-011](../../repository/2022-04-28-011.png)

### 第五步：修改测试

修改代码后 => 提交至SVN => 再次构建 => 再次访问http://10.8.109.60:8080/jenkins/test?name=JustryDeng

![2022-04-28-012](../../repository/2022-04-28-012.png)

给出修改后的controller对应方法：

![2022-04-28-013](../../repository/2022-04-28-013.png)

由此可见，Jenkins与Docker位于同一台电脑上的联合使用，演示成功！