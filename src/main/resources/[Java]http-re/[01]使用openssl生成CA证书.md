# 使用openssl生成CA证书

- [使用openssl生成CA证书](#使用openssl生成ca证书)
  - [CA证书的几个相关文件说明](#ca证书的几个相关文件说明)
  - [使用openssl生成CA证书](#使用openssl生成ca证书-1)
    - [生成根证书](#生成根证书)
    - [生成（根证书签名的）服务端证书](#生成根证书签名的服务端证书)
    - [生成（根证书签名的）客户端证书](#生成根证书签名的客户端证书)
  - [生成的证书文件说明](#生成的证书文件说明)
  - [使用证书](#使用证书)
    - [nginx配置证书（双向认证）](#nginx配置证书双向认证)
    - [java程序配置证书（以spring-boot为例）](#java程序配置证书以spring-boot为例)
      - [单向认证](#单向认证)
        - [准备工作：](#准备工作)
        - [项目结构](#项目结构)
        - [配置文件](#配置文件)
        - [配置类和测试类](#配置类和测试类)
        - [测试一下](#测试一下)
      - [双向认证](#双向认证)
        - [相关知识说明](#相关知识说明)
        - [准备工作](#准备工作-1)
        - [项目结构](#项目结构-1)
        - [配置文件](#配置文件-1)
        - [配置类和测试类](#配置类和测试类-1)
        - [测试一下](#测试一下-1)
  - [相关资料](#相关资料)

---

## CA证书的几个相关文件说明

- `.crt`：CA证书，一般的**公钥信息 + 额外的其他信息（比如所属的实体，采用的加密解密算法等）= 证书**

- `.key`：证书的认证机制是基于`RSA`公私钥加解密机制实现的，其中公钥信息包含在证书中，而私钥信息则存储在.key文件中。一个`.crt`文件的生成，依赖于一个与其对应的.key文件(即:要生成`.crt`证书，必须先生成一个.key私钥)

  注：这个.key文件的内容，本身也可以是加密的，如果指定需要加密的话。

- `csr`：证书申请请求。在生成`.crt`文件时，*`.csr`文件不是必须的*。若没有`.csr`文件,那么就要求在生成`.crt`证书时，指定相关的额外信息(所属实体、加密算法、组织、邮箱等等)。而如果有`.csr`文件，那么生成证书时只需指定`.csr`文件，而不用再指定额外信息了。因为`.csr`就是这些额外信息的封装

## 使用openssl生成CA证书

### 生成根证书

1. 生成一个`.key`私钥

   ```bash
   # 生成私钥文件root.key，并对内容进行des3加密，并设置des3加密的加密密码为ds1994
   openssl genrsa -des3 -passout pass:ds1994 -out root.key 2048
   ```

   执行效果：

   ![image-20210803003426742](../repository/image-20210803003426742.png)

2. 生成`.csr`证书申请请求

   ```bash
   # 生成证书申请请求文件
   # 因为root.key本身是经过加密的，这里使用到了root.key的话，需要输入自身加密时用到的面，如这里的ds1994
   # subj指定额外的信息:
   # C:  国家
   # ST: 省
   # L:  市
   # CN: 公众名称(对于SSL证书，一般为网站域名或ip地址; 对于代码签名,证书则为申请单位名称; 对于客户端证书,则为证书申请者的姓名)
   # O:  组织名称(对于SSL证书，一般为网站域名; 对于代码签名,证书则为申请单位名称; 对于客户端证书,则为证书申请者所在单位的名称)
   # emailAddress: 邮箱地址
   # OU: 其它信息
   openssl req -new -key root.key -passin pass:ds1994 -out root.csr -subj "/C=CN/ST=GuangDong/L=ShenZhen/CN=www.niantou.com/O=www.niantou.com/emailAddress=1612513157@qq.com/OU=dev"
   ```

   执行效果：

   ![image-20210803003750848](../repository/image-20210803003750848.png)

3. 生成`.crt`证书

   ```bash
   # 通过.key和.csr, 生成crt证书
   # 因为root.key本身是经过加密的，这里使用到了root.key的话，需要输入自身加密时用到的面，如这里的ds1994
   openssl x509 -req -days 3650 -sha256 -extensions v3_ca -signkey root.key -passin pass:ds1994 -in root.csr -out root.crt
   ```

   执行效果：

   ![image-20210803003834607](../repository/image-20210803003834607.png)

4. 查看一下生成的证书

   ```bash
   # 查看生成的证书
   openssl x509 -in root.crt -noout -text -purpose
   ```

   执行效果：

   ![image-20210803003902708](../repository/image-20210803003902708.png)

### 生成（根证书签名的）服务端证书

> 提示：
>
> - 第一二步与生成根证书是一样的，只有第三部略有差别
> - 这里生成服务端证书，就懒得对server.key自身进行加密了

1. 生成一个`.key`私钥

   ```bash
   openssl genrsa -out server.key 2048
   ```

   执行效果：

   ![image-20210803004043990](../repository/image-20210803004043990.png)

2. 生成`.csr`证书申请请求

   ```bash
   openssl req -new -key server.key -out server.csr -subj "/C=CN/ST=GuangDong/L=ShenZhen/CN=www.niantou.com/O=www.niantou.com/emailAddress=1612513157@qq.com/OU=dev"
   ```

   执行效果：

   ![image-20210803004114362](../repository/image-20210803004114362.png)

3. 生成(根证书签名了的)`.crt`证书

   ```bash
   # 因为root.key本身是经过加密的，这里使用到了root.key的话，需要输入自身加密时用到的面，如这里的ds1994
   openssl x509 -req -days 730 -sha256 -extensions v3_req -CA root.crt -CAkey root.key -passin pass:ds1994 -CAcreateserial -in server.csr -out server.crt 
   ```

   执行效果：

   ![image-20210803004212124](../repository/image-20210803004212124.png)

4. 查看一下生成的证书

   ```bash
   # 查看生成的证书
   openssl x509 -in server.crt -noout -text -purpose
   ```

   执行效果：

   ![image-20210803004350834](../repository/image-20210803004350834.png)

### 生成（根证书签名的）客户端证书

> 提示：
>
> - 第一二步与生成根证书是一样的，只有第三部略有差别
> - 这里生成客户端证书，就懒得对client.key自身进行加密了

1. 生成一个`.key`私钥

   ```bash
   openssl genrsa -out client.key 2048
   ```

   执行效果：

   ![image-20210803004504448](../repository/image-20210803004504448.png)

2. 生成`.csr`证书申请请求

   ```bash
   openssl req -new -key client.key -out client.csr -subj "/C=CN/ST=GuangDong/L=ShenZhen/CN=www.niantou.com/O=www.niantou.com/emailAddress=1612513157@qq.com/OU=dev"
   ```

   执行效果：

   ![image-20210803004556378](../repository/image-20210803004556378.png)

3. 生成（根证书签名了的）`.crt`证书

   ```bash
   openssl x509 -req -days 730 -sha256 -extensions v3_req -CA root.crt -CAkey root.key -passin pass:ds1994 -CAcreateserial -in client.csr -out client.crt 
   ```

   执行效果：

   ![image-20210803004649936](../repository/image-20210803004649936.png)

5. 查看一下生成的证书

   ```bash
   # 查看生成的证书
   openssl x509 -in client.crt -noout -text -purpose
   ```

   执行效果：

   ![image-20210803004717037](../repository/image-20210803004717037.png)

## 生成的证书文件说明

![image-20210803004728817](../repository/image-20210803004728817.png)

## 使用证书

### nginx配置证书（双向认证）

![image-20210803004824900](../repository/image-20210803004824900.png)

- `ssl_certificate`：服务器证书(提示：证书内部包含有对应的公钥)

- `ssl_certificate_key`：服务器私钥

- `ssl_client_certificate`：CA公钥地址(用以验证客户端证书是否是同一CA签发)

- `ssl_verify_clien`t：on 打开双向认证

注：配置好后需要`service nginx reload`重新加载`nginx`
注：如果只需要单向认证的话，那么配置`ssl_certificate`和`ssl_certificate_key`即可

### java程序配置证书（以spring-boot为例）

#### 单向认证

##### 准备工作：

> （`java`程序无法直接使用`crt`文件，所以先）将服务端的证书`server.crt`进行`pkcs12`分析转换

```bash
# openssl pkcs12解析server.crt转换为密码为pwd123的(加密后的)server.pfx文件
openssl pkcs12 -export -in server.crt -inkey server.key -certfile root.crt -out server.pfx -passout pass:为pwd123
```

执行效果：

![image-20210803005221466](../repository/image-20210803005221466.png)

##### 项目结构

![image-20210803005237452](../repository/image-20210803005237452.png)

##### 配置文件

```properties
# 指定https的端口
server.port=9527
# 指定证书
server.ssl.key-store=classpath:server.pfx
# server.ssl.key-store文件本身加密时的密码
server.ssl.key-store-password=pwd123
```

##### 配置类和测试类

```java
import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * start class
 *
 * @author JustryDeng
 * @since 2021/2/22 21:45:17
 */
@RestController
@SpringBootApplication
public class ImageProcessApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(ImageProcessApplication.class, args);
    }
    
    @GetMapping("/test")
    public String test() {
        return "hello~";
    }
    
    @Value("${server.port}")
    private int httpsPort;
    
    /**
     * springboot2.x + 写法
     */
    @Bean
    public TomcatServletWebServerFactory servletContainer() {
        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                SecurityConstraint constraint = new SecurityConstraint();
                constraint.setUserConstraint("CONFIDENTIAL");
                SecurityCollection collection = new SecurityCollection();
                collection.addPattern("/*");
                constraint.addCollection(collection);
                context.addConstraint(constraint);
            }
        };
        tomcat.addAdditionalTomcatConnectors(httpConnector());
        return tomcat;
    }
    
    @Bean
    public Connector httpConnector() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        connector.setScheme("http");
        // Connector监听的http的端口号
        connector.setPort(8080);
        connector.setSecure(false);
        // http重定向至https
        connector.setRedirectPort(httpsPort);
        return connector;
    }
}
```

##### 测试一下

- 使用`https`访问`https://localhost:9527/test` ，成功

  ![image-20210803005430372](../repository/image-20210803005430372.png)

- 使用`http`访问`http://localhost:8080/test`，请求会自动重定向为`https://localhost:9527/test` ，最终还是一个`https`的请求

  ![image-20210803005458320](../repository/image-20210803005458320.png)

#### 双向认证

> 提示：双向认证当然也需要单向认证时需要的文件

##### 相关知识说明

![image-20210803005533442](../repository/image-20210803005533442.png)

![image-20210803005538843](../repository/image-20210803005538843.png)



##### 准备工作

- 处理一下给客户端使用的证书

> (在某些场景下无法直接使用`crt`文件，所以先)将客户端的证书`client.crt`进行`pkcs12`分析转换

```ba
# openssl pkcs12解析client.crt转换为密码为JustryDeng的(加密后的)server.pfx文件
openssl pkcs12 -export -in client.crt -inkey client.key -certfile root.crt -out client.pfx -passout pass:JustryDeng
```

执行效果：

![image-20210803005701221](../repository/image-20210803005701221.png)

- 生成维护信任哪些证书的文件`server_trust.keystore`

```bash
# 生成维护信任哪些证书的server_trust.keystore文件, 并向这个文件中添加client.crt为可信任证书； 设置server_trust.keystore本身的密码为qwer996
keytool -importcert -trustcacerts -alias www.niantou.com -file client.crt -keystore server_trust.keystore -storepass qwer996
```

执行效果：

![image-20210803005755334](../repository/image-20210803005755334.png)

注：`keytool`是`jdk`自带的工具，如果直接输入报错找不到指令，那么可以进入`jdk`的`bin`目录下再执行`keytool`指令

##### 项目结构

![image-20210803005842411](../repository/image-20210803005842411.png)

##### 配置文件

```properties
# 指定https的端口
server.port=9527
# 指定证书
server.ssl.key-store=classpath:server.pfx
# server.ssl.key-store文件本身加密时的密码
server.ssl.key-store-password=pwd123

# 表示服务端需要验证客户端的证书
server.ssl.client-auth=need
server.ssl.trust-store=classpath:server_trust.keystore
# server.ssl.trust-store文件本身加密时的密码
server.ssl.trust-store-password=qwer996
```

##### 配置类和测试类

```java
import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * start class
 *
 * @author JustryDeng
 * @since 2021/2/22 21:45:17
 */
@RestController
@SpringBootApplication
public class ImageProcessApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(ImageProcessApplication.class, args);
    }
    
    @GetMapping("/test")
    public String test() {
        return "hello~";
    }
    
    @Value("${server.port}")
    private int httpsPort;
    
    /**
     * springboot2.x + 写法
     */
    @Bean
    public TomcatServletWebServerFactory servletContainer() {
        TomcatServletWebServerFactory tomcat = new TomcatServletWebServerFactory() {
            @Override
            protected void postProcessContext(Context context) {
                SecurityConstraint constraint = new SecurityConstraint();
                constraint.setUserConstraint("CONFIDENTIAL");
                SecurityCollection collection = new SecurityCollection();
                collection.addPattern("/*");
                constraint.addCollection(collection);
                context.addConstraint(constraint);
            }
        };
        tomcat.addAdditionalTomcatConnectors(httpConnector());
        return tomcat;
    }
    
    @Bean
    public Connector httpConnector() {
        Connector connector = new Connector("org.apache.coyote.http11.Http11NioProtocol");
        connector.setScheme("http");
        // Connector监听的http的端口号
        connector.setPort(8080);
        connector.setSecure(false);
        // http重定向至https
        connector.setRedirectPort(httpsPort);
        return connector;
    }
}
```

##### 测试一下

- 先不配置客户端证书，访问`https://localhost:9527/test `，提示需要证书

  ![image-20210803005949069](../repository/image-20210803005949069.png)

- 浏览器安装证书(以`chrome`为例，进入其设置，以添加证书)

  ![image-20210803010006214](../repository/image-20210803010006214.png)

  ![image-20210803010010034](../repository/image-20210803010010034.png)

  ![image-20210803010014143](../repository/image-20210803010014143.png)

  ![image-20210803010023239](../repository/image-20210803010023239.png)

  注：不论是导入`.pfx`格式的还是`.p12`格式的，还是其它格式的证书；千万记得在它提示【导入成功】后，检查一下证书列表中是否真的导入成功(见下面的截图)了。有时，导入`.crt`文件时，提示导入成功，但是实际上没有成功。

  ![image-20210803010047927](../repository/image-20210803010047927.png)

  ![image-20210803010056330](../repository/image-20210803010056330.png)

- 再次访问`https://localhost:9527/test` ，发现正常了

  ![image-20210803010114782](../repository/image-20210803010114782.png)

  ![image-20210803010118079](../repository/image-20210803010118079.png)

  访问`http://localhost:8080/test` 也能跳转至`https://localhost:9527/test` 进而正常访问

  ![image-20210803010137548](../repository/image-20210803010137548.png)

---

## 相关资料

- [`https://blog.csdn.net/bbwangj/article/details/82503675`](https://blog.csdn.net/bbwangj/article/details/82503675)

- [`https://www.jb51.cc/note/422210.html`](https://www.jb51.cc/note/422210.html)

