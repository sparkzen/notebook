# Ubuntu常用指令

- [Ubuntu常用指令](#ubuntu常用指令)
  - [设置允许root用户ssh登录ubuntu](#设置允许root用户ssh登录ubuntu)

---

## 设置允许root用户ssh登录ubuntu

1. 在`/etc/ssh/sshd_config`文件最后一行追加

```bash
PermitRootLogin yes
```

2. 然后输入以下指令刷新配置

```bash
/etc/init.d/sshd restart
```



