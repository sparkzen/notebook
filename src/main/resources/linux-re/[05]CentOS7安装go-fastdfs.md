# CentO S7安装go-fastdfs

- [CentO S7安装go-fastdfs](#cento-s7安装go-fastdfs)
  - [CentO S7安装go-fastdfs](#cento-s7安装go-fastdfs-1)
    - [第一步：去官网下载安装包](#第一步去官网下载安装包)
    - [第二步：创建对应的文件夹并上传安装包](#第二步创建对应的文件夹并上传安装包)
    - [第三步：赋予可执行的权限](#第三步赋予可执行的权限)
    - [第四步：直接运行下，使生成相关文件](#第四步直接运行下使生成相关文件)
    - [第五步（可选）：修改配置文件](#第五步可选修改配置文件)
    - [第六步：启动go-fastdfs](#第六步启动go-fastdfs)
    - [测试一下](#测试一下)
  - [相关资料](#相关资料)

## CentO S7安装go-fastdfs

### 第一步：去[官网](https://github.com/sjqzhang/go-fastdfs/releases)下载安装包

![image-20220622023556332](../repository/image-20220622023556332.png)



### 第二步：创建对应的文件夹并上传安装包

```shell
cd /opt/
mkdir gofastdfs
```



![image-20220622024137061](../repository/image-20220622024137061.png)

### 第三步：赋予可执行的权限

```shell
cd gofastdfs
chmod +x fileserver
```

![image-20220622024305332](../repository/image-20220622024305332.png)

### 第四步：直接运行下，使生成相关文件

```shell
./fileserver server
# 运行一下，然后ctrl+c进行取消，目的是生成好配置文件
# 开始目录下没有文件的，执行下面的命令后再取消，可以看到目录下的配置文件成功自动生成
```

![image-20220622024501345](../repository/image-20220622024501345.png)

### 第五步（可选）：修改配置文件

> 可指定端口、集群、鉴权、白名单之类的

```shell
vim conf/cfg.json
```

### 第六步：启动go-fastdfs

```shell
# 前台运行
#./fileserver server
# 后台运行（需要有nohup，否者shell客户端断开后，子进程将会被挂起，进而停止；所以需要有nohup）
nohup ./fileserver server &
```

![image-20220622025139229](../repository/image-20220622025139229.png)

### 测试一下

访问：127.0.0.1:10000，注意：其默认端口是8080，本人在第五步的时候修改了端口

![image-20220622025509925](../repository/image-20220622025509925.png)

## 相关资料

- [CSDN](https://blog.csdn.net/jxlhljh/article/details/123086876)