# CentOS7 firewall防火墙&端口相关指令

- [CentOS7 firewall防火墙&端口相关指令](#centos7-firewall防火墙端口相关指令)
  - [firewall防火墙&端口相关指令](#firewall防火墙端口相关指令)

## firewall防火墙&端口相关指令

- **开启firewall防火墙：**`systemctl start firewalld.service`

- **关闭firewall防火墙：**`systemctl stop firewalld.service`

- **查看默认防火墙状态：**`firewall-cmd --state`

- **重新载入(重启)防火墙：**`firewall-cmd --reload`

- **设置firewall防火墙开机启动：**`systemctl enable firewalld.service`

- **禁止firewall防火墙开机启动：**`systemctl disable firewalld.service`

- **开放指定端口：**`firewall-cmd --zone=public --add-port={端口号}/tcp --permanent`

  注：--permanent表示永久生效，没有此参数重启后失效

  注：需要firewall-cmd --reload重新载入后才能生效

- **移除开放的端口：**`firewall-cmd --zone=public --remove-port={端口号}/tcp --permanent`

  注：--permanent表示永久生效，没有此参数重启后失效

  注：需要firewall-cmd --reload重新载入后才能生效

- **查询指定端口是否开放：**`firewall-cmd --zone=public --query-port={端口号}/tcp`

- **查看所有开放的端口：**`firewall-cmd --zone=public --list-ports`

- **查看所有被占用的端口的情况：**`netstat -tunlp`

- **查看指定端口的占用情况：**`netstat -tunlp|grep {端口号}`

  执行效果如图（其中21501即为进程号）：

  ```shell
  [root@iz2ze3g8ar99kw6tbd22xrz nginx]# netstat -tunlp|grep 8182
  tcp        0      0 0.0.0.0:8182            0.0.0.0:*               LISTEN      21501/nginx: master
  ```

  