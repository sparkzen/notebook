# smart-doc工具类

- [smart-doc工具类](#smart-doc工具类)
  - [相关依赖](#相关依赖)
  - [工具类](#工具类)
  - [配置静态资源映射](#配置静态资源映射)

## 相关依赖

> 在pom中引入依赖

```xml
<dependency>
    <groupId>com.github.shalousun</groupId>
    <artifactId>smart-doc</artifactId>
    <version>2.4.2</version>
    <scope>test</scope>
</dependency>
```

## 工具类

> 提示：放于test包下

```java
import com.ideaaedi.commonds.path.PathUtil;
import com.power.doc.builder.HtmlApiDocBuilder;
import com.power.doc.model.ApiConfig;
import com.power.doc.model.SourceCodePath;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * smart-doc文档生成 &emsp
 * <a href="https://smart-doc-group.github.io/#/zh-cn/diy/config">官网说明</a>
 * <a href="https://gitee.com/smart-doc-team/smart-doc/blob/master/src/test/java/com/power/doc/ApiDocTest.java">code使用示例</a>
 *
 * @author kuoyi
 * @since 1.0.0
 */
@SpringBootTest
class SmartDocHelper {
    
    @Value("${server.port:8080}")
    private int port;
    
    @Test
    void generate() {
        String outputRootDir = PathUtil.getProjectRootDir(SmartDocHelper.class)
                .replace("/target/test-classes/", "/src/main/resources/api-doc/");
        /// 先清空文件夹
        ///IOUtil.delete(new File(outputRootDir));
        
        ApiConfig config = new ApiConfig();
        config.setRequestFieldToUnderline(false);
        config.setResponseFieldToUnderline(false);
        config.setCreateDebugPage(true);
        String url = "http://localhost:" + port;
        config.setServerUrl(url);
        config.setDebugEnvUrl(url);
        config.setOutPath(outputRootDir);
        config.setAuthor("邓沙利文");
        config.setAllInOne(true);
        config.setAllInOneDocFileName("api.html");
        String currRoot = PathUtil.getProjectRootDir(SmartDocHelper.class)
                .replace("/target/test-classes/", "");
        config.setSourceCodePaths(
                SourceCodePath.builder().setPath(currRoot)
        );
        
        
        // 生成文档
        /// OpenApiBuilder.buildOpenApi(config);
        /// RpcTornaBuilder.buildApiDoc(config);
        /// TornaBuilder.buildApiDoc(config);
        /// RpcHtmlBuilder.buildApiDoc(config);
        HtmlApiDocBuilder.buildApiDoc(config);
    }
}
```

## 配置静态资源映射

> 在配置文件中进行配置

```properties
# api-doc静态资源映射 (api文档访问 http://{address}:{port}/api-doc/api.html 即可)
spring.mvc.static-path-pattern=/api-doc/**
spring.web.resources.static-locations=classpath:/api-doc/
```

