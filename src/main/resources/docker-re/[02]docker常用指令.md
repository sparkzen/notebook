# docker常用指令

- [docker-compose相关指令](#docker-compose相关指令)
  - [指令](#指令)

## 指令

| 指令                                                         | 说明                                                         |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| `yum list installed | grep docker`                           | 查看安装过的Docker                                           |
| `docker exec -it 容器id(或容器name) /bin/bash`               | 进入正在运行的指定id(name)容器的shell界面                    |
| `docker run --name 容器名 -d 镜像名`                         | 使用镜像创建名为"容器名"的容器                               |
| `docker run -it 镜像名 /bin/bash`                            | 运行指定镜像创建容器,并进入该容器的shell界面                 |
| `docker run --name 容器名 -it 镜像名 /bin/bash`              | 运行指定镜像创建指定容器名的容器,并进入该容器的shell界面     |
| `docker run --name 容器名 -d 镜像名`                         | 使用镜像创建名为“容器名”的容器                               |
| `ctrl + p + q `                                              | 退出但不关闭当前容器<br />注：退出后docker ps 查看有         |
| `ctrl + d`                                                   | 退出且关闭当前容器<br />注：退出后 docker ps 查看无          |
| `docker ps [OPTIONS]`                                        | 列出正在运行的容器注<br />可选参数有：<br />-a：显示所有的容器，包括未运行的<br />-f：根据条件过滤显示的内容<br />--format：指定返回值的模板文件<br />-l：显示最近创建的容器<br />-n：列出最近创建的n个容器<br />--no-trunc：不截断输出<br />-q：静默模式，只显示容器编号<br />-s：显示总的文件大小 |
| `docker cp 容器id(或容器name):要复制的文件全路径文件(夹)名 要复制到Linux上的文件夹位置` | 复制指定容器ID(或指定容器名)的容器中的指定文件到 Linux下的指定位置<br />如：`docker cp my-docker:/logDir/sql.2018-09-07.log /root/` |
| `sudo docker cp Linux上要复制的文件(夹)全路径文件名 容器id(或容器name):要复制到Docker中的文件夹位置` | 复制Linux下制定文件 到 指定容器ID(或指定容器名)的容器中的指定路径下<br />如：`sudo docker cp /root/mailTemplate.xlsx spc-middlenum-dao-custom01:/var/axb/excel/template` |
| `docker logs [OPTIONS] 容器ID(或容器name) `                  | 查看日志信息<br />注：可选参数有：<br />--details显示更多的信息<br />-f(或--follow)跟踪实时日志<br />--since string 显示自某个timestamp之后的日志，或相对时间。如--since "2018-09-08"<br />--tail string从日志末尾显示多少行日志， 默认是all，如`--tail=1000`<br />-t(或--timestamps)显示时间戳-<br />-until string显示自某个timestamp之前的日志,或相对时间 |
| `docker start 容器ID(或容器name)`                            | 启动(现有)容器                                               |
| `docker stop 容器ID(或容器name)`                             | 关闭容器                                                     |
| `docker stop $(docker ps -a -q)`                             | 关闭所有的容器                                               |
| `docker rm $(docker ps -a -q)`                               | 删除所有的容器                                               |
| 进入容器，运行`cat /etc/issue`                               | 查看Docker容器的Linux版本                                    |
| `docker images`                                              | 查看当前有哪些镜像                                           |
| `docker rmi 镜像ID(或镜像名)`                                | 删除指定镜像<br />注：删除前,需要先停止该镜像对应的容器      |
| `docker rmi $(docker images -q)`                             | 删除所有镜像                                                 |
| `docker inspect -f{{.NetworkSettings.IPAddress}} 容器ID(或容器名)` | 查看指定容器的IP                                             |
| `systemctl restart docker`                                   | 重启docker                                                   |
| ……                                                           |                                                              |

提示：容器NAME就像是容器ID的别名一样（容器ID不好记，而容器NAME好记）

**判断是否进入了容器：**

没有进入docker容器前,指令前面的字母有[]号包着，如：

![1](../repository/1-1656564009317.png)

 

进入docker容器后,指令前面的字母没有[]号包着，如：

![2](../repository/2-1656564013196.png)

 