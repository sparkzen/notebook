# docker-compose相关指令

- [docker-compose相关指令](#docker-compose相关指令)
  - [指令](#指令)

## 指令

|                         指令                          | 说明                                                         |
| :---------------------------------------------------: | ------------------------------------------------------------ |
|                   docker-compose ps                   | 列出所有运行容器                                             |
|         docker-compose logs {container-name}          | 查看服务日志输出                                             |
|        docker-compose logs -f {container-name}        | （实时）查看服务日志输出<br />注：ctrl + c即可退出           |
|  docker-compose logs -f {container-name} >> tmp.log   | 将日志输出到文件（前台进行）                                 |
| docker-compose logs -f {container-name} >> tmp.log  & | 将日志输出到文件（后台进行）                                 |
|         docker-compose start {container-name}         | 启动docker-compose中的指定容器                               |
|        docker-compose restart {container-name}        | 重启docker-compose中的指定容器                               |
|         docker-compose stop {container-name}          | 停止docker-compose中的指定容器                               |
|          docker-compose rm {container-name}           | 移除docker-compose中的指定容器                               |
|          docker-compose up {container-name}           | 前台启动docker-compose中的指定容器                           |
|         docker-compose up -d {container-name}         | 后台启动docker-compose中的指定容器                           |
|                   docker-compose up                   | 前台启动docker-compose<br />注：自动检查docker-compose.yml中配置的所有容器是否均已启动，如果未启动，将自动（创建并）启动 |
|                 docker-compose up -d                  | 后台启动docker-compose<br />注：自动检查docker-compose.yml中配置的所有容器是否均已启动，如果未启动，将自动（创建并）启动 |
|         docker-compose kill {container-name}          | 通过发送 SIGKILL 信号来停止指定服务的容器                    |
|       docker-compose  logs -f {container-name}        | 实时查看指定容器的日志                                       |
|                        ......                         | ......                                                       |



