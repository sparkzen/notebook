package com.ideaaedi.notebook.helper;

import java.util.List;

/**
 * 节点
 *
 * @author JustryDeng
 * @since 2022/4/25 14:41
 */
public class Node {
    
    /** key */
    private String key;
    
    /** 父key */
    private String parentKey;
    
    /** md文件文件名(如果不是md文件，则此字段为null) */
    private String mdSimpleFileName;
    
    /** 文件夹名(如果不是文件夹，则此字段为null) */
    private String dirSimpleFileName;
    
    /** 缩进 */
    private String indentation;
    
    /** 子节点 */
    private List<Node> children;
    
    public String getDirSimpleFileName() {
        return dirSimpleFileName;
    }
    
    public void setDirSimpleFileName(String dirSimpleFileName) {
        this.dirSimpleFileName = dirSimpleFileName;
    }
    
    public String getKey() {
        return key;
    }
    
    public void setKey(String key) {
        this.key = key;
    }
    
    public String getParentKey() {
        return parentKey;
    }
    
    public void setParentKey(String parentKey) {
        this.parentKey = parentKey;
    }
    
    public String getIndentation() {
        return indentation;
    }
    
    public void setIndentation(String indentation) {
        this.indentation = indentation;
    }
    
    public List<Node> getChildren() {
        return children;
    }
    
    public void setChildren(List<Node> children) {
        this.children = children;
    }
    
    public String getMdSimpleFileName() {
        return mdSimpleFileName;
    }
    
    public void setMdSimpleFileName(String mdSimpleFileName) {
        this.mdSimpleFileName = mdSimpleFileName;
    }
}
