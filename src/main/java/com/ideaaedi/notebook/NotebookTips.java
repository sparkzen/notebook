package com.ideaaedi.notebook;

/**
 * 君不见黄河之水天上来，奔流到海不复回。
 *
 * @author JustryDeng
 * @since 2021/7/28 0:53:01
 */
public class NotebookTips {
    
    /**
     * @see Notebook#buildImages()
     */
    public static void main(String[] args) {
        System.out.println("1. 如果您预览不了md文件中的图片，您可以尝试调用一下Notebook#buildImages()方法来解决此问题.");
        System.out.println("2. 当引入com.idea-aedi:notebook:{版本号}依赖时，建议将此依赖的scope设置为test，以避免给你的项目jar/war包带来额外的空间占用.");
    }
}
