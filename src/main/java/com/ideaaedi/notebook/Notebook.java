package com.ideaaedi.notebook;

import com.ideaaedi.notebook.helper.Node;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * 构建图片
 *
 * @author JustryDeng
 * @since 2021/7/28 0:53:01
 */
public class Notebook {
    
    private static final String APPEND_FROM_HERE = "### 目录";
    
    private static final String DIRECTORY_FILENAME = "README.md";
    
    private static final String TARGET_FILE_SUFFIX = ".md";
    
    /** resourcesDir相对目录文件所在Dir的位置 */
    private static final String RELATIVE_POSITION = "./src/main/resources/";
    
    /** 排除的文件的后缀 */
    private static final String[] EXCLUDE_FILE_NAME_SUFFIX_ARRAY = new String[]{"xxx.md"};
    
    public static void main(String[] args) {
        // buildImages();
        buildTableOfContents();
    }
    
    /**
     * 构建图片
     * <p>
     * 若不构建，预览.md文件时，可能加载图片失败
     * </p>
     */
    public static void buildImages() {
        String projectRootDir = getProjectRootDir();
        // 只有jar包才会进行build逻辑
        if (!projectRootDir.endsWith(".jar")) {
            System.err.println("Only jar file need buildImages.");
            return;
        }
        File file = new File(projectRootDir);
        String parent = file.getParent();
        parent = parent == null ? "/" : parent;
        System.out.println("Build images to " + new File(parent, "repository").getAbsolutePath() + ".");
        unJarWar(projectRootDir, parent, Collections.singletonList("repository/"));
        System.out.println("Build images completed.");
    }
    
    /**
     * 构建文章目录
     */
    public static void buildTableOfContents() {
        String projectRootDir = getProjectRootDir();
        if (!new File(projectRootDir).isDirectory()) {
            System.err.println("Skip. projectRootDir is nor dir.");
            return;
        }
        String projectDir = projectRootDir.substring(0, projectRootDir.length() - "target/classes/".length());
        String contentOfTableFilepath = projectDir + DIRECTORY_FILENAME;
        String resourcesDir = projectDir + "src/main/resources/";
        // 获取md文件相对路径
        List<String> mdRelativeFilepathList = buildRelativeFilepath(resourcesDir);
        // 构建node关系
        LinkedHashMap<String, Node> nodeMap = buildNodeMap(mdRelativeFilepathList);
        // 排除指定文件
        for (String fileNameSuffix : EXCLUDE_FILE_NAME_SUFFIX_ARRAY) {
            Set<String> toRemoveKeySet = new HashSet<>();
            nodeMap.forEach((k, v) -> {
                if (k.endsWith(fileNameSuffix)) {
                    toRemoveKeySet.add(k);
                }
            });
            toRemoveKeySet.forEach(nodeMap::remove);
        }
        // 构建目录内容
        StringBuilder tableOfContentsBuilder = buildTableOfContents(RELATIVE_POSITION, nodeMap);
        // 输出文件
        String oldReadmeContent = readContentFromFile(new File(contentOfTableFilepath));
        int tableStartIndex = oldReadmeContent.indexOf(APPEND_FROM_HERE);
        String newReadmeContent =
                oldReadmeContent.substring(0, tableStartIndex + APPEND_FROM_HERE.length() + 1) + tableOfContentsBuilder;
        File readmeFile = new File(contentOfTableFilepath);
        toFile(newReadmeContent.getBytes(StandardCharsets.UTF_8), readmeFile);
        System.out.println("Update readme completed. See [" + readmeFile.getAbsolutePath() + "].");
    }
    
    /* ----------------------------------- 以下为私有方法 ----------------------------------- */
    
    /**
     * 解压jar(or war)至指定的目录
     *
     * @param jarWarPath
     *         待解压的jar(or war)文件
     * @param targetDir
     *         解压后文件放置的文件夹
     * @param entryNamePrefixes
     *         只有当entryName为指定的前缀时，才对该entry进行解压(若为null或者长度为0， 则解压所有文件)   如: ["BOOT-INF/classes/",
     *         "BOOT-INF/classes/com/example/ssm/author/JustryDeng.class"] <br/> 注:当entry对应jar或者war中的目录时，那么其值形如
     *         BOOT-INF/classes/ <br/> 注:当entry对应jar或者war中的文件时，那么其值形如
     *         BOOT-INF/classes/com/example/ssm/author/JustryDeng.class
     *
     * @return 解压出来的文件(包含目录)的完整路径
     */
    private static <T extends Collection<String>> List<String> unJarWar(String jarWarPath, String targetDir,
                                                                        T entryNamePrefixes) {
        List<String> list = new ArrayList<>();
        File target = new File(targetDir);
        guarantyDirExist(target);
        
        ZipFile zipFile = null;
        try {
            zipFile = new ZipFile(new File(jarWarPath));
            ZipEntry entry;
            File targetFile;
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                entry = entries.nextElement();
                String entryName = entry.getName();
                // 若entryNamePrefixes不为空，则不解压前缀不匹配的文件或文件夹
                if (entryNamePrefixes != null && entryNamePrefixes.size() > 0
                        && entryNamePrefixes.stream().noneMatch(entryName::startsWith)) {
                    continue;
                }
                if (entry.isDirectory()) {
                    targetFile = new File(target, entryName);
                    guarantyDirExist(targetFile);
                } else {
                    // 有时遍历时，文件先于文件夹出来，所以也得保证目录存在
                    int lastSeparatorIndex = entryName.lastIndexOf("/");
                    if (lastSeparatorIndex > 0) {
                        guarantyDirExist(new File(target, entryName.substring(0, lastSeparatorIndex)));
                    }
                    // 解压文件
                    targetFile = new File(target, entryName);
                    byte[] bytes = toBytes(zipFile.getInputStream(entry));
                    toFile(bytes, targetFile);
                    list.add(targetFile.getAbsolutePath());
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            close(zipFile);
        }
        return list;
    }
    
    /**
     * 保证目录存在
     *
     * @param dir
     *         目录
     */
    private static void guarantyDirExist(File dir) {
        if (!dir.exists()) {
            //noinspection ResultOfMethodCallIgnored
            dir.mkdirs();
        }
    }
    
    /**
     * 将inputStream转换为byte[]
     * <p>
     * 注：此方法会释放inputStream
     * </p>
     *
     * @param inputStream
     *         输入流
     *
     * @return 字节
     */
    private static byte[] toBytes(InputStream inputStream) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            byte[] buffer = new byte[4096];
            int n;
            while (-1 != (n = inputStream.read(buffer))) {
                output.write(buffer, 0, n);
            }
            return output.toByteArray();
        } finally {
            close(output, inputStream);
        }
    }
    
    /**
     * 获取targetDir目录(及子孙目录)下的md文件相对路径
     * <p>
     * 相对路径即：md文件的绝对路径 截去targetDir的绝对路径
     * </p>
     *
     * @param targetDir
     *         要查找的目录
     *
     * @return resourcesDir目录(及子孙目录)下的md文件相对路径
     */
    private static List<String> buildRelativeFilepath(String targetDir) {
        return listFileOnly(new File(targetDir), TARGET_FILE_SUFFIX)
                .stream()
                .map(mdFile -> {
                    String filepath = mdFile.getAbsolutePath().replace("\\", "/");
                    return filepath.substring(targetDir.length());
                }).collect(Collectors.toList());
    }
    
    /**
     * 构建node节点
     *
     * @param mdRelativeFilepathList
     *         {@link Notebook#buildRelativeFilepath(String)}返回的相对路径
     *
     * @return md文件、md文件所在文件夹的节点信息
     */
    private static LinkedHashMap<String, Node> buildNodeMap(List<String> mdRelativeFilepathList) {
        String fixedIndentation = "  ";
        LinkedHashMap<String, Node> nodeMap = new LinkedHashMap<>();
        for (String relativeFilepath : mdRelativeFilepathList) {
            String[] fileNameArray = relativeFilepath.split("/");
            if (fileNameArray.length == 0) {
                continue;
            }
            for (int i = 0; i < fileNameArray.length; i++) {
                String simpleFileName = fileNameArray[i];
                if (nodeMap.containsKey(simpleFileName)) {
                    continue;
                }
                Node node = new Node();
                if (i == 0) {
                    node.setKey(simpleFileName);
                    node.setIndentation("");
                    node.setParentKey("");
                } else {
                    StringBuilder parentKeyBuilder = new StringBuilder();
                    for (int j = 0; j < i; j++) {
                        if (parentKeyBuilder.length() != 0) {
                            parentKeyBuilder.append("/");
                        }
                        parentKeyBuilder.append(fileNameArray[j]);
                    }
                    String parentKey = parentKeyBuilder.toString();
                    Node parent = nodeMap.get(parentKey);
                    node.setParentKey(parent.getKey());
                    node.setKey(parentKey + "/" + simpleFileName);
                    node.setIndentation(parent.getIndentation() + fixedIndentation);
                }
                if (simpleFileName.endsWith(TARGET_FILE_SUFFIX)) {
                    node.setMdSimpleFileName(simpleFileName);
                } else {
                    node.setDirSimpleFileName(simpleFileName);
                }
                nodeMap.put(node.getKey(), node);
            }
        }
        return nodeMap;
    }
    
    /**
     * 组装目录内容
     *
     * @param relativePosition
     *         targetDir目录(即：{@link Notebook#buildRelativeFilepath(String)}的参数所代表的的目录)
     *         相对本markdown文件（即：要把内容写去的markdown文件）的相对位置
     * @param nodeMap
     *         md文件、md文件所在文件夹的节点信息
     *
     * @return 目录内容
     */
    private static StringBuilder buildTableOfContents(String relativePosition, LinkedHashMap<String, Node> nodeMap) {
        StringBuilder tableOfContentsBuilder = new StringBuilder();
        for (Node node : nodeMap.values()) {
            if (tableOfContentsBuilder.length() != 0) {
                tableOfContentsBuilder.append("\n");
            }
            tableOfContentsBuilder.append(node.getIndentation()).append("- ");
            String key = node.getKey();
            String mdSimpleFileName = node.getMdSimpleFileName();
            if (mdSimpleFileName != null) {
                mdSimpleFileName = mdSimpleFileName.substring(0,
                        mdSimpleFileName.length() - TARGET_FILE_SUFFIX.length());
                // 将空格换为%20，使得URL能被所有markdown工具识别
                key = key.replace(" ", "%20");
                tableOfContentsBuilder.append("[").append(mdSimpleFileName).append("]")
                        .append("(")
                        .append(relativePosition).append(key)
                        .append(")");
            } else if (node.getDirSimpleFileName() != null) {
                tableOfContentsBuilder.append(node.getDirSimpleFileName());
            } else {
                throw new IllegalArgumentException("node is illegal.\t" + node);
            }
        }
        return tableOfContentsBuilder;
    }
    
    /**
     * 读取文件内容
     *
     * @param file
     *         文件
     *
     * @return 内容
     */
    private static String readContentFromFile(File file) {
        StringBuilder content = new StringBuilder();
        FileInputStream fileInputStream = null;
        InputStreamReader read = null;
        BufferedReader bufferedReader = null;
        try {
            fileInputStream = new FileInputStream(file);
            read = new InputStreamReader(fileInputStream, StandardCharsets.UTF_8);
            bufferedReader = new BufferedReader(read);
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line).append(System.lineSeparator());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            close(bufferedReader, read, fileInputStream);
        }
        return content.toString();
    }
    
    /**
     * 关闭流
     *
     * @param ioArr
     *         待关闭的io
     */
    private static void close(Closeable... ioArr) {
        if (ioArr == null) {
            return;
        }
        for (Closeable io : ioArr) {
            if (io == null) {
                continue;
            }
            try {
                io.close();
            } catch (IOException e) {
                // ignore
            }
        }
    }
    
    /**
     * 获取clazz类的全类名对应包的根路径
     *
     * <ul>
     *     <li>如果编译后未打包时直接调用这个方法(即:直接在开发工具里调用此方法)，那么结果形如:
     *     D:/资料整理/demo模板/class-winter/class-winter-core/target/classes/</li>
     *     <li>如果是jar包运行时，(clazz位于jar包中，)调用这个方法，(假设jar包位置是D:/资料整理/demo模板/abc.jar)那么结果形如：   D:/资料整理/demo模板/abc.jar</li>
     * </ul>
     *
     * @return 项目根目录
     */
    private static String getProjectRootDir() {
        URL url = Notebook.class.getProtectionDomain().getCodeSource().getLocation();
        String filePath;
        try {
            filePath = URLDecoder.decode(url.getPath(), StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
        File file = new File(filePath);
        String slash = "/";
        filePath = file.getAbsolutePath().replace("\\", slash);
        // 如果file是文件夹，那么保证filePath是以/结尾的
        if (file.isDirectory() && !filePath.endsWith(slash)) {
            filePath = filePath + slash;
        }
        return filePath;
    }
    
    /**
     * 将srcFileBytes写出为destFile文件
     * <p>
     * 注: 若源文件存在，则会覆盖原有的内容。
     * </p>
     *
     * @param srcFileBytes
     *         字节
     * @param destFile
     *         文件
     */
    private static void toFile(byte[] srcFileBytes, File destFile) {
        OutputStream os = null;
        try {
            if (destFile.isDirectory()) {
                throw new RuntimeException("destFile [" + destFile.getAbsolutePath() + "] must be file rather than "
                        + "dir.");
            }
            
            if (!destFile.exists()) {
                File parentFile = destFile.getParentFile();
                if (!parentFile.exists()) {
                    //noinspection ResultOfMethodCallIgnored
                    parentFile.mkdirs();
                }
                //noinspection ResultOfMethodCallIgnored
                destFile.createNewFile();
            } else if (!destFile.exists()) {
                throw new IllegalArgumentException("destFile [" + destFile.getAbsolutePath() + "] non exist.");
            }
            os = new FileOutputStream(destFile);
            os.write(srcFileBytes, 0, srcFileBytes.length);
            os.flush();
        } catch (IOException e) {
            throw new RuntimeException(" toFile [" + destFile.getAbsolutePath() + "] occur exception.", e);
        } finally {
            close(os);
        }
    }
    
    /**
     * 只罗列文件(即：只返回文件)
     * <p>
     * 注：dirOrFile对象本身也会被作为罗列对象。
     * </p>
     *
     * @param dirOrFile
     *         要罗列的文件夹(或者文件)
     * @param suffix
     *         要筛选的文件的后缀(若suffix为null， 则不作筛选)
     *
     * @return 罗列结果
     */
    private static List<File> listFileOnly(File dirOrFile, String... suffix) {
        if (!dirOrFile.exists()) {
            throw new IllegalArgumentException("listFileOnly [" + dirOrFile.getAbsolutePath() + "] non exist.");
        }
        return listFile(dirOrFile).stream()
                .filter(file -> {
                    if (suffix == null) {
                        return true;
                    }
                    String fileName = file.getName();
                    return Arrays.stream(suffix).anyMatch(fileName::endsWith);
                }).collect(Collectors.toList());
    }
    
    /**
     * 罗列所有文件文件夹
     * <p>
     * 注：dirOrFile对象本身也会被作为罗列对象。
     * </p>
     *
     * @param dirOrFile
     *         要罗列的文件夹(或者文件)
     *
     * @return 罗列结果
     */
    private static List<File> listFile(File dirOrFile) {
        List<File> fileContainer = new ArrayList<>(16);
        listFile(dirOrFile, fileContainer, 1);
        return fileContainer;
    }
    
    /**
     * 罗列所有文件文件夹
     * <p>
     * 注：dirOrFile对象本身也会被作为罗列对象。
     * </p>
     *
     * @param dirOrFile
     *         要罗列的文件夹(或者文件)
     * @param fileContainer
     *         罗列结果
     * @param mode
     *         罗列模式(0-罗列文件和文件夹； 1-只罗列文件； 2-只罗列文件夹)
     */
    private static void listFile(File dirOrFile, List<File> fileContainer, int mode) {
        if (!dirOrFile.exists()) {
            return;
        }
        int fileAndDirMode = 0;
        int onlyFileMode = 1;
        int onlyDirMode = 2;
        if (mode != fileAndDirMode && mode != onlyFileMode && mode != onlyDirMode) {
            throw new IllegalArgumentException("mode [" + mode + "] is non-supported. 0,1,2is only support.");
        }
        if (dirOrFile.isDirectory()) {
            File[] files = dirOrFile.listFiles();
            if (files != null) {
                for (File f : files) {
                    listFile(f, fileContainer, mode);
                }
            }
            if (mode == fileAndDirMode || mode == onlyDirMode) {
                fileContainer.add(dirOrFile);
            }
        } else {
            if (mode == fileAndDirMode || mode == onlyFileMode) {
                fileContainer.add(dirOrFile);
            }
        }
    }
}
